<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class TanggapDarurat extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'tanggap_darurat';

	protected $fillable = [
		'name',
		'tanggap_darurat_category_id',
		'short_description',
		'description',
		'seo',
		'display',
		'created_by',
	];
}
