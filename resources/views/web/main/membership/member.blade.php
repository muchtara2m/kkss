@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="{{ url('membership') }}">Our Member</a></li>
              <li class="active">Detail Member</li>
            </ol>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <h2 class="title text-center" style="margin-bottom: 30px;">Detail Member</h2>
            <div class="product-details">
                <div class="col-sm-5">
                    <div class="view-product">
                        <img src="{{ $member->image_url }}" alt="image" />
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="product-information" style="padding-top: 0; padding-bottom: 20px;">
                        <span>
                            <span>{{ $member->name }}</span>
                        </span>
                        <p style="font-size: 15px;margin-top: -20px;color: orange;"><i class="fa fa-envelope-o"></i> {{ $member->email }}</p>
                        <span>Profesi:</span>
                        <p style="font-size: 18px;"><label>{{ $member->profile->profession_name }}</label></p>
                        <br/>
                        <p><b>Alamat:</b></p>
                        <p>
                            {{ $member->profile->address }}
                            @if(isset($member->profile->village_name))
                            ,
                            <br/>
                            {{ $member->profile->village_name }},
                            {{ $member->profile->district_name }},
                            {{ $member->profile->city_name }},
                            <br/>
                            {{ $member->profile->province_name }}
                            @endif
                        </p>
                    </div>
                    <br/>
                    <a href="{{ url('membership') }}" class="btn btn-sm btn-default add-to-cart">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection