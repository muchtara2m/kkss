<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMenuTable20200511063600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO menu (name, link, parent_id, favicon, position, for_role) VALUES
            ('Contact', 'contents/contact', 21, NULL, 4, 'admin'),
            ('Social Media', 'contents/socialmedia', 21, NULL, 5, 'admin');
        ");

        DB::statement("
            UPDATE menu SET status='Inactive' where id in (22,23,24);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
