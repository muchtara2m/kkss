<!DOCTYPE html>
<html lang="en">
    <head>
        @include('web/'.$pView.'/head')
    </head>
    <body>
        <div class="be-wrapper">
            @include('web/'.$pView.'/header')
            @include('web/'.$pView.'/sidebar')
            @yield('body')
            @include('web/'.$pView.'/footer')
            @include('web/'.$pView.'/bottom') 
            @include('web/'.$pView.'/additional')
            {{ $vSubFolder = (strpos($cView, '/index') == true) ? '/'.str_replace("/index", "", $cView) : null }}
            @if($vAdditional['css'])
                <link href="{{ asset('assets/'.$pView.'/views/css/'.$sView.$vSubFolder.'/'.$vAdditional['css'].'.css') }}">
            @endif
            @if($vAdditional['html'])
                @include('web/'.$pView.'/'.$sView.$vSubFolder.'/additional/'.$vAdditional['html'])
            @endif
            @if($vAdditional['js'])
                <script src="{{ asset('assets/'.$pView.'/views/js/'.$sView.$vSubFolder.'/'.$vAdditional['js'].'.js') }}"></script>
            @endif
        </div>
    </body>
    @include('web/'.$pView.'/onloading')
</html>