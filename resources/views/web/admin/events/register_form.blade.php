@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Form {{ $events->name }}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url($pView.'/'.$sView) }}"># Events</a></li>
            <li><a href="{{ url($pView.'/'.$sView.'/register/'.$events->id) }}">Register</a></li>
            <li><a href="{{ url($pView.'/'.$sView.'/register/'.$events->id) }}">{{ $events->name }}</a></li>
            <li class="active">Form</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ ($events) ? 'Last updated : '.$events->updated_date : '' }}
            </span>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <h2 style="margin-top: -15px; font-size: 20px;">
            <label for="editor1">Form {{ $events_register?'Edit':'Add' }} Register - {{ $events->name }}</label>
        </h2>
        <div class="box">
            <div class="box-body" style="padding-top: 25px;">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/register/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Nama *</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" id="name" class="form-control form-control-md" placeholder="Tulis disini" value="{{ $events_register?$events_register->name:'' }}" autocomplete="off" required="" {{ $events_register?'readonly=""':'' }}>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Nama harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Email *</label>
                                        <div class="col-md-6">
                                            <input type="text" name="email" id="email" class="form-control form-control-md" placeholder="Tulis disini" value="{{ $events_register?$events_register->email:'' }}" autocomplete="off" required="" {{ $events_register?'readonly=""':'' }}>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('email'))
                                                <span class="btn btn-danger">Email harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">No. Handphone *</label>
                                        <div class="col-md-6">
                                            <input type="number" name="phone" onkeypress="return isNumberKey(event)" id="phone" class="form-control form-control-md" placeholder="Tulis disini" value="{{ $events_register?$events_register->phone:'' }}" autocomplete="off" required="" {{ $events_register?'readonly=""':'' }}>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('phone'))
                                                <span class="btn btn-danger">No. Handphone harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Pekerjaan *</label>
                                        <div class="col-md-6">
                                            <input type="text" name="job_title" id="job_title" class="form-control form-control-md" placeholder="Tulis disini" value="{{ $events_register?$events_register->job_title:'' }}" autocomplete="off" required="" {{ $events_register?'readonly=""':'' }}>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('job_title'))
                                                <span class="btn btn-danger">Pekerjaan harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Alamat *</label>
                                        <div class="col-md-8">
                                            <textarea name="address" id="address" class="form-control" required="" placeholder="Tulis disini" {{ $events_register?'readonly=""':'' }}>{{ $events_register?$events_register->address:'' }}</textarea>
                                        </div>
                                        <div class="col-md-2">
                                            @if($errors->get('address'))
                                                <span class="btn btn-danger">Alamat harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Status *</label>
                                        <div class="col-md-8">
                                            <select class="form-control form-control-md" name="is_status" style="width: 200px;">
                                                <option value="Register" selected="" disabled="">Menunggu Konfirmasi</option>
                                                <option value="Approve" {{ $events_register?($events_register->is_status=='Approve'?'selected=""':''):'' }}>Disetujui</option>
                                                <option value="Reject" {{ $events_register?($events_register->is_status=='Reject'?'selected=""':''):'' }}>Ditolak</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            @if($errors->get('is_status'))
                                                <span class="btn btn-danger">Status harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="str_id" id="str_id" value="{{ $events_register?$events_register->str_id:'' }}">
                                            <input type="hidden" name="user_id" id="user_id" value="{{ $events_register?$events_register->user_id:'' }}">
                                            <input type="hidden" name="events_id" id="events_id" value="{{ $events?$events->id:'' }}">
                                            <button type="submit" class="btn btn-primary btn-class" style="border-radius: 0;" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default" style="border-radius: 0;"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop