@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li>International</li>
              <li class="active">Kantor Perwakilan</li>
            </ol>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <h2 class="title text-center" style="margin-bottom: 30px;">Kantor Perwakilan</h2>
            <div class="col-md-12">
                @foreach($kantor_perwakilan as $i => $row)
                <div class="panel-group category-products" id="accordian{{ $i }}">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordian{{ $i }}" href="#{{ $row->id }}">
                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                    {{ $row->title }}
                                </a>
                            </h4>
                        </div>
                        <div id="{{ $row->id }}" class="panel-collapse collapse">
                            <div class="panel-body" style="padding: 20px;">
                                <img src="{{ $row->image_url }}" style="max-width: 100%; max-height: 200px; margin-bottom: 20px;"><br/>
                                <strong>{{ $row->location }}</strong>
                                {!! $row->description !!}<br/>
                                {!! $row->embed_maps !!}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
