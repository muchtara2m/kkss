<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <form role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
    </form>
    <ul class="nav menu">
        @foreach($vMenu as $i => $value)
        <li class="{{ ($sMenu==$value['link'])?'active':'' }} {{ ($value['link']=='#')?'parent':'' }} {{ ($value['link']=='header')?'divider':'' }} {{ explode('/', $sMenu)[0]==strtolower(str_replace(' ', '', $value['name']))?'active':'' }}">
            @if($value['link'] == '#')
                <a data-toggle="collapse" href="#sub-item-1"> 
                    <i class="fa fa-database"></i>&nbsp;&nbsp;
                    {{ $value['name'] }} 
                    <i class="fa fa-caret-down pull-right"></i>
                </a>
                <ul class="children collapse {{ explode('/', $sMenu)[0]==strtolower(str_replace(' ', '', $value['name']))?'in':'' }}" id="sub-item-1">
                    @foreach($value['sub'] as $j => $valsub)
                        <li>
                            <a href="{{ url($pView.'/'.$valsub['link']) }}" class="{{ ($sMenu==$valsub['link'])?'active':'' }}">
                                <i class="fa fa-angle-right"></i> 
                                {{ $valsub['name'] }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @elseif($value['link'] == 'header')
                <span></span>
            @else
                <a href="{{ url($pView.'/'.$value['link']) }}">
                    <i class="fa fa-database"></i>&nbsp;&nbsp;
                    {{ $value['name'] }}
                </a>
            @endif
        </li>
        @endforeach
    </ul>
</div>