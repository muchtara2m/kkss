<?php

namespace App\Http\Controllers\web\admin\home;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\home\StrukturOrganisasiQueries;

class StrukturOrganisasiController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
	protected $pView = 'admin';

	// Sub Folder (Child) in views
	protected $sView = 'home/strukturorganisasi';

	// For description select menu by role from database
	protected $vMenu = 'admin';
	
	// For selected menu (Active menu)
	protected $sMenu = 'home/strukturorganisasi';

	// Session by user for access this controllers
	protected $sessions = 'admin';
	
	function __construct(ControllerQueries $queries_controller, StrukturOrganisasiQueries $queries_struktur_organisasi)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_struktur_organisasi = $queries_struktur_organisasi;
	}

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Struktur Organisasi > Home';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_struktur_organisasi->get_struktur_organisasi_last();
        $data['last'] = $last;
        // Get Struktur Organisasi
        $struktur_organisasi = $this->queries_struktur_organisasi->get_struktur_organisasi_by_id(1);
        if ($struktur_organisasi) {
            $struktur_organisasi->image_id = $struktur_organisasi->image?$struktur_organisasi->image->id:null;
            $struktur_organisasi->image_url = $struktur_organisasi->image?\ImageUrl::url($this->sView.'/images/', $struktur_organisasi->image):null;
            $struktur_organisasi->thumb_url = $struktur_organisasi->image?\ImageUrl::url($this->sView.'/thumbs/', $struktur_organisasi->image):null;
        }
        $data['struktur_organisasi'] = $struktur_organisasi;

        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'content' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $content = $request->input('content');
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $post['content'] = $content;
        $_post = false; // for upload file
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_struktur_organisasi->insert_struktur_organisasi($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
                $_struktur_organisasi = $this->queries_struktur_organisasi->get_struktur_organisasi_last();
                $_param_id = $_struktur_organisasi->id;
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_struktur_organisasi->update_struktur_organisasi($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
                $_param_id = $id;
            }
        }

        // Proccess Upload
        if ($_post and $file) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'struktur_organisasi'; // Set Description for DB Image
            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
            $path = $this->sView; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload)
                return \ErrorWeb::notfound();
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }
}
