<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'mysql';
    
    protected $table = 'user';
    
    protected $fillable = [
        'name',
        'email',
        'phone',
        'username',
        'password',
        'url',
        'level',
        'type',
        'login',
        'logout'
    ];
}
