<?php

namespace App\Queries\web\admin\master;

class EventsCategoryQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $EventsCategoryModel = app($this->model_path.'EventsCategory');
        $this->EventsCategoryModel = $EventsCategoryModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_events_category_datatable($whereRaw, $order, $offset, $limit)
    {
        $events_category = $this->EventsCategoryModel::select(
                        'events_category.*'
                    )
                    ->where('events_category.status', 'Active');
        if ($whereRaw!='')
            $events_category = $events_category->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'events_category.'.$order['coloumn'];
        $events_category = $events_category->orderBy($order_column, $order['dir'])
                    ->orderBy('events_category.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($events_category as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($events_category);
    }

    function get_events_category_total($whereRaw)
    {
        $events_category = $this->EventsCategoryModel::select('events_category.id')
                    ->where('events_category.status', 'Active');
        if ($whereRaw!='')
            $events_category = $events_category->whereRaw($whereRaw);
        $events_category = $events_category->count();
        return $this->_resJson($events_category);
    }

    function get_events_category_by_id($id)
    {
        $data = $this->EventsCategoryModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_events_category_last()
	{
		$data = $this->EventsCategoryModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_events_category($post)
	{
		$data = $this->EventsCategoryModel::insert($post);
		return $this->_resJson($data);
	}

	function update_events_category($post)
	{
		$data = $this->EventsCategoryModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_events_category($id)
	{
		$data = $this->EventsCategoryModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_events_category($id)
	{
		$data = $this->EventsCategoryModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
