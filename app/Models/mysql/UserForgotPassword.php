<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class UserForgotPassword extends Model
{
    protected $connection = 'mysql';
    
    protected $table = 'user_forgot_password';
    
    protected $fillable = [
        'user_id',
        'token_code',
        'is_open',
        'old_password'
    ];
}
