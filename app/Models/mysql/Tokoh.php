<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Tokoh extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'tokoh';

	protected $fillable = [
		'name',
		'description',
		'short_description',
		'type',
		'display'
	];
}
