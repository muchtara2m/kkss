<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class CategoryBlog extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'category_blog';

	protected $fillable = [
		'name',
		'display'
	];
}
