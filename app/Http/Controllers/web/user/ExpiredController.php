<?php

namespace App\Http\Controllers\web\user;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\user\ExpiredQueries;

class ExpiredController extends \App\Http\Core\WebController
{
    // Parent Folder in views
    protected $pView = 'user';

    // Sub Folder (Child) in views
    protected $sView = 'expired';

    // For description select menu by role from database
    protected $vMenu = '';
    
    // For selected menu (Active menu)
    protected $sMenu = '';

    // Global model in this controllers
    protected $model = '';

    // Session by user for access this controllers
    protected $sessions = '';
	protected function session()
    {
        if (!empty(Session::has(env('SESSION').'root'))) {
            return env('SESSION').'root';
        } else if (!empty(Session::has(env('SESSION').'superadmin'))) {
            return env('SESSION').'superadmin';
        } else if (!empty(Session::has(env('SESSION').'admin'))) {
            return env('SESSION').'admin';
        } else if (!empty(Session::has(env('SESSION').'member'))) {
            return env('SESSION').'member';
        } else {
            return '';
        }
    }

	function __construct(ControllerQueries $queries_controller, ExpiredQueries $queries_expired)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_expired = $queries_expired;
    }

	public function index($param)
    {
    	// Get User
        $user = $this->queries_expired->get_user_by_url($param);
        if (!$user)
            return \ErrorWeb::notfound();

    	// Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Session Expired';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        /* Collection variable data */
        // Data User
        $data['userdata'] = $user;
        
        // Render to View
        return $this->_render($data);
    }

    public function login(Request $request)
    {
        // Check Validate Request
        $validate = [];
        $validate['email'] = 'required|email';
        $validate['password'] = 'required';
        $validate['url'] = 'required';
        $validate['level'] = 'required';

        // Define Variable Request
        $url = $request->input('url');
        $level = $request->input('level');
        $email = $request->input('email');
        $password = $request->input('password');
        $redirect = 'user/expired/'.$url;
        $flash_post = 'Error';
        $flash_message = 'Email or Password not match';

        // Get User
        $param = [];
        $param['email'] = $email;
        $param['level'] = $level;
        $param['url'] = $url;
        $user = $this->queries_expired->get_user_by_email($param);
        if ($user && (Hash::check($password, $user->password) == true)) {
            $session_user = Session::get(env('SESSION').$level);
            $token = $this->queries_expired->get_token_by_session_key($session_user);
            if ($token) {
                // Update token
                $post_token = [];
                $post_token['token_key'] = Crypt::encryptString($email);
                $this->queries_expired->update_token($post_token, $token->id);
                // Set COOKIE
                Cookie::queue(Cookie::make(env('COOKIE').$user->level, $post_token['token_key'], 60));
                $redirect = $user->level.'/dashboard';
                // Error Message
                $flash_post = 'Success';
                $flash_message = 'Welcome back to '.$user->level.' page';
            } else 
                $flash_message = 'Session not found, you must logout';
        }

        // Set Session Flash
        Session::flash('post', $flash_post);
        Session::flash('message', $flash_message);
        // Redirect to Function
        return $this->_redirect($redirect);
    }
}
