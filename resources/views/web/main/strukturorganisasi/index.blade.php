@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Struktur Organisasi</li>
            </ol>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <h2 class="title text-center">Struktur Organisasi</h2>
            <div class="blog-post-area">
                <div class="col-sm-12">
                    <div class="single-blog-post" style="margin-bottom: 50px;">
                        @if($struktur_organisasi)
                        <a href="#" style="pointer-events: none;">
                            <img src="{{ $struktur_organisasi->image_url }}" alt="">
                        </a>
                        {!! $struktur_organisasi->content !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
