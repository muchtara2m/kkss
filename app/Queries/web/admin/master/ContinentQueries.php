<?php

namespace App\Queries\web\admin\master;

class ContinentQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $ContinentModel = app($this->model_path.'Continent');
        $this->ContinentModel = $ContinentModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_continent_datatable($whereRaw, $order, $offset, $limit)
    {
        $continent = $this->ContinentModel::select(
                        'continent.*'
                    )
                    ->where('continent.status', 'Active');
        if ($whereRaw!='')
            $continent = $continent->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'continent.'.$order['coloumn'];
        $continent = $continent->orderBy($order_column, $order['dir'])
                    ->orderBy('continent.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($continent as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($continent);
    }

    function get_continent_total($whereRaw)
    {
        $continent = $this->ContinentModel::select('continent.id')
                    ->where('continent.status', 'Active');
        if ($whereRaw!='')
            $continent = $continent->whereRaw($whereRaw);
        $continent = $continent->count();
        return $this->_resJson($continent);
    }

    function get_continent_by_id($id)
    {
        $data = $this->ContinentModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_continent_last()
	{
		$data = $this->ContinentModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_continent($post)
	{
		$data = $this->ContinentModel::insert($post);
		return $this->_resJson($data);
	}

	function update_continent($post)
	{
		$data = $this->ContinentModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_continent($id)
	{
		$data = $this->ContinentModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_continent($id)
	{
		$data = $this->ContinentModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
