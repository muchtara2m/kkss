<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class TanggapDaruratCategory extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'tanggap_darurat_category';

	protected $fillable = [
		'name',
		'description'
	];
}
