<?php

namespace App\Http\Controllers\web\user;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\user\LoginQueries;

class LoginController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
    protected $pView = 'user';

    // Sub Folder (Child) in views
    protected $sView = 'login';

    // For description select menu by role from database
    protected $vMenu = '';
    
    // For selected menu (Active menu)
    protected $sMenu = '';

    // Global model in this controllers
    protected $model = '';

    // Session by user for access this controllers
    protected $sessions = '';
	protected function session()
    {
        if (!empty(Session::has(env('SESSION').'root'))) {
            return env('SESSION').'root';
        } else if (!empty(Session::has(env('SESSION').'superadmin'))) {
            return env('SESSION').'superadmin';
        } else if (!empty(Session::has(env('SESSION').'admin'))) {
            return env('SESSION').'admin';
        } else if (!empty(Session::has(env('SESSION').'member'))) {
            return env('SESSION').'member';
        } else {
            return '';
        }
    }
	
	function __construct(ControllerQueries $queries_controller, LoginQueries $queries_login)
	{
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_login = $queries_login;
	}

    public function index($param)
    {
        // Get User
        $user = $this->queries_login->get_user_by_url($param);
        if (!$user) 
            return \ErrorWeb::notfound();

        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Login | Administrator';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get captcha
        $data['captcha'] = null;
        if (env('MEWS_CAPTCHA'))
            $data['captcha'] = captcha_img();
        if (env('GOOGLE_RECAPTCHA'))
            $data['g_recaptcha'] = true;
        // Data User
        $data['userdata'] = $user;
        
        // Render to View
        return $this->_render($data);
    }

    public function get_captcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

    public function post(Request $request)
    {
        // Check Google Captcha Valid
        if (env('GOOGLE_RECAPTCHA')) {
            $g_recaptcha = $this->check_g_recaptcha($request);
            if (!isset($g_recaptcha->success)or$g_recaptcha->success==false) {
                return $this->response_g_recaptcha_error($request);
            }
        }
        // Check Validate Request
        $validate = [];
        $validate['email'] = 'required|email';
        $validate['password'] = 'required';
        // $validate['remember'] = '';
        $validate['url'] = 'required';
        $validate['level'] = 'required';
        // Check Mews Captcha
        $captcha = [];
        if (env('MEWS_CAPTCHA')) 
            $captcha['captcha.captcha'] = 'Invalid captcha code.';
        $this->validate($request, $validate, $captcha);

        // Define Variable Request
        $url = $request->input('url');
        $level = $request->input('level');
        $email = $request->input('email');
        $password = $request->input('password');
        $remember = $request->input('remember');
        $redirect = 'user/'.$url;
        if ($level=='member') $redirect = 'signin';
        $flash_post = 'Error';
        $flash_message = 'Email or Password not match';

        // Get User
        $param = [];
        $param['email'] = $email;
        $param['level'] = $level;
        $param['url'] = $url;
        $user = $this->queries_login->get_user_by_email($param);
        if ($user && (Hash::check($password, $user->password) == true)) {
            // Update Login
            $post_user = [];
            $post_user['id'] = $user->id;
            $post_user['login'] = date('Y-m-d H:i:s');
            $this->queries_login->update_user($post_user);
            // Insert token
            $post_token = [];
            $post_token['session_key'] = Crypt::encryptString($user->id);
            $post_token['token_key'] = Crypt::encryptString($email);
            $this->queries_login->insert_token($post_token);
            // Set COOKIE
            Cookie::queue(Cookie::make(env('COOKIE').$user->level, $post_token['token_key'], 60));
            // Set Session User
            Session::put(env('SESSION').$user->level, $post_token['session_key']);
            Session::save();
        	$redirect = $user->level.'/dashboard';
            if ($level=='member') $redirect = 'home';
            // Error Message
            $flash_post = 'Success';
            $flash_message = 'Welcome to '.$user->level.' page';
        }

        // Set Session Flash
        Session::flash('post', $flash_post);
        Session::flash('message', $flash_message);
        // Redirect to Function
        return $this->_redirect($redirect);
    }

    private function check_g_recaptcha($request)
    {
        // Define Request Body
        $post = [
            'secret' => env('GOOGLE_RECAPTCHA_SECRET_KEY'),
            'response' => $request->input('g-recaptcha-response'),
            'remoteip' => $request->ip()
        ];

        // Create CURL
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('GOOGLE_RECAPTCHA_URL_VERIFY'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                // 'accept: */*',
                'accept-language: en-US,en;q=0.8',
                'content-type: multipart/form-data',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        // Response CURL
        if ($err)
            return $err;
        return json_decode($response);
    }

    private function response_g_recaptcha_error($request)
    {
        // Set Session Flash
        Session::flash('post', 'Error');
        Session::flash('message', 'Invalid captcha key');
        // Redirect to Function
        return $this->_redirect('user/'.$request->input('url'));
    }
}
