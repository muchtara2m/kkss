@extends('web/'.$pView.'/index')

@section('body')
<section id="form">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 box-form-a">
                <div class="login-form">
                    <h2 class="text-center">Masuk dengan akun kamu</h2>
                    @if(Session::has('post'))
                        @if(Session::get('post') == 'Error')
                            <span class="help-block text-center" style="margin-top: -15px;">
                                <strong style="color: red;">{{ Session::get("message") }}</strong>
                            </span>
                        @endif
                    @endif
                    {{ Form::open(['route' => 'login', 'method' => 'post', 'id' => 'myForm']) }}
                        <input type="email" name="email" placeholder="Email Address" autocomplete="off" required="" />
                        <input type="password" name="password" placeholder="Password" autocomplete="off" required="" />
                        <span>
                            <input type="checkbox" class="checkbox"> 
                            Ingat saya
                        </span>
                        {{ Form::hidden('level', 'member') }}
                        {{ Form::hidden('url', 'member') }}
                        <button type="submit" class="btn btn-default pull-right" style="margin-top: 10px;">Masuk</button>
                    {{ Form::close() }}
                    <br/><hr/><span>Belum punya akun ? <a href="{{ $base_url.'signup' }}">Daftar</a></span>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection