@extends('web/'.$pView.'/index')

@section('body')
<section class="login-box">
    <div class="login-logo">
        <a href="" style="font-size: 24px;">
            <b>CHANGE</b> 
            <span> PASSWORD</span>
        </a>
    </div>
    <div>
        <div class="col-md-12" style="background-color: #c6000d; height: 2.5px;"></div>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">You must create new password</p>
        {{ Form::open(['route' => 'user/forgotpassword/changepassword', 'method' => 'post']) }}
            @if(Session::has('post'))
                @if(Session::get('post') == 'success')
                    <div class="alert alert-success alert-dismissable" style="padding-top: 5px; padding-bottom: 5px;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <span>
                            <i class="icon fa fa-check"></i>
                            <span>Change password has been processed</span>
                        </span>
                    </div>
                @else
                    <div class="alert alert-danger alert-dismissable" style="padding-top: 5px; padding-bottom: 5px;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <span>
                            <i class="icon fa fa-ban"></i>
                            <span>{{ Session::get("message") }}</span>
                        </span>
                    </div>
                @endif
            @endif
            <div class="form-group has-feedback">
                {{ Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email', 'autocomplete' => 'off', 'required' => 'required', 'id' => 'myForm']) }}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                {{ Form::password('new_password', ['class' => 'form-control', 'id' => 'new_password', 'placeholder' => 'New Password', 'autocomplete' => 'off', 'required' => 'required']) }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {{ Form::password('confirm_password', ['class' => 'form-control', 'id' => 'confirm_password', 'placeholder' => 'Confirm Password', 'autocomplete' => 'off', 'required' => 'required']) }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <small id="confirm_password_error" style="color: #a94442; display: none;">Password must be same</small>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    {!! Form::token() !!}
                    {{ Form::hidden('url', $userdata->url, ['required' => 'required']) }}
                    {{ Form::hidden('level', $userdata->level, ['required' => 'required']) }}
                    {{ Form::submit('Change Password', ['class' => 'btn btn-danger btn-block btn-flat', 'id' => 'btn-submit', 'onclick' => 'onLoading();']) }}
                </div>
            </div>
        {{ Form::close() }}
        <br />
        <center>
            <a href="/user/{{ $userdata->url }}" style="color: #666;">Back to login page</a>
        </center>
    </div>
</section>
@stop