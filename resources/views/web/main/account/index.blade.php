@extends('web/'.$pView.'/index')

@section('body')
<section id="form">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 box-form-a">
                <div class="login-form">
                    <h2 class="text-center">Detail akun anda</h2>
                    <p class="text-center" style="margin-top: -20px; margin-bottom: 30px;">Isi form dibawah dengan benar untuk ubah data.</p>
                    @if(Session::has('post'))
                        @if(Session::get('post') == 'Error')
                            <span class="help-block text-center" style="margin-top: -15px;">
                                <strong style="color: red;">{{ Session::get("message") }}</strong>
                            </span>
                        @endif
                    @endif
                    @if(Session::has('post'))
                        @if(Session::get('post') == 'Success')
                            <span class="help-block text-center" style="margin-top: -15px;">
                                <strong style="color: green;">{{ Session::get("message") }}</strong>
                            </span>
                        @endif
                    @endif
                    {{ Form::open(['route' => 'member/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'account']) }}
                        <div class="form-group">
                            <span class="text-right">Nama Lengkap</span>
                            <input type="text" name="name" id="name" placeholder="Nama Lengkap" required="" autocomplete="off" value="{{ $user->name }}">
                            @if($errors->get('name'))
                                <small class="btn btn-sm btn-danger">Nama harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">No. Handphone</span>
                            <input type="number" name="phone" onkeypress="return isNumberKey(event)" id="phone" autocomplete="off" required="">
                            @if($errors->get('phone'))
                                <small class="btn btn-sm btn-danger">No. Handphone harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Profesi</span>
                            <select name="profession" id="profession" required=""></select>
                            <input type="hidden" id="profession_id" value="{{ $user->profile->profession_id }}">
                            @if($errors->get('profession'))
                                <small class="btn btn-sm btn-danger">Profesi harus diisi</small>
                            @endif
                        </div>
                        <hr/>
                        <div class="form-group">
                            <span class="text-right">Alamat</span>
                            <textarea name="address" id="address" cols="10" rows="3" placeholder="Alamat Lengkap" required="" autocomplete="off">{{ $user->profile->address }}</textarea>
                            @if($errors->get('address'))
                                <small class="btn btn-sm btn-danger">Alamat harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Provinsi</span>
                            <select name="province" id="province" required=""></select>
                            <input type="hidden" id="province_id" value="{{ $user->profile->province_id }}">
                            @if($errors->get('province'))
                                <small class="btn btn-sm btn-danger">Provinsi harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Kota/Kabupaten</span>
                            <select name="city" id="city" required=""></select>
                            <input type="hidden" id="city_id" value="{{ $user->profile->city_id }}">
                            @if($errors->get('city'))
                                <small class="btn btn-sm btn-danger">Kota/Kabupaten harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Kecamatan</span>
                            <select name="district" id="district" required=""></select>
                            <input type="hidden" id="district_id" value="{{ $user->profile->district_id }}">
                            @if($errors->get('district'))
                                <small class="btn btn-sm btn-danger">Kecamatan harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Desa/Kelurahan</span>
                            <select name="village" id="village" required=""></select>
                            <input type="hidden" id="village_id" value="{{ $user->profile->village_id }}">
                            @if($errors->get('village'))
                                <small class="btn btn-sm btn-danger">Desa/Kelurahan harus diisi</small>
                            @endif
                        </div>
                        <hr/>
                        <div class="form-group">
                            <span class="text-right">Email</span>
                            <input type="email" name="email" id="email" placeholder="Email" required="" autocomplete="off" value="{{ $user->email }}">
                            @if($errors->get('email'))
                                <small class="btn btn-sm btn-danger">Email harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Password</span>
                            <input type="password" name="password" id="password" placeholder="Password" autocomplete="off">
                            @if($errors->get('password'))
                                <small class="btn btn-sm btn-danger">Password harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Konfirmasi Password</span>
                            <input type="password" name="confirm_password" id="confirm_password" placeholder="Konfirmasi Password" autocomplete="off">
                            <small id="confirm_password_error" class="btn btn-sm btn-danger" style="display: none;">Password harus sama</small>
                        </div>
                        <div class="form-group">
                            <span class="text-right">Foto Profil</span>
                            <p>Jenis file <small>(.jpeg/.jpg/.png)</small></p>
                            <input type="file" name="file" id="file_attachment" class="file_attachment">
                            <small style="color: red !important; display: none;" id="error_image">* mohon untuk melilih gambar tipe (.jpeg/.jpg/.png)</small>
                            <img src="{{ $user?$user->thumb_url?$user->thumb_url:'':'' }}" id="preview_image" alt="Preview Image" style="width: 30%; {{ $user?$user->image_url?'margin-top: 15px;':'display: none':'display: none' }}">
                            @if($user&&$user->image_url)
                            <br /><a href="{{ $user->image_url }}" target="_blank" id="link_image">Klik untuk melihat gambar lebih besar</a>
                            <input type="hidden" name="image_id" id="image_id" value="{{ $user?$user->image_id:'' }}">
                            @endif
                            @if($errors->get('image'))
                                <small class="btn btn-sm btn-danger">Foto profil harus diisi</small>
                            @endif
                        </div>
                        <center>
                            <input type="hidden" id="user_id" name="user_id" value="{{ $user->id }}">
                            <button type="submit" class="btn btn-default btn-class" style="width: 40%;">Simpan</button>
                        </center>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection