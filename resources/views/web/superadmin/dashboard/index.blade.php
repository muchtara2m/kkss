@extends('web/'.$pView.'/index')

@section('body')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Dashboard</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header" style="margin-top: 10px;">
                Dashboard
            </h2>
            <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
                <a class="btn btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
                    <i class="fa fa-arrow-left"></i>
                </a>
                <a class="btn btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
                    <i class="fa fa-arrow-right"></i>
                </a>
                <a class="btn btn-default" href="" title="Refresh">
                    <i class="fa fa-rotate-right"></i>
                </a>
                <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
                    Welcome to Dashboard Page!
                </small>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Welcome to Dashboard Page!
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Last logout :</h4>
                                    <h2>{{ $user->logout }}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Login (now) at :</h4>
                                    <h2>{{ $user->login }}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection