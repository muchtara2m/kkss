<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'image';

	protected $fillable = [
		'param_id',
		'name',
		'ext',
		'size',
		'resolution',
		'description',
		'storage'
	];
}
