<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>{{$vTitle}}</title>
<!-- <link rel="shortcut icon" type="image/png" href="{{ $vContents['favicon']['image_url'] }}"> -->
<link rel="shortcut icon" href="{{ asset('assets/main/images/favicon/favicon.jpg') }}">
<!-- Bootstrap 3.3.2  -->
<link href="{{ asset('assets/admin/bootstrap/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<!-- Font Awesome Icons  -->
<link href="{{ asset('assets/admin/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<!-- Theme style  -->
<link href="{{ asset('assets/admin/dist/css/AdminLTE.css') }}" rel="stylesheet" type="text/css">
<!-- iCheck  -->
<link href="{{ asset('assets/admin/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries  -->
<!-- WARNING: Respond.js doesn't work if you view the page via file://  -->
<!--[if lt IE 9]>
    <script src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'></script>
    <script src='https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js'></script>
<![endif]-->
@if(env('GOOGLE_RECAPTCHA')&&isset($g_recaptcha))
<script src="https://www.google.com/recaptcha/api.js?render={{ env('GOOGLE_RECAPTCHA_SITE_KEY') }}"></script>
@endif