<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/admin/dist/img/default.png') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ $user->name }}</p>
                <span>{{ $user->level }}</span>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">PRIMARY CONTENT</li>
            @foreach($vMenu as $i => $value)
                <li class="{{ ($sMenu==$value['link'])?'active':'' }} {{ ($value['link']=='#')?'treeview':'' }} {{ ($value['link']=='header')?'header':'' }} {{ explode('/', $sMenu)[0]==strtolower(str_replace(' ', '', $value['name']))?'active':'' }}">
                    @if($value['link'] == '#')
                        <a href="#"> 
                            <span>{{ $value['name'] }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            @foreach($value['sub'] as $j => $valsub)
                                <li class="{{ ($sMenu==$valsub['link'])?'active':'' }}">
                                    <a href="{{ url($pView.'/'.$valsub['link']) }}">
                                        <i class="fa fa-angle-right"></i> 
                                        {{ $valsub['name'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @elseif($value['link'] == 'header')
                        {{ $value['name'] }}
                    @else
                        <a href="{{ url($pView.'/'.$value['link']) }}">
                            {{ $value['name'] }}
                        </a>
                    @endif
                </li>
            @endforeach
        </ul>
    </section>
</aside>