<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class ForumConversation extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'forum_conversation';

	protected $fillable = [
		'forum_id',
		'parent_id',
		'user_id',
		'message'
	];
}
