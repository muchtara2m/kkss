<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class PostalCode extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'postal_code';

	protected $fillable = [
		'country_id',
		'village',
		'district',
		'city',
		'province',
		'postal_code'
	];
}
