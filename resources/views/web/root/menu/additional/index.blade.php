<!-- MODAL -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 0 !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Are you sure want to delete this data ?</h4>
            </div>
            {{ Form::open(['route' => $pView.'/'.$sView.'/delete', 'method' => 'post']) }}
                <div class="modal-footer">
                    {{ Form::hidden('delete', 'soft', ['id' => 'delete_elete']) }}
                    {{ Form::hidden('str_id', null, ['id' => 'str_id_delete', 'url' => url($pView.'/'.$sView.'/get/data/id')]) }}
                    {{ Form::button('Yes', ['class' => 'btn btn-sm btn-danger', 'style' => 'border-radius: 0;', 'onclick' => 'onDelete("'.url($pView.'/'.$sView.'/delete').'")']) }}
                    {{ Form::button('Cancel', ['class' => 'btn btn-sm btn-default', 'style' => 'border-radius: 0;', 'data-dismiss' => 'modal']) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>