<header id="header">
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="tel:{{ $contact['phone']->value_text }}"><i class="{{ $contact['phone']->icon }}"></i> {{ $contact['phone']->value_text }}</a></li>
                            <li><a href="mailto:{{ $contact['email']->value_text }}"><i class="{{ $contact['email']->icon }}"></i> {{ $contact['email']->value_text }}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            @foreach($social_media as $r)
                            <li><a href="{{ $r->value_text }}"><i class="{{ $r->icon }}"></i></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-middle">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="/">
                            <img src="{{ asset('assets/main/images/logo/logo.png') }}" alt="" style="width: 200px;" />
                        </a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            @if($user)
                                <li><a href="{{ $base_url.'forum/followed/status' }}"><i class="fa fa-comments"></i> Forum Followed</a></li>
                                <li><a href="{{ $base_url.'events/registered/status' }}"><i class="fa fa-bookmark"></i> Daftar Events</a></li>
                                <li><a href="{{ $base_url.'member/account' }}"><i class="fa fa-user"></i> {{ $user->name }}</a></li>
                                <li><a href="{{ $base_url.'logout' }}"><i class="fa fa-sign-out"></i> Logout</a></li>
                            @else
                                <li><a href="{{ $base_url.'signup' }}"><i class="fa fa-lock"></i> Register</a></li>
                                <li><a href="{{ $base_url.'signin' }}"><i class="fa fa-sign-in"></i> Login</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            @foreach($vMenu as $row)
                                @if($row['link']=='#')
                                    <li class="dropdown">
                                        <a href="#">{{ $row['name'] }}<i class="fa fa-angle-down"></i></a>
                                        <ul role="menu" class="sub-menu">
                                            @foreach($row['sub'] as $r)
                                                <li>
                                                    <a href="{{ $base_url.$r['link'] }}" class="{{ $sMenu==$row['link']?'active':'' }}">
                                                        {{ $r['name'] }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{ $base_url.$row['link'] }}" class="{{ $sMenu==$row['link']?'active':'' }}">
                                            {{ $row['name'] }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>