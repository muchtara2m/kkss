$(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	})
})
// Captcha
$(".btn-refresh").click(function(){
    $.ajax({
        type:'GET',
        url:'/get/captcha',
        success:function(data){
            $(".captcha span").html(data.captcha)
        }
    })
})