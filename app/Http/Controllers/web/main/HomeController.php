<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\HomeQueries;

class HomeController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'home';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'home';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, HomeQueries $queries_home)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_home = $queries_home;
	}

    public function index()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Home';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		$background = $this->queries_home->get_background();
		if (count($background)==0) {
			$image = [
				'image_id' => 0,
				'image_url' => asset('assets/main/img/background/bg-12.jpg'),
				'thumb_url' => asset('assets/main/img/background/bg-12.jpg')
			];
			$background[0] = (object) $image;
		}
		$data['background'] = $background;
		
		// Render to View
        return $this->_render($data);
    }
}
