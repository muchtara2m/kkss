<?php

$router->group([
    'middleware' => 'role.root',
    'prefix'     => 'root'
], function () use ($router) {
	$router->get('/root', 'error\NotFoundController@index')->name('root');
	$router->group([
            'prefix'     => 'account'
        ], function () use ($router) {
            $router->get('', 'web\root\AccountController@index')->name('root/account');
            $router->post('/post', 'web\root\AccountController@post')->name('root/account/post');
    });
    $router->group([
            'prefix'     => 'dashboard'
        ], function () use ($router) {
            $router->get('', 'web\root\DashboardController@index')->name('root/dashboard');
    });
    $router->group([
            'prefix'     => 'contents'
        ], function () use ($router) {
            $router->get('', 'error\NotFoundController@index')->name('root/contents');
            $router->group([
                    'prefix'     => 'logo'
                ], function () use ($router) {
                    $router->get('', 'web\root\contents\LogoController@index')->name('root/contents/logo');
                    $router->post('/post', 'web\root\contents\LogoController@post')->name('root/contents/logo/post');
            });
            $router->group([
                    'prefix'     => 'favicon'
                ], function () use ($router) {
                    $router->get('', 'web\root\contents\FaviconController@index')->name('root/contents/favicon');
                    $router->post('/post', 'web\root\contents\FaviconController@post')->name('root/contents/favicon/post');
            });
            $router->group([
                    'prefix'     => 'title'
                ], function () use ($router) {
                    $router->get('', 'web\root\contents\TitleController@index')->name('root/contents/title');
                    $router->post('/post', 'web\root\contents\TitleController@post')->name('root/contents/title/post');
            });
    });
    $router->group([
            'prefix'     => 'menu'
        ], function () use ($router) {
            $router->get('', 'web\root\MenuController@index')->name('root/menu');
            $router->get('/form/', 'web\root\MenuController@form')->name('root/menu/form');
            $router->get('/form/{str_id}', 'web\root\MenuController@form')->name('root/menu/form/id');
            $router->get('/get/datatable', 'web\root\MenuController@get_datatable')->name('root/menu/get/datatable');
            $router->get('/get/data/id/{param}', 'web\root\MenuController@get_data_by_id')->name('root/menu/get/data/id');
            $router->post('/post', 'web\root\MenuController@post')->name('root/menu/post');
            $router->post('/delete', 'web\root\MenuController@delete')->name('root/menu/delete');
    });
    $router->group([
            'prefix'     => 'user'
        ], function () use ($router) {
            $router->get('', 'web\root\UserController@index')->name('root/user');
            $router->get('/form/', 'web\root\UserController@form')->name('root/user/form');
            $router->get('/form/{str_id}', 'web\root\UserController@form')->name('root/user/form/id');
            $router->get('/get/datatable', 'web\root\UserController@get_datatable')->name('root/user/get/datatable');
            $router->get('/get/data/id/{param}', 'web\root\UserController@get_data_by_id')->name('root/user/get/data/id');
            $router->post('/post', 'web\root\UserController@post')->name('root/user/post');
            $router->post('/delete', 'web\root\UserController@delete')->name('root/user/delete');
    });

    $router->group([
            'prefix'     => 'log'
        ], function () use ($router) {
            $router->get('', 'error\NotFoundController@index')->name('root/log');
            $router->group([
                    'prefix'     => 'activity'
                ], function () use ($router) {
                    $router->get('', 'web\root\log\ActivityController@index')->name('root/log/activity');
                    $router->get('/form/{str_id}', 'web\root\log\ActivityController@form')->name('root/log/activity/form/id');
                    $router->get('/get/datatable', 'web\root\log\ActivityController@get_datatable')->name('root/log/activity/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\root\log\ActivityController@get_data_by_id')->name('root/log/activity/get/data/id');
            });
            $router->group([
                    'prefix'     => 'queries'
                ], function () use ($router) {
                    $router->get('', 'web\root\log\QueriesController@index')->name('root/log/queries');
                    $router->get('/form/{str_id}', 'web\root\log\QueriesController@form')->name('root/log/queries/form/id');
                    $router->get('/get/datatable', 'web\root\log\QueriesController@get_datatable')->name('root/log/queries/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\root\log\QueriesController@get_data_by_id')->name('root/log/queries/get/data/id');
            });
    });
});