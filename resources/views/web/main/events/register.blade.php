@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="{{ url($sView) }}">Events</a></li>
              <li><a href="{{ url($sView.'/'.($events?$events->seo:null)) }}">{{ $events?$events->name:null }}</a></li>
              <li class="active">Register</li>
            </ol>
        </div>
    </div>
</section>
<section class="mg-50">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Filter</h2>
                    <div class="brands_products">
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($events_category as $row)
                                <li><a href="{{ url('events/kategori/'.$row->id) }}"> <span class="pull-right">({{ $row->total }})</span>{{ $row->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="blog-post-area">
                    <h2 class="title text-center">Daftar Events</h2>
                    <div class="single-blog-post">
                        <h3>{{ $events->name }}</h3>
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-tag"></i> {{ $events->category_name }}</li>
                                <li><i class="fa fa-user"></i> {{ $events->user_name }}</li>
                                <li><i class="fa fa-calendar"></i> {{ $events->created_date }}</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="login-form">
                    <h2 style="font-size: 18px;">Daftar Event Ini</h2>
                    <p style="margin-top: -20px; margin-bottom: 30px;">Isi form dibawah dengan benar.</p>
                    @if(Session::has('post'))
                        @if(Session::get('post') == 'Error')
                            <span class="help-block" style="margin-top: -15px;">
                                <strong style="color: red;">{{ Session::get("message") }}</strong>
                            </span>
                        @endif
                    @endif
                    @if(Session::has('post'))
                        @if(Session::get('post') == 'Success')
                            <span class="help-block" style="margin-top: -15px;">
                                <strong style="color: green;">{{ Session::get("message") }}</strong>
                            </span>
                        @endif
                    @endif
                    {{ Form::open(['route' => $sView.'/register/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'signup']) }}
                        <div class="row" style="border: 1px solid #eee;padding-top: 15px;">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="text-right">Nama Lengkap</span>
                                    <input type="text" name="name" id="name" required="" {{ $user?'readonly=""':null }} autocomplete="off" value="{{ $user?$user->name:old('name') }}">
                                    @if($errors->get('name'))
                                        <small class="btn btn-sm btn-danger">Nama harus diisi</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <span class="text-right">Email</span>
                                    <input type="email" name="email" id="email" required="" {{ $user?'readonly=""':null }} autocomplete="off" value="{{ $user?$user->email:old('email') }}">
                                    @if($errors->get('email'))
                                        <small class="btn btn-sm btn-danger">Email harus diisi</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <span class="text-right">No. Handphone</span>
                                    <input type="number" name="phone" onkeypress="return isNumberKey(event)" id="phone" autocomplete="off" required="" {{ $user?'readonly=""':null }} value="{{ $user?$user->phone:old('phone') }}">
                                    @if($errors->get('phone'))
                                        <small class="btn btn-sm btn-danger">No. Handphone harus diisi</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span class="text-right">Pekerjaan</span>
                                    <input type="text" name="job_title" id="job_title" autocomplete="off" required="" {{ $user?'readonly=""':null }} value="{{ $user?$user->profession_name:old('job_title') }}">
                                    @if($errors->get('job_title'))
                                        <small class="btn btn-sm btn-danger">Pekerjaan harus diisi</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <span class="text-right">Alamat Lengkap</span>
                                    <textarea name="address" id="address" cols="10" rows="3" required="" {{ $user?'readonly=""':null }} autocomplete="off">{{ $user?$user->address:old('address') }}</textarea>
                                    @if($errors->get('address'))
                                        <small class="btn btn-sm btn-danger">Alamat harus diisi</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="seo" value="{{ $events?$events->seo:null }}">
                        <input type="hidden" name="events_id" value="{{ $events?$events->id:null }}">
                        <input type="hidden" name="user_id" value="{{ $user?$user->id:null }}">
                        <button type="submit" class="btn btn-default btn-class">Daftar</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection