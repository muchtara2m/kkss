<?php

namespace App\Queries\web\admin\master;

class CityQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $CityModel = app($this->model_path.'City');
        $this->CityModel = $CityModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

	function get_province()
	{
        $ProvinceModel = app($this->model_path.'Province');
		$province = $ProvinceModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        foreach ($province as $value) {
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
        }
        return $this->_resJson($province);
	}

    function get_city_datatable($whereRaw, $order, $offset, $limit)
    {
        $city = $this->CityModel::select(
                        'city.*', 
                        'region_type.name as region_type_$_name',
                        'province.name as province_$_name',
                        'country.name as country_$_name'
                    )
                    ->join('region_type', 'region_type.id', 'city.region_type_id')
                    ->join('province', 'province.id', 'city.province_id')
                    ->join('country', 'country.id', 'province.country_id')
                    ->where('city.status', 'Active');
        if ($whereRaw!='')
            $city = $city->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'city.'.$order['coloumn'];
        $city = $city->orderBy($order_column, $order['dir'])
                    ->orderBy('city.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($city as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($city);
    }

    function get_city_total($whereRaw)
    {
        $city = $this->CityModel::select('city.id')
                    ->join('region_type', 'region_type.id', 'city.region_type_id')
                    ->join('province', 'province.id', 'city.province_id')
                    ->join('country', 'country.id', 'province.country_id')
                    ->where('city.status', 'Active');
        if ($whereRaw!='')
            $city = $city->whereRaw($whereRaw);
        $city = $city->count();
        return $this->_resJson($city);
    }

    function get_city_by_id($id)
    {
        $data = $this->CityModel::select('city.*', 'province.name as province_name')
                ->join('province', 'province.id', 'city.province_id')
                ->where('city.status', 'Active')
                ->where('city.id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_city_last()
	{
		$data = $this->CityModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_city($post)
	{
		$data = $this->CityModel::insert($post);
		return $this->_resJson($data);
	}

	function update_city($post)
	{
		$data = $this->CityModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_city($id)
	{
		$data = $this->CityModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_city($id)
	{
		$data = $this->CityModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
