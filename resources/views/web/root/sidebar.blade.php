<div class="navbar-collapse collapse templatemo-sidebar">
    <ul class="templatemo-sidebar-menu">
        <li>
            <form class="navbar-form">
              <input type="text" class="form-control" id="templatemo_search_box" placeholder="Search...">
              <span class="btn btn-default">Go</span>
            </form>
        </li>
        <hr/>
        @foreach($vMenu as $i => $value)
        <li class="{{ ($sMenu==$value['link'])?'active':'' }} {{ ($value['link']=='#')?'sub':'' }} {{ ($value['link']=='header')?'divider':'' }} {{ explode('/', $sMenu)[0]==strtolower(str_replace(' ', '', $value['name']))?'open':'' }}">
            @if($value['link'] == '#')
                <a href="javascript:;"> 
                    <i class="fa fa-database"></i>
                    {{ $value['name'] }} 
                    <div class="pull-right"><span class="caret"></span></div>
                </a>
                <ul class="templatemo-submenu">
                    @foreach($value['sub'] as $j => $valsub)
                        <li class="{{ ($sMenu==$valsub['link'])?'active':'' }}">
                            <a href="{{ url($pView.'/'.$valsub['link']) }}">
                                <i class="fa fa-angle-right"></i> 
                                {{ $valsub['name'] }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @elseif($value['link'] == 'header')
                <hr/>
            @else
                <a href="{{ url($pView.'/'.$value['link']) }}">
                    <i class="fa fa-database"></i>
                    {{ $value['name'] }}
                </a>
            @endif
        </li>
        @endforeach
        <hr/>
        <li><a href="{{ route($pView.'/account') }}"><i class="fa fa-user"></i>Account</a></li>
        <li><a href="javascript:;" data-toggle="modal" data-target="#confirmModal"><i class="fa fa-sign-out"></i>Logout</a></li>
    </ul>
</div>