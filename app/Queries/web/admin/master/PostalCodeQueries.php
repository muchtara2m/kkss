<?php

namespace App\Queries\web\admin\master;

class PostalCodeQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $PostalCodeModel = app($this->model_path.'PostalCode');
        $this->PostalCodeModel = $PostalCodeModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_postal_code_datatable($whereRaw, $order, $offset, $limit)
    {
        $postal_code = $this->PostalCodeModel::select(
                        'postal_code.*'
                    )
                    ->where('postal_code.status', 'Active');
        if ($whereRaw!='')
            $postal_code = $postal_code->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'postal_code.'.$order['coloumn'];
        $postal_code = $postal_code->orderBy($order_column, $order['dir'])
                    ->orderBy('postal_code.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($postal_code as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($postal_code);
    }

    function get_postal_code_total($whereRaw)
    {
        $postal_code = $this->PostalCodeModel::select('postal_code.id')
                    ->where('postal_code.status', 'Active');
        if ($whereRaw!='')
            $postal_code = $postal_code->whereRaw($whereRaw);
        $postal_code = $postal_code->count();
        return $this->_resJson($postal_code);
    }

    function get_postal_code_by_id($id)
    {
        $data = $this->PostalCodeModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_postal_code_last()
	{
		$data = $this->PostalCodeModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->limit(1)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_postal_code($post)
	{
		$data = $this->PostalCodeModel::insert($post);
		return $this->_resJson($data);
	}

	function update_postal_code($post)
	{
		$data = $this->PostalCodeModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_postal_code($id)
	{
		$data = $this->PostalCodeModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_postal_code($id)
	{
		$data = $this->PostalCodeModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
