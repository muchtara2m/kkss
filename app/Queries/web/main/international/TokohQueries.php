<?php

namespace App\Queries\web\main\international;

class TokohQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $TokohModel = app($this->model_path.'Tokoh');
        $this->TokohModel = $TokohModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_tokoh($keyword=null)
    {
        $data = $this->TokohModel::where('status', 'Active')
                ->where('display', true)
                ->where('type', 'International');
        if ($keyword)
            $data = $data->where('name', 'like', '%'.$keyword.'%');
        $data = $data->get();
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'tokoh')
                    ->get()
                    ->first();
            $v->image = $image;
        }
        return $this->_resJson($data);
    }
}