<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable20190326170000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('value_string', 255)->nullable();
            $table->text('value_text')->nullable();
            $table->string('favcolor', 255)->nullable();
            $table->string('fontsize', 255)->nullable();
            $table->string('icon', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->boolean('display')->default(true);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
        });

        $data = [
            [
                'value_string' => null,
                'value_text' => null,
                'favcolor' => null,
                'fontsize' => null,
                'icon' => null,
                'description' => 'favicon'
            ],
            [
                'value_string' => null,
                'value_text' => null,
                'favcolor' => null,
                'fontsize' => null,
                'icon' => null,
                'description' => 'logo'
            ],
            [
                'value_string' => null,
                'value_text' => null,
                'favcolor' => null,
                'fontsize' => null,
                'icon' => null,
                'description' => 'title'
            ],
        ];
        DB::table('contents')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
