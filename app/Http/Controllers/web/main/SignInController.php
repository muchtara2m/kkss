<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\SignInQueries;

class SignInController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'signin';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'signin';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, SignInQueries $queries_signin)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_signin = $queries_signin;
	}

    public function index()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Sign In';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'index';
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		//
		
		// Render to View
        return $this->_render($data);
    }
}
