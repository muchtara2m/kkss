<?php

namespace App\Queries\web\main;

class HomeQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $BackgroundModel = app($this->model_path.'Background');
        $this->BackgroundModel = $BackgroundModel;

        // Start Query Log
        // \DB::connection()->enableQueryLog();
	}

    function get_background()
    {
        $data = $this->BackgroundModel::where('status', 'Active')
                ->where('display', true)
                ->get();

        // Get Image
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $k => $v) {
            if ($v->type=='Image') {
                $image = $ImageModel::where('param_id', $v->id)
                        ->where('description', 'background')
                        ->get()
                        ->first();
                $v->image = $image;
                $v->image_id = $v->image?$v->image->id:null;
                $v->image_url = $v->image?\ImageUrl::url('home/background/images/', $v->image):null;
                $v->thumb_url = $v->image?\ImageUrl::url('home/background/thumbs/', $v->image):null;
            } else 
            if ($v->type=='Link') {
                if ($v->modul=='Peluang Bisnis') {
                    $peluang_bisnis = $this->get_peluang_bisnis_by_id($v->param_id);
                    $v->param = $peluang_bisnis;
                    $v->image = $peluang_bisnis->image;
                    $v->image_id = $v->image?$v->image->id:null;
                    $v->image_url = $v->image?\ImageUrl::url('peluangbisnis/images/', $v->image):null;
                    $v->thumb_url = $v->image?\ImageUrl::url('peluangbisnis/thumbs/', $v->image):null;
                } else
                if ($v->modul=='Tanggap Darurat') {
                    $peluang_bisnis = $this->get_tanggap_darurat_by_id($v->param_id);
                    $v->param = $tanggap_darurat;
                    $v->image = $tanggap_darurat->image;
                    $v->image_id = $v->image?$v->image->id:null;
                    $v->image_url = $v->image?\ImageUrl::url('tanggapdarurat/images/', $v->image):null;
                    $v->thumb_url = $v->image?\ImageUrl::url('tanggapdarurat/thumbs/', $v->image):null;
                }
            }
        }
        return $this->_resJson($data);
    }

    function get_peluang_bisnis_by_id($id)
    {
        $PeluangBisnisModel = app($this->model_path.'PeluangBisnis');
        $peluang_bisnis = $PeluangBisnisModel::where('status', 'Active')
                ->where('display', true)
                ->where('id', $id)
                ->get()
                ->first();
        if ($peluang_bisnis) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $peluang_bisnis->id)
                    ->where('description', 'peluang_bisnis')
                    ->get()
                    ->first();
            $peluang_bisnis->image = $image;
        }
        return $this->_resJson($peluang_bisnis);
    }

    function get_tanggap_darurat_by_id($id)
    {
        $TanggapDaruratModel = app($this->model_path.'TanggapDarurat');
        $tanggap_darurat = $TanggapDaruratModel::where('status', 'Active')
                ->where('display', true)
                ->where('id', $id)
                ->get()
                ->first();
        if ($tanggap_darurat) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $tanggap_darurat->id)
                    ->where('description', 'tanggap_darurat')
                    ->get()
                    ->first();
            $tanggap_darurat->image = $image;
        }
        return $this->_resJson($tanggap_darurat);
    }
}