@extends('web/'.$pView.'/index')

@section('body')
<section id="slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($background as $i => $row)
                            <li data-target="#slider-carousel" data-slide-to="{{ $i }}" class="{{ $i==0?'active':'' }}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach($background as $i => $row)
                            <div class="item {{ $i==0?'active':'' }}">
                                <div class="col-sm-12" style="padding: 0;">
                                    <img class="img-slider" src="{{ $row->image_url }}">
                                    @if(isset($row->type) and $row->type=='Link')
                                    <div style="position: absolute;top: 15%;left: 10%;background: rgba(255, 255, 255, 0.5);padding: 20px;min-width: 30%;">
                                        <h1 style="margin-top: 0;"><span>{{ $row->modul }}</span></h1>
                                        <h2>{{ $row->param->name }}</h2>
                                        <p>{{ $row->param->short_description }}</p>
                                        <a href="{{ url($row->link) }}" class="btn btn-default get">Selengkapnya</a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection