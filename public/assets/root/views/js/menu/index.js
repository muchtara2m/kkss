$(function () {
    // Datatable
    let table
    let thead = `
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th>ID</th>
            <th>TYPE</th>
            <th>NAME</th>
            <th>LINK</th>
            <th>PARENT ID</th>
            <th>POSITION</th>
            <th>FOR ROLE</th>
            <th>STATUS</th>
            <th>CREATED AT</th>
            <th>UPDATED AT</th>
        </tr>
    `
    $('#example1').append($('<thead/>').append(thead))
    // Define Url get datatable
    let _datatable_url = $('#example1').attr('url')
    // Process render data
    table = $('#example1').DataTable({
        processing: true,
        serverSide: true,
        ajax: _datatable_url,
        type: 'GET',
        // dataType: "json",
        aaSorting: [[12, 'desc']],
        aaColumnDefs: [
            { 'bSortable': false, 'aTargets': [ 0, 1, 2 ] },
            // { 'bSearchable': false }
        ],
        columns: [
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <button 
                            type="button" 
                            onclick="onPost('`+data.id+`');" 
                            class="btn btn-sm btn-primary"
                            style="padding: 3px 6px;"
                        >
                            Save
                        </button>
                    `
                }

            },
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <button 
                            type="button" 
                            title="Edit"
                            onclick="_form('`+_datatable_url.replace('/get/datatable', '')+'/form/'+data.str_id+`')" 
                            class="btn btn-sm btn-warning"
                            style="padding: 3px 6px;"
                        >
                            Edit
                        </button>
                    `
                }

            },
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <a 
                            data-toggle="modal" 
                            href="#delete"
                            title="Delete"
                            data-str_id="`+data.str_id+`"
                            class="btn btn-sm btn-danger"
                            style="padding: 3px 6px;"
                        >
                            Delete
                        </a>
                    `
                }
            },
            {
                data: 'id',
                class: 'text-right',
                width: '5%',
                'bSortable': true,
                'bSearchable': false
            },
            {
                data: 'type', 
                name: 'parent_id',
                width: '5%'
            },
            {
                data: null, 
                name: 'name',
                width: '10%',
                render:
                function(data) {
                    return `
                        <input type="text" name="name[]" id="name-`+data.id+`" style="width: 100%;" value="`+data.name+`">
                    `
                }
            },
            {
                data: null, 
                name: 'link',
                width: '10%',
                render:
                function(data) {
                    return `
                        <input type="text" name="link[]" id="link-`+data.id+`" style="width: 100%;" value="`+data.link+`">
                    `
                }
            },
            {
                data: null, 
                name: 'parent_id',
                width: '10%',
                render:
                function(data) {
                    return `
                        <input type="text" name="parent_id[]" id="parent_id-`+data.id+`" style="width: 100%;" value="`+(data.parent_id?data.parent_id:'')+`">
                    `
                }
            },
            {
                data: null, 
                name: 'position',
                width: '5%',
                render:
                function(data) {
                    return `
                        <input type="text" name="position[]" id="position-`+data.id+`" style="width: 100%;" value="`+data.position+`">
                    `
                }
            },
            {
                data: null, 
                name: 'for_role',
                width: '10%',
                render:
                function(data) {
                    return `
                        <select name="for_role[]" id="for_role-`+data.id+`" style="width: 100%; height: 30px; border-radius: 0px;">
                            <option value="root" `+(data.for_role=='root'?'selected=""':'')+`>root</option>
                            <option value="superadmin" `+(data.for_role=='superadmin'?'selected=""':'')+`>superadmin</option>
                            <option value="admin" `+(data.for_role=='admin'?'selected=""':'')+`>admin</option>
                            <option value="client" `+(data.for_role=='client'?'selected=""':'')+`>client</option>
                            <option value="ambassador" `+(data.for_role=='ambassador'?'selected=""':'')+`>ambassador</option>
                        </select>
                    `
                }
            },
            {
                data: null, 
                name: 'status',
                width: '10%',
                render:
                function(data) {
                    return `
                        <select name="status[]" id="status-`+data.id+`" style="width: 100%; height: 30px; border-radius: 0px;">
                            <option value="Active" `+(data.status=='Active'?'selected=""':'')+`>Active</option>
                            <option value="Inactive" `+(data.status=='Inactive'?'selected=""':'')+`>Inactive</option>
                        </select>
                    `
                }
            },
            {
                data: 'created_date', 
                name: 'created_at',
                width: '10%'
            },
            {
                data: 'updated_date', 
                name: 'updated_at',
                width: '10%'
            },
        ],
        // 'sDom': 'Bfrtpil',
        // dom: 'Bfrtip',
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    })
})

// Modal Form
function _form(url) {
    window.location.href=url
}
// Modal Inactive
$('#delete').on('show.bs.modal', function (event) {
    let div = $(event.relatedTarget)
    let str_id = div.data('str_id')
    let modal = $(this)
    let _url = modal.find('#str_id_delete').attr('url')+'/'+str_id
    get_data_by_str_id(modal, _url, 'delete')
})

function get_data_by_str_id(modal, _url, type) {
    $.ajax({
        type: 'GET',
        url: _url, 
        success: function(response){
            if (type=='delete')
                set_modal_delete(modal, response)
        }
    })
}
function set_modal_delete(modal, response) {
    let str_id = response.str_id
        modal.find('#str_id_delete').val(str_id)
}

// Post Ajax
function onPost(id) {
    let _post = $('#example1').attr('post')
    let _redirect = $('#example1').attr('redirect')
    // Define Post
    let name = $('#name-'+id).val()
    let link = $('#link-'+id).val()
    let parent_id = $('#parent_id-'+id).val()
    let position = $('#position-'+id).val()
    let for_role = $('#for_role-'+id).val()
    let status = $('#status-'+id).val()
    // Create Form data
    let form_data = new FormData()
        form_data.append('str_id', id)
        form_data.append('name', name)
        form_data.append('link', link)
        form_data.append('parent_id', parent_id||'')
        form_data.append('position', position)
        form_data.append('for_role', for_role)
        form_data.append('status', status)
        form_data.append('is_ajax', true)
        form_data.append('_token', _token)
    // Proccess Ajax
    $.ajax({
        url         : _post,
        // dataType    : 'json',
        data        : form_data, // Setting the data attribute of ajax with file_data
        cache       : false,
        contentType : false,
        processData : false,
        method      : 'POST',
        type        : 'POST',
        beforeSend: function(){
            // $('#overlay').show()
        },
        success: function (response) {
            // $('#overlay').hide()
            if (response.post=='Success') {
                $('#success_message').text(response.message)
                $('#success').modal('show')
                setTimeout(function(){
                    $('#success').modal('hide')
                }, 2000)
            } else {
                $('#error_message').text(response.message)
                $('#error').modal('show')
                setTimeout(function(){
                    $('#error').modal('hide')
                }, 2000)
            }
            $('#example1').DataTable().ajax.reload()
        },
        error: function (response) {
            // $('#overlay').hide()
            console.log(response)
            alert('Error Proccess Ajax!')
            $('#example1').DataTable().ajax.reload()
        },
        complete: function() {

        }
    })
}
function onDelete(url) {
    let _post = url
    // Define Post
    let str_id = $('#str_id_delete').val()
    // Create Form data
    let form_data = new FormData()
        form_data.append('str_id', str_id)
        form_data.append('is_ajax', true)
        form_data.append('_token', _token)
    // Proccess Ajax
    $.ajax({
        url         : _post,
        // dataType    : 'json',
        data        : form_data, // Setting the data attribute of ajax with file_data
        cache       : false,
        contentType : false,
        processData : false,
        method      : 'POST',
        type        : 'POST',
        beforeSend: function(){
            // $('#overlay').show()
        },
        success: function (response) {
            // $('#overlay').hide()
            $('#delete').modal('hide')
            if (response.post=='Success') {
                $('#success_message').text(response.message)
                $('#success').modal('show')
                setTimeout(function(){
                    $('#success').modal('hide')
                }, 2000)
            } else {
                $('#error_message').text(response.message)
                $('#error').modal('show')
                setTimeout(function(){
                    $('#error').modal('hide')
                }, 2000)
            }
            $('#example1').DataTable().ajax.reload()
        },
        error: function (response) {
            // $('#overlay').hide()
            console.log(response)
            alert('Error Proccess Ajax!')
            $('#example1').DataTable().ajax.reload()
        },
        complete: function() {

        }
    })
}