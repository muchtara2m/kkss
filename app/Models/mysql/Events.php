<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'events';

	protected $fillable = [
		'name',
		'events_category_id',
		'seo',
		'location',
		'date',
		'pic',
		'short_description',
		'description',
		'display',
		'created_by'
	];
}
