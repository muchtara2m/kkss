@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Forum</li>
              <li>Kategori</li>
            </ol>
        </div>
    </div>
</section>
<section id="cart_items">
    <div class="container">
        <div class="row">
            <h2 class="title text-center">Forum</h2>
            <div class="col-sm-12">
                <div class="panel-body" style="border: 1px solid #E6E4DF; margin-bottom: 20px;">
                    <form method="get" action="">
                        <div class="row">
                            <div class="col-sm-3">
                                <select name="filter">
                                    <option value="0" selected="">Default</option>
                                    <option value="name" {{ $filter=='name'?'selected=""':'' }}>Nama Kategori</option>
                                    <option value="forum_visitor" {{ $filter=='forum_visitor'?'selected=""':'' }}>Views</option>
                                    <option value="posting" {{ $filter=='posting'?'selected=""':'' }}>Posting</option>
                                    <option value="followers" {{ $filter=='followers'?'selected=""':'' }}>Followers</option>
                                    <option value="updated_at" {{ $filter=='updated_at'?'selected=""':'' }}>Tanggal Update</option>
                                </select> 
                            </div>
                            <div class="col-sm-3">
                                <select name="sort">
                                    <option value="0" selected="">Default</option>
                                    <option value="asc" {{ $sort=='asc'?'selected=""':'' }}>Ascending</option>
                                    <option value="desc" {{ $sort=='desc'?'selected=""':'' }}>Descending</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <div class="search_box">
                                    <input type="text" name="keyword" placeholder="Keyword" value="{{ $keyword }}"/>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-default update" style="margin-top: 0px;">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <tbody>
                            @foreach($forum_category as $row)
                                <tr>
                                    <td class="cart_description" style="width: 15%; text-align: right;">
                                        <img src="{{ $row->image_url }}" alt="image" style="width: 130px; height: 80px;">
                                    </td>
                                    <td class="cart_description" style="width: 35%;">
                                        <h4>
                                            <a href="{{ url($sView.'/kategori/'.$row->seo) }}" style="color: #dc3545;">{{ $row->name }}</a>
                                            <small class="text-muted pull-right" style="font-size: 10px; font-style: italic;">{{ $row->last_update }}</small>
                                        </h4>
                                        <p style="min-height: 55px;">{{ $row->description }}</p>
                                    </td>
                                    <td class="cart_price" style="width: 15%; text-align: center;">
                                        <p>{{ $row->forum_visitor }}</p>
                                        <span>Views</span>
                                    </td>
                                    <td class="cart_price" style="width: 15%; text-align: center;">
                                        <p>{{ $row->posting }}</p>
                                        <span>Post</span>
                                    </td>
                                    <td class="cart_price" style="width: 20%; text-align: center;">
                                        @if(!$row->is_follow_status)
                                        <a class="btn btn-primary" href="{{ ($is_user?url($sView.'/follow/'.$row->seo):url('signin')) }}">{{ $row->is_follow_str }}</a>
                                        @else
                                        <a class="btn btn-default" href="javascript:;" disabled>{{ $row->is_follow_str }}</a>
                                        @endif
                                        <br/>
                                        <small class="text-muted" style="font-size: 10px; font-style: italic;">{{ $row->followers }} Followers</small>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="recommended_items">
                    <h2 class="title text-center">Postingan Terakhir</h2>  
                    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                @foreach($forum as $row)
                                    <div class="col-sm-4">
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo">
                                                    <img src="{{ $row->image_url }}" alt="image" style="height: 250px;" />
                                                    <div style="padding: 10px;">
                                                        <small class="font-size-xs" style="background: grey; color: white; padding: 2px 4px;">{{ $row->forum_date }}</small>
                                                        <small class="pull-right"><a href="{{ url('forum/kategori/'.$row->category->seo) }}" style="color: #dc3545;">{{ $row->category->name }}</a></small>
                                                        <h4><a href="{{ url('forum/'.$row->seo) }}" style="color: #dc3545;">{{ $row->title }}</a></h4>
                                                        <p>{{ $row->short_description }}</p>
                                                        <div style="border-top: 1px solid grey; padding-top: 10px;">
                                                            <div class="btn-group">
                                                                <span class="text-muted"><b>{{ $row->comments }}</b> Komentar </span> 
                                                                <span class="text-muted" style="margin-left: 10px;"><b>{{ $row->followers }}</b> Followers</span>
                                                                <span class="text-muted" style="margin-left: 10px;"><b>{{ $row->visitor }}</b> Pengunjung</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="item" style="display: none;">  
                                @for ($i = 0; $i < 3; $i++) 
                                    <div class="col-sm-4">
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo">
                                                    <img src="{{ asset('assets/main/images/blog/blog-2.jpg') }}" alt="" />
                                                    <div style="padding: 10px;">
                                                        <small class="font-size-xs" style="background: grey; color: white; padding: 2px 4px;">12 Feb 2020</small>
                                                        <small class="pull-right"><a href="" style="color: #dc3545;">Politik</a></small>
                                                        <h2><a href="" style="color: #dc3545;">Nama Topik</a></h2>
                                                        <p>
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Uts equi temporibus sit, quam ipsum laudantium vitae, eligendi illum assumenda
                                                        </p>
                                                        <div style="border-top: 1px solid grey; padding-top: 10px;">
                                                            <div class="btn-group">
                                                                <span><b>0</b> Komentar </span> 
                                                                <span style="margin-left: 10px;"><b>0</b> Followers</span>
                                                                <span style="margin-left: 10px;"><b>0</b> Pengunjung</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>          
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection