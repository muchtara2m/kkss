<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\TanggapDaruratQueries;

class TanggapDaruratController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'tanggapdarurat';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'tanggapdarurat';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, TanggapDaruratQueries $queries_tanggap_darurat)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_tanggap_darurat = $queries_tanggap_darurat;
	}

    public function index(Request $request)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Tanggap Darurat';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

		/* Collection variable data */
		// Get Tanggap Darurat
		$tanggap_darurat = $this->queries_tanggap_darurat->get_tanggap_darurat($keyword);
        foreach ($tanggap_darurat as $r) {
        	$r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('tanggapdarurat/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('tanggapdarurat/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
        }
        $data['tanggap_darurat'] = $tanggap_darurat;
        // Get Tanggap Darurat Category
        $tanggap_darurat_category = $this->queries_tanggap_darurat->get_tanggap_darurat_category();
        foreach ($tanggap_darurat_category as $r) {
        	$r->total = $this->queries_tanggap_darurat->count_tanggap_darurat_by_tanggap_darurat_category_id($r->id);
        }
        $data['tanggap_darurat_category'] = $tanggap_darurat_category;
		
		// Render to View
        return $this->_render($data);
    }

    public function category(Request $request, $tanggap_darurat_category_id)
    {
        // Global variable data
        $data = [];
        // Title in website
        $data['vTitle'] = 'Not set';
        // Filename in folder view
        $data['cView'] = 'category';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

        /* Collection variable data */
        // Get Tanggap Darurat Category Detail
        $category = $this->queries_tanggap_darurat->get_tanggap_darurat_category_by_id($tanggap_darurat_category_id);
        if ($category)
            $data['vTitle'] = $category->name.' | Tanggap Darurat ';
        $data['category'] = $category;
        // Get Tanggap Darurat
        $tanggap_darurat = $this->queries_tanggap_darurat->get_tanggap_darurat_by_tanggap_darurat_category_id($tanggap_darurat_category_id, $keyword);
        foreach ($tanggap_darurat as $r) {
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('tanggapdarurat/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('tanggapdarurat/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
        }
        $data['tanggap_darurat'] = $tanggap_darurat;
        // Get Tanggap Darurat Category
        $tanggap_darurat_category = $this->queries_tanggap_darurat->get_tanggap_darurat_category();
        foreach ($tanggap_darurat_category as $r) {
            $r->total = $this->queries_tanggap_darurat->count_tanggap_darurat_by_tanggap_darurat_category_id($r->id);
            $r->bold = $category?($category->id==$r->id?true:false):false;
        }
        $data['tanggap_darurat_category'] = $tanggap_darurat_category;
        
        // Render to View
        return $this->_render($data);
    }

    public function single($seo)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Not set';
		// Filename in folder view
		$data['cView'] = 'single';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'single';
		$data['vAdditional']['js'] = 'single';

		/* Collection variable data */
		// Get Tanggap Darurat
		$tanggap_darurat = $this->queries_tanggap_darurat->get_tanggap_darurat_by_seo($seo);
        if ($tanggap_darurat) {
			$data['vTitle'] = $tanggap_darurat->name.' | Tanggap Darurat';
        	$tanggap_darurat->created_date = date('d M Y, (H:i)', strtotime($tanggap_darurat->updated_at));
            $tanggap_darurat->image_url = $tanggap_darurat->image?\ImageUrl::url('tanggapdarurat/images/', $tanggap_darurat->image):null;
            $tanggap_darurat->thumb_url = $tanggap_darurat->image?\ImageUrl::url('tanggapdarurat/thumbs/', $tanggap_darurat->image):null;
            $tanggap_darurat->user_name = $tanggap_darurat->user?$tanggap_darurat->user->name:null;
            $tanggap_darurat->category_name = $tanggap_darurat->category?$tanggap_darurat->category->name:null;
        }
        $data['tanggap_darurat'] = $tanggap_darurat;
        // Get Tanggap Darurat Category
        $tanggap_darurat_category = $this->queries_tanggap_darurat->get_tanggap_darurat_category();
        foreach ($tanggap_darurat_category as $r) {
        	$r->total = $this->queries_tanggap_darurat->count_tanggap_darurat_by_tanggap_darurat_category_id($r->id);
        }
        $data['tanggap_darurat_category'] = $tanggap_darurat_category;
        // Get Tanggap Darurat Comments
        $count_comments = 0;
        $tanggap_darurat_comments = [];
        if ($tanggap_darurat) {
        	$tanggap_darurat_comments = $this->queries_tanggap_darurat->get_tanggap_darurat_comments_by_tanggap_darurat_id($tanggap_darurat->id);
        	foreach ($tanggap_darurat_comments as $r) {
                $r->sub_comments = $this->queries_tanggap_darurat->get_tanggap_darurat_comments_by_tanggap_darurat_id($tanggap_darurat->id, $r->id);
                $count_comments += count($r->sub_comments);
        	}
            $count_comments += count($tanggap_darurat_comments);
    	}
        $data['comments'] = $tanggap_darurat_comments;
        $data['count_comments'] = $count_comments;
		
		// Render to View
        return $this->_render($data);
    }

    public function comments(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'seo' => 'required',
            'tanggap_darurat_id' => 'required',
            'message' => 'required',
        ]);

        // Define Variable Request
        $seo = $request->input('seo');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        $tanggap_darurat_id = $request->input('tanggap_darurat_id');
        $tanggap_darurat_comments_id = $request->input('tanggap_darurat_comments_id');
        $message = $request->input('message');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['email'] = $email;
        $post['user_id'] = $user_id;
        $post['tanggap_darurat_id'] = $tanggap_darurat_id;
        $post['parent_id'] = $tanggap_darurat_comments_id;
        $post['message'] = $message;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Insert
        $_post = $this->queries_tanggap_darurat->insert_tanggap_darurat_comments($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully created!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->sView.'/'.$seo);
    }
}
