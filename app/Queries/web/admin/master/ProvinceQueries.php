<?php

namespace App\Queries\web\admin\master;

class ProvinceQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $ProvinceModel = app($this->model_path.'Province');
        $this->ProvinceModel = $ProvinceModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_province_datatable($whereRaw, $order, $offset, $limit)
    {
        $province = $this->ProvinceModel::select(
                        'province.*', 
                        'country.name as country_$_name'
                    )
                    ->join('country', 'country.id', 'province.country_id')
                    ->where('province.status', 'Active');
        if ($whereRaw!='')
            $province = $province->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'province.'.$order['coloumn'];
        $province = $province->orderBy($order_column, $order['dir'])
                    ->orderBy('province.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($province as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($province);
    }

    function get_province_total($whereRaw)
    {
        $province = $this->ProvinceModel::select('province.id')
                    ->join('country', 'country.id', 'province.country_id')
                    ->where('province.status', 'Active');
        if ($whereRaw!='')
            $province = $province->whereRaw($whereRaw);
        $province = $province->count();
        return $this->_resJson($province);
    }

    function get_province_by_id($id)
    {
        $data = $this->ProvinceModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_province_last()
	{
		$data = $this->ProvinceModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_province($post)
	{
		$data = $this->ProvinceModel::insert($post);
		return $this->_resJson($data);
	}

	function update_province($post)
	{
		$data = $this->ProvinceModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_province($id)
	{
		$data = $this->ProvinceModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_province($id)
	{
		$data = $this->ProvinceModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
