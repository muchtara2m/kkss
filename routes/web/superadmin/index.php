<?php

$router->group([
    'middleware' => 'role.super.admin',
    'prefix'     => 'superadmin'
], function () use ($router) {
	$router->get('/superadmin', 'error\NotFoundController@index')->name('superadmin');
	$router->group([
            'prefix'     => 'account'
        ], function () use ($router) {
            $router->get('', 'web\superadmin\AccountController@index')->name('superadmin/account');
            $router->post('/post', 'web\superadmin\AccountController@post')->name('superadmin/account/post');
    });
    $router->group([
            'prefix'     => 'dashboard'
        ], function () use ($router) {
            $router->get('', 'web\superadmin\DashboardController@index')->name('superadmin/dashboard');
    });
    $router->group([
            'prefix'     => 'contents'
        ], function () use ($router) {
            $router->get('', 'error\NotFoundController@index')->name('superadmin/contents');
            $router->group([
                    'prefix'     => 'logo'
                ], function () use ($router) {
                    $router->get('', 'web\superadmin\contents\LogoController@index')->name('superadmin/contents/logo');
                    $router->post('/post', 'web\superadmin\contents\LogoController@post')->name('superadmin/contents/logo/post');
            });
            $router->group([
                    'prefix'     => 'favicon'
                ], function () use ($router) {
                    $router->get('', 'web\superadmin\contents\FaviconController@index')->name('superadmin/contents/favicon');
                    $router->post('/post', 'web\superadmin\contents\FaviconController@post')->name('superadmin/contents/favicon/post');
            });
            $router->group([
                    'prefix'     => 'title'
                ], function () use ($router) {
                    $router->get('', 'web\superadmin\contents\TitleController@index')->name('superadmin/contents/title');
                    $router->post('/post', 'web\superadmin\contents\TitleController@post')->name('superadmin/contents/title/post');
            });
    });
    $router->group([
            'prefix'     => 'menu'
        ], function () use ($router) {
            $router->get('', 'web\superadmin\MenuController@index')->name('superadmin/menu');
            $router->get('/form/', 'web\superadmin\MenuController@form')->name('superadmin/menu/form');
            $router->get('/form/{str_id}', 'web\superadmin\MenuController@form')->name('superadmin/menu/form/id');
            $router->get('/get/datatable', 'web\superadmin\MenuController@get_datatable')->name('superadmin/menu/get/datatable');
            $router->get('/get/data/id/{param}', 'web\superadmin\MenuController@get_data_by_id')->name('superadmin/menu/get/data/id');
            $router->post('/post', 'web\superadmin\MenuController@post')->name('superadmin/menu/post');
            $router->post('/delete', 'web\superadmin\MenuController@delete')->name('superadmin/menu/delete');
    });
    $router->group([
            'prefix'     => 'user'
        ], function () use ($router) {
            $router->get('', 'web\superadmin\UserController@index')->name('superadmin/user');
            $router->get('/form/', 'web\superadmin\UserController@form')->name('superadmin/user/form');
            $router->get('/form/{str_id}', 'web\superadmin\UserController@form')->name('superadmin/user/form/id');
            $router->get('/get/datatable', 'web\superadmin\UserController@get_datatable')->name('superadmin/user/get/datatable');
            $router->get('/get/data/id/{param}', 'web\superadmin\UserController@get_data_by_id')->name('superadmin/user/get/data/id');
            $router->post('/post', 'web\superadmin\UserController@post')->name('superadmin/user/post');
            $router->post('/delete', 'web\superadmin\UserController@delete')->name('superadmin/user/delete');
    });
});