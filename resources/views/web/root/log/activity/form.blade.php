@extends('web/'.$pView.'/index')

@section('body')
<div class="templatemo-content-wrapper">
    <div class="templatemo-content">
        <ol class="breadcrumb">
            <li># Log</li>
            <li><a href="{{ route($pView.'/'.$sView) }}">Activity</a></li>
            <li class="active">Detail</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
                    <a class="btn btn-sm btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="" title="Refresh">
                        <i class="fa fa-rotate-right"></i>
                    </a>
                    <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
                        {{ ($log_activity) ? 'Last updated : '.$log_activity->updated_date : '' }}
                    </small>
                </div>
            </div>
        </div>
    	<div class="row" style="padding-bottom: 40px;">
    		<div class="col-lg-12">
    			<div class="panel panel-default" style="border: none;">
                    <h3 class="margin-bottom-15">
                        Detail Activity
                    </h3>
                    <hr/>
    				<div class="panel-body" style="border: 1px solid #eee;">
                        <fieldset>
                            <legend>General</legend>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">_id</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->_id:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Message</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->message:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Request</span>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="4" style="resize: vertical;" readonly="">{{ $log_activity?$log_activity->request:'' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Response</span>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="4" style="resize: vertical;" readonly="">{{ $log_activity?$log_activity->response:'' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Path</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->path:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">URL</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->url:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Method</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->method:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">IP</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->ip:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">User Agent</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->user_agent:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Session Key</span>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="4" style="resize: vertical;" readonly="">{{ $log_activity?$log_activity->session_key:'' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">created_at</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->created_at:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">updated_at</span>
                                    <div class="col-md-6">
                                        {{ $log_activity?$log_activity->updated_at:'' }}
                                    </div>
                                </div>
                            </div>
                        </fieldset>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
@endsection