<?php

namespace App\Queries\web\member;

class AccountQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    public function get_user_by_id($id)
    {
        $user = $this->UserModel::select()
                    ->where('id', $id)
                    ->get()
                    ->first();
        if ($user) {
            $user_profile = $this->get_user_profile_by_user_id($user->id);
            $user->profile = $user_profile;
            $village = $this->get_village_by_id($user->profile->village_id);
            $user->profile->district_id = $village->district_id;
            $district = $this->get_district_by_id($user->profile->district_id);
            $user->profile->city_id = $district->city_id;
            $city = $this->get_city_by_id($user->profile->city_id);
            $user->profile->province_id = $city->province_id;
            $province = $this->get_province_by_id($user->profile->province_id);
            $user->profile->country_id = $province->country_id;
            $image = $this->get_image_by_param_id($user->id);
            $user->image = $image;
        }
        return $this->_resJson($user);
    }

    public function get_user_by_email($email, $id)
    {
        $user = $this->UserModel::select()
                    ->where('email', $email)
                    ->where('id', '<>', $id)
                    ->get()
                    ->first();
        return $this->_resJson($user);
    }

    public function get_user_profile_by_user_id($user_id)
    {
        // Define Model
        $UserProfileModel = app($this->model_path.'UserProfile');
        // Get User Profile
        $user_profile = $UserProfileModel::select()
                    ->where('user_id', $user_id)
                    ->get()
                    ->first();
        return $this->_resJson($user_profile);
    }

    public function get_country()
    {
        // Define Model Country
        $CountryModel = app($this->model_path.'Country');
        // Get Country
        $country = $CountryModel::select()
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($country);
    }

    public function get_province_by_id($id)
    {
        // Define Model Province
        $ProvinceModel = app($this->model_path.'Province');
        // Get Province
        $province = $ProvinceModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($province);
    }

    public function get_city_by_id($id)
    {
        // Define Model City
        $CityModel = app($this->model_path.'City');
        // Get City
        $city = $CityModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($city);
    }

    public function get_district_by_id($id)
    {
        // Define Model District
        $DistrictModel = app($this->model_path.'District');
        // Get District
        $district = $DistrictModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($district);
    }

    public function get_village_by_id($id)
    {
        // Define Model Village
        $VillageModel = app($this->model_path.'Village');
        // Get Village
        $village = $VillageModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($village);
    }

    public function get_image_by_param_id($param_id)
    {
        // Define Model
        $ImageModel = app($this->model_path.'Image');
        // Get Image
        $image = $ImageModel::where('description', 'member')
                    ->where('param_id', $param_id)
                    ->get()
                    ->first();
        return $this->_resJson($image);
    }

    public function update_user($post)
    {
        $user = $this->UserModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($user);
    }

    public function update_user_profile($post)
    {
        // Define Model
        $UserProfileModel = app($this->model_path.'UserProfile');
        // Update User Profile
        $user_profile = $UserProfileModel::where('user_id', $post['user_id'])
                ->update($post);
        return $this->_resJson($user_profile);
    }
}