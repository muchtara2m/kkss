@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 40px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="{{ url($sView) }}">Forum</a></li>
              <li class="active">Followed Status</li>
            </ol>
        </div>
    </div>
</section>
<section id="cart_items" class="mg-50">
	<div class="container">
		<div class="table-responsive">
			<table class="table table-responsive table-hover">
				<thead>
					<tr style="background: #dc3545; color: white;">
						<th>Nama Forum Kategori</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($forum_category_followers as $row)
					<tr>
						<td><h4><a href="{{ url('forum/kategori/'.$row->forum_category->seo) }}" style="color: #dc3545;">{{ $row->forum_category->name }}</a></h4></td>
						<td><a href="javascript:;" class="btn btn-sm btn-default">{{ $row->is_status }}</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>
@endsection