<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'village';

	protected $fillable = [
		'district_id',
		'name',
		'region_type_id'
	];
}
