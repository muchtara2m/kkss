<?php

namespace App\Helpers;

use Storage;

class VideoUrl
{
    public static function url($path, $video)
    {
        $url = null;
        if (env('STORAGE_PUBLIC'))
            $url = asset(Storage::url('public/'.$path.$video->storage));
        else if (env('UPLOAD_PUBLIC'))
            if (env('APP_URL'))
                $url = asset(env('APP_URL').'uploads/'.$path.$video->name.$video->ext);
            else
                $url = asset(APP_URL.'uploads/'.$path.$video->name.$video->ext);
    	return $url;
    }
}