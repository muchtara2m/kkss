<?php

namespace App\Queries\web\main\domestik;

class HomeBaseQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $RepresentativeOfficeModel = app($this->model_path.'RepresentativeOffice');
        $this->RepresentativeOfficeModel = $RepresentativeOfficeModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_representative_office()
    {
        $data = $this->RepresentativeOfficeModel::where('status', 'Active')
                ->where('display', true)
                ->where('type', 'Domestik')
                ->get();
        // Get Image
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'representative_office')
                    ->get()
                    ->first();
            $v->image = $image;
        }
        return $this->_resJson($data);
    }
}