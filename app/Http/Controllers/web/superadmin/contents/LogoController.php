<?php

namespace App\Http\Controllers\web\superadmin\contents;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\superadmin\contents\LogoQueries;

class LogoController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
    protected $pView = 'superadmin';

    // Sub Folder (Child) in views
    protected $sView = 'contents/logo';

    // For description select menu by role from database
    protected $vMenu = 'superadmin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'contents/logo';

    // Session by user for access this controllers
    protected $sessions = 'superadmin';
    
    function __construct(ControllerQueries $queries_controller, LogoQueries $queries_logo)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_logo = $queries_logo;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Logo > Contents';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Logo
        $logo = $this->queries_logo->get_contents_by_description('logo');
        if ($logo) {
            $logo->image_id = $logo->image?$logo->image->id:null;
            $logo->image_url = $logo->image?\ImageUrl::url('contents/images/', $logo->image):null;
            $logo->thumb_url = $logo->image?\ImageUrl::url('contents/thumbs/', $logo->image):null;
        }
        $data['logo'] = $logo;

    	// Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $validate = [];
        $validate['str_id'] = 'required';
        $this->validate($request, $validate);

        // Define Variable Request
        $id = $request->input('str_id');
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $_post = false; // for upload file
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        // Update
        $post['id'] = $id;
        $post['updated_at'] = date('Y-m-d H:i:s');
        $_post = $this->queries_logo->update_contents($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully updated!.';
            $_param_id = $id;
        }

        // Proccess Upload
        if ($_post) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'logo'; // Set Description for DB Image
            $name = 'logo'; // Set File Image Name
            $path = 'contents'; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload)
                return \ErrorWeb::notfound();
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $_post);
    }
}
