@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="{{ url($sView) }}">Forum</a></li>
              <li class="active">{{ $forum?$forum->name:null }}</li>
            </ol>
        </div>
    </div>
</section>
<section class="mg-50">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Filter</h2>
                    <div class="brands_products">
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($forum_category as $row)
                                <li><a href="{{ url('forum/kategori/'.$row->seo) }}"> <span class="pull-right">({{ $row->total }})</span>{{ $row->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="blog-post-area" style="min-height: 500px;">
                    <h2 class="title text-center">Forum</h2>
                    <div class="single-blog-post">
                        <h3>{{ $forum->title }}</h3>
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-tag"></i> {{ $forum->category_name }}</li>
                                <li><i class="fa fa-user"></i> {{ $forum->user_name }}</li>
                                <li><i class="fa fa-calendar"></i> {{ $forum->created_date }}</li>
                                <li><i class="fa fa-comments"></i> {{ $count_conversation }} Kutipan</li>
                            </ul>
                        </div>
                        <div style="border: 1px solid #F7F7F0; padding: 20px;">
                            <img src="{{ $forum->image_url }}" alt="image" style="float: left; max-width: 650px; margin-right: 15px; margin-bottom: 5px;">
                            {!! $forum->description !!}
                        </div>
                    </div>
                </div>
                <div class="response-area">


                    @if(count($conversation))
                    <ul class="media-list">
                        @foreach($conversation as $row)
                        <li class="media" style="padding: 18px 0 0 0;">
                            <div class="media-body">
                                <a class="pull-left" href="#" style="pointer-events: none; margin-left: 20px;">
                                    <img class="media-object" src="{{ $row->image }}" alt="image" style="height: 25px;">
                                </a>
                                <ul class="sinlge-post-meta">
                                    <li>{{ $row->name }}</li>
                                    <li>{{ $row->created_date }}</li>
                                </ul>
                                <hr/>
                                <div class="panel-body" style="padding: 0 15px 15px 15px;">
                                    <p id="p{{ $row->id }}" style="font-size: 15px;">{{ $row->message }}</p>
                                    @if(isset($row->parent_message))
                                        <small style="margin-left: 15px;">Kutipan:</small>
                                        <div class="panel-body" style="border: 1px solid #f7f7f0; background: #f7f7f7;">
                                            <label><span class="text-muted">Pesan dari</span> {{ $row->parent_user }} <i class="fa fa-caret-right"></i></label>
                                            <small class="pull-right text-muted">{{ $row->parent_date }}</small>
                                            <p style="font-size: 15px;">{{ $row->parent_message }}</p>
                                        </div>
                                    @endif
                                    <div>
                                        <a href="javascript:;" class="btn btn-primary pull-right" id="conversation_dialog" data-forum_conversation_id="{{ $row->id }}"><i class="fa fa-reply"></i> Balas</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul> 
                    @endif

                    <div class="row" id="dconversation" style="margin-top: 50px;">
                        <div class="col-sm-12">
                            <fieldset>
                                <a href="javascript:;" id="conversation_cancel" class="btn btn-sm btn-link pull-right" style="display: none;"><i class="fa fa-times"></i></a>
                                <div id="conversation_reply" class="media-body" style="padding: 15px;border: 1px solid #f7f7f0;" hidden>
                                    <p id="p15" style="font-size: 15px;"></p>
                                </div>
                                {{ Form::open(['route' => $sView.'/conversation/post', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                                    <textarea name="message" placeholder="Tulis pesan" rows="3" style="padding: 10px;" required=""></textarea>
                                    <input type="hidden" name="forum_conversation_id" id="forum_conversation_id">
                                    <input type="hidden" name="seo" value="{{ $forum?$forum->seo:null }}">
                                    <input type="hidden" name="forum_id" value="{{ $forum->id }}">
                                    <input type="hidden" name="user_id" id="user_id" value="{{ $user?$user->id:null }}">
                                    <button type="submit" class="btn btn-sm btn-primary">Kirim</button>
                                {{ Form::close() }}
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</section>
@endsection
