<?php

namespace App\Queries\web\main;

class SignUpQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    public function get_user_by_email($email)
    {
        $user = $this->UserModel::select('id', 'name', 'email')
                    ->where('email', $email)
                    ->get()
                    ->first();
        return $this->_resJson($user);
    }

    public function insert_user($post)
    {
        $user = $this->UserModel::insert($post);
        return $this->_resJson($user);
    }

    public function insert_user_profile($post)
    {
        // Define Model
        $UserProfileModel = app($this->model_path.'UserProfile');
        // Insert User Profile
        $user_profile = $UserProfileModel::insert($post);
        return $this->_resJson($user_profile);
    }

    public function get_user_verified_by_verified_code($verified_code, $user_id = null)
    {
        // Define Model
        $UserVerifiedModel = app($this->model_path.'UserVerified');
        // Get User Verified
        $user_verified = $UserVerifiedModel::where('verified_code', $verified_code)
                            ->where('status', 'Active');
        if ($user_id)
            $user_verified = $user_verified->where('user_id', $user_id);
        $user_verified = $user_verified->get()
                            ->first();
        return $this->_resJson($user_verified);
    }

    public function insert_user_verified($post)
    {
        // Define Model
        $UserVerifiedModel = app($this->model_path.'UserVerified');
        // Insert User Verified
        $user_verified = $UserVerifiedModel::insert($post);
        return $this->_resJson($user_verified);
    }
}