<?php

namespace App\Queries\web\admin;

class GalleryQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $GalleryModel = app($this->model_path.'Gallery');
        $this->GalleryModel = $GalleryModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_gallery_datatable($whereRaw, $order, $offset, $limit)
    {
        $gallery = $this->GalleryModel::select(
                        'gallery.*', 
                        'gallery_category.id as gallery_category_$_id',
                        'gallery_category.name as gallery_category_$_name'
                    )
                    ->join('gallery_category', 'gallery_category.id', 'gallery.gallery_category_id')
                    ->where('gallery.status', 'Active');
        if ($whereRaw!='')
            $gallery = $gallery->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'gallery.'.$order['coloumn'];
        $gallery = $gallery->orderBy($order_column, $order['dir'])
                    ->orderBy('gallery.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($gallery as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'gallery')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->date = date('d M Y', strtotime($value->date));
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($gallery);
    }

    function get_gallery_total($whereRaw)
    {
        $gallery = $this->GalleryModel::select('gallery.id')
                    ->where('gallery.status', 'Active');
        if ($whereRaw!='')
            $gallery = $gallery->whereRaw($whereRaw);
        $gallery = $gallery->count();
        return $this->_resJson($gallery);
    }

    function get_gallery_by_id($id)
    {
        $data = $this->GalleryModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'gallery')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_gallery_last()
    {
        $data = $this->GalleryModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function get_gallery_category()
    {
        // Define Model
        $GalleryCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\GalleryCategory');
        // Get Data
        $category = $GalleryCategoryModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $category;
    }

    function insert_gallery($post)
    {
        $data = $this->GalleryModel::insert($post);
        return $this->_resJson($data);
    }

    function update_gallery($post)
    {
        $data = $this->GalleryModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_gallery($id)
    {
        $data = $this->GalleryModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_gallery($id)
    {
        $data = $this->GalleryModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
