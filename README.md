# KKSS

This project for sulawesi community 

# [Requirement]
* Install PHP version >= 7.0
* Install MySQL
* Install Apache / NginX

# [Initial Start]
* Copy file ".env.example" to ".env"
* Setup Database (Host, DBName, Username, Password) at ".env"
* Run terminal
  ```sh
	$ composer update
	$ php artisan key:generate
	$ php artisan storage:link
	$ php artisan migrate
	$ php artisan serve --port=2055
  ```
* Run at browser with url: http://localhost:2055
  * Optional
	chmod -R 777 storage