<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\PeluangBisnisQueries;

class PeluangBisnisController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'peluangbisnis';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'peluangbisnis';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, PeluangBisnisQueries $queries_peluang_bisnis)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_peluang_bisnis = $queries_peluang_bisnis;
	}

    public function index(Request $request)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Peluang Bisnis';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

		/* Collection variable data */
		// Get Peluang Bisnis
		$peluang_bisnis = $this->queries_peluang_bisnis->get_peluang_bisnis($keyword);
        foreach ($peluang_bisnis as $r) {
        	$r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('peluangbisnis/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('peluangbisnis/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
        }
        $data['peluang_bisnis'] = $peluang_bisnis;
        // Get Peluang Bisnis Category
        $peluang_bisnis_category = $this->queries_peluang_bisnis->get_peluang_bisnis_category();
        foreach ($peluang_bisnis_category as $r) {
        	$r->total = $this->queries_peluang_bisnis->count_peluang_bisnis_by_peluang_bisnis_category_id($r->id);
        }
        $data['peluang_bisnis_category'] = $peluang_bisnis_category;
		
		// Render to View
        return $this->_render($data);
    }

    public function category(Request $request, $peluang_bisnis_category_id)
    {
        // Global variable data
        $data = [];
        // Title in website
        $data['vTitle'] = 'Not set';
        // Filename in folder view
        $data['cView'] = 'category';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

        /* Collection variable data */
        // Get Peluang Bisnis Category Detail
        $category = $this->queries_peluang_bisnis->get_peluang_bisnis_category_by_id($peluang_bisnis_category_id);
        if ($category)
            $data['vTitle'] = $category->name.' | Peluang Bisnis ';
        $data['category'] = $category;
        // Get Peluang Bisnis
        $peluang_bisnis = $this->queries_peluang_bisnis->get_peluang_bisnis_by_peluang_bisnis_category_id($peluang_bisnis_category_id, $keyword);
        foreach ($peluang_bisnis as $r) {
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('peluangbisnis/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('peluangbisnis/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
        }
        $data['peluang_bisnis'] = $peluang_bisnis;
        // Get Peluang Bisnis Category
        $peluang_bisnis_category = $this->queries_peluang_bisnis->get_peluang_bisnis_category();
        foreach ($peluang_bisnis_category as $r) {
            $r->total = $this->queries_peluang_bisnis->count_peluang_bisnis_by_peluang_bisnis_category_id($r->id);
            $r->bold = $category?($category->id==$r->id?true:false):false;
        }
        $data['peluang_bisnis_category'] = $peluang_bisnis_category;
        
        // Render to View
        return $this->_render($data);
    }

    public function single($seo)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Not set';
		// Filename in folder view
		$data['cView'] = 'single';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'single';
		$data['vAdditional']['js'] = 'single';

		/* Collection variable data */
		// Get Peluang Bisnis
		$peluang_bisnis = $this->queries_peluang_bisnis->get_peluang_bisnis_by_seo($seo);
        if ($peluang_bisnis) {
			$data['vTitle'] = $peluang_bisnis->name.' | Peluang Bisnis';
        	$peluang_bisnis->created_date = date('d M Y, (H:i)', strtotime($peluang_bisnis->updated_at));
            $peluang_bisnis->image_url = $peluang_bisnis->image?\ImageUrl::url('peluangbisnis/images/', $peluang_bisnis->image):null;
            $peluang_bisnis->thumb_url = $peluang_bisnis->image?\ImageUrl::url('peluangbisnis/thumbs/', $peluang_bisnis->image):null;
            $peluang_bisnis->user_name = $peluang_bisnis->user?$peluang_bisnis->user->name:null;
            $peluang_bisnis->category_name = $peluang_bisnis->category?$peluang_bisnis->category->name:null;
        }
        $data['peluang_bisnis'] = $peluang_bisnis;
        // Get Peluang Bisnis Category
        $peluang_bisnis_category = $this->queries_peluang_bisnis->get_peluang_bisnis_category();
        foreach ($peluang_bisnis_category as $r) {
        	$r->total = $this->queries_peluang_bisnis->count_peluang_bisnis_by_peluang_bisnis_category_id($r->id);
        }
        $data['peluang_bisnis_category'] = $peluang_bisnis_category;
        // Get Peluang Bisnis Comments
        $count_comments = 0;
        $peluang_bisnis_comments = [];
        if ($peluang_bisnis) {
        	$peluang_bisnis_comments = $this->queries_peluang_bisnis->get_peluang_bisnis_comments_by_peluang_bisnis_id($peluang_bisnis->id);
        	foreach ($peluang_bisnis_comments as $r) {
                $r->sub_comments = $this->queries_peluang_bisnis->get_peluang_bisnis_comments_by_peluang_bisnis_id($peluang_bisnis->id, $r->id);
                $count_comments += count($r->sub_comments);
        	}
            $count_comments += count($peluang_bisnis_comments);
    	}
        $data['comments'] = $peluang_bisnis_comments;
        $data['count_comments'] = $count_comments;
		
		// Render to View
        return $this->_render($data);
    }

    public function comments(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'seo' => 'required',
            'peluang_bisnis_id' => 'required',
            'message' => 'required',
        ]);

        // Define Variable Request
        $seo = $request->input('seo');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        $peluang_bisnis_id = $request->input('peluang_bisnis_id');
        $peluang_bisnis_comments_id = $request->input('peluang_bisnis_comments_id');
        $message = $request->input('message');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['email'] = $email;
        $post['user_id'] = $user_id;
        $post['peluang_bisnis_id'] = $peluang_bisnis_id;
        $post['parent_id'] = $peluang_bisnis_comments_id;
        $post['message'] = $message;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Insert
        $_post = $this->queries_peluang_bisnis->insert_peluang_bisnis_comments($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully created!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->sView.'/'.$seo);
    }
}
