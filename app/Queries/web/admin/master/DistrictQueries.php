<?php

namespace App\Queries\web\admin\master;

class DistrictQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $DistrictModel = app($this->model_path.'District');
        $this->DistrictModel = $DistrictModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

	function get_province()
    {
        $ProvinceModel = app($this->model_path.'Province');
        $province = $ProvinceModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        foreach ($province as $value) {
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
        }
        return $this->_resJson($province);
    }

    function get_city()
    {
        $CityModel = app($this->model_path.'City');
        $city = $CityModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        foreach ($city as $value) {
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
        }
        return $this->_resJson($city);
    }

    function get_city_by_province_id($province_id)
    {
        $CityModel = app($this->model_path.'City');
        $city = $CityModel::where('status', 'Active')
                    ->where('province_id', $province_id)
                    ->orderBy('updated_at', 'desc')
                    ->get();
        foreach ($city as $value) {
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
        }
        return $this->_resJson($city);
    }

    function get_district_datatable($whereRaw, $order, $offset, $limit)
    {
        $district = $this->DistrictModel::select(
                        'district.*', 
                        'city.name as city_$_name', 
                        'province.name as province_$_name',
                        'country.name as country_$_name'
                    )
                    ->join('city', 'city.id', 'district.city_id')
                    ->join('province', 'province.id', 'city.province_id')
                    ->join('country', 'country.id', 'province.country_id')
                    ->where('district.status', 'Active');
        if ($whereRaw!='')
            $district = $district->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'district.'.$order['coloumn'];
        $district = $district->orderBy($order_column, $order['dir'])
                    ->orderBy('district.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($district as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($district);
    }

    function get_district_total($whereRaw)
    {
        $district = $this->DistrictModel::select('district.id')
                    ->join('city', 'city.id', 'district.city_id')
                    ->join('province', 'province.id', 'city.province_id')
                    ->join('country', 'country.id', 'province.country_id')
                    ->where('district.status', 'Active');
        if ($whereRaw!='')
            $district = $district->whereRaw($whereRaw);
        $district = $district->count();
        return $this->_resJson($district);
    }

    function get_district_by_id($id)
    {
        $data = $this->DistrictModel::select('district.*', 'province.id as province_id')
                ->join('city', 'city.id', 'district.city_id')
                ->join('province', 'province.id', 'city.province_id')
                ->where('district.status', 'Active')
                ->where('district.id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_district_last()
	{
		$data = $this->DistrictModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_district($post)
	{
		$data = $this->DistrictModel::insert($post);
		return $this->_resJson($data);
	}

	function update_district($post)
	{
		$data = $this->DistrictModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_district($id)
	{
		$data = $this->DistrictModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_district($id)
	{
		$data = $this->DistrictModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
