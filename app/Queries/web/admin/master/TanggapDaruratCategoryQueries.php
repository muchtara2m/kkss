<?php

namespace App\Queries\web\admin\master;

class TanggapDaruratCategoryQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $TanggapDaruratCategoryModel = app($this->model_path.'TanggapDaruratCategory');
        $this->TanggapDaruratCategoryModel = $TanggapDaruratCategoryModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_tanggap_darurat_category_datatable($whereRaw, $order, $offset, $limit)
    {
        $tanggap_darurat_category = $this->TanggapDaruratCategoryModel::select(
                        'tanggap_darurat_category.*'
                    )
                    ->where('tanggap_darurat_category.status', 'Active');
        if ($whereRaw!='')
            $tanggap_darurat_category = $tanggap_darurat_category->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'tanggap_darurat_category.'.$order['coloumn'];
        $tanggap_darurat_category = $tanggap_darurat_category->orderBy($order_column, $order['dir'])
                    ->orderBy('tanggap_darurat_category.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($tanggap_darurat_category as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($tanggap_darurat_category);
    }

    function get_tanggap_darurat_category_total($whereRaw)
    {
        $tanggap_darurat_category = $this->TanggapDaruratCategoryModel::select('tanggap_darurat_category.id')
                    ->where('tanggap_darurat_category.status', 'Active');
        if ($whereRaw!='')
            $tanggap_darurat_category = $tanggap_darurat_category->whereRaw($whereRaw);
        $tanggap_darurat_category = $tanggap_darurat_category->count();
        return $this->_resJson($tanggap_darurat_category);
    }

    function get_tanggap_darurat_category_by_id($id)
    {
        $data = $this->TanggapDaruratCategoryModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_tanggap_darurat_category_last()
	{
		$data = $this->TanggapDaruratCategoryModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_tanggap_darurat_category($post)
	{
		$data = $this->TanggapDaruratCategoryModel::insert($post);
		return $this->_resJson($data);
	}

	function update_tanggap_darurat_category($post)
	{
		$data = $this->TanggapDaruratCategoryModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_tanggap_darurat_category($id)
	{
		$data = $this->TanggapDaruratCategoryModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_tanggap_darurat_category($id)
	{
		$data = $this->TanggapDaruratCategoryModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
