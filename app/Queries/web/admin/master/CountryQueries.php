<?php

namespace App\Queries\web\admin\master;

class CountryQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $CountryModel = app($this->model_path.'Country');
        $this->CountryModel = $CountryModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_country_datatable($whereRaw, $order, $offset, $limit)
    {
        $country = $this->CountryModel::select(
                        'country.*'
                    )
                    ->where('country.status', 'Active');
        if ($whereRaw!='')
            $country = $country->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'country.'.$order['coloumn'];
        $country = $country->orderBy($order_column, $order['dir'])
                    ->orderBy('country.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($country as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($country);
    }

    function get_country_total($whereRaw)
    {
        $country = $this->CountryModel::select('country.id')
                    ->where('country.status', 'Active');
        if ($whereRaw!='')
            $country = $country->whereRaw($whereRaw);
        $country = $country->count();
        return $this->_resJson($country);
    }

    function get_country_by_id($id)
    {
        $data = $this->CountryModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_country_last()
	{
		$data = $this->CountryModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_country($post)
	{
		$data = $this->CountryModel::insert($post);
		return $this->_resJson($data);
	}

	function update_country($post)
	{
		$data = $this->CountryModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_country($id)
	{
		$data = $this->CountryModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_country($id)
	{
		$data = $this->CountryModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
