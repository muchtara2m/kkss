<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class RegionType extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'region_type';

	protected $fillable = [
		'name'
	];
}
