@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="{{ url($sView) }}">Forum</a></li>
              <li>Kategori</li>
              <li class="active">{{ $forum_category->name }}</li>
            </ol>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <h2 class="title text-center">Forum</h2>
            <div class="col-sm-12">
                <div class="panel-body" style="border: 1px solid #E6E4DF; margin-bottom: 20px;">
                    <form method="get" action="">
                        <div class="row">
                            <div class="col-sm-3">
                                <select name="filter">
                                    <option value="0" selected="">Default</option>
                                    <option value="name" {{ $filter=='name'?'selected=""':'' }}>Nama Kategori</option>
                                    <option value="visitor" {{ $filter=='visitor'?'selected=""':'' }}>Pengunjung</option>
                                    <option value="comments" {{ $filter=='comments'?'selected=""':'' }}>Komentar</option>
                                    <option value="followers" {{ $filter=='followers'?'selected=""':'' }}>Followers</option>
                                    <option value="updated_at" {{ $filter=='updated_at'?'selected=""':'' }}>Tanggal Update</option>
                                </select> 
                            </div>
                            <div class="col-sm-3">
                                <select name="sort">
                                    <option value="0" selected="">Default</option>
                                    <option value="asc" {{ $sort=='asc'?'selected=""':'' }}>Ascending</option>
                                    <option value="desc" {{ $sort=='desc'?'selected=""':'' }}>Descending</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <div class="search_box">
                                    <input type="text" name="keyword" placeholder="Keyword" value="{{ $keyword }}"/>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-default update" style="margin-top: 0px;">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
                <label>Terdapat {{ count($forum) }} postingan</label>
                @if(count($forum))
                    @foreach($forum as $row)
                        <div class="media commnets">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="{{ $row->image_url }}" alt="image" style="max-height: 100px;margin-left: 10px;margin-right: 10px;width: 95%;">
                            </a>
                            <div class="media-body" style="padding-left: 10px;padding-right: 10px;">
                                <h4 class="media-heading">{{ $row->title }}<small class="text-muted pull-right" style="font-size: 10px; font-style: italic;">{{ $row->last_update }}</small></h4>
                                <p>{{ $row->short_description }}</p>
                                <div class="blog-socials">
                                    <a href="#" class="btn btn-primary" style="background: white; color: #222; pointer-events: none;"><i class="text-muted">{{ $row->comments }}</i> Komentar</a>
                                    <a href="#" class="btn btn-primary" style="background: white; color: #222; pointer-events: none;"><i class="text-muted">{{ $row->followers }}</i> Followers</a>
                                    <a href="#" class="btn btn-primary" style="background: white; color: #222; pointer-events: none;"><i class="text-muted">{{ $row->visitor }}</i> Pengunjung</a>
                                    <a class="btn btn-primary" href="{{ url($sView.'/'.$row->seo) }}" style="margin-left: 20px;">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="text-center">
                        {!! $forum->render() !!}
                    </div>
                @else
                    <div class="row col-sm-12">
                        <h4 class="text-muted" style="margin-bottom: 50px;">No Content</h4>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection