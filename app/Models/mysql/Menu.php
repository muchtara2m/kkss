<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'menu';

	protected $fillable = [
		'name',
		'link',
		'parent_id',
		'favicon',
		'position',
		'for_role',
	];
}
