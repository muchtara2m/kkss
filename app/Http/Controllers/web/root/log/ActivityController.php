<?php

namespace App\Http\Controllers\web\root\log;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\root\log\ActivityQueries;

class ActivityController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
	protected $pView = 'root';

	// Sub Folder (Child) in views
	protected $sView = 'log/activity';

	// For description select menu by role from database
	protected $vMenu = 'root';
	
	// For selected menu (Active menu)
	protected $sMenu = 'log/activity';

	// Session by user for access this controllers
	protected $sessions = 'root';
	
	function __construct(ControllerQueries $queries_controller, ActivityQueries $queries_activity)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_activity = $queries_activity;
	}

    public function index()
    {
        // Global variable data
		$data = [];
		// Title in Website
		$data['vTitle'] = 'Activity > Log';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = 'index';

		/* Collection variable data */
		// Get Last
        $last = $this->queries_activity->get_log_activity_last();
        $data['last'] = $last;
		
		// Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('log_activity', $_GET);
        // Get Data
        $data = $this->queries_activity->get_log_activity_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_activity->get_log_activity_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_activity->get_log_activity_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Detail > Activity > Log';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        /* Collection variable data */
        // Get Data Activity
        $log_activity = null;
        if ($str_id) {
            $log_activity = $this->queries_activity->get_log_activity_by_id($str_id);
        }
        $data['log_activity'] = $log_activity;
        
        // Render to View
        return $this->_render($data);
    }
}