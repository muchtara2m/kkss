<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\StrukturOrganisasiQueries;

class StrukturOrganisasiController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'strukturorganisasi';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'strukturorganisasi';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, StrukturOrganisasiQueries $queries_struktur_organisasi)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_struktur_organisasi = $queries_struktur_organisasi;
	}

    public function index()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Struktur Organisasi';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		// Get Struktur Organisasi
        $struktur_organisasi = $this->queries_struktur_organisasi->get_struktur_organisasi_by_id(1);
        if ($struktur_organisasi) {
            $struktur_organisasi->image_id = $struktur_organisasi->image?$struktur_organisasi->image->id:null;
            $struktur_organisasi->image_url = $struktur_organisasi->image?\ImageUrl::url('home/strukturorganisasi/images/', $struktur_organisasi->image):null;
            $struktur_organisasi->thumb_url = $struktur_organisasi->image?\ImageUrl::url('home/strukturorganisasi/thumbs/', $struktur_organisasi->image):null;
        }
        $data['struktur_organisasi'] = $struktur_organisasi;
		
		// Render to View
        return $this->_render($data);
    }
}
