@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Our Member</li>
            </ol>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <h2 class="title text-center" style="margin-bottom: 10px;">Membership</h2>
            <div class="panel-body">
                <form method="get" action="">
                    <div class="search_box">
                        <input type="text" name="keyword" placeholder="Search" value="{{ $keyword }}"/>
                    </div>
                </form>
            </div>
            <div class="category-tab">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#nasional" data-toggle="tab">Nasional</a></li>
                        <li><a href="#nternasional" data-toggle="tab">Internasional</a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="nasional" >
                        @if(count($membership['nasional']))
                            @foreach ($membership['nasional'] as $row)
                                <div class="col-sm-3">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="{{ $row->image_url }}" alt="image" style="width: 100%; height: 240px;" />
                                                <h4>{{ $row->name }}</h4>
                                                <p style="color: #dc3545; {{ (strlen($row->profile->profession->name)>29?'font-size: 11px;':'') }}"><label>{{ $row->profile->profession->name }}</label></p>
                                                <a href="{{ url('membership/member/'.$row->id) }}" class="btn btn-sm btn-default add-to-cart"><i class="fa fa-eye"></i> Detail</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                        <div class="col-sm-12">
                            <h4 class="text-muted" style="margin-bottom: 50px;">No Content</h4>
                        </div>
                        @endif
                    </div>
                    <div class="tab-pane fade" id="nternasional" >
                        @if(count($membership['international']))
                            @foreach ($membership['international'] as $row)
                                <div class="col-sm-3">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="{{ $row->image_url }}" alt="image" style="width: 100%; height: 240px;" />
                                                <h4>{{ $row->name }}</h4>
                                                <p style="color: #dc3545; {{ (strlen($row->profile->profession->name)>29?'font-size: 11px;':'') }}"><label>{{ $row->profile->profession->name }}</label></p>
                                                <a href="{{ url('membership/member/'.$row->id) }}" class="btn btn-sm btn-default add-to-cart"><i class="fa fa-eye"></i> Detail</a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                        <div class="col-sm-12">
                            <h4 class="text-muted" style="margin-bottom: 50px;">No Content</h4>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection