<?php

namespace App\Queries;

use Session;

class Queries
{
	protected function _resJson($data)
	{
		// End Query Log
		$queries = \DB::connection()->getQueryLog();
		$this->set_log_queries($data, $queries);
		// $this->set_log_activity($data);
		return $data;
	}

    protected function set_log_activity($response, $session_key=null)
	{	
		// Get Session User
		if (!$session_key) {
			if (isset($this->sessions)) {
				if ($this->sessions=='') {
					$_previous = Session::get('_previous');
					if (!$_previous) return null;
					$_previous_url = Session::get('_previous')['url'];
			        $_previous_url = str_replace(env('APP_URL'), '', $_previous_url);
			        $_previous_url = explode('/', $_previous_url);
			        $_previous_url = $_previous_url[0];
			        $session_key = Session::get(env('SESSION').$_previous_url);
				} else 
					$session_key = Session::get(env('SESSION').$this->sessions);
			}
		}
        // Set Log Activity
        $param = [];
        $param['message'] = 'Success';
        $param['response'] = $response;
        $param['session_key'] = $session_key;
        if (env('MONGODB_DATABASE')!='')
        	\LogActivity::insert_log_activity($param);
	}

	protected function set_log_queries($response, $queries, $session_key=null)
	{
		// Get Session User
		if (!$session_key) {
			if (isset($this->sessions)) {
				if ($this->sessions=='') {
					$_previous = Session::get('_previous');
        			if (!$_previous) return null;
					$_previous_url = Session::get('_previous')['url'];
			        $_previous_url = str_replace(env('APP_URL'), '', $_previous_url);
			        $_previous_url = explode('/', $_previous_url);
			        $_previous_url = $_previous_url[0];
			        $session_key = Session::get(env('SESSION').$_previous_url);
				} else 
					$session_key = Session::get(env('SESSION').$this->sessions);
			}
		}
        // Set Log Queries
        $param = [];
        $param['message'] = 'Success';
        $param['response'] = $response;
        $param['log'] = json_encode($queries);
        $param['session_key'] = $session_key;
        if (env('MONGODB_DATABASE')!='')
        	\LogQueries::insert_log_queries($param);
	}
}
