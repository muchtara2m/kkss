<?php

$router->group([
		'middleware' => 'api',
        // 'prefix'     => '',
    ], function () use ($router) {
		include __DIR__.'/_global.php';
		include __DIR__.'/web.php';
});