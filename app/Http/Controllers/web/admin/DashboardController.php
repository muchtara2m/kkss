<?php

namespace App\Http\Controllers\web\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\DashboardQueries;

class DashboardController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
	protected $pView = 'admin';

	// Sub Folder (Child) in views
	protected $sView = 'dashboard';

	// For description select menu by role from database
	protected $vMenu = 'admin';
	
	// For selected menu (Active menu)
	protected $sMenu = 'dashboard';

	// Session by user for access this controllers
	protected $sessions = 'admin';
	
	function __construct(ControllerQueries $queries_controller, DashboardQueries $queries_dashboard)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
		$this->queries_dashboard = $queries_dashboard;
	}

    public function index()
    {
        // Global variable data
		$data = [];
		// Title in Website
		$data['vTitle'] = 'Dashboard';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		// 
		
		// Render to View
        return $this->_render($data);
    }
}