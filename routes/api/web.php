<?php

$router->group([
        'prefix'     => 'web',
    ], function () use ($router) {
		$router->get('/get/user', 'api\WebController@get_user')->name('api/get/user');
		$router->get('/get/country', 'api\WebController@get_country')->name('api/get/country');
		$router->get('/get/province/{country_id}', 'api\WebController@get_province_by_country_id')->name('api/get/province/country_id');
		$router->get('/get/city/{province_id}', 'api\WebController@get_city_by_province_id')->name('api/get/city/province_id');
		$router->get('/get/district/{city_id}', 'api\WebController@get_district_by_city_id')->name('api/get/district/city_id');
		$router->get('/get/village/{district_id}', 'api\WebController@get_village_by_district_id')->name('api/get/village/district_id');
		$router->get('/get/profession', 'api\WebController@get_profession')->name('api/get/profession');
		$router->get('/get/events', 'api\WebController@get_events')->name('api/get/events');
		$router->get('/get/peluangbisnis', 'api\WebController@get_peluang_bisnis')->name('api/get/peluangbisnis');
		$router->get('/get/tanggapdarurat', 'api\WebController@get_tanggap_darurat')->name('api/get/tanggapdarurat');
});