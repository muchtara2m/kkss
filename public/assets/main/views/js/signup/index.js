$(function () {
    $('#profession').select2({
        placeholder: "Pilih Profesi",
        allowClear: true
    });
    $('#province').select2({
        placeholder: "Pilih Provinsi",
        allowClear: true
    });
    $('#city').select2({
        placeholder: "Pilih Kota/Kabupaten",
        allowClear: true
    });
    $('#district').select2({
        placeholder: "Pilih Kecamatan",
        allowClear: true
    });
    $('#village').select2({
        placeholder: "Pilih Desa/Kelurahan",
        allowClear: true
    });
    profession_get()
    province_get_by_country_id(1)
    // Validation Form
    $('#file_attachment').on('change', function(){
        $('#link_image').hide()
        let file = $(this)[0].files[0]
        if(file.type == 'image/jpeg' || file.type == 'image/jpg' || file.type == 'image/png') {
            $('.btn-class').attr('disabled', false)
            $('#error_image').css('display', 'none')
        } else {
            $('.btn-class').attr('disabled', true)
            $('#error_image').css('display', '')
        }
    })
    // Confirm Password
    $('#confirm_password').keyup(function(){
        let confirm_password = $(this).val()
        let password = $('#password').val()
        if (confirm_password == password) {
            $('#confirm_password_error').css('display', 'none')
            $('.btn-class').attr('disabled', false)
        } else {
            $('#confirm_password_error').css('display', '')
            $('.btn-class').attr('disabled', true)
        }
    })
})
// Get Profession
function profession_get(profession_id='') {
    $.ajax({  
        type    : 'get',  
        url     : location.origin+'/api/web/get/profession/',  
        cache   : false, 
        beforeSend: function() {
            $('.se-pre-con').fadeIn('slow')
        },                  
        success: function(response) {
            // console.log(response)
            if (profession_id=='')
                $('#profession').append('<option value="" disabled="" selected="">Pilih Profesi</option>')
            for (let i in response) {
                let selected = ''
                if (response[i].id==profession_id)
                    selected = 'selected=""'
                $('#profession').append('<option value="'+response[i].id+'" '+selected+'>'+response[i].name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
            $('.se-pre-con').fadeOut('slow')
        }
    })   
}
// Get Province by Country ID
function province_get_by_country_id(country_id, province_id='') {
    $.ajax({  
        type    : 'get',  
        url     : location.origin+'/api/web/get/province/'+country_id,  
        cache   : false, 
        beforeSend: function() {
            $('.se-pre-con').fadeIn('slow')
        },                  
        success: function(response) {
            // console.log(response)
            if (province_id=='')
                $('#province').append('<option value="" disabled="" selected="">Pilih Provinsi</option>')
            for (let i in response) {
                let selected = ''
                if (response[i].id==province_id)
                    selected = 'selected=""'
                $('#province').append('<option value="'+response[i].id+'" '+selected+'>'+response[i].name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
            $('.se-pre-con').fadeOut('slow')
        }
    })   
}
// Get City by Province ID
function city_get_by_province_id(province_id, city_id='') {
    $.ajax({
        type    : 'get',
        url     : location.origin+'/api/web/get/city/'+province_id,
        data    : null,
        beforeSend: function() {
            $('.se-pre-con').fadeIn('slow')
        },
        success: function(response) {
            // console.log(response)
            if (city_id=='')
                $('#city').append('<option value="" disabled="" selected="">Pilih Kota/Kabupaten</option>')
            for (let i in response) {
                let selected = ''
                if (response[i].id==city_id)
                    selected = 'selected=""'
                $('#city').append('<option value="'+response[i].id+'" '+selected+'>'+response[i].name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function() {
            $('.se-pre-con').fadeOut('slow')
        }
    })
}
// Get District by City ID
function district_get_by_city_id(city_id, district_id='') {
    $.ajax({
        type    : 'get',
        url     : location.origin+'/api/web/get/district/'+city_id,
        data    : null,
        beforeSend: function() {
            $('.se-pre-con').fadeIn('slow')
        },
        success: function(response) {
            // console.log(response)
            if (district_id=='')
                $('#district').append('<option value="" disabled="" selected="">Pilih Kecamatan</option>')
            for (let i in response) {
                let selected = ''
                if (response[i].id==district_id)
                    selected = 'selected=""'
                $('#district').append('<option value="'+response[i].id+'" '+selected+'>'+response[i].name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function() {
            $('.se-pre-con').fadeOut('slow')
        }
    })
}
// Get Village by District ID
function village_get_by_district_id(district_id, village_id='') {
    $.ajax({
        type    : 'get',
        url     : location.origin+'/api/web/get/village/'+district_id,
        data    : null,
        beforeSend: function() {
            $('.se-pre-con').fadeIn('slow')
        },
        success: function(response) {
            // console.log(response)
            if (village_id=='')
                $('#village').append('<option value="" disabled="" selected="">Pilih Desa/Kelurahan</option>')
            for (let i in response) {
                let selected = ''
                if (response[i].id==village_id)
                    selected = 'selected=""'
                $('#village').append('<option value="'+response[i].id+'" '+selected+'>'+response[i].name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function() {
            $('.se-pre-con').fadeOut('slow')
        }
    })
}
// Select Option Province
$('#province').on('change', function() {
    let province_id = $(this).val()
    $('#city').empty()
    $('#district').empty()
    $('#village').empty()
    city_get_by_province_id(province_id)
})
// Select Option City
$('#city').on('change', function() {
    let city_id = $(this).val()
    $('#district').empty()
    $('#village').empty()
    district_get_by_city_id(city_id)
})
// Select Option District
$('#district').on('change', function() {
    let district_id = $(this).val()
    $('#village').empty()
    village_get_by_district_id(district_id)
})
// Preview Image
function preview_image(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader()
        reader.onload = function (e) {
            $('#preview_image').attr('src', e.target.result)
            $('#preview_image').css('display', '')
            $('#preview_image').css('margin-top', '15px')
        }
        reader.readAsDataURL(input.files[0])
    }
}
$('.file_attachment').change(function(){
    preview_image(this)
})
// Phone Type
function isNumberKey(evt){
    let phone = $('#phone').val()
        phone = phone.length
    if (phone>11)
        return false
    let charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false
    // if (phone==0 && charCode == 48)
    //     return false
    return true
}