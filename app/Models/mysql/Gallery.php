<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'gallery';

	protected $fillable = [
		'name',
		'gallery_category_id',
		'description',
		'short_description',
		'display'
	];
}
