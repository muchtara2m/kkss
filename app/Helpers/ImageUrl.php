<?php

namespace App\Helpers;

use Storage;

class ImageUrl
{
    public static function url($path, $image)
    {
        $url = null;
        if (env('STORAGE_PUBLIC'))
            $url = asset(Storage::url('public/'.$path.$image->storage));
        else if (env('UPLOAD_PUBLIC'))
            if (env('APP_URL'))
                $url = asset(env('APP_URL').'uploads/'.$path.$image->name.$image->ext);
            else
                $url = asset(APP_URL.'uploads/'.$path.$image->name.$image->ext);
    	return $url;
    }
}