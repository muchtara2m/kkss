<?php

namespace App\Http\Controllers\web\admin\home;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\home\AdatIstiadatQueries;

class AdatIstiadatController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
	protected $pView = 'admin';

	// Sub Folder (Child) in views
	protected $sView = 'home/adatistiadat';

	// For description select menu by role from database
	protected $vMenu = 'admin';
	
	// For selected menu (Active menu)
	protected $sMenu = 'home/adatistiadat';

	// Session by user for access this controllers
	protected $sessions = 'admin';
	
	function __construct(ControllerQueries $queries_controller, AdatIstiadatQueries $queries_adat_istiadat)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_adat_istiadat = $queries_adat_istiadat;
	}

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Adat Istiadat > Home';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_adat_istiadat->get_adat_istiadat_last();
        $data['last'] = $last;

        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('adat_istiadat', $_GET);
        // Get Data
        $data = $this->queries_adat_istiadat->get_adat_istiadat_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        foreach ($data as $r) {
            $r->image_url = $r->image?\ImageUrl::url($this->sView.'/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url($this->sView.'/thumbs/', $r->image):null;
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_adat_istiadat->get_adat_istiadat_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_adat_istiadat->get_adat_istiadat_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
            $data['images_id'] = $get->image->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Adat Istiadat > Home';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'form';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'form';

        /* Collection variable data */
        // Get Data adat_istiadat
        $adat_istiadat = null;
        if ($str_id) {
            $adat_istiadat = $this->queries_adat_istiadat->get_adat_istiadat_by_id($str_id);
            $adat_istiadat->image_id = $adat_istiadat->image?$adat_istiadat->image->id:null;
            $adat_istiadat->image_url = $adat_istiadat->image?\ImageUrl::url($this->sView.'/images/', $adat_istiadat->image):null;
            $adat_istiadat->thumb_url = $adat_istiadat->image?\ImageUrl::url($this->sView.'/thumbs/', $adat_istiadat->image):null;
        }
        $data['adat_istiadat'] = $adat_istiadat;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'title' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'display' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $title = $request->input('title');
        $seo = strtolower(str_replace(' ', '-', $title));
        $short_description = $request->input('short_description');
        $description = $request->input('description');
        $display = $request->input('display');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = Crypt::decryptString($session_user);
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $post['title'] = $title;
        $post['seo'] = $seo;
        $post['short_description'] = $short_description;
        $post['description'] = $description;
        $post['display'] = ($display=='true')?true:false;
        $post['created_by'] = $user_id;
        $_post = false; // for upload file
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_adat_istiadat->insert_adat_istiadat($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
                $_adat_istiadat = $this->queries_adat_istiadat->get_adat_istiadat_last();
                $_param_id = $_adat_istiadat->id;
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_adat_istiadat->update_adat_istiadat($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
                $_param_id = $id;
            }
        }

        // Proccess Upload
        if ($_post and $file) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'adat_istiadat'; // Set Description for DB Image
            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
            $path = $this->sView; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload)
                return \ErrorWeb::notfound();
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        // Delete
        // $_post = $this->queries_adat_istiadat->destroy_adat_istiadat($id);
        // Soft Delete
        $_post = $this->queries_adat_istiadat->delete_adat_istiadat($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }
}
