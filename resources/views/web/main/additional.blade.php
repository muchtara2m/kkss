<div class="modal fade" id="modalProduct" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">

            <!-- Close -->
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="fe fe-x" aria-hidden="true"></i>
            </button>

            <!-- Content -->
            <div class="container-fluid px-xl-0">
                <div class="row align-items-center mx-xl-0">
                    <div class="col-12 col-lg-6 col-xl-5 py-4 py-xl-0 px-xl-0">

                        <!-- Image -->
                        {{-- <img class="img-fluid" src="../assets/main/img/products/product-7.jpg" alt="..."> --}}
                        <img class="img-fluid" src="../assets/main/img/products/product-13.jpg" alt="...">

                        <!-- Button -->
                        <a class="btn btn-sm btn-block btn-primary" href="../product.html">
                            Find More <i class="fe fe-info ml-2"></i>
                        </a>

                    </div>
                    <div class="col-12 col-lg-6 col-xl-7 py-9 px-md-9">
                        {{-- <img class="img-fluid" src="../assets/main/img/products/product-13.jpg" alt="..."> --}}

                        <!-- Heading -->
                        <h4 class="mb-3">Judul Berita 1</h4>

                        <!-- Price -->
                        <div class="mb-7">
                            <span class="h5"> Tema Berita, </span>
                            <span class="font-size-m">( Aybis - </span>
                            <span class="font-size-sm"><time datetime="2020-01-01">01 Jan 2020</time> )</span>
                        </div>

                        <!-- Form -->
                        <form>
                            <div class="form-group">

                                <!-- Label -->
                                <p hidden>
                                    Color: <strong id="modalProductColorCaption">White</strong>
                                </p>

                                <!-- Radio -->
                                <div class="mb-8 ml-n1 text-justify">
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Optio minima vel nemo
                                        qui sit voluptatem reprehenderit expedita aut, facere nam placeat amet
                                        temporibus numquam non ut quae possimus repudiandae consequatur.</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum iste nisi
                                        ipsam eveniet. Harum facilis eum exercitationem doloremque velit dolores nobis
                                        voluptatem odit repellat! Consectetur molestias asperiores quis porro
                                        distinctio. Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                                        Necessitatibus enim doloribus placeat. Tempora officia modi aspernatur iste
                                        nobis laboriosam ad ipsum voluptatem repellat, veritatis fugiat nulla odio
                                        accusamus quae quo!</p>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Excepturi, nostrum
                                        neque nesciunt distinctio repudiandae totam id ipsa inventore nemo sint,
                                        cupiditate, quaerat qui possimus dicta expedita cum voluptas ducimus aperiam.
                                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Architecto odit at
                                        autem quisquam modi provident tempora, unde eos similique voluptatum in enim
                                        quasi sint corrupti dolorem delectus accusamus nihil? Voluptate.</p>
                                </div>

                            </div>

                            <div class="form-group mb-0">
                                <div class="form-row text-center">

                                    <div class="col-12 col-lg-auto">

                                        <!-- Wishlist -->
                                        <button class="btn btn-outline-dark btn-block mb-2" data-toggle="button">
                                            Like <i class="fe fe-heart ml-2"></i>
                                        </button>

                                    </div>
                                    <div class="col-12 col-lg-auto">

                                        <!-- Wishlist -->
                                        <button class="btn btn-outline-dark btn-block mb-2" data-toggle="button">
                                            Dislike <i class="fe fe-heart ml-2"></i>
                                        </button>

                                    </div>
                                    <div class="col-12 col-lg-auto">

                                        <!-- Wishlist -->
                                        <button class="btn btn-outline-dark btn-block mb-2" data-toggle="button">
                                            Report <i class="fe fe-heart ml-2"></i>
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>




<div class="modal fade" id="modalGambar" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Close -->
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="fe fe-x" aria-hidden="true"></i>
            </button>

            <!-- Content -->
            <div class=" m-8 shadow p-3 mb-5 bg-white rounded">
                <div class="row align-items-center mx-xl-0">
                    {{-- <div class="col-12 col-lg-6 col-xl-5 py-4 py-xl-0 px-xl-0">

                        <!-- Image -->
                        <img class="img-fluid" src="../assets/main/img/background/bg-1.jpg" alt="...">



                    </div> --}}
                    <div class="col text-center mt-5">

                        <!-- Heading -->
                        <span class="font-size-sm">5 Mei 2019</span>
                        <h4 class="mb-3">50Z IRL Chicago</h4>

                        <!-- Price -->
                        <div class="mb-7">
                            <span class="font-size-sm">I’m an event description. Click here to open up the Event Editor
                                and change my text.</span>
                        </div>
                        <!-- Register  -->
                        <button class="btn btn-outline-dark mb-5" data-toggle="button">
                            Register Now
                        </button>

                        <div class="mb-7">
                            <p class="text-primary">Time & Location</p>
                            <p>Jan 05, 2023, 7:00 PM <br> Chicago, Chicago, IL, USA</p>
                        </div>


                        <!-- Form -->
                        <form>
                            <div class="form-group">

                                <!-- Label -->
                                <p class="text-primary">
                                    About This Event
                                </p>
                                <div class="isi text-justify">
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum eum beatae
                                        quibusdam nam hic adipisci suscipit, nisi inventore laboriosam velit voluptates,
                                        harum possimus voluptatem fuga? Numquam atque enim modi inventore!.
                                    </p>
                                    <p>
                                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. In assumenda, at,
                                        distinctio inventore rem, necessitatibus neque ad optio consequatur quas quis!
                                        Cupiditate nostrum magnam ad nisi veniam magni temporibus facere. Lorem ipsum
                                        dolor, sit amet consectetur adipisicing elit. Doloribus numquam blanditiis
                                        laudantium dicta ea in, nobis, architecto, earum harum impedit corrupti eum
                                        aperiam eaque eos esse illo suscipit hic quasi.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum eum beatae
                                        quibusdam nam hic adipisci suscipit, nisi inventore laboriosam velit voluptates,
                                        harum possimus voluptatem fuga? Numquam atque enim modi inventore!.
                                    </p>
                                </div>

                                <!-- Register  -->
                                <button class="btn btn-outline-dark mb-5" data-toggle="button">
                                    Register Now
                                </button>
                            </div>
                            <div class="row" hidden>
                                <div class="col mx-auto">
                                    <img class="img-fluid shadow p-3 mb-5 bg-white rounded"
                                        src="../assets/main/img/background/bg-1.jpg" alt="..."
                                        style="max-height: 300px; width:80%">
                                </div>
                            </div>
                            <div class="form-group mb-0" hidden>
                                <div class="form-row text-center">

                                    <div class="col-12 col-lg-auto">

                                        <!-- Wishlist -->
                                        <button class="btn btn-outline-dark btn-block mb-2" data-toggle="button">
                                            Like <i class="fe fe-heart ml-2"></i>
                                        </button>

                                    </div>
                                    <div class="col-12 col-lg-auto">

                                        <!-- Wishlist -->
                                        <button class="btn btn-outline-dark btn-block mb-2" data-toggle="button">
                                            Dislike <i class="fe fe-heart ml-2"></i>
                                        </button>

                                    </div>
                                    <div class="col-12 col-lg-auto">

                                        <!-- Wishlist -->
                                        <button class="btn btn-outline-dark btn-block mb-2" data-toggle="button">
                                            Report <i class="fe fe-heart ml-2"></i>
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">

        <!-- Close -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="fe fe-x" aria-hidden="true"></i>
        </button>

        <!-- Header-->
        <div class="modal-header line-height-fixed font-size-lg">
          <strong class="mx-auto">LOGIN</strong>
          
        </div>

        <!-- Body -->
        <div class="modal-body text-center">

          <!-- Text -->
          <p class="mb-7 font-size-sm text-gray-500">
            Please enter your Email Address Correctly. You will receive a link
            to verify your account via Email.
          </p>

          <!-- Form -->
          <div class="card-body">
            {{ Form::open(['route' => 'login', 'method' => 'post', 'id' => 'myForm']) }}
              <div class="form-group">
                <label class="float-left">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
              </div>
              <div class="form-group pb-2">
                <label class="float-left">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                <small for="" class="float-right text-muted"><a href="#">Forgot Password ?</a></small>
              </div>
              {{ Form::hidden('level', 'member') }}
              {{ Form::hidden('url', 'member') }}
              <button type="submit" class="btn btn-block btn-primary">Login</button>
            {{ Form::close() }}
          </div>
          <p class="mb-7 font-size-sm text-gray-500">
            Don't have an account ? <a href="/signup" class="">Register</a>
          </p>
          <hr>

        </div>

      </div>

    </div>
  </div>