<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'city';

	protected $fillable = [
		'province_id',
		'name'
	];
}
