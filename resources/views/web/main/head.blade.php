<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>{{$vTitle}} | KKSS</title>
<link href="{{ asset('assets/main/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/main/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/main/css/prettyPhoto.css') }}" rel="stylesheet">
<link href="{{ asset('assets/main/css/price-range.css') }}" rel="stylesheet">
<link href="{{ asset('assets/main/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/main/css/main.css') }}" rel="stylesheet">
<link href="{{ asset('assets/main/css/responsive.css') }}" rel="stylesheet">
<link href="{{ asset('assets/main/libs/select2/select2.min.css') }}" rel="stylesheet">
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" rel="stylesheet" /> -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->       
<link rel="shortcut icon" href="{{ asset('assets/main/images/favicon/favicon.jpg') }}">