<?php

namespace App\Helpers;

use Request;
use App\Models\mongodb\LogActivity as LogActivityModel;

class LogActivity
{
    public static function insert_log_activity($param)
    {
    	$log = [];
    	$log['message'] = $param['message'];
        $log['request'] = json_encode(Request::input());
        $log['response'] = json_encode($param['response']);
        $log['path'] = Request::path();
    	$log['url'] = Request::fullUrl();
    	$log['method'] = Request::method();
    	$log['ip'] = Request::ip();
    	$log['user_agent'] = Request::header('user-agent');
    	$log['session_key'] = isset($param['session_key'])?$param['session_key']:'client';
    	LogActivityModel::create($log);
    }

    public static function get_log_activity()
    {
    	return LogActivityModel::latest()->get();
    }
}