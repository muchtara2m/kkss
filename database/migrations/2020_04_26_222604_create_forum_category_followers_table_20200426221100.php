<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumCategoryFollowersTable20200426221100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_category_followers', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('forum_category_id')->index();
            $table->integer('user_id')->index()->nullable();
            $table->enum('is_status', ['Request', 'Approve', 'Reject'])->default('Request');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_category_followers');
    }
}
