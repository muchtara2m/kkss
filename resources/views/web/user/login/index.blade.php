@extends('web/'.$pView.'/index')

@section('body')
<section class="login-box">
    <div class="login-logo">
        <a href=""><b>LOGIN</b> | ADMINISTRATOR</a>
    </div>
    <div>
        <div class="col-md-12" style="background-color: #c6000d; height: 2.5px;"></div>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Enter your account</p>
        @if(Session::has('post'))
            @if(Session::get('post') == 'Error')
                <span class="help-block text-center" style="margin-top: -15px;">
                    <strong>{{ Session::get("message") }}</strong>
                </span>
            @endif
        @endif
        {{ Form::open(['route' => 'login', 'method' => 'post', 'id' => 'myForm']) }}
            <div class="form-group has-feedback">
                {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'autocomplete' => 'off', 'required' => 'required']) }}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'autocomplete' => 'off', 'required' => 'required']) }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            @if($captcha!=null)
            <div class="form-group has-feedback">
                <div class="input-group">
                    <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0" style="padding: 0;">
                        <div class="captcha">
                            <button type="button" class="btn btn-danger btn-refresh" style="border-radius: 0;">
                                <i class="fa fa-refresh"></i>
                            </button>
                            <span>{!! captcha_img() !!}</span>
                        </div>
                    </div>
                    {{ Form::text('captcha', null, ['class' => 'form-control', 'id' => 'captcha', 'style' => 'height: 40px;', 'placeholder' => 'Enter Captcha', 'autocomplete' => 'off', 'required' => 'required']) }}
                </div>
                @if ($errors->has('captcha'))
                    <span class="help-block">
                        <strong>{{ $errors->first('captcha') }}</strong>
                    </span>
                @endif
            </div>
            @endif
            <div class="row">
                <div class="col-xs-8">    
                    <div class="checkbox icheck">
                        <label>
                            {{ Form::checkbox('remember', false) }}
                            Remember Me
                        </label>
                    </div>                        
                </div>
                <div class="col-xs-4">
                    {!! Form::token() !!}
                    {{ Form::hidden('url', $userdata->url) }}
                    {{ Form::hidden('g-recaptcha-response', null, ['id' => 'g-recaptcha-response']) }}
                    {{ Form::hidden('level', $userdata->level) }}
                    {{ Form::submit('Sign In', ['class' => 'btn btn-danger btn-block btn-flat', 'onclick' => 'onLoading();']) }}
                </div>
            </div>
        {{ Form::close() }}
        <a href="/user/forgotpassword/{{ $userdata->url }}" style="color: #666;">I forgot my password</a>
    </div>
</section>
@stop