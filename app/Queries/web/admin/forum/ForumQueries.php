<?php

namespace App\Queries\web\admin\forum;

class ForumQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $ForumModel = app($this->model_path.'Forum');
        $this->ForumModel = $ForumModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_forum_datatable($whereRaw, $order, $offset, $limit)
    {
        $forum = $this->ForumModel::select(
                        'forum.*', 
                        'forum_category.id as forum_category_$_id',
                        'forum_category.name as forum_category_$_name'
                    )
                    ->join('forum_category', 'forum_category.id', 'forum.forum_category_id')
                    ->where('forum.status', 'Active');
        if ($whereRaw!='')
            $forum = $forum->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'forum.'.$order['coloumn'];
        $forum = $forum->orderBy($order_column, $order['dir'])
                    ->orderBy('forum.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($forum as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'forum')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($forum);
    }

    function get_forum_total($whereRaw)
    {
        $forum = $this->ForumModel::select('forum.id')
                    ->join('forum_category', 'forum_category.id', 'forum.forum_category_id')
                    ->where('forum.status', 'Active');
        if ($whereRaw!='')
            $forum = $forum->whereRaw($whereRaw);
        $forum = $forum->count();
        return $this->_resJson($forum);
    }

    function get_forum_by_id($id)
    {
        $data = $this->ForumModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'forum')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_forum_last()
    {
        $data = $this->ForumModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function get_forum_category()
    {
        // Define Model
        $ForumCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\ForumCategory');
        // Get Data
        $forum_category = $ForumCategoryModel::where('status', 'Active')
                    ->where('display', true)
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $forum_category;
    }

    function insert_forum($post)
    {
        $data = $this->ForumModel::insert($post);
        return $this->_resJson($data);
    }

    function update_forum($post)
    {
        $data = $this->ForumModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_forum($id)
    {
        $data = $this->ForumModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_forum($id)
    {
        $data = $this->ForumModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
