@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Membership</h1>
        <ol class="breadcrumb">
            <li># <a>Membership</a></li>
            <li class="active">Form</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ ($membership) ? 'Last updated : '.$membership->updated_date : '' }}
            </span>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <h2 style="margin-top: -15px; font-size: 20px;">
            <label for="editor1">Form {{ $membership?'Edit':'Add' }} Membership</label>
        </h2>
        <div class="box">
            <div class="box-body" style="padding-top: 25px;">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Tipe Member *</label>
                                        <div class="col-md-6">
                                            <label>
                                                <input type="radio" name="type" class="flat-red" id="nasional" value="Nasional" required="" {{ $membership?$membership->type=='Nasional'?'checked=""':'':'' }}>
                                                &nbsp;&nbsp;
                                                Nasional
                                            </label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="radio" name="type" class="flat-red" id="international" value="International" required="" {{ $membership?$membership->type=='International'?'checked=""':'':'' }}>
                                                &nbsp;&nbsp;
                                                International
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('type'))
                                                <span class="btn btn-danger">Tipe Member harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Nama *</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" id="name" class="form-control form-control-md input-class" placeholder="Tulis disini" value="{{ $membership?$membership->name:'' }}" required="">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Nama harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">No. Telepon *</label>
                                        <div class="col-md-6">
                                            <input type="number" onkeypress="return isNumberKey(event)" name="phone" id="phone" class="form-control form-control-md input-class" placeholder="Tulis disini" value="{{ $membership?$membership->phone:'' }}" required="">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('phone'))
                                                <span class="btn btn-danger">Nama harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Profesi *</label>
                                        <div class="col-md-6">
                                            <select name="profession" id="profession" class="form-control form-control-md" required=""></select>
                                            <input type="hidden" id="profession_id" value="{{ $membership?$membership->profile->profession_id:'' }}">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('profession'))
                                                <span class="btn btn-danger">Profesi harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Alamat *</label>
                                        <div class="col-md-6">
                                            <textarea name="address" id="address" class="form-control form-control-md input-class" placeholder="Type here. ." required="">{{ $membership?$membership->profile->address:'' }}</textarea>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('address'))
                                                <span class="btn btn-danger">Alamat harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group national-address" style="display: {{ $membership?$membership->type=='International'?'none':'':'' }}">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Provinsi *</label>
                                        <div class="col-md-6">
                                            <select name="province" id="province" class="form-control form-control-md" style="width: 100%;"></select>
                                            <input type="hidden" id="province_id" value="{{ $membership?$membership->profile->province_id:'' }}">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('province'))
                                                <span class="btn btn-danger">Provinsi harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group national-address" style="display: {{ $membership?$membership->type=='International'?'none':'':'' }}">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Kota/Kabupaten *</label>
                                        <div class="col-md-6">
                                            <select name="city" id="city" class="form-control form-control-md" style="width: 100%;"></select>
                                            <input type="hidden" id="city_id" value="{{ $membership?$membership->profile->city_id:'' }}">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('city'))
                                                <span class="btn btn-danger">Kota/Kabupaten harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group national-address" style="display: {{ $membership?$membership->type=='International'?'none':'':'' }}">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Kecamatan *</label>
                                        <div class="col-md-6">
                                            <select name="district" id="district" class="form-control form-control-md" style="width: 100%;"></select>
                                            <input type="hidden" id="district_id" value="{{ $membership?$membership->profile->district_id:'' }}">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('district'))
                                                <span class="btn btn-danger">Kecamatan harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group national-address" style="display: {{ $membership?$membership->type=='International'?'none':'':'' }}">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Desa/Kelurahan *</label>
                                        <div class="col-md-6">
                                            <select name="village" id="village" class="form-control form-control-md" style="width: 100%;"></select>
                                            <input type="hidden" id="village_id" value="{{ $membership?$membership->profile->village_id:'' }}">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('village'))
                                                <span class="btn btn-danger">Desa/Kelurahan harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group national-address" style="display: {{ $membership?$membership->type=='International'?'none':'':'' }}">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Email *</label>
                                        <div class="col-md-6">
                                            <input type="email" name="email" id="email" class="form-control form-control-md input-class" placeholder="Tulis disini" value="{{ $membership?$membership->email:'' }}" required="">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('email'))
                                                <span class="btn btn-danger">Email harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Password</label>
                                        <div class="col-md-6">
                                            <input type="password" name="password" id="password" class="form-control form-control-md input-class" placeholder="Tulis disini">
                                            <small class="form-text text-muted g-font-size-default">Isi password jika anda ingin mengubahnya</small>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Konfirmasi Password</label>
                                        <div class="col-md-6">
                                            <input type="password" name="confirm_password" id="confirm_password" class="form-control form-control-md input-class" placeholder="Tulis disini">
                                            <small class="form-text text-muted g-font-size-default" id="confirm_password_error" style="display: none;">Password harus sama</small>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Foto profil</label>
                                        <div class="col-md-6">
                                            <p>Jenis file <small>(.jpeg/.jpg/.png)</small></p>
                                            <label class="u-file-attach-v2 g-color-gray-dark-v5 mb-20">
                                                <input type="file" name="file" id="file_attachment" class="file_attachment" {{ $membership?'':'required=""' }}>
                                                <small style="color: red !important; display: none;" id="error_image">* mohon untuk melilih gambar tipe (.jpeg/.jpg/.png)</small>
                                                <img src="{{ $membership?$membership->thumb_url?$membership->thumb_url:'':'' }}" id="preview_image" alt="Preview Image" style="width: 50%; {{ $membership?$membership->image_url?'margin-top: 15px;':'display: none':'display: none' }}">
                                                @if($membership&&$membership->image_url)
                                                <br /><a href="{{ $membership->image_url }}" target="_blank" id="link_image">Klik untuk melihat gambar lebih besar</a>
                                                <input type="hidden" name="image_id" id="image_id" value="{{ $membership?$membership->image_id:'' }}">
                                                @endif
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('image'))
                                                <span class="btn btn-danger">Foto profil harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="str_id" id="str_id" value="{{ $membership?$membership->str_id:'' }}">
                                            <button type="submit" class="btn btn-primary btn-class" style="border-radius: 0;" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default" style="border-radius: 0;"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop