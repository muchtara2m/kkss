<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $connection = 'mysql';
    
    protected $table = 'user_profile';
    
    protected $fillable = [
        'user_id',
        'gender',
        'place_of_birth',
        'date_of_birth',
        'village_id',
        'postal_code',
        'address',
        'biography',
        'national_identity_number',
        'profession_id'
    ];
}
