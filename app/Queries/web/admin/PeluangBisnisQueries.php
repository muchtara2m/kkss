<?php

namespace App\Queries\web\admin;

class PeluangBisnisQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $PeluangBisnisModel = app($this->model_path.'PeluangBisnis');
        $this->PeluangBisnisModel = $PeluangBisnisModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_peluang_bisnis_datatable($whereRaw, $order, $offset, $limit)
    {
        $peluang_bisnis = $this->PeluangBisnisModel::select(
                        'peluang_bisnis.*', 
                        'peluang_bisnis_category.id as peluang_bisnis_category_$_id',
                        'peluang_bisnis_category.name as peluang_bisnis_category_$_name'
                    )
                    ->join('peluang_bisnis_category', 'peluang_bisnis_category.id', 'peluang_bisnis.peluang_bisnis_category_id')
                    ->where('peluang_bisnis.status', 'Active');
        if ($whereRaw!='')
            $peluang_bisnis = $peluang_bisnis->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'peluang_bisnis.'.$order['coloumn'];
        $peluang_bisnis = $peluang_bisnis->orderBy($order_column, $order['dir'])
                    ->orderBy('peluang_bisnis.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($peluang_bisnis as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'peluang_bisnis')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->date = date('d M Y', strtotime($value->date));
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($peluang_bisnis);
    }

    function get_peluang_bisnis_total($whereRaw)
    {
        $peluang_bisnis = $this->PeluangBisnisModel::select('peluang_bisnis.id')
                    ->where('peluang_bisnis.status', 'Active');
        if ($whereRaw!='')
            $peluang_bisnis = $peluang_bisnis->whereRaw($whereRaw);
        $peluang_bisnis = $peluang_bisnis->count();
        return $this->_resJson($peluang_bisnis);
    }

    function get_peluang_bisnis_by_id($id)
    {
        $data = $this->PeluangBisnisModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'peluang_bisnis')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_peluang_bisnis_last()
    {
        $data = $this->PeluangBisnisModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function get_peluang_bisnis_category()
    {
        // Define Model
        $PeluangBisnisCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\PeluangBisnisCategory');
        // Get Data
        $category = $PeluangBisnisCategoryModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $category;
    }

    function insert_peluang_bisnis($post)
    {
        $data = $this->PeluangBisnisModel::insert($post);
        return $this->_resJson($data);
    }

    function update_peluang_bisnis($post)
    {
        $data = $this->PeluangBisnisModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_peluang_bisnis($id)
    {
        $data = $this->PeluangBisnisModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_peluang_bisnis($id)
    {
        $data = $this->PeluangBisnisModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
