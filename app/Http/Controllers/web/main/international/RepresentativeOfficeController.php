<?php

namespace App\Http\Controllers\web\main\international;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\international\RepresentativeOfficeQueries;

class RepresentativeOfficeController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'international/representativeoffice';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'international/representativeoffice';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, RepresentativeOfficeQueries $queries_representative_office)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_representative_office = $queries_representative_office;
	}

    public function index()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Representative Office > International';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		// Get Kantor Perwakilan
		$kantor_perwakilan = $this->queries_representative_office->get_representative_office();
		foreach ($kantor_perwakilan as $r) {
            $r->image_url = $r->image?\ImageUrl::url('international/representativeoffice/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('international/representativeoffice/thumbs/', $r->image):null;
        }
		$data['kantor_perwakilan'] = $kantor_perwakilan;
		
		// Render to View
        return $this->_render($data);
    }
}
