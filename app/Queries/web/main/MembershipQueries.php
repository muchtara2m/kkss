<?php

namespace App\Queries\web\main;

class MembershipQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_membership($type, $keyword=null)
    {
        $data = $this->UserModel::where('status', 'Active')
                ->where('level', 'member')
                ->where('type', $type);
        if ($keyword)
            $data = $data->where('name', 'like', '%'.$keyword.'%');
        $data = $data->get();
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserProfileModel = app($this->model_path.'UserProfile');
        $ProfessionModel = app($this->model_path.'Profession');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'member')
                    ->get()
                    ->first();
            $v->image = $image;
            $user_profile = $UserProfileModel::where('user_id', $v->id)
                    ->get()
                    ->first();
            $v->profile = $user_profile;
            $profession = $ProfessionModel::where('id', $v->profile->profession_id)
                    ->get()
                    ->first();
            $v->profile->profession = $profession;
        }
        return $this->_resJson($data);
    }

    public function get_user_by_id($id)
    {
        $user = $this->UserModel::select()
                    ->where('id', $id)
                    ->get()
                    ->first();
        if ($user) {
            $user_profile = $this->get_user_profile_by_user_id($user->id);
            $user->profile = $user_profile;
            $profession = $this->get_profession_by_id($user->profile->profession_id);
            $user_profile->profession_name = $profession->name;
            $village = $this->get_village_by_id($user->profile->village_id);
            if ($village) {
                $user->profile->village_name = $village->name;
                $district = $this->get_district_by_id($village->district_id);
                $user->profile->district_name = $district->name;
                $city = $this->get_city_by_id($district->city_id);
                $user->profile->city_name = $city->name;
                $province = $this->get_province_by_id($city->province_id);
                $user->profile->province_name = $province->name;
            }
            $image = $this->get_image_by_param_id($user->id);
            $user->image = $image;
        }
        return $this->_resJson($user);
    }

    public function get_profession_by_id($id)
    {
        // Define Model
        $ProfessionModel = app($this->model_path.'Profession');
        // Get User Profile
        $profession = $ProfessionModel::select()
                    ->where('id', $id)
                    ->get()
                    ->first();
        return $this->_resJson($profession);
    }

    public function get_user_profile_by_user_id($user_id)
    {
        // Define Model
        $UserProfileModel = app($this->model_path.'UserProfile');
        // Get User Profile
        $user_profile = $UserProfileModel::select()
                    ->where('user_id', $user_id)
                    ->get()
                    ->first();
        return $this->_resJson($user_profile);
    }

    public function get_country()
    {
        // Define Model Country
        $CountryModel = app($this->model_path.'Country');
        // Get Country
        $country = $CountryModel::select()
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($country);
    }

    public function get_province_by_id($id)
    {
        // Define Model Province
        $ProvinceModel = app($this->model_path.'Province');
        // Get Province
        $province = $ProvinceModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($province);
    }

    public function get_city_by_id($id)
    {
        // Define Model City
        $CityModel = app($this->model_path.'City');
        // Get City
        $city = $CityModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($city);
    }

    public function get_district_by_id($id)
    {
        // Define Model District
        $DistrictModel = app($this->model_path.'District');
        // Get District
        $district = $DistrictModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($district);
    }

    public function get_village_by_id($id)
    {
        // Define Model Village
        $VillageModel = app($this->model_path.'Village');
        // Get Village
        $village = $VillageModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($village);
    }

    public function get_image_by_param_id($param_id)
    {
        // Define Model
        $ImageModel = app($this->model_path.'Image');
        // Get Image
        $image = $ImageModel::where('description', 'member')
                    ->where('param_id', $param_id)
                    ->get()
                    ->first();
        return $this->_resJson($image);
    }
}