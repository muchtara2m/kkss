@extends('error/index')

@section('body')
<div class="flex-center position-ref full-height">
    @if($message)
        <div class="code">Error</div>
        <div class="message" style="margin: 10px; background-color: red; color: white;">{{ $message }}</div>
    @else
        <div class="code">404</div>
        <div class="message" style="padding: 10px;">Not Found</div>
    @endif
</div>
@stop