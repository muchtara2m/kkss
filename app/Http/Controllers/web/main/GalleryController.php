<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\GalleryQueries;

class GalleryController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'gallery';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'gallery';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, GalleryQueries $queries_gallery)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_gallery = $queries_gallery;
	}

    public function index(Request $request)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Gallery';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'index';
		$data['vAdditional']['js'] = null;

		// Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

		/* Collection variable data */
		// Get Gallery
        $gallery_category = $this->queries_gallery->get_gallery_category();
		foreach ($gallery_category as $k => $v) {
			$gallery = $this->queries_gallery->get_gallery_by_gallery_category_id($v->id, $keyword);
	        foreach ($gallery as $r) {
	        	$r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
	            $r->image_url = $r->image?\ImageUrl::url('gallery/images/', $r->image):null;
	            $r->thumb_url = $r->image?\ImageUrl::url('gallery/thumbs/', $r->image):null;
	            $r->user_name = $r->user?$r->user->name:null;
	            $r->short_desc = strlen($r->short_description)>25?substr($r->short_description, 0, 25).'..':$r->short_description;
	        }
	        $v->gallery = $gallery;
	    }
	    $gallery = $gallery_category;
        $data['gallery'] = $gallery;
		
		// Render to View
        return $this->_render($data);
    }
}
