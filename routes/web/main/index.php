<?php

$router->group([
    'prefix'     => ''
], function () use ($router) {
	$router->get('/', 'web\main\HomeController@index')->name('/');
	$router->get('/home', 'web\main\HomeController@index')->name('home');
    $router->get('/adatistiadat', 'web\main\AdatIstiadatController@index')->name('adatistiadat');
    $router->get('/adatistiadat/{seo}', 'web\main\AdatIstiadatController@single')->name('adatistiadat/seo');
    $router->post('/adatistiadat/comments', 'web\main\AdatIstiadatController@comments')->name('adatistiadat/comments');
    $router->get('/strukturorganisasi', 'web\main\StrukturOrganisasiController@index')->name('strukturorganisasi');
    $router->group([
            'prefix'     => 'forum'
        ], function () use ($router) {
            $router->get('/', 'web\main\ForumController@index')->name('forum');
            $router->get('/kategori/{seo}', 'web\main\ForumController@category')->name('forum/kategori');
            $router->get('/{seo}', 'web\main\ForumController@single')->name('forum/single');
            $router->post('/comments', 'web\main\ForumController@comments')->name('forum/comments');
            $router->get('/conversation/{seo}', 'web\main\ForumController@conversation')->name('forum/conversation');
            $router->post('/conversation/post', 'web\main\ForumController@conversation_post')->name('forum/conversation/post');
            $router->get('/follow/{seo}', 'web\main\ForumController@follow')->name('forum/follow');
            $router->get('/followed/status', 'web\main\ForumController@followed_status')->name('forum/followed/status');
    });
	$router->get('/membership', 'web\main\MembershipController@index')->name('membership');
    $router->get('/membership/member/{param}', 'web\main\MembershipController@member')->name('membership/member');
	$router->get('/events', 'web\main\EventsController@index')->name('events');
    $router->get('/events/{seo}', 'web\main\EventsController@single')->name('events/id');
    $router->get('/events/kategori/{id}', 'web\main\EventsController@category')->name('events/kategori');
    $router->post('/events/comments', 'web\main\EventsController@comments')->name('events/comments');
    $router->get('/events/register/{seo}', 'web\main\EventsController@register')->name('events/register');
    $router->post('/events/register/post', 'web\main\EventsController@register_post')->name('events/register/post');
    $router->get('/events/registered/status', 'web\main\EventsController@registered_status')->name('events/registered/status');
    $router->get('/gallery', 'web\main\GalleryController@index')->name('gallery');
    $router->get('/peluangbisnis', 'web\main\PeluangBisnisController@index')->name('peluangbisnis');
    $router->get('/peluangbisnis/{seo}', 'web\main\PeluangBisnisController@single')->name('peluangbisnis/id');
    $router->get('/peluangbisnis/kategori/{id}', 'web\main\PeluangBisnisController@category')->name('peluangbisnis/kategori');
    $router->post('/peluangbisnis/comments', 'web\main\PeluangBisnisController@comments')->name('peluangbisnis/comments');
    $router->get('/tanggapdarurat', 'web\main\TanggapDaruratController@index')->name('tanggapdarurat');
    $router->get('/tanggapdarurat/{seo}', 'web\main\TanggapDaruratController@single')->name('tanggapdarurat/seo');
    $router->get('/tanggapdarurat/kategori/{id}', 'web\main\TanggapDaruratController@category')->name('tanggapdarurat/kategori');
    $router->post('/tanggapdarurat/comments', 'web\main\TanggapDaruratController@comments')->name('tanggapdarurat/comments');
    $router->group([
            'prefix'     => 'international'
        ], function () use ($router) {
            $router->get('/tokoh', 'web\main\international\TokohController@index')->name('international/tokoh');
            $router->get('/representativeoffice', 'web\main\international\RepresentativeOfficeController@index')->name('international/representativeoffice');
    });
    $router->group([
            'prefix'     => 'domestik'
        ], function () use ($router) {
            $router->get('/tokoh', 'web\main\domestik\TokohController@index')->name('domestik/tokoh');
            $router->get('/homebase', 'web\main\domestik\HomeBaseController@index')->name('domestik/homebase');
    });
	$router->group([
            'prefix'     => 'signup'
        ], function () use ($router) {
            $router->get('', 'web\main\SignUpController@index')->name('signup');
            $router->post('post', 'web\main\SignUpController@post')->name('signup/post');
            $router->get('success', 'web\main\SignUpController@success')->name('signup/success');
    });
    $router->group([
            'prefix'     => 'signin'
        ], function () use ($router) {
            $router->get('', 'web\main\SignInController@index')->name('signin');
    });
	
});