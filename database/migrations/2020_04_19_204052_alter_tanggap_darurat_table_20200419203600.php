<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTanggapDaruratTable20200419203600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE tanggap_darurat Add short_description Text NULL;');
        DB::statement('ALTER TABLE tanggap_darurat Add created_by Int(11) NOT NULL DEFAULT 3;');
        DB::statement('ALTER TABLE tanggap_darurat ADD INDEX created_by (created_by);');
        DB::statement('ALTER TABLE tanggap_darurat Add seo varchar(255) NULL;');
        DB::statement('ALTER TABLE tanggap_darurat ADD INDEX seo (seo);');
        DB::statement('ALTER TABLE tanggap_darurat Add tanggap_darurat_category_id Int(11) NULL;');
        DB::statement('ALTER TABLE tanggap_darurat ADD INDEX tanggap_darurat_category_id (tanggap_darurat_category_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
