<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class StrukturOrganisasi extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'struktur_organisasi';

	protected $fillable = [
		'content'
	];
}
