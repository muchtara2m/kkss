<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProfessionTable20200207162010 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO profession (name) VALUES
            ('Profesor'),
            ('Industri Kreatif'),
            ('Perdagangan'),
            ('Energi, Minyak dan Gas'),
            ('Sumber Daya Mineral, Batubara dan Kelistrikan'),
            ('Investasi'),
            ('Finansial dan Pasar Modal'),
            ('Kontruksi dan Infrastruktur'),
            ('Agrobisnis'),
            ('Makanan'),
            ('Kehutanan'),
            ('Energi Terbarukan'),
            ('Lingkungan Hidup'),
            ('Properti'),
            ('Telematika'),
            ('Penyiaran'),
            ('Ristek'),
            ('Pendidikan'),
            ('Kesehatan'),
            ('Industri'),
            ('Makanan Olahan'),
            ('Industri Peternakan'),
            ('Pariwisata'),
            ('Hukum dan Regulasi'),
            ('UKM'),
            ('Pemberdayaan Perempuan'),
            ('Moneter Fiskal dan Kebijakan Publik'),
            ('Industri Olahraga'),
            ('Logistik dan Pengelolaan Rantai Pasok'),
            ('Hubungan Internasional'),
            ('Pengembangan Kawasan Ekonomi'),
            ('Perbankan'),
            ('Kelautan dan Perikanan');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
