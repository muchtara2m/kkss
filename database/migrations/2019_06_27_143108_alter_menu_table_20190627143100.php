<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMenuTable20190627143100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO menu (id, name, link, parent_id, favicon, position, for_role) VALUES
            ('30', 'Master', '#', NULL, NULL, 6, 'admin'),
            ('31', 'Region Type', 'master/regiontype', 30, NULL, 1, 'admin'),
            ('32', 'Country', 'master/country', 30, NULL, 2, 'admin'),
            ('33', 'Province', 'master/province', 30, NULL, 3, 'admin'),
            ('34', 'City', 'master/city', 30, NULL, 4, 'admin'),
            ('35', 'District', 'master/district', 30, NULL, 5, 'admin'),
            ('36', 'Village', 'master/village', 30, NULL, 6, 'admin'),
            ('37', 'Postal Code', 'master/postalcode', 30, NULL, 7, 'admin');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
