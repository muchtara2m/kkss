@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="{{ url($sView) }}">Adat Istiadat</a></li>
              <li class="active">{{ $adat_istiadat?$adat_istiadat->title:null }}</li>
            </ol>
        </div>
    </div>
</section>
<section class="mg-50">
    <div class="container">
        <div class="row">
            <h2 class="title text-center">Adat Istiadat</h2>
            <div class="col-sm-12">
                <div class="blog-post-area">
                    <div class="single-blog-post">
                        <h3>{{ $adat_istiadat->title }}</h3>
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-user"></i> {{ $adat_istiadat->user_name }}</li>
                                <li><i class="fa fa-calendar"></i> {{ $adat_istiadat->created_date }}</li>
                            </ul>
                        </div>
                        <img src="{{ $adat_istiadat->image_url }}" alt="image" style="float: left; max-width: 650px; margin-right: 15px; margin-bottom: 5px;">
                        {!! $adat_istiadat->description !!}
                    </div>
                </div>
            </div>  
            <div class="col-sm-12">
                <div class="response-area">
                    @if(count($comments))
                    <h4 style="margin-top: 50px;">{{ $count_comments }} Komentar</h4>
                    <ul class="media-list">
                        @foreach($comments as $row)
                        <li class="media">
                            <a class="pull-left" href="#" style="pointer-events: none; margin-left: 20px;">
                                <img class="media-object" src="{{ $row->image }}" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>{{ $row->name }}</li>
                                    <li><i class="fa fa-calendar"></i> {{ $row->created_date }}</li>
                                </ul>
                                <p>{{ $row->message }}</p>
                                <a href="javascript:;" class="btn btn-primary" id="komentar_dialog" data-adat_istiadat_comments_id="{{ $row->id }}"><i class="fa fa-reply"></i> Balas</a>
                            </div>
                        </li>
                        @if(count($row->sub_comments))
                            @foreach($row->sub_comments as $sub_row)
                            <li class="media second-media">
                                <a class="pull-left" href="#" style="pointer-events: none; margin-left: 20px;">
                                    <img class="media-object" src="{{ $sub_row->image }}" alt="">
                                </a>
                                <div class="media-body">
                                    <ul class="sinlge-post-meta">
                                        <li><i class="fa fa-user"></i>{{ $sub_row->name }}</li>
                                        <li><i class="fa fa-calendar"></i> {{ $sub_row->created_date }}</li>
                                    </ul>
                                    <p>{{ $sub_row->message }}</p>
                                    <a href="javascript:;" class="btn btn-primary" id="komentar_dialog_sub" data-adat_istiadat_comments_id="{{ $row->id }}"><i class="fa fa-reply"></i> Balas</a>
                                </div>
                            </li>
                            @endforeach
                        @endif
                        @endforeach
                    </ul> 
                    @endif
                    <div class="row" id="dcomments" style="margin-top: 50px;">
                        <div class="col-sm-6">
                            <fieldset>
                                <a href="javascript:;" id="komentar_cancel" style="font-size: 17px;color: red;font-weight: 500;" hidden>Batalkan balasan</a>
                                <h4>Tulis Komentar</h4>
                                {{ Form::open(['route' => $sView.'/comments', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                                    @if(!$user)
                                    <input type="text" name="name" placeholder="Nama lengkap">
                                    <input type="email" name="email" placeholder="Alamat email">
                                    <input type="text" name="phone" placeholder="No telepon">
                                    @endif
                                    <textarea name="message" placeholder="Tulis pesan anda" rows="11" style="padding: 10px;" required=""></textarea>
                                    <input type="hidden" name="adat_istiadat_comments_id" id="adat_istiadat_comments_id">
                                    <input type="hidden" name="seo" value="{{ $adat_istiadat?$adat_istiadat->seo:null }}">
                                    <input type="hidden" name="adat_istiadat_id" value="{{ $adat_istiadat->id }}">
                                    <input type="hidden" name="user_id" id="user_id" value="{{ $user?$user->id:null }}">
                                    <button type="submit" class="btn btn-sm btn-primary">Kirim komentar</button>
                                {{ Form::close() }}
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
