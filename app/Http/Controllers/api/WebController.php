<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\api\WebQueries;
use Validator;

class WebController extends \App\Http\Core\ApiController
{
	function __construct(ControllerQueries $queries_controller, WebQueries $queries_web)
	{
        $this->queries_controller = $queries_controller;
        $this->queries_web = $queries_web;
	}

	public function get_country(Request $request)
	{   
		try {
			// Get Country
			$country = $this->queries_web->get_country();
			// Response
	        return response()->json($country, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}

	public function get_province_by_country_id(Request $request, $country_id=1)
	{   
		try {
			// Get Province
			$province = $this->queries_web->get_province_by_country_id($country_id);
			if (count($province) == 0)
				return $this->res('Province not found!')->error();
			// Response
	        return response()->json($province, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}

	public function get_city_by_province_id(Request $request, $province_id)
	{   
		try {
			// Check Province ID
			if (!$province_id)
				return $this->res('Province ID not found!')->error();
			// Get City
			$city = $this->queries_web->get_city_by_province_id($province_id);
			if (count($city) == 0)
				return $this->res('City not found!')->error();
			// Response
	        return response()->json($city, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}

	public function get_district_by_city_id(Request $request, $city_id)
	{   
		try {
			// Check City ID
			if (!$city_id)
				return $this->res('City ID not found!')->error();
			// Get District
			$district = $this->queries_web->get_district_by_city_id($city_id);
			if (count($district) == 0)
				return $this->res('District not found!')->error();
			// Response
	        return response()->json($district, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}

	public function get_village_by_district_id(Request $request, $district_id)
	{   
		try {
			// Check District ID
			if (!$district_id)
				return $this->res('District ID not found!')->error();
			// Get Village
			$village = $this->queries_web->get_village_by_district_id($district_id);
			if (count($village) == 0)
				return $this->res('Village not found!')->error();
			// Response
	        return response()->json($village, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}

	public function get_profession(Request $request)
	{   
		try {
			// Get Profession
			$profession = $this->queries_web->get_profession();
			// Response
	        return response()->json($profession, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}

	public function get_events(Request $request)
	{   
		try {
			// Get Peluang Bisnis
			$events = $this->queries_web->get_events();
			// Response
	        return response()->json($events, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}

	public function get_peluang_bisnis(Request $request)
	{   
		try {
			// Get Peluang Bisnis
			$peluang_bisnis = $this->queries_web->get_peluang_bisnis();
			// Response
	        return response()->json($peluang_bisnis, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}

	public function get_tanggap_darurat(Request $request)
	{   
		try {
			// Get Peluang Bisnis
			$tanggap_darurat = $this->queries_web->get_tanggap_darurat();
			// Response
	        return response()->json($tanggap_darurat, 200);
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return response()->json($e->getMessage(), 500);
    	}
	}
}
