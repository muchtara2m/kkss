// Validation Form
$(function() {
    $('#file_attachment').on('change', function(){
        $('#link_image').hide()
        let file = $(this)[0].files[0]
        if(file.type == 'image/jpeg' || file.type == 'image/jpg' || file.type == 'image/png') {
            $('.btn-class').attr('disabled', false)
            $('#error_image').css('display', 'none')
        } else {
            $('.btn-class').attr('disabled', true)
            $('#error_image').css('display', '')
        }
    })
    // Froala JS
    $('#edit').froalaEditor({
        height: 200
    })
    $('#link').select2({
        placeholder: "Pilih Link",
        allowClear: true
    });
    $('input[name=type]').on('ifChanged', function() {
        let radios = document.getElementsByName('type')
        let value = ''
        for (let i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                // do whatever you want with the checked radio
                value = radios[i].value;
                // only one radio can be logically checked, don't check the rest
                // break;
            }
        }
        if (value=='Image') {
            $('.div-link').attr('hidden', true)
            $('.div-image').attr('hidden', false)
            $('#file_attachment').attr('required', true)
            $('#modul').attr('required', false)
            $('#link').attr('required', false)
        } else 
        if (value=='Link') {
            $('.div-link').attr('hidden', false)
            $('.div-image').attr('hidden', true)
            $('#file_attachment').attr('required', false)
            $('#modul').attr('required', true)
            $('#link').attr('required', true)
        }
    })
    let modul = $('#modul_').val()
    let param_id = $('#param_id').val()
    if (modul && param_id)
        link_get(modul, param_id)
})
// Preview Image
function preview_image(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader()
        reader.onload = function (e) {
            $('#preview_image').attr('src', e.target.result)
            $('#preview_image').css('display', '')
            $('#preview_image').css('margin-top', '15px')
        }
        reader.readAsDataURL(input.files[0])
    }
}
$('.file_attachment').change(function(){
    preview_image(this)
})
// Get Link
function link_get(modul, link_id='') {
    let url = location.origin+'/api/web/get/'
    if (modul=='Event') url = url+'events'
    else if (modul=='Peluang Bisnis') url = url+'peluangbisnis'
    else if (modul=='Tanggap Darurat') url = url+'tanggapdarurat'
    $.ajax({
        type    : 'get',
        url     : url,
        data    : null,
        beforeSend: function() {
            $('.se-pre-con').fadeIn('slow')
        },
        success: function(response) {
            // console.log(response)
            if (link_id=='')
                $('#link').append('<option value="" disabled="" selected="">Pilih Link</option>')
            for (let i in response) {
                $('#link').append('<option value="'+response[i].id+'-delimiter-'+response[i].seo+'">'+response[i].name+'</option>')
                if (link_id==response[i].id) $("#link").val(response[i].id+'-delimiter-'+response[i].seo).trigger('change')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function() {
            $('.se-pre-con').fadeOut('slow')
        }
    })
}

// Set Froala JS Value
$('#edit').froalaEditor('html.set', '-- Value --')

// Select Option Modul
$('#modul').on('change', function() {
    let modul = $(this).val()
    $('#link').empty()
    link_get(modul)
})