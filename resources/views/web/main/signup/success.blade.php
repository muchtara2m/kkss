@extends('web/'.$pView.'/index')

@section('body')
<section id="form">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <h2 class="text-center">Success</h2>
                <div class="text-center">
                    <h2>You Registration Success!</h2>
                    <div id="div-success">
                        <div class="m-login__head">
                            <div class="m-login__desc">
                                Now you can login with your account.
                            </div>
                        </div>
                        <br/>
                        <div class="m-login__form-action text-center">
                            <a href="{{ $base_url.'signin' }}" class="btn btn-primary">
                                Login
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection