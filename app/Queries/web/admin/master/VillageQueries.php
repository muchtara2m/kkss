<?php

namespace App\Queries\web\admin\master;

class VillageQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $VillageModel = app($this->model_path.'Village');
        $this->VillageModel = $VillageModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

	function get_village_datatable($whereRaw, $order, $offset, $limit)
	{
		$village = $this->VillageModel::select(
                        'village.*', 
                        'region_type.name as region_type_$_name',
                        'district.name as district_$_name',
                        'city.name as city_$_name', 
                        'province.name as province_$_name',
                        'country.name as country_$_name'
                    )
                    ->join('region_type', 'region_type.id', 'village.region_type_id')
                    ->join('district', 'district.id', 'village.district_id')
                    ->join('city', 'city.id', 'district.city_id')
                    ->join('province', 'province.id', 'city.province_id')
                    ->join('country', 'country.id', 'province.country_id')
                    ->where('village.status', 'Active');
        if ($whereRaw!='')
            $village = $village->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'village.'.$order['coloumn'];
        $village = $village->orderBy($order_column, $order['dir'])
                    ->orderBy('village.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($village as $key => $value) {
        	$value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($village);
	}

    function get_village_total($whereRaw)
    {
        $village = $this->VillageModel::select('village.id')
                    ->join('region_type', 'region_type.id', 'village.region_type_id')
                    ->join('district', 'district.id', 'village.district_id')
                    ->join('city', 'city.id', 'district.city_id')
                    ->join('province', 'province.id', 'city.province_id')
                    ->join('country', 'country.id', 'province.country_id')
                    ->where('village.status', 'Active');
        if ($whereRaw!='')
            $village = $village->whereRaw($whereRaw);
        $village = $village->count();
        return $this->_resJson($village);
    }

    function get_village_by_id($id)
    {
        $data = $this->VillageModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_village_last()
	{
		$data = $this->VillageModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_village($post)
	{
		$data = $this->VillageModel::insert($post);
		return $this->_resJson($data);
	}

	function update_village($post)
	{
		$data = $this->VillageModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_village($id)
	{
		$data = $this->VillageModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_village($id)
	{
		$data = $this->VillageModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
