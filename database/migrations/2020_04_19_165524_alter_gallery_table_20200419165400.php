<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterGalleryTable20200419165400 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE gallery Add short_description Text NULL;');
        DB::statement('ALTER TABLE gallery Add gallery_category_id Int(11) NULL;');
        DB::statement('ALTER TABLE gallery ADD INDEX gallery_category_id (gallery_category_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
