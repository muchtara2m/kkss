<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterManuTable20200209154500 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO menu (id, name, link, parent_id, favicon, position, for_role) VALUES
            ('50', 'Forum', 'forum', NULL, NULL, NULL, 'admin'),
            ('51', 'Events', 'events', NULL, NULL, NULL, 'admin'),
            ('52', 'Membership', 'membership', NULL, NULL, NULL, 'admin'),
            ('53', 'Peluang Bisnis', 'peluangbisnis', NULL, NULL, NULL, 'admin'),
            ('54', 'Gallery', 'gallery', NULL, NULL, NULL, 'admin'),
            ('55', 'Tanggap Darurat', 'tanggapdarurat', NULL, NULL, NULL, 'admin'),
            ('56', 'International', '#', NULL, NULL, NULL, 'admin'),
            ('57', 'Tokoh', 'international/tokoh', NULL, NULL, 1, 'admin'),
            ('58', 'Representative Office', 'international/representativeoffice', NULL, NULL, 2, 'admin');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
