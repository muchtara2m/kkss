// Conversation
$('#conversation_dialog, #conversation_dialog_sub').on('click', function() {
	let forum_conversation_id = $(this).attr('data-forum_conversation_id')
	let message = $('#p'+forum_conversation_id).text()
	$('#p15').text(message)
	$('#forum_conversation_id').val(forum_conversation_id)
	$('#conversation_cancel').css('display', '')
	$('#conversation_reply').attr('hidden', false)
	window.location = '#dconversation'
	// $('#bottom').animate({ scrollTop: $(document).height() }, 'slow')
  	// return false
})

$('#conversation_cancel').on('click', function() {
	$('#forum_conversation_id').val('')
	$('#conversation_cancel').css('display', 'none')
	$('#conversation_reply').attr('hidden', true)
})