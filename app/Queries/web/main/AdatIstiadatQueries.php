<?php

namespace App\Queries\web\main;

class AdatIstiadatQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $AdatIstiadatModel = app($this->model_path.'AdatIstiadat');
        $this->AdatIstiadatModel = $AdatIstiadatModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_adat_istiadat($keyword=null)
    {
        $data = $this->AdatIstiadatModel::where('status', 'Active')
                ->where('display', true);
        if ($keyword)
            $data = $data->where('title', 'like', '%'.$keyword.'%');
        $data = $data->orderBy('updated_at', 'desc')
                ->paginate(2);
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserModel = app($this->model_path.'User');
        $AdatIstiadatCommentsModel = app($this->model_path.'AdatIstiadatComments');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'adat_istiadat')
                    ->get()
                    ->first();
            $v->image = $image;
            $user = $UserModel::where('id', $v->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $comments = $AdatIstiadatCommentsModel::where('adat_istiadat_id', $v->id)
                        ->count();
            $v->comments = $comments;
        }
        return $this->_resJson($data);
    }

    function get_adat_istiadat_by_seo($seo)
    {
        $data = $this->AdatIstiadatModel::where('status', 'Active')
                ->where('display', true)
                ->where('seo', $seo)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'adat_istiadat')
                    ->get()
                    ->first();
            $data->image = $image;
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->created_by)
                    ->get()
                    ->first();
            $data->user = $user;
        }
        return $this->_resJson($data);
    }

    function get_adat_istiadat_by_id($id)
    {
        $data = $this->AdatIstiadatModel::where('status', 'Active')
                ->where('display', true)
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'adat_istiadat')
                    ->get()
                    ->first();
            $data->image = $image;
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->created_by)
                    ->get()
                    ->first();
            $data->user = $user;
        }
        return $this->_resJson($data);
    }

    function get_adat_istiadat_comments_by_adat_istiadat_id($adat_istiadat_id, $parent_id=null)
    {
        $AdatIstiadatCommentsModel = app($this->model_path.'AdatIstiadatComments');
        $data = $AdatIstiadatCommentsModel::where('status', 'Active')
                ->where('adat_istiadat_id', $adat_istiadat_id);
        if ($parent_id) {
            $data = $data->where('parent_id', $parent_id);
        } else
            $data = $data->whereNull('parent_id');
        $data = $data->get();
        // Get User & Image
        $UserModel = app($this->model_path.'User');
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $r) {
            $r->image = asset('assets/main/images/user-photo.jpg');
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            if ($r->user_id) {
                $user = $UserModel::where('status', 'Active')
                        ->where('id', $r->user_id)
                        ->get()
                        ->first();
                $r->user = $user ?? null;
                if ($user) {
                    $r->name = $r->user->name;
                    $r->email = $r->user->email;
                    $r->phone = $r->user->phone;
                    $image = $ImageModel::where('param_id', $user->id)
                            ->where('description', 'member')
                            ->get()
                            ->first();
                    if ($image)
                        $r->image = \ImageUrl::url('member/images/', $image);
                }
            }
        }
        return $this->_resJson($data);
    }

    function insert_adat_istiadat_comments($post)
    {
        $AdatIstiadatCommentsModel = app($this->model_path.'AdatIstiadatComments');
        $data = $AdatIstiadatCommentsModel::insert($post);
        return $this->_resJson($data);
    }
}