<?php

$router->group([
		// 'middleware' => '',
	    'prefix'     => ''
	], function () use ($router) {
		$router->get('notfound', 'error\NotFoundController@index')->name('notfound');
});