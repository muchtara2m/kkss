<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class ForumCategoryFollowers extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'forum_category_followers';

	protected $fillable = [
		'forum_category_id',
		'user_id',
		'is_status'
	];
}
