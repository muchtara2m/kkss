<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Crypt;
use App\Http\Core\ApiController;
use App\Queries\ControllerQueries;
use Closure;

class OAuthToken
{
    public function handle($request, Closure $next)
    {
        // Define Header
        $session_key = $request->header('SessionKey');
        $token_key = $request->header('TokenKey');

        // Define Library Extends
        $queries_controller = new ControllerQueries();
        $controller_api = new ApiController();

        try {
            // Check Token Key
            $token = $queries_controller->check_token($session_key, $token_key);
            if (!$token) 
                return $controller_api->res('Please Provide a Valid Token!')->forbidden();
            // Check Expired Token
            if (CURRENT_DATE > $token->expired_at) 
                return $controller_api->res('Token has Expired!')->error();

            \DB::beginTransaction();
            // Update Expired Token
            $post_token = [];
            $post_token['id'] = $token->id;
            $post_token['expired_at'] = date('Y-m-d H:i:s', strtotime(CURRENT_DATE . ' +1 day'));
            $update = $queries_controller->update_token($post_token);
            if (!$update)
                return $controller_api->res('Failed Update to Token')->error();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return $controller_api->res($e->getMessage())->error();
        }
        return $next($request);
    
    }
}