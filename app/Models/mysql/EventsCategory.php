<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class EventsCategory extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'events_category';

	protected $fillable = [
		'name',
		'description'
	];
}
