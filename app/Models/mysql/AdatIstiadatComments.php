<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class AdatIstiadatComments extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'adat_istiadat_comments';

	protected $fillable = [
		'adat_istiadat_id',
		'parent_id',
		'user_id',
		'name',
		'email',
		'phone',
		'message'
	];
}
