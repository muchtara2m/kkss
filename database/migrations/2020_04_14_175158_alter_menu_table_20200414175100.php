<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMenuTable20200414175100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            UPDATE menu SET position=8 where id = 60;
        ");
        DB::statement("
            UPDATE menu SET position=9 where id = 59;
        ");

        DB::statement("
            INSERT INTO menu (name, link, parent_id, favicon, position, for_role) VALUES
            ('Events Category', 'master/eventscategory', 30, NULL, 10, 'admin'),
            ('Domestik', '#', NULL, NULL, 13, 'admin');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
