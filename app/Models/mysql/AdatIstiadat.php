<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class AdatIstiadat extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'adat_istiadat';

	protected $fillable = [
		'title',
		'short_description',
		'description',
		'seo',
		'display',
		'created_by',
	];
}
