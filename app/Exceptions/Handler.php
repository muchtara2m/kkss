<?php

namespace App\Exceptions;

use Request;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Helpers\LogActivity as LogActivityHelper;
use Illuminate\Support\Facades\Session;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        // Get Session User
        $_previous = Session::get('_previous');
        if ($_previous) {
            $_previous_url = Session::get('_previous')['url'];
            $_previous_url = str_replace(env('APP_URL'), '', $_previous_url);
            $_previous_url = explode('/', $_previous_url);
            $_previous_url = $_previous_url[0];
            $session_key = Session::get(env('SESSION').$_previous_url);
            // Set Log Activity
            $param = [];
            $param['message'] = 'Error';
            $param['response'] = $exception->getMessage();
            $param['session_key'] = $session_key;
            if (env('MONGODB_DATABASE')!='')
                LogActivityHelper::insert_log_activity($param);
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if (((env('APP_ENV') == 'production') or (env('APP_ENV') == 'development')) and strpos(Request::fullUrl(), '/api/') !== false) {
            $apiController = app('App\Http\Core\ApiController');
            $code = $exception->getCode();
            if ($code === 0) {
                $message = $exception->getMessage();
                if ($message=='')
                    return $apiController->res('Page not found!')->notfound();
                // if (!empty($exception->getStatusCode())) {
                //     dd('OK');
                //     $status_code = $exception->getStatusCode();
                //     if ($status_code==404)
                //         return $apiController->res('Page not found!')->notfound();
                // }
            }
            $message = $exception->getMessage();
            $file = $exception->getFile();
            $line = $exception->getLine();
            // $severity = $exception->getSeverity();
            return $apiController->res([
                'code' => $code,
                '_message' => $message,
                'file' => $file,
                'line' => $line,
                // 'severity' => $severity,
            ])->error();
        } else
        if (env('APP_ENV') == 'production') {
            return \ErrorWeb::notfound();
        } else 
        if (env('APP_ENV') == 'development') {
            return \ErrorWeb::notfound($exception->getMessage());
        } else
        if (env('APP_ENV') == 'local') {
            return parent::render($request, $exception);
        }
    }
}
