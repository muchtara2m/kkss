<?php

namespace App\Queries\web\main;

class GalleryQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $GalleryModel = app($this->model_path.'Gallery');
        $this->GalleryModel = $GalleryModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_gallery_by_gallery_category_id($gallery_category_id, $keyword=null)
    {
        $data = $this->GalleryModel::where('status', 'Active')
                ->where('display', true)
                ->where('gallery_category_id', $gallery_category_id);
        if ($keyword)
            $data = $data->where('name', 'like', '%'.$keyword.'%');
        $data = $data->get();
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'gallery')
                    ->get()
                    ->first();
            $v->image = $image;
        }
        return $this->_resJson($data);
    }

    function get_gallery_category()
    {
        // Define Model
        $GalleryCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\GalleryCategory');
        // Get Data
        $category_blog = $GalleryCategoryModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $category_blog;
    }
}