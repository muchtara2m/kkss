<?php

namespace App\Queries\web\admin\forum;

class ForumCategoryFollowersQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $ForumCategoryFollowersModel = app($this->model_path.'ForumCategoryFollowers');
        $this->ForumCategoryFollowersModel = $ForumCategoryFollowersModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_forum_category_followers_datatable($whereRaw, $order, $offset, $limit)
    {
        $forum_category_followers = $this->ForumCategoryFollowersModel::select(
                        'forum_category_followers.*',
                        'forum_category.name as forum_category_$_name',
                        'user.name as user_$_name',
                        'user.email as user_$_email'
                    )
                    ->join('forum_category', 'forum_category.id', 'forum_category_followers.forum_category_id')
                    ->join('user', 'user.id', 'forum_category_followers.user_id')
                    ->where('forum_category_followers.status', 'Active');
        if ($whereRaw!='')
            $forum_category_followers = $forum_category_followers->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'forum_category_followers.'.$order['coloumn'];
        $forum_category_followers = $forum_category_followers->orderBy($order_column, $order['dir'])
                    ->orderBy('forum_category_followers.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($forum_category_followers as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($forum_category_followers);
    }

    function get_forum_category_followers_total($whereRaw)
    {
        $forum_category_followers = $this->ForumCategoryFollowersModel::select('forum_category_followers.id')
                    ->join('forum_category', 'forum_category.id', 'forum_category_followers.forum_category_id')
                    ->join('user', 'user.id', 'forum_category_followers.user_id')
                    ->where('forum_category_followers.status', 'Active');
        if ($whereRaw!='')
            $forum_category_followers = $forum_category_followers->whereRaw($whereRaw);
        $forum_category_followers = $forum_category_followers->count();
        return $this->_resJson($forum_category_followers);
    }

    function get_forum_category_followers_by_id($id)
    {
        $data = $this->ForumCategoryFollowersModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Forum Category
            $ForumCategoryModel = app($this->model_path.'ForumCategory');
            $forum_category = $ForumCategoryModel::where('id', $data->forum_category_id)
                                ->get()
                                ->first();
            $data->forum_category = $forum_category; 
            // 
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->user_id)
                                ->get()
                                ->first();
            $data->user = $user; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_forum_category_followers_last()
    {
        $data = $this->ForumCategoryFollowersModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function update_forum_category_followers($post)
    {
        $data = $this->ForumCategoryFollowersModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_forum_category_followers($id)
    {
        $data = $this->ForumCategoryFollowersModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_forum_category_followers($id)
    {
        $data = $this->ForumCategoryFollowersModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
