<?php

namespace App\Queries\web\user;

class LoginQueries extends \App\Queries\Queries
{	
    protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    public function get_user_by_url($url)
    {
        $data = $this->UserModel::where('status', 'Active')
                ->where('url', $url)
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    public function get_user_by_email($param)
    {
        $data = $this->UserModel::where('email', $param['email'])
                ->where('level', $param['level']);
        if ($param['level'] != 'member')
            $data = $data->where('url', $param['url']);
        $data = $data->get()
                ->first();
        return $this->_resJson($data);
    }

    public function insert_token($post)
    {
        $TokenModel = app($this->model_path.'Token');
        $data = $TokenModel::insert($post);
        return $this->_resJson($data);
    }

    public function update_user($post)
    {
        $data = $this->UserModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }
}
