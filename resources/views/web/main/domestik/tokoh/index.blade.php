@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li>Domestik</li>
              <li class="active">Tokoh</li>
            </ol>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <h2 class="title text-center" style="margin-bottom: 10px;">Tokoh Nasional KKSS</h2>
            <div class="panel-body">
                <form method="get" action="">
                    <div class="search_box">
                        <input type="text" name="keyword" placeholder="Search" value="{{ $keyword }}"/>
                    </div>
                </form>
            </div>
            <div class="category-tab">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="category1">
                        @if(count($tokoh))
                            @foreach($tokoh as $r)
                                <div class="col-sm-3">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="{{ $r->image_url }}" alt="image" style="width: 100%; height: 240px;" />
                                                <h4>{{ $r->name }}</h4>
                                                <p>{{ $r->short_description }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                        <div class="col-sm-12">
                            <h4 class="text-muted" style="margin-bottom: 50px;">No Content</h4>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection