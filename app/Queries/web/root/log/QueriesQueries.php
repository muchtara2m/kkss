<?php

namespace App\Queries\web\root\log;

class QueriesQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'root';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\mongodb';

        // Define Model Global
        $LogQueriesModel = app($this->model_path.'\LogQueries');
        $this->LogQueriesModel = $LogQueriesModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_log_queries_datatable($whereRaw, $order, $offset, $limit)
    {
        $log_queries = $this->LogQueriesModel::select();
        if ($whereRaw!='')
            $log_queries = $log_queries->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'log_queries.'.$order['coloumn'];
        $log_queries = $log_queries->orderBy($order_column, $order['dir'])
                    ->orderBy('log_queries._id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($log_queries as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($log_queries);
    }

    function get_log_queries_total($whereRaw)
    {
        $log_queries = $this->LogQueriesModel::select();
        if ($whereRaw!='')
            $log_queries = $log_queries->whereRaw($whereRaw);
        $log_queries = $log_queries->count();
        return $this->_resJson($log_queries);
    }

    function get_log_queries_by_id($id)
    {
        $data = $this->LogQueriesModel::where('_id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_log_queries_last()
	{
		$data = $this->LogQueriesModel::orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}
}
