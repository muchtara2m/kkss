<?php

namespace App\Http\Controllers\web\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\TanggapDaruratQueries;

class TanggapDaruratController extends \App\Http\Core\WebController 
{
    // Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'tanggapdarurat';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'tanggapdarurat';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, TanggapDaruratQueries $queries_tanggap_darurat)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_tanggap_darurat = $queries_tanggap_darurat;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Tanggap Darurat';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_tanggap_darurat->get_tanggap_darurat_last();
        $data['last'] = $last;

        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('tanggap_darurat', $_GET);
        // Get Data
        $data = $this->queries_tanggap_darurat->get_tanggap_darurat_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        foreach ($data as $r) {
            $r->image_url = $r->image?\ImageUrl::url($this->sView.'/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url($this->sView.'/thumbs/', $r->image):null;
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_tanggap_darurat->get_tanggap_darurat_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_tanggap_darurat->get_tanggap_darurat_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Tanggap Darurat';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'form';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'form';

        /* Collection variable data */
        // Get Data Tanggap Darurat
        $tanggap_darurat = null;
        if ($str_id) {
            $tanggap_darurat = $this->queries_tanggap_darurat->get_tanggap_darurat_by_id($str_id);
            $tanggap_darurat->image_id = $tanggap_darurat->image?$tanggap_darurat->image->id:null;
            $tanggap_darurat->image_url = $tanggap_darurat->image?\ImageUrl::url($this->sView.'/images/', $tanggap_darurat->image):null;
            $tanggap_darurat->thumb_url = $tanggap_darurat->image?\ImageUrl::url($this->sView.'/thumbs/', $tanggap_darurat->image):null;
        }
        $data['tanggap_darurat'] = $tanggap_darurat;
        // Get Tanggap Darurat Category
        $tanggap_darurat_category = $this->queries_tanggap_darurat->get_tanggap_darurat_category();
        $data['tanggap_darurat_category'] = $tanggap_darurat_category;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'name' => 'required',
            'tanggap_darurat_category' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'display' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');
        $seo = strtolower(str_replace(' ', '-', $name));
        $tanggap_darurat_category_id = $request->input('tanggap_darurat_category');
        $short_description = $request->input('short_description');
        $description = $request->input('description');
        $display = $request->input('display');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = Crypt::decryptString($session_user);
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['tanggap_darurat_category_id'] = $tanggap_darurat_category_id;
        $post['seo'] = $seo;
        $post['short_description'] = $short_description;
        $post['description'] = $description;
        $post['display'] = ($display=='true')?true:false;
        $post['created_by'] = $user_id;
        $_post = false; // for upload file
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_tanggap_darurat->insert_tanggap_darurat($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
                $_tanggap_darurat = $this->queries_tanggap_darurat->get_tanggap_darurat_last();
                $_param_id = $_tanggap_darurat->id;
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_tanggap_darurat->update_tanggap_darurat($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
                $_param_id = $id;
            }
        }

        // Proccess Upload
        if ($_post and $file) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'tanggap_darurat'; // Set Description for DB Image
            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
            $path = $this->sView; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload)
                return \ErrorWeb::notfound();
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        // Delete
        // $_post = $this->queries_tanggap_darurat->destroy_tanggap_darurat($id);
        // Soft Delete
        $_post = $this->queries_tanggap_darurat->delete_tanggap_darurat($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }
}
