$(function () {
    // Datatable
    let table
    let thead = `
        <tr>
            <th></th>
            <th style="text-align: left;">NO</th>
            <th style="padding-left: 10px;">NAME</th>
            <th style="padding-left: 10px;">EMAIL</th>
            <th style="padding-left: 10px;">PHONE</th>
            <th style="padding-left: 10px;">LEVEL</th>
            <th style="padding-left: 10px;">URL</th>
            <th style="padding-left: 10px;">STATUS</th>
            <th style="padding-left: 10px;">CREATED DATE</th>
            <th style="padding-left: 10px;">UPDATED DATE</th>
        </tr>
    `
    $('#example1').append($('<thead/>').append(thead))
    let tfoot = `
        <tr>
            <th search="false"></th>
            <th search="false"></th>
            <th search="true">NAME</th>
            <th search="true">EMAIL</th>
            <th search="true">PHONE</th>
            <th search="true">LEVEL</th>
            <th search="true">URL</th>
            <th search="true">STATUS</th>
            <th search="true">CREATED DATE</th>
            <th search="true">UPDATED DATE</th>
        </tr>
    ` 
    $('#example1').append($('<tfoot/>').append(tfoot))
    // Setup - add a text input to each footer cell
    $('#example1 tfoot th').each( function () {
        let search = $(this).attr('search')
        if (search == 'true') {
            let title = $(this).text()
            // let title = ''
            $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: 110%; margin-left: -10px;" />' )
        }
    })
    // Define Url get datatable
    let _datatable_url = $('#example1').attr('url')
    // Process render data
    table = $('#example1').DataTable({
        processing: true,
        serverSide: true,
        ajax: _datatable_url,
        type: 'GET',
        // dataType: "json",
        aaSorting: [[1, 'asc']],
        aaColumnDefs: [
            { 'bSortable': false, 'aTargets': [ 0 ] },
            // { 'bSearchable': false }
        ],
        columns: [
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <button 
                            type="button" 
                            title="Edit"
                            onclick="_form('`+_datatable_url.replace('/get/datatable', '')+'/form/'+data.str_id+`')" 
                            class="btn btn-sm btn-warning"
                        >
                            <i class="fa fa-edit"></i> Action
                        </button>
                    `
                }

            },
            {
                data: 'no',
                class: 'text-right',
                width: '5%',
                'bSortable': true,
                'bSearchable': false
            },
            {
                data: 'name', 
                name: 'name',
                width: '10%'
            },
            {
                data: 'email', 
                name: 'email',
                width: '10%'
            },
            {
                data: 'phone', 
                name: 'phone',
                width: '10%'
            },
            {
                data: 'level', 
                name: 'level',
                width: '10%'
            },
            {
                data: 'url', 
                name: 'url',
                width: '12.5%'
            },
            {
                data: 'status', 
                name: 'status',
                width: '7.5%'
            },
            {
                data: 'created_date', 
                name: 'created_at',
                width: '12.5%'
            },
            {
                data: 'updated_date', 
                name: 'updated_at',
                width: '12.5%'
            },
        ],
        // 'sDom': 'Bfrtpil',
        // dom: 'Bfrtip',
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    })
    // Apply the search
    table.columns().every( function () {
        let that = this
        $('input', this.footer()).on('keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw()
            }
        })
    })
})

// Modal Form
function _form(url) {
    window.location.href=url
}