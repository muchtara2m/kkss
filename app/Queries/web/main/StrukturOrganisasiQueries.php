<?php

namespace App\Queries\web\main;

class StrukturOrganisasiQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $StrukturOrganisasiModel = app($this->model_path.'StrukturOrganisasi');
        $this->StrukturOrganisasiModel = $StrukturOrganisasiModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_struktur_organisasi_by_id($id)
    {
        $data = $this->StrukturOrganisasiModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'struktur_organisasi')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }
}