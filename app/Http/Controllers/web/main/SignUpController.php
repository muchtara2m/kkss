<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\SignUpQueries;

class SignUpController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'signup';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'signup';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, SignUpQueries $queries_signup)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_signup = $queries_signup;
	}

    public function index()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Sign Up';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'index';
		$data['vAdditional']['js'] = 'index';

		/* Collection variable data */
		//
		
		// Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'name' => 'required|max:255',
            'phone' => 'required',
            'address' => 'required',
            'village' => 'required',
            'profession' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',
            'confirm_password' => 'required|max:255',
            'agreement' => 'required|max:255',
        ]);

        // Define Variable Request
        $name = $request->input('name');
        $phone = $request->input('phone');
        $address = $request->input('address');
        $village_id = $request->input('village');
        $profession_id = $request->input('profession');
        $email = $request->input('email');
        $password = $request->input('password');
        $confirm_password = $request->input('confirm_password');
        $file = $request->file('file');

        // Process Query
        try {
        	// Check Confirm Password
			if ($password != $confirm_password) {
				Session::flash('post', 'Error');
	        	Session::flash('message', 'Password Confirm Not Match!');
				return $this->_redirect('signup');
			}
        	// Check Email
        	$check_email = $this->queries_signup->get_user_by_email($email);
        	if ($check_email) {
        		Session::flash('post', 'Error');
	        	Session::flash('message', 'Email Already Exist!');
        		return $this->_redirect('signup');
        	}

        	// Transaction Query
        	\DB::beginTransaction();

        	// Insert User
	        $post_user = [];
	        $post_user['name'] = $name;
	        $post_user['phone'] = $phone;
	        $post_user['email'] = $email;
	        $post_user['password'] = Hash::make($password);
	        $post_user['level'] = 'member';
	        $post_user['type'] = 'Nasional';
	        $this->queries_signup->insert_user($post_user);
	        // Get User by Email
        	$user = $this->queries_signup->get_user_by_email($email);
        	// Insert User Profile
	        $post_user_profile = [];
	        $post_user_profile['user_id'] = $user->id;
	        $post_user_profile['address'] = $address;
	        $post_user_profile['village_id'] = $village_id;
	        $post_user_profile['profession_id'] = $profession_id;
	        $this->queries_signup->insert_user_profile($post_user_profile);
	        // Insert User Verified
	        $post_user_verified = [];
        	$post_user_verified['user_id'] = $user->id;
        	$verified_code = $this->_random_string(8);
        	$get_user_verified_by_verified_code = $this->queries_signup->get_user_verified_by_verified_code($verified_code);
        	while ($get_user_verified_by_verified_code) {
        		$verified_code = $this->_random_string(8);
        	}
        	$post_user_verified['verified_code'] = $verified_code;
        	$post_user_verified['is_verified'] = 1;
        	$post_user_verified['status'] = 'Inactive';
        	$this->queries_signup->insert_user_verified($post_user_verified);

        	// Proccess Upload
	        if ($file) {
	            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
	            $description = 'member'; // Set Description for DB Image
	            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
	            $path = 'member'; // Set Path File Image
	            // Proccess Upload
	            $upload = $this->upload_single_file_image($user->id, $name, $description, $file, $path, $image_id);
	            if (!$upload) {
	            	Session::flash('post', 'Error');
	        		Session::flash('message', 'Failed Upload Image!');
        			return $this->_redirect('signup');
	            }
	        }

	        \DB::commit();

	        // Set Session Flash
	        Session::flash('post', 'Success');
	        Session::flash('message', 'Data was successfully created!.');

	        // Redirect to Function
	        return $this->_redirect('signup/success');
	    } catch (\Exception $e) {
	    	\DB::rollback();

	    	// Set Session Flash
	        Session::flash('post', 'Error');
	        Session::flash('message', 'Something wrong database server :( please try again.');

	        // Redirect to Function
	        return $this->_redirect('signup');
    	}
    }

    private function _random_string($length = 8) 
    {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $string = '';
	    for ($i = 0; $i < $length; $i++)
	        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
	    return $string;
	}

	public function success()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Success Registration';
		// Filename in folder view
		$data['cView'] = 'success';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		//
		
		// Render to View
        return $this->_render($data);
    }
}
