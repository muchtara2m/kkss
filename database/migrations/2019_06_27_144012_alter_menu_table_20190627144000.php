<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMenuTable20190627144000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO menu (id, name, link, parent_id, favicon, position, for_role) VALUES
            ('38', 'Content Web', '#', NULL, NULL, 7, 'admin'),
            ('39', 'Login Page', 'contentweb/loginpage', 38, NULL, 1, 'admin');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
