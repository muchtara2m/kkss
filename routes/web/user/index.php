<?php

$router->group([
    'prefix'     => 'user'
], function () use ($router) {
	$router->get('', 'web\error\Error404Controller@index');
	$router->get('/{param}', 'web\user\LoginController@index')->name('user');
	$router->get('/forgotpassword/{param}', 'web\user\ForgotpasswordController@index')->name('user/forgotpassword');
	$router->post('/forgotpassword/changepassword', 'web\user\ForgotpasswordController@changepassword')->name('user/forgotpassword/changepassword');
	$router->get('/expired/{param}', 'web\user\ExpiredController@index')->name('user/expired');
	$router->post('/expired/login', 'web\user\ExpiredController@login')->name('user/expired/login');

});
$router->group([
    'prefix'     => ''
], function () use ($router) {
	$router->get('/get/captcha', 'web\user\LoginController@get_captcha')->name('get/captcha');
	$router->post('/login', 'web\user\LoginController@post')->name('login');
	$router->get('/login', 'web\error\Error404Controller@index')->name('login');
	$router->get('/logout', 'web\user\LogoutController@post')->name('logout');
});