$(function() {
    $('.input-class').keyup(function(){
        let value = $(this).val()
        let content = $(this).attr('content')
        if (value != '')
            $('#btn-'+content).attr('disabled', false)
        else
            $('#btn-'+content).attr('disabled', true)
    })
    $('.btn-class').click(function(){
        let content = $(this).attr('content')
        let str_id = $('#str_id-'+content).val()
        let name = $('#input-name-'+content).val()
        let email = $('#input-email-'+content).val()
        let phone = $('#input-phone-'+content).val()
        let username = $('#input-username-'+content).val()
        let password = $('#input-password-'+content).val()
        let url = $('#input-url-'+content).val()
        let urlPost = $('#url-'+content).val()
        let csrf_token_name = $('#csrf_token').attr('name')
        let csrf_token_value = $('#csrf_token').val()
        $.ajax({
            type: 'POST',
            url: urlPost,
            data: {
                str_id: str_id,
                name: name,
                email: email,
                phone: phone,
                username: username,
                password: password,
                url: url,
                '_token': csrf_token_value,
                post_form: 'ajax'
            },
            beforeSend: function(){
                $('#spin-'+content).css('display', '')
                $('#btn-'+content).attr('disabled', true)
            },
            success: function(data){
                data = JSON.parse(data)
                if (data.ajax == 'Success') {
                    $('#csrf_token').val(data.csrf_token)
                    $('#success_message').text(data.message)
                    $('#success').modal('show')
                    setTimeout(function(){
                        $('#success').modal('hide')
                    }, 1000)
                } else {
                    $('#csrf_token').val(data.csrf_token)
                    $('#error_message').text(data.message)
                    $('#error').modal('show')
                    setTimeout(function(){
                        $('#error').modal('hide')
                    }, 2000)
                }
            },
            error: function(data) { 
                $('#spin-'+content).css('display', 'none')
                $('#btn-'+content).attr('disabled', true)
                $('#error').modal('show')
                setTimeout(function(){
                    $('#error').modal('hide')
                }, 2000)
            },
            complete: function() {
                $('#spin-'+content).css('display', 'none')
                $('#btn-'+content).attr('disabled', true)
                $('#input-password-'+content).val('')
            }
        })
    })
})
function isNumberKey(evt){
    let charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false
    return true
}