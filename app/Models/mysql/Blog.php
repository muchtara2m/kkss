<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $connection = 'mysql';

    protected $table = 'blog';
    
	protected $fillable = [
		'category_id',
		'title',
		'description',
		'display'
	];
}
