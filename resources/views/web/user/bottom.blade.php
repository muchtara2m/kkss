<!-- jQuery 2.1.3 -->
<script src="{{ asset('assets/admin/plugins/jQuery/jQuery-2.1.3.min.js') }}" type="text/javascript"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('assets/admin/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ asset('assets/admin/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<!-- Google ReCaptcha v3 -->
@if(env('GOOGLE_RECAPTCHA')&&isset($g_recaptcha))
<script type="text/javascript">
	if(!navigator.onLine) {
		alert("You're Offline :(")
		window.location.href=location.origin+'/error'
	}
	grecaptcha.ready(function() {
		grecaptcha.execute("{{ env('GOOGLE_RECAPTCHA_SITE_KEY') }}", {action: 'login'})
		.then(function(token) {
			document.getElementById('g-recaptcha-response').value = token
			// Verify the token on the server.
		})
	})
</script>
@endif