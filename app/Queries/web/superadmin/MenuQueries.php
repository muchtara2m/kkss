<?php

namespace App\Queries\web\superadmin;

class MenuQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'superadmin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $MenuModel = app($this->model_path.'Menu');
        $this->MenuModel = $MenuModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_menu_datatable($whereRaw, $order, $offset, $limit)
    {
        $menu = $this->MenuModel::select(
                        'menu.*'
                    )
                    ->where('menu.status', 'Active');
        if ($whereRaw!='')
            $menu = $menu->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'menu.'.$order['coloumn'];
        $menu = $menu->orderBy($order_column, $order['dir'])
                    ->orderBy('menu.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($menu as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($menu);
    }

    function get_menu_total($whereRaw)
    {
        $menu = $this->MenuModel::select('menu.id')
                    ->where('menu.status', 'Active');
        if ($whereRaw!='')
            $menu = $menu->whereRaw($whereRaw);
        $menu = $menu->count();
        return $this->_resJson($menu);
    }

    function get_menu_by_id($id)
    {
        $data = $this->MenuModel::where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_menu_by_parent_id()
    {
        $data = $this->MenuModel::where('status', 'Active')
                ->where('link', '#')
                ->get();
        return $this->_resJson($data);
    }

	function get_menu_last()
	{
		$data = $this->MenuModel::orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_menu($post)
	{
        $MenuModel = $this->MenuModel;
        $post['id'] = $MenuModel::max('id')+1;
        $data = $MenuModel::insert($post);
		return $this->_resJson($data);
	}

	function update_menu($post)
	{
		$data = $this->MenuModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}
}
