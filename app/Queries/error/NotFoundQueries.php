<?php

namespace App\Queries\error;

class NotFoundQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'error';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        // $ModelName = app($this->model_path.'Model');
        // $this->ModelName = $ModelName;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}
}
