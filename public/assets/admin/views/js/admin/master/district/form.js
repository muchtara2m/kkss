$(function () {
    $('#province_id').on('change', function() {
        let province_id = $(this).val()
        let url = $(this).attr('url')
        $('#city_id').css('pointer-events', '')
        $('#city_id').attr('readonly', false)
        $('#city_id').empty()
        $.ajax({
            type: 'GET',
            url: url+'/'+province_id,
            data: null,
            beforeSend: function(){
            },
            success: function(response){
                $('#city_id').append('<option value="">Select City</option>')
                for (let i in response) {
                    $('#city_id').append('<option value="'+response[i].id+'">'+response[i].name+'</option>')
                }
            },
            error: function(response) {
            },
            complete: function() {
            }
        })
    })
})