<?php

namespace App\Queries\web\root;

class AccountQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'root';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

	function get_user_by_id($id)
	{
		$data = $this->UserModel::where('status', 'Active')
        		->where('id', $id)
        		->get()
        		->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
	}

    function update_user($post)
    {
        $data = $this->UserModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }
}
