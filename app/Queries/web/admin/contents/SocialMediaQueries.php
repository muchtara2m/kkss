<?php

namespace App\Queries\web\admin\contents;

class SocialMediaQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $ContentsModel = app($this->model_path.'Contents');
        $this->ContentsModel = $ContentsModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

	public function get_social_media()
    {
        $data = $this->ContentsModel::where('description', 'social_media')->get();
        return $this->_resJson($data);
    }

    function update_contents($post)
    {
        $data = $this->ContentsModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }
}
