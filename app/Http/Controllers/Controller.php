<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // start:: For LOG ACTIVITY
    function set_log_activity($response, $session_key=null)
    {
        // Proccess to Mongo
        $_set = $this->queries_controller->_set_log_activity($response, $session_key);
        return $_set;
    }
    // end:: For LOG ACTIVITY

    // start:: For UPLOAD FILE
	function upload_single_file_image($param_id, $name, $description, $file, $path, $image_id=0)
	{
        // Define Info File
        $ext = $file->getClientOriginalExtension();
        $size = $file->getClientSize();
        $resolution = getimagesize($file);
        
    	// Create Thumbnail Image
    	$thumbnail = Image::make($file->getRealPath())->resize($resolution[0]/5, $resolution[1]/5);

    	// Upload File to Storage
    	$storage = null;
        if (env('STORAGE_PUBLIC')) {
            $isFolderExists = $this->check_existing_folder_storage('app/public/'.$path.'/images');
            if ($isFolderExists)
	           $storage = $file->store('public/'.$path.'/images'); // Upload to Storage
            if ($storage)
	           $storage = str_replace('public/'.$path.'/images/', '', $storage); // Set File Image include Extention
            $isFolderExists = $this->check_existing_folder_storage('app/public/'.$path.'/thumbs');
            if ($isFolderExists)
	           $thumbs_storage = $thumbnail->save(storage_path('app/public/'.$path.'/thumbs/'.$storage)); // Upload Thumbnails to Storage
	    }

    	// Upload File to Public
    	if (env('UPLOAD_PUBLIC')) {
		    $filename = $name.'.'.$ext; // Set File Image include Extention
            $isFolderExists = $this->check_existing_folder_public('uploads/'.$path.'/images');
            if ($isFolderExists)
                $uploads = $file->move(public_path('uploads/'.$path.'/images/'), $filename); // Upload to Public
                // $uploads = $file->move($path.'/images', $filename); // Upload to Public
            $isFolderExists = $this->check_existing_folder_public('uploads/'.$path.'/thumbs');
            if ($isFolderExists)
                $thumbs_uploads = $thumbnail->save(public_path('uploads/'.$path.'/thumbs/'.$filename)); // Upload Thumbnails to Public
		}

	    // Set Data to Create Image DB
		$post = [];
    	$post['param_id'] = $param_id;
    	$post['name'] = $name;
    	$post['ext'] = '.'.$ext;
    	$post['size'] = $size;
    	$post['resolution'] = $resolution[0].'x'.$resolution[1];
    	$post['description'] = $description;
    	$post['storage'] = $storage;
    	// $post['original_name'] = $file->getClientOriginalName();
    	// $post['file_type'] = $file->getClientMimeType();

	  	// Peoccess to DB
    	$_post = false;
    	$image = $this->queries_controller->get_image_by_id($image_id, $description);
    	if ($image) {
    		$post['id'] = $image->id;
    		$_post = $this->queries_controller->update_image($post);
    	} else 
			$_post = $this->queries_controller->insert_image($post);
        if ($_post)
            $image = $this->queries_controller->get_image($post);
	  	return $image;
	}

    function insert_single_file_image($post)
    {
        // Peoccess to DB
        $_update = $this->queries_controller->update_image($post);
        return $_update;
    }

	function delete_single_file_image($id)
	{
    	// Peoccess to DB
		$_delete = $this->queries_controller->delete_image($id);
	  	return $_delete;
	}

	function upload_single_file_video($param_id, $name, $description, $file, $path, $video_id=0)
	{
        // Define Info File
        $ext = $file->getClientOriginalExtension();
        $size = $file->getClientSize();

        // Upload File to Storage
        $storage = null;
        if (env('STORAGE_PUBLIC')) {
            $isFolderExists = $this->check_existing_folder_storage('app/public/'.$path.'/videos');
            if ($isFolderExists)
        	   $storage = $file->store('public/'.$path.'/videos'); // Upload to Storage
            if ($storage)
        	   $storage = str_replace('public/'.$path.'/videos/', '', $storage); // Set File Video include Extention
    	}

        // Upload File to Public
        if (env('UPLOAD_PUBLIC')) {
	        $filename = $name.'.'.$ext; // Set File Video include Extention
            $isFolderExists = $this->check_existing_folder_public('uploads/'.$path.'/videos');
            if ($isFolderExists)
                $upload = $file->move(public_path('uploads/'.$path.'/videos/'), $filename); // Upload to Public
                // $upload = $file->move($path.'/videos', $filename); // Upload to Public
		}

		// Set Data to Create Video DB
		$post = [];
    	$post['param_id'] = $param_id;
    	$post['name'] = $name;
    	$post['ext'] = '.'.$ext;
    	$post['size'] = $size;
    	$post['resolution'] = '';
    	$post['description'] = $description;
    	$post['storage'] = $storage;
    	// $post['original_name'] = $file->getClientOriginalName();
    	// $post['file_type'] = $file->getClientMimeType();

    	// Peoccess to DB
    	$_post = false;
    	$video = $this->queries_controller->get_video_by_id($video_id, $description);
    	if ($video) {
    		$post['id'] = $video->id;
    		$_post = $this->queries_controller->update_video($post);
    	} else 
			$_post = $this->queries_controller->insert_video($post);
	  	$video = $this->queries_controller->get_video($post);
        return $video;
	}

    function insert_single_file_video($post)
    {
        // Peoccess to DB
        $_update = $this->queries_controller->update_video($post);
        return $_update;
    }

	function delete_single_file_video($id)
	{
    	// Peoccess to DB
		$_delete = $this->queries_controller->delete_video($id);
	  	return $_delete;
	}

    function check_existing_folder_storage($path)
    {
        $file_exist = File::exists(storage_path($path));
        if (!$file_exist) {
            $folderPath = '';
            $exp = explode('/', $path);
            foreach ($exp as $k => $v) {
                $folderPath .= $v.'/';
                if (!File::exists(storage_path($folderPath))) {
                    File::makeDirectory(
                        storage_path($folderPath), // Path
                        0755, // Permission
                        true, // Recursive
                        false // Force
                    );
                } 
                // else
                    // chmod(public_path($folderPath), 0755);
            }
        }
        return true;
    }

    function check_existing_folder_public($path)
    {
        $file_exist = File::exists(public_path($path));
        if (!$file_exist) {
            $folderPath = '';
            $exp = explode('/', $path);
            foreach ($exp as $k => $v) {
                $folderPath .= $v.'/';
                if (!File::exists(public_path($folderPath))) {
                    File::makeDirectory(
                        public_path($folderPath), // Path
                        0755, // Permission
                        true, // Recursive
                        false // Force
                    );
                } 
                // else
                    // chmod(public_path($folderPath), 0755);
            }
        }
        return true;
    }
    // end:: For UPLOAD FILE

    // start:: For API
    function convert_memory_usage($size)
    {
        $unit=array('Byte','KB','MB','GB','TB','PB');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }

    function convert_elapsed_time($time)
    {
        return number_format($time, 3).' seconds';
    }
    // end:: For API

    // start:: For SEND EMAIL
    function send_mail_to($data_send_mail, $mail_template=null, $to, $cc=[], $bcc=[])
    {
        try {
            // Send Mail Process
            // Mail::to($to)
            //         ->cc($cc)
            //         ->bcc($bcc)
            //         ->send($mail_template)
            //         ;
            Mail::send(
                'mail.'.$mail_template, 
                $data_send_mail, 
                function($message) use ($data_send_mail, $to) {
                    $message->to($to, $data_send_mail['name_to'])
                            ->subject($data_send_mail['subject'])
                            ->from($data_send_mail['sender'], $data_send_mail['name_sender']);
                            // ->setBody($data_send_mail['content'], 'text/html');
                    // if (isset($v['file'])) {
                    //     $message->attach(Request::file('file'), [
                    //         'as' => Request::file('file')->getClientOriginalName(), 
                    //         'mime' => Request::file('file')->getClientOriginalExtension()
                    //     ]);
                    // }
                }
            );
            $data_send_mail['notes'] = null;
            $data_send_mail['status'] = 'Success';

            // Check Mail Response
            if (count(Mail::failures()) > 0) {
                $data_send_mail['notes'] = json_encode(Mail::failures());
                $data_send_mail['status'] = 'Failed';
            }
        } catch (\Exception $e) {
            $data_send_mail['notes'] = json_encode($e->getMessage());
            $data_send_mail['status'] = 'Failed';
        }

        // Update Send Mail DB Table
        $this->update_send_mail($data_send_mail);
        return true;
    }

    function update_send_mail($data_send_mail)
    {
        $post_send_mail = [];
        $post_send_mail['id'] = $data_send_mail['send_email_id'];
        $post_send_mail['notes'] = $data_send_mail['notes'];
        $post_send_mail['status'] = $data_send_mail['status'];
        $this->queries_controller->update_send_mail($post_send_mail);
        return true;
    }
    // end:: For SEND EMAIL

    function set_queue_send_email($jobClass, $onQueue)
    {
        // Define Jobs (Register Verified)
        $job = ($jobClass)
                ->onQueue($onQueue)
                ;
        // Send Jobs
        dispatch($job);
        // Call Comand Artisan
        $this->call_command_queue_send_mail($onQueue);
        return true;
    }

    function call_command_queue_send_mail($onQueue)
    {
        // Call Comand Artisan
        Artisan::call('queue:work', 
            [
                '--once' => true, 
                // '--daemon' => true, 
                '--queue' => $onQueue, 
                // '--tries' => '1'
            ]
        );
        return true;
    }
}