<?php

namespace App\Queries\web\root;

class UserQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'root';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_user_datatable($whereRaw, $order, $offset, $limit)
    {
        $user = $this->UserModel::select(
                        'user.*'
                    )
                    ->where('user.id', '<>', '1');
        if ($whereRaw!='')
            $user = $user->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'user.'.$order['coloumn'];
        $user = $user->orderBy($order_column, $order['dir'])
                    ->orderBy('user.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($user as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($user);
    }

    function get_user_total($whereRaw)
    {
        $user = $this->UserModel::select('user.id')
                    ->where('user.id', '<>', '1');
        if ($whereRaw!='')
            $user = $user->whereRaw($whereRaw);
        $user = $user->count();
        return $this->_resJson($user);
    }

    function get_user_by_id($id)
    {
        $data = $this->UserModel::where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_user_last()
	{
		$data = $this->UserModel::orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_user($post)
	{
		$data = $this->UserModel::insert($post);
		return $this->_resJson($data);
	}

	function update_user($post)
	{
		$data = $this->UserModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

    function inactive_user($id)
    {
        $data = $this->UserModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_user($id)
    {
        $data = $this->UserModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
