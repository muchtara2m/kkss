<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Queries\ControllerQueries;
use Closure;
use Redirect;

class RoleAdmin
{
    public function handle($request, Closure $next)
    {
        // Define Session
        $session = 'admin';

        // Define Library Extends
        $queries_controller = new ControllerQueries();

        // Check Authentication User
        $_auth = $queries_controller->check_auth(env('SESSION').$session, env('COOKIE').$session);
        if (!$_auth) {
            $session_user = Session::get(env('SESSION').$session);
            if ($session_user) {
                $user_id = Crypt::decryptString($session_user);
                $user = $queries_controller->get_user_by_id($user_id);
                return redirect('user/expired/'.$user->url);
            } else
                return \ErrorWeb::notfound();
        }

        return $next($request);
    
    }
}