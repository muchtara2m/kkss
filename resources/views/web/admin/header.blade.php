<header class="main-header">
    <a href="<?php // echo site_url(); ?>" target="_blank" class="logo"><b>ADMIN</b> PAGE</a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route($pView.'/account') }}">
                        <i class="fa fa-user"></i>
                        <span> Account</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}">
                        <i class="fa fa-sign-out"></i>
                        <span> Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>