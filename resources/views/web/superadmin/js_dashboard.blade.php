<link href="{{ asset('assets/'.$pView.'/css/datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('assets/'.$pView.'/css/bootstrap-table.css') }}" rel="stylesheet">
<script src="{{ asset('assets/'.$pView.'/js/chart.min.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/js/chart-data.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/js/easypiechart.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/js/easypiechart-data.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/js/bootstrap-table.js') }}"></script>
<script>
	$('#calendar').datepicker()
</script>