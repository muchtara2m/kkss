<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class EventsComments extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'events_comments';

	protected $fillable = [
		'events_id',
		'parent_id',
		'user_id',
		'name',
		'email',
		'phone',
		'message'
	];
}
