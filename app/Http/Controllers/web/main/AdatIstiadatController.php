<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\AdatIstiadatQueries;

class AdatIstiadatController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'adatistiadat';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'adatistiadat';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, AdatIstiadatQueries $queries_adat_istiadat)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_adat_istiadat = $queries_adat_istiadat;
	}

    public function index(Request $request)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Adat Istiadat';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'index';
		$data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

		/* Collection variable data */
		// Get Adat Istiadat
		$adat_istiadat = $this->queries_adat_istiadat->get_adat_istiadat($keyword);
        foreach ($adat_istiadat as $r) {
        	$r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('home/adatistiadat/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('home/adatistiadat/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
        }
        $data['adat_istiadat'] = $adat_istiadat;
		
		// Render to View
        return $this->_render($data);
    }

    public function single($seo)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Not set';
		// Filename in folder view
		$data['cView'] = 'single';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'single';
		$data['vAdditional']['js'] = 'single';

		/* Collection variable data */
		// Get Adat Istiadat
		$adat_istiadat = $this->queries_adat_istiadat->get_adat_istiadat_by_seo($seo);
        if ($adat_istiadat) {
			$data['vTitle'] = $adat_istiadat->title.' | Adat Istiadat';
        	$adat_istiadat->created_date = date('d M Y, (H:i)', strtotime($adat_istiadat->updated_at));
            $adat_istiadat->image_url = $adat_istiadat->image?\ImageUrl::url('home/adatistiadat/images/', $adat_istiadat->image):null;
            $adat_istiadat->thumb_url = $adat_istiadat->image?\ImageUrl::url('home/adatistiadat/thumbs/', $adat_istiadat->image):null;
            $adat_istiadat->user_name = $adat_istiadat->user?$adat_istiadat->user->name:null;
        }
        $data['adat_istiadat'] = $adat_istiadat;
        // Get Adat Istiadat Comments
        $count_comments = 0;
        $adat_istiadat_comments = [];
        if ($adat_istiadat) {
        	$adat_istiadat_comments = $this->queries_adat_istiadat->get_adat_istiadat_comments_by_adat_istiadat_id($adat_istiadat->id);
        	foreach ($adat_istiadat_comments as $r) {
                $r->sub_comments = $this->queries_adat_istiadat->get_adat_istiadat_comments_by_adat_istiadat_id($adat_istiadat->id, $r->id);
                $count_comments += count($r->sub_comments);
        	}
            $count_comments += count($adat_istiadat_comments);
    	}
        $data['comments'] = $adat_istiadat_comments;
        $data['count_comments'] = $count_comments;
		
		// Render to View
        return $this->_render($data);
    }

    public function comments(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'seo' => 'required',
            'adat_istiadat_id' => 'required',
            'message' => 'required',
        ]);

        // Define Variable Request
        $seo = $request->input('seo');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        $adat_istiadat_id = $request->input('adat_istiadat_id');
        $adat_istiadat_comments_id = $request->input('adat_istiadat_comments_id');
        $message = $request->input('message');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['email'] = $email;
        $post['user_id'] = $user_id;
        $post['adat_istiadat_id'] = $adat_istiadat_id;
        $post['parent_id'] = $adat_istiadat_comments_id;
        $post['message'] = $message;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Insert
        $_post = $this->queries_adat_istiadat->insert_adat_istiadat_comments($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully created!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->sView.'/'.$seo);
    }
}
