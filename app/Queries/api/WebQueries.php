<?php

namespace App\Queries\api;

class WebQueries extends \App\Queries\Queries
{	
	protected $model_path;

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    public function get_country()
    {
        // Define Model Country
        $CountryModel = app($this->model_path.'Country');
        // Get Country
        $country = $CountryModel::select(
                        'id',
                        'name'
                    )
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($country);
    }

    public function get_province_by_country_id($country_id)
    {
        // Define Model Province
        $ProvinceModel = app($this->model_path.'Province');
        // Get Province
        $province = $ProvinceModel::select(
                        'id',
                        'name'
                    )
                    ->where('country_id', $country_id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($province);
    }

    public function get_city_by_province_id($province_id)
    {
        // Define Model City
        $CityModel = app($this->model_path.'City');
        // Get City
        $city = $CityModel::select(
                        'id',
                        'name'
                    )
                    ->where('province_id', $province_id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($city);
    }

    public function get_district_by_city_id($city_id)
    {
        // Define Model District
        $DistrictModel = app($this->model_path.'District');
        // Get District
        $district = $DistrictModel::select(
                        'id',
                        'name'
                    )
                    ->where('city_id', $city_id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($district);
    }

    public function get_Village_by_district_id($district_id)
    {
        // Define Model Village
        $VillageModel = app($this->model_path.'Village');
        // Get Village
        $village = $VillageModel::select(
                        'id',
                        'name'
                    )
                    ->where('district_id', $district_id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($village);
    }

    public function get_profession()
    {
        // Define Model Profession
        $ProfessionModel = app($this->model_path.'Profession');
        // Get Profession
        $profession = $ProfessionModel::select(
                        'id',
                        'name'
                    )
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($profession);
    }

    public function get_events()
    {
        // Define Model Events
        $EventsModel = app($this->model_path.'Events');
        // Get Events
        $events = $EventsModel::select(
                        'id',
                        'name',
                        'seo'
                    )
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($events);
    }

    public function get_peluang_bisnis()
    {
        // Define Model Peluang Bisnis
        $PeluangBisnisModel = app($this->model_path.'PeluangBisnis');
        // Get Peluang Bisnis
        $peluang_bisnis = $PeluangBisnisModel::select(
                        'id',
                        'name',
                        'seo'
                    )
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($peluang_bisnis);
    }

    public function get_tanggap_darurat()
    {
        // Define Model Tanggap Darurat
        $TanggapDaruratModel = app($this->model_path.'TanggapDarurat');
        // Get Tanggap Darurat
        $tanggap_darurat = $TanggapDaruratModel::select(
                        'id',
                        'name',
                        'seo'
                    )
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($tanggap_darurat);
    }
}