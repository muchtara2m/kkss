<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class PeluangBisnisComments extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'peluang_bisnis_comments';

	protected $fillable = [
		'peluang_bisnis_id',
		'parent_id',
		'user_id',
		'name',
		'email',
		'phone',
		'message'
	];
}
