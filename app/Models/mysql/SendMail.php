<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class SendMail extends Model
{
    protected $connection = 'mysql';
    
    protected $table = 'send_mail';
    
    protected $fillable = [
        'param_id',
        'user_id',
        'module',
        'to',
        'name_to',
        'sender',
        'name_sender',
        'subject',
        'content',
        'view',
        'notes',
        'status'
    ];
}
