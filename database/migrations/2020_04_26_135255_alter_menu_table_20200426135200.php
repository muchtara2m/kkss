<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMenuTable20200426135200 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            TRUNCATE TABLE menu;
        ");
        DB::statement("
            INSERT INTO menu(id, name, link, parent_id, favicon, position, for_role, created_at, updated_at, status) 
            VALUES 
            ('1', 'Dashboard', 'dashboard', NULL, NULL, '1', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('2', 'Contents', '#', NULL, NULL, '2', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('3', 'Logo', 'contents/logo', '2', NULL, '1', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('4', 'Favicon', 'contents/favicon', '2', NULL, '2', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('5', 'Title', 'contents/title', '2', NULL, '3', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('6', 'Menu', 'menu', NULL, NULL, '3', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('7', 'Header', 'header', NULL, NULL, '4', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('8', 'User', 'user', NULL, NULL, '4', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('9', 'Log', '#', NULL, NULL, '5', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('10', 'Activity', 'log/activity', '9', NULL, '1', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('11', 'Queries', 'log/queries', '9', NULL, '2', 'root', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('12', 'Dashboard', 'dashboard', NULL, NULL, '1', 'superadmin', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('13', 'Contents', '#', NULL, NULL, '2', 'superadmin', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('14', 'Logo', 'contents/logo', '13', NULL, '1', 'superadmin', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('15', 'Favicon', 'contents/favicon', '13', NULL, '2', 'superadmin', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('16', 'Title', 'contents/title', '13', NULL, '3', 'superadmin', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('17', 'Menu', 'menu', NULL, NULL, '3', 'superadmin', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('18', 'Header', 'header', NULL, NULL, '4', 'superadmin', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('19', 'User', 'user', NULL, NULL, '4', 'superadmin', '2020-02-06 14:40:15', '2020-02-06 14:40:15', 'Active' ),
            ('20', 'Dashboard', 'dashboard', NULL, NULL, '1', 'admin', '2020-02-06 14:40:16', '2020-02-06 14:40:16', 'Active' ),
            ('21', 'Contents', '#', NULL, NULL, '2', 'admin', '2020-02-06 14:40:16', '2020-02-06 14:40:16', 'Active' ),
            ('22', 'Logo', 'contents/logo', '21', NULL, '1', 'admin', '2020-02-06 14:40:16', '2020-02-06 14:40:16', 'Active' ),
            ('23', 'Favicon', 'contents/favicon', '21', NULL, '2', 'admin', '2020-02-06 14:40:16', '2020-02-06 14:40:16', 'Active' ),
            ('24', 'Title', 'contents/title', '21', NULL, '3', 'admin', '2020-02-06 14:40:16', '2020-02-06 14:40:16', 'Active' ),
            ('25', 'MAIN MENU', 'header', NULL, NULL, '4', 'admin', '2020-02-06 14:40:16', '2020-02-06 14:40:16', 'Active' ),
            ('26', 'Home', '#', NULL, NULL, '5', 'admin', '2020-02-06 14:40:16', '2020-02-06 14:40:16', 'Active' ),
            ('27', 'Blog', '#', NULL, NULL, '5', 'admin', '2020-02-06 14:40:16', '2020-02-09 09:01:17', 'Inactive' ),
            ('28', 'Category', 'blog/category', '27', NULL, '1', 'admin', '2020-02-06 14:40:16', '2020-02-09 09:01:44', 'Inactive' ),
            ('29', 'Blog', 'blog/blog', '27', NULL, '2', 'admin', '2020-02-06 14:40:16', '2020-02-09 09:01:33', 'Inactive' ),
            ('30', 'Master', '#', NULL, NULL, '3', 'admin', '2020-02-06 14:40:21', '2020-02-06 14:40:21', 'Active' ),
            ('31', 'Region Type', 'master/regiontype', '30', NULL, '1', 'admin', '2020-02-06 14:40:21', '2020-02-06 14:40:21', 'Active' ),
            ('32', 'Country', 'master/country', '30', NULL, '2', 'admin', '2020-02-06 14:40:21', '2020-02-06 14:40:21', 'Active' ),
            ('33', 'Province', 'master/province', '30', NULL, '3', 'admin', '2020-02-06 14:40:21', '2020-02-06 14:40:21', 'Active' ),
            ('34', 'City', 'master/city', '30', NULL, '4', 'admin', '2020-02-06 14:40:21', '2020-02-06 14:40:21', 'Active' ),
            ('35', 'District', 'master/district', '30', NULL, '5', 'admin', '2020-02-06 14:40:21', '2020-02-06 14:40:21', 'Active' ),
            ('36', 'Village', 'master/village', '30', NULL, '6', 'admin', '2020-02-06 14:40:21', '2020-02-06 14:40:21', 'Active' ),
            ('37', 'Postal Code', 'master/postalcode', '30', NULL, '7', 'admin', '2020-02-06 14:40:21', '2020-02-06 14:40:21', 'Active' ),
            ('38', 'Content Web', '#', NULL, NULL, '7', 'admin', '2020-02-06 14:40:21', '2020-02-09 09:03:14', 'Inactive' ),
            ('39', 'Login Page', 'contentweb/loginpage', '38', NULL, '1', 'admin', '2020-02-06 14:40:21', '2020-02-09 09:02:38', 'Inactive' ),
            ('40', 'Home', '#', NULL, NULL, '1', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('41', 'Forum', 'forum', NULL, NULL, '2', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('42', 'Events', 'events', NULL, NULL, '3', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('43', 'Membership', 'membership', NULL, NULL, '4', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('44', 'Peluang Bisnis', 'peluangbisnis', NULL, NULL, '5', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('45', 'Gallery', 'gallery', NULL, NULL, '6', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('46', 'Tanggap Darurat', 'tanggapdarurat', NULL, NULL, '7', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('47', 'International', '#', NULL, NULL, '8', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('48', 'Tokoh', 'international/tokoh', '47', NULL, '1', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('49', 'Kantor Perwakilan', 'international/representativeoffice', '47', NULL, '2', 'member', '2020-02-09 15:42:39', '2020-02-09 15:42:39', 'Active' ),
            ('50', 'Forum', 'forum', NULL, NULL, '6', 'admin', '2020-02-09 15:50:40', '2020-02-09 08:57:25', 'Active' ),
            ('51', 'Events', 'events', NULL, NULL, '7', 'admin', '2020-02-09 15:50:40', '2020-02-09 08:56:47', 'Active' ),
            ('52', 'Membership', 'membership', NULL, NULL, '8', 'admin', '2020-02-09 15:50:40', '2020-02-09 08:58:50', 'Active' ),
            ('53', 'Peluang Bisnis', 'peluangbisnis', NULL, NULL, '9', 'admin', '2020-02-09 15:50:40', '2020-02-09 08:59:06', 'Active' ),
            ('54', 'Gallery', 'gallery', NULL, NULL, '10', 'admin', '2020-02-09 15:50:40', '2020-02-09 08:59:15', 'Active' ),
            ('55', 'Tanggap Darurat', 'tanggapdarurat', NULL, NULL, '11', 'admin', '2020-02-09 15:50:40', '2020-02-09 08:59:24', 'Active' ),
            ('56', 'International', '#', NULL, NULL, '12', 'admin', '2020-02-09 15:50:40', '2020-02-09 08:59:41', 'Active' ),
            ('57', 'Tokoh', 'international/tokoh', '56', NULL, '1', 'admin', '2020-02-09 15:50:40', '2020-02-09 09:00:34', 'Active' ),
            ('58', 'Kantor Perwakilan', 'international/representativeoffice', '56', NULL, '2', 'admin', '2020-02-09 15:50:40', '2020-02-09 09:00:48', 'Active' ),
            ('59', 'Forum Category', 'master/forumcategory', '30', NULL, '9', 'admin', '2020-02-09 17:43:10', '2020-02-09 17:43:10', 'Active' ),
            ('60', 'Continent', 'master/continent', '30', NULL, '8', 'admin', '2020-02-10 02:07:34', '2020-02-10 02:07:34', 'Active' ),
            ('61', 'Domestik', '#', NULL, NULL, '9', 'member', '2020-02-10 11:44:41', '2020-02-10 11:44:41', 'Active' ),
            ('62', 'Tokoh', 'domestik/tokoh', '61', NULL, '1', 'member', '2020-02-10 11:44:41', '2020-02-10 11:44:41', 'Active' ),
            ('63', 'Kantor Perwakilan', 'domestik/homebase', '61', NULL, '2', 'member', '2020-02-10 11:44:41', '2020-02-10 11:44:41', 'Active' ),
            ('64', 'Adat Istiadat', 'adatistiadat', '40', NULL, '1', 'member', '2020-03-23 21:01:49', '2020-03-23 21:01:49', 'Active' ),
            ('65', 'Struktur Organisasi', 'strukturorganisasi', '40', NULL, '2', 'member', '2020-03-23 21:01:49', '2020-03-23 21:01:49', 'Active' ),
            ('66', 'Events Category', 'master/eventscategory', '30', NULL, '10', 'admin', '2020-04-14 17:58:45', '2020-04-14 17:58:45', 'Active' ),
            ('67', 'Domestik', '#', NULL, NULL, '13', 'admin', '2020-04-14 17:58:45', '2020-04-14 17:58:45', 'Active' ),
            ('68', 'Tokoh', 'domestik/tokoh', '67', NULL, '1', 'admin', '2020-04-14 18:03:42', '2020-04-14 18:03:42', 'Active' ),
            ('69', 'Kantor Perwakilan', 'domestik/representativeoffice', '67', NULL, '2', 'admin', '2020-04-14 18:03:42', '2020-04-14 18:03:42', 'Active' ),
            ('70', 'Background', 'home/background', '26', NULL, '1', 'admin', '2020-04-14 18:03:42', '2020-04-14 18:03:42', 'Active' ),
            ('71', 'Adat Istiadat', 'home/adatistiadat', '26', NULL, '2', 'admin', '2020-04-14 18:03:42', '2020-04-14 18:03:42', 'Active' ),
            ('72', 'Struktur Organisasi', 'home/strukturorganisasi', '26', NULL, '3', 'admin', '2020-04-14 18:03:42', '2020-04-14 18:03:42', 'Active' ),
            ('73', 'Gallery Category', 'master/gallerycategory', '30', NULL, '12', 'admin', '2020-04-19 16:58:17', '2020-04-19 16:58:17', 'Active' ),
            ('74', 'Peluang Bisnis Category', 'master/peluangbisniscategory', '30', NULL, '11', 'admin', '2020-04-19 18:15:43', '2020-04-19 18:15:43', 'Active' ),
            ('75', 'Tanggap Darurat Category', 'master/tanggapdaruratcategory', '30', NULL, '13', 'admin', '2020-04-19 20:43:33', '2020-04-19 20:43:33', 'Active' );
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
