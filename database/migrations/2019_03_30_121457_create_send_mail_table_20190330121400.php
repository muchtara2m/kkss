<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendMailTable20190330121400 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_mail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('param_id')->index()->nullable();
            $table->bigInteger('user_id')->index()->nullable();
            $table->string('module', 255);
            $table->string('to', 255);
            $table->string('name_to', 255);
            $table->string('sender', 255);
            $table->string('name_sender', 255);
            $table->string('subject', 255)->nullable();
            $table->text('content')->nullable();
            $table->string('view', 255)->nullable();
            $table->text('notes')->nullable();
            $table->boolean('is_verified')->default(false);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Pending', 'Success', 'Failed'])->default('Pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('send_mail');
    }
}
