<!-- on Loading -->
<style type="text/css">
	.no-js #loader { 
		display: none;  
	}
	.js #loader { 
		display: block; 
		position: absolute; 
		left: 100px; 
		top: 0; 
	}
	.se-pre-con {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url({{ asset('assets/admin/loader/images/rubic-loader.gif') }}) center no-repeat rgba(255, 255, 255, 0.7);
	}
</style>
<div class="se-pre-con"></div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script type="text/javascript">
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$('.se-pre-con').fadeOut('slow')
		return true
	})
	$(window).back(function() {
		// Animate loader off screen
		$('.se-pre-con').fadeOut('slow')
		return true
	})
	// On Link
	$('a').on('click', function() {
		let load = true
		let href = $(this).prop('href')
		let exc = [
			'#',
			'javascript:;',
			'drive.google',
			'history'
		]
		for (let i in exc)
			if (href.indexOf(exc[i]) >= 0)
				load = false
		let target = $(this).attr('target')
		if (target=='_blank') load = false
		if (load)
			$('.se-pre-con').fadeIn('slow')
		return true
	})
	// On Submit
	function onLoading() {
		let myForm = $('form#myForm')
	  	// If the form is invalid, submit it. The form won't actually submit;
	  	// this will just cause the browser to display the native HTML5 error messages.
	  	let isValid = myForm[0].checkValidity()
	  	if (isValid == true) {
	  		let isSubmit = myForm.find('submit').click()
	  		if (isSubmit) {
			  	$('#loading_modal').modal({
		            backdrop:'static'
		        })
		        return false
		    }
    	}
    }
</script>