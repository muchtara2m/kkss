<?php

namespace App\Queries\web\main;

class TanggapDaruratQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = '';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $TanggapDaruratModel = app($this->model_path.'TanggapDarurat');
        $this->TanggapDaruratModel = $TanggapDaruratModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_tanggap_darurat($keyword=null)
    {
        $data = $this->TanggapDaruratModel::where('status', 'Active')
                ->where('display', true);
        if ($keyword)
            $data = $data->where('name', 'like', '%'.$keyword.'%');
        $data = $data->orderBy('updated_at', 'desc')
                ->paginate(2);
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserModel = app($this->model_path.'User');
        $TanggapDaruratCategoryModel = app($this->model_path.'TanggapDaruratCategory');
        $TanggapDaruratCommentsModel = app($this->model_path.'TanggapDaruratComments');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'tanggap_darurat')
                    ->get()
                    ->first();
            $v->image = $image;
            $user = $UserModel::where('id', $v->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $tanggap_darurat_category = $TanggapDaruratCategoryModel::where('id', $v->tanggap_darurat_category_id)
                                        ->get()
                                        ->first();
            $v->category = $tanggap_darurat_category;
            $comments = $TanggapDaruratCommentsModel::where('tanggap_darurat_id', $v->id)
                        ->count();
            $v->comments = $comments;
        }
        return $this->_resJson($data);
    }

    function get_tanggap_darurat_by_tanggap_darurat_category_id($tanggap_darurat_category_id, $keyword=null)
    {
        $data = $this->TanggapDaruratModel::where('status', 'Active')
                ->where('display', true)
                ->where('tanggap_darurat_category_id', $tanggap_darurat_category_id);
        if ($keyword)
            $data = $data->where('name', 'like', '%'.$keyword.'%');
        $data = $data->orderBy('updated_at', 'desc')
                ->paginate(2);
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserModel = app($this->model_path.'User');
        $TanggapDaruratCategoryModel = app($this->model_path.'TanggapDaruratCategory');
        $TanggapDaruratCommentsModel = app($this->model_path.'TanggapDaruratComments');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'tanggap_darurat')
                    ->get()
                    ->first();
            $v->image = $image;
            $user = $UserModel::where('id', $v->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $tanggap_darurat_category = $TanggapDaruratCategoryModel::where('id', $v->tanggap_darurat_category_id)
                                        ->get()
                                        ->first();
            $v->category = $tanggap_darurat_category;
            $comments = $TanggapDaruratCommentsModel::where('tanggap_darurat_id', $v->id)
                        ->count();
            $v->comments = $comments;
        }
        return $this->_resJson($data);
    }

    function get_tanggap_darurat_by_seo($seo)
    {
        $data = $this->TanggapDaruratModel::where('status', 'Active')
                ->where('display', true)
                ->where('seo', $seo)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'tanggap_darurat')
                    ->get()
                    ->first();
            $data->image = $image;
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->created_by)
                    ->get()
                    ->first();
            $data->user = $user;
            // Get Tanggap Darurat Category
            $TanggapDaruratCategoryModel = app($this->model_path.'TanggapDaruratCategory');
            $tanggap_darurat_category = $TanggapDaruratCategoryModel::where('id', $data->tanggap_darurat_category_id)
                    ->get()
                    ->first();
            $data->category = $tanggap_darurat_category;
        }
        return $this->_resJson($data);
    }

    function get_tanggap_darurat_by_id($id)
    {
        $data = $this->TanggapDaruratModel::where('status', 'Active')
                ->where('display', true)
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'tanggap_darurat')
                    ->get()
                    ->first();
            $data->image = $image;
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->created_by)
                    ->get()
                    ->first();
            $data->user = $user;
        }
        return $this->_resJson($data);
    }

    function get_tanggap_darurat_category()
    {
        // Define Model
        $TanggapDaruratCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\TanggapDaruratCategory');
        // Get Data
        $category_blog = $TanggapDaruratCategoryModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $category_blog;
    }

    function get_tanggap_darurat_category_by_id($id)
    {
        // Define Model
        $TanggapDaruratCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\TanggapDaruratCategory');
        // Get Data
        $category_blog = $TanggapDaruratCategoryModel::where('status', 'Active')
                    ->where('id', $id)
                    ->get()
                    ->first();
        return $category_blog;
    }

    function count_tanggap_darurat_by_tanggap_darurat_category_id($tanggap_darurat_category_id)
    {
        $data = $this->TanggapDaruratModel::where('status', 'Active')
                ->where('display', true)
                ->where('tanggap_darurat_category_id', $tanggap_darurat_category_id)
                ->get()
                ->count();
        return $this->_resJson($data);
    }

    function get_tanggap_darurat_comments_by_tanggap_darurat_id($tanggap_darurat_id, $parent_id=null)
    {
        $TanggapDaruratCommentsModel = app($this->model_path.'TanggapDaruratComments');
        $data = $TanggapDaruratCommentsModel::where('status', 'Active')
                ->where('tanggap_darurat_id', $tanggap_darurat_id);
        if ($parent_id) {
            $data = $data->where('parent_id', $parent_id);
        } else
            $data = $data->whereNull('parent_id');
        $data = $data->get();
        // Get User & Image
        $UserModel = app($this->model_path.'User');
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $r) {
            $r->image = asset('assets/main/images/user-photo.jpg');
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            if ($r->user_id) {
                $user = $UserModel::where('status', 'Active')
                        ->where('id', $r->user_id)
                        ->get()
                        ->first();
                $r->user = $user ?? null;
                if ($user) {
                    $r->name = $r->user->name;
                    $r->email = $r->user->email;
                    $r->phone = $r->user->phone;
                    $image = $ImageModel::where('param_id', $user->id)
                            ->where('description', 'member')
                            ->get()
                            ->first();
                    if ($image)
                        $r->image = \ImageUrl::url('member/images/', $image);
                }
            }
        }
        return $this->_resJson($data);
    }

    function insert_tanggap_darurat_comments($post)
    {
        $TanggapDaruratCommentsModel = app($this->model_path.'TanggapDaruratComments');
        $data = $TanggapDaruratCommentsModel::insert($post);
        return $this->_resJson($data);
    }
}