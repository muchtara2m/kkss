@extends('web/'.$pView.'/index')

@section('body')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">		
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li><a href="{{route($pView.'/'.$sView)}}">User</a></li>
			<li class="active">Form</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header" style="margin-top: 10px;">
	            User
	        </h2>
	        <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
	            <a class="btn btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
	                <i class="fa fa-arrow-left"></i>
	            </a>
	            <a class="btn btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
	                <i class="fa fa-arrow-right"></i>
	            </a>
	            <a class="btn btn-default" href="" title="Refresh">
	                <i class="fa fa-rotate-right"></i>
	            </a>
	            <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
	                {{ ($user) ? 'Last updated : '.$user->updated_date : '' }}
	            </small>
	        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Form {{ $user?'Edit':'Add' }} User
				</div>
				<div class="panel-body">
					{{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                        <fieldset>
                            <legend>General</legend>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Name *</span>
                                    <div class="col-md-6">
                                        <input type="text" name="name" id="name" class="form-control form-control-md" placeholder="Type here.." value="{{ $user?$user->name:'' }}" autocomplete="off" required="">
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('name'))
                                            <span class="btn btn-danger">Name is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Username</span>
                                    <div class="col-md-6">
                                        <input type="text" name="username" id="username" class="form-control form-control-md" placeholder="Type here.." value="{{ $user?$user->username:'' }}" autocomplete="off">
                                        <small class="form-text text-muted g-font-size-default">Optional</small>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Email *</span>
                                    <div class="col-md-6">
                                        <input type="email" name="email" id="email" class="form-control form-control-md" placeholder="Type here.." value="{{ $user?$user->email:'' }}" autocomplete="off" required="">
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('email'))
                                            <span class="btn btn-danger">Email is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Password *</span>
                                    <div class="col-md-6">
                                        <input type="password" name="password" id="password" class="form-control form-control-md" placeholder="Type here.." value="" autocomplete="off" @if(!$user)required=""@endif>
                                        @if($user)
                                            <small class="form-text text-muted g-font-size-default">Enter if you want to change password</small>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('password'))
                                            <span class="btn btn-danger">Password is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Phone *</span>
                                    <div class="col-md-6">
                                        <input type="number" name="phone" id="phone" class="form-control form-control-md" placeholder="Type here.." value="{{ $user?$user->phone:'' }}" autocomplete="off" required="">
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('phone'))
                                            <span class="btn btn-danger">Phone is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Level *</span>
                                    <div class="col-md-6">
                                        <select name="level" id="level" class="form-control">
                                            <option value="" disabled="" selected="">Select Level</option>
                                            <option value="admin" {{ $user?$user->level=='admin'?'selected=':'':'' }}>Admin</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('level'))
                                            <span class="btn btn-danger">Level is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">URL *</span>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                {{ $base_url }}/user/
                                            </div>
                                            <input type="text" name="url" id="url" class="form-control form-control-md" placeholder="Type here.." value="{{ $user?$user->url:'' }}" autocomplete="off" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('url'))
                                            <span class="btn btn-danger">URL is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Status *</span>
                                    <div class="col-md-6">
                                        <select name="status" id="status" class="form-control">
                                            <option value="Active" {{ $user?$user->status=='Active'?'selected=':'':'' }}>Active</option>
                                            <option value="Inactive" {{ $user?$user->status=='Inactive'?'selected=':'':'' }}>Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('status'))
                                            <span class="btn btn-danger">Status is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend></legend>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right"></span>
                                    <div class="col-md-6">
                                        <input type="hidden" name="str_id" id="str_id" value="{{ $user?$user->str_id:'' }}">
                                        <button type="submit" class="btn btn-primary btn-class" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                        <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left"></i> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    {{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection