<?php

$router->group([
    'middleware' => 'role.member',
    'prefix'     => 'member'
], function () use ($router) {
    $router->get('', 'error\NotFoundController@index')->name('member');
    $router->group([
            'prefix'     => 'account'
        ], function () use ($router) {
            $router->get('', 'web\member\AccountController@index')->name('member/account');
            $router->post('post', 'web\member\AccountController@post')->name('member/account/post');
    });
	
});