<?php

namespace App\Http\Controllers\web\root;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\root\MenuQueries;

class MenuController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
	protected $pView = 'root';

	// Sub Folder (Child) in views
	protected $sView = 'menu';

	// For description select menu by role from database
	protected $vMenu = 'root';
	
	// For selected menu (Active menu)
	protected $sMenu = 'menu';

	// Session by user for access this controllers
	protected $sessions = 'root';
	
	function __construct(ControllerQueries $queries_controller, MenuQueries $queries_menu)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_menu = $queries_menu;
	}

    public function index()
    {
        // Global variable data
		$data = [];
		// Title in Website
		$data['vTitle'] = 'Menu';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = 'index';
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = 'index';

		/* Collection variable data */
		// Get Last
        $last = $this->queries_menu->get_menu_last();
        $data['last'] = $last;
		
		// Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('menu', $_GET);
        // Get Data
        $data = $this->queries_menu->get_menu_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        foreach ($data as $k => $v) {
            $v->type = !$v->parent_id?$v->link=='#'?'Parent Menu':'Stand Alone':'Child Menu';
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_menu->get_menu_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_menu->get_menu_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Menu';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        /* Collection variable data */
        // Get Data menu
        $menu = null;
        if ($str_id) {
            $menu = $this->queries_menu->get_menu_by_id($str_id);
            if ($menu)
                $menu->type = !$menu->parent_id?$menu->link=='#'?'Parent Menu':'Stand Alone':'Child Menu';
        }
        $data['menu'] = $menu;
        // Get Parent Menu
        $parent_menu = $this->queries_menu->get_menu_by_parent_id();
        $data['parent_menu'] = $parent_menu;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $validate = [];
        $validate['name'] = 'required';
        $validate['link'] = 'required';
        $validate['position'] = 'required';
        $validate['for_role'] = 'required';
        $validate['status'] = 'required';
        $this->validate($request, $validate);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');
        $link = $request->input('link');
        $position = $request->input('position');
        $for_role = $request->input('for_role');
        $parent_id = $request->input('parent_id');
        $status = $request->input('status');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['link'] = $link;
        $post['position'] = $position;
        $post['for_role'] = $for_role;
        if ($parent_id)
            $post['parent_id'] = $parent_id;
        $post['status'] = $status;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_menu->insert_menu($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_menu->update_menu($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
            }
        }

        // Response
        if ($request->input('is_ajax')!==null) {
            // Redirect to Function
            return response()->json([
                'post' => $session_post,
                'message' => $session_message
            ]);
        } else {
            // Set Session Flash
            Session::flash('post', $session_post);
            Session::flash('message', $session_message);
            // Redirect to Function
            return $this->_redirect($this->global_path, $_post);
        }
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        if ($id) {
            // Delete
            $_post = $this->queries_menu->destroy_menu($id);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully deleted!.';
            }
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return response()->json([
            'post' => $session_post,
            'message' => $session_message
        ]);
    }
}