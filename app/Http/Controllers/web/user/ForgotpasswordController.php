<?php

namespace App\Http\Controllers\web\user;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\user\ForgotPasswordQueries;
use Session;

class ForgotpasswordController extends \App\Http\Core\WebController
{
    // Parent Folder in views
    protected $pView = 'user';

    // Sub Folder (Child) in views
    protected $sView = 'forgotpassword';

    // For description select menu by role from database
    protected $vMenu = '';
    
    // For selected menu (Active menu)
    protected $sMenu = '';

    // Global model in this controllers
    protected $model = '';

    // Session by user for access this controllers
    protected $sessions = '';
	protected function session()
    {
        if (!empty(Session::has(env('SESSION').'root'))) {
            return env('SESSION').'root';
        } else if (!empty(Session::has(env('SESSION').'superadmin'))) {
            return env('SESSION').'superadmin';
        } else if (!empty(Session::has(env('SESSION').'admin'))) {
            return env('SESSION').'admin';
        } else if (!empty(Session::has(env('SESSION').'member'))) {
            return env('SESSION').'member';
        } else {
            return '';
        }
    }

	function __construct(ControllerQueries $queries_controller, ForgotPasswordQueries $queries_forgot_password)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_forgot_password = $queries_forgot_password;
    }

	public function index($param)
    {
    	// Get User
        $user = $this->queries_forgot_password->get_user_by_url($param);
        if (!$user)
            return \ErrorWeb::notfound();

    	// Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Forgot Password';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Data User
        $data['userdata'] = $user;
        
        // Render to View
        return $this->_render($data);
    }

    public function changepassword(Request $request)
    {
    	// Check Validate Request
        $this->validate(
        	$request, 
        	[
	            'email' => 'required|email',
	            'new_password' => 'required',
	            'confirm_password' => 'required',
	            'url' => 'required',
                'level' => 'required',
	        ]
        );

        // Define Variable Request
        $url = $request->input('url');
        $level = $request->input('level');
        $email = $request->input('email');
        $new_password = $request->input('new_password');
        $confirm_password = $request->input('confirm_password');
        $flash_post = 'Error';
        $flash_message = '';

        // Check Password
    	if ($new_password == $confirm_password) {
            // Get User
            $param = [];
            $param['email'] = $email;
            $param['level'] = $level;
            $param['url'] = $url;
            $user = $this->queries_forgot_password->get_user_by_email($param);                                    
            if ($user) {
                // Update Password
                $post_user = [];
                $post_user['id'] = $user->id;
                $post_user['password'] = Hash::make($new_password);
                $this->queries_forgot_password->update_user($post_user);
            	$flash_post = 'success';
            } else 
    			$flash_message = 'Email or Password not match';
        } else 
        	$flash_message = 'Confirm Password not match';

        // Set Session Flash
        Session::flash('post', $flash_post);
        Session::flash('message', $flash_message);
        // Redirect to Function
    	return $this->_redirect($this->global_path.$url);
    }
}
