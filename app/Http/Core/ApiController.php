<?php

namespace App\Http\Core;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class ApiController extends Controller 
{
	protected $data;

	function get_menu_for_role($request)
	{
		$for_role = $request->for_role;
		$s_menu = $request->s_menu;

		// Get Menu by Role
		$data = $this->queries_controller->get_menu_by_for_role($for_role, $s_menu);

        return $data;
	}

	function get_user_has_login($user_id)
	{
		// Get User by ID
		$data = $this->queries_controller->get_user_by_id($user_id);

		return $data;
	}

	function update_token($token)
	{
		$post_token = [];
        $post_token['id'] = $token->id;
        $post_token['expired_at'] = date('Y-m-d H:i:s', strtotime(CURRENT_DATE . ' +1 day'));
        $_post = $this->queries_controller->update_token($post_token);
        if (!$_post)
        	return $this->res('Failed Update to Token')->error();
        return true;
	}

	function res($param=[])
	{
		$_p = $param;
		if (is_array($param)==false) {
			$res = $param;
			$param = [];
			$param['data'] = $res;
		}
		if (!isset($param['data']) and $_p!==null)
			$param['data'] = $param;
		$this->data = $param;

		return $this;
	}

	function success($http_code = 200)
	{
		return response()->json($this->_response($this->data, $http_code), $http_code);
	} 

	function error($http_code = 500)
	{
		return response()->json($this->_response($this->data, $http_code), $http_code);
	} 

	function notfound($http_code = 404)
	{
		return response()->json($this->_response($this->data, $http_code), $http_code);
	} 

	function forbidden($http_code = 403)
	{
		return response()->json($this->_response($this->data, $http_code), $http_code);
	}

	function unauthorized($http_code = 401)
	{
		return response()->json($this->_response($this->data, $http_code), $http_code);
	}

	function _response($data, $http_code)
	{
		$_response = [];
		$_response['status'] = $http_code==200?true:false;
		$_response['http_code'] = $http_code;
		$_response['elapsed_time'] = $this->convert_elapsed_time(microtime(true)-LARAVEL_START);
		$_response['memory_usage'] = $this->convert_memory_usage(memory_get_usage(true));
		$_response['message'] = (isset($data['message'])?$data['message']:($http_code==200?'Success':'Error'));
		// $_response['message'] = $http_code==200?'Success':'Error';
		$_response['data'] = $data['data'];

		return $_response;
	}
}
