<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMenuTable20200419181300 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            UPDATE menu SET position=12 where id = 73;
        ");

        DB::statement("
            INSERT INTO menu (name, link, parent_id, favicon, position, for_role) VALUES
            ('Peluang Bisnis Category', 'master/peluangbisniscategory', 30, NULL, 11, 'admin');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
