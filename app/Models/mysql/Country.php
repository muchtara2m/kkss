<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'country';

	protected $fillable = [
		'name'
	];
}
