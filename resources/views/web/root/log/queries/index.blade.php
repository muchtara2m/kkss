@extends('web/'.$pView.'/index')

@section('body')
<div class="templatemo-content-wrapper">
    <div class="templatemo-content">
      	<ol class="breadcrumb">
	        <li># Log</li>
	        <li class="active"># Queries</li>
      	</ol>
      	<div class="row">
			<div class="col-lg-12">
		        <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
		            <a class="btn btn-sm btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
		                <i class="fa fa-arrow-left"></i>
		            </a>
		            <a class="btn btn-sm btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
		                <i class="fa fa-arrow-right"></i>
		            </a>
		            <a class="btn btn-sm btn-default" href="" title="Refresh">
		                <i class="fa fa-rotate-right"></i>
		            </a>
		            <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
		                {{ isset($last) ? 'Last updated : '.$last->updated_date : 'No Data' }}
		            </small>
		        </div>
			</div>
		</div>
      	<div class="row" style="padding-bottom: 40px;">
	        <div class="col-md-12">
	          	<div class="table-responsive" style="overflow-x: auto;">
	            	<h3 class="margin-bottom-15">
	            		Data Queries
	            	</h3>
	            	<hr/>
		            <table id="example1" class="table table-striped table-hover" url="{{ route($pView.'/'.$sView.'/get/datatable') }}" style="width: 100%; font-size: 11px;">
		            </table>
	          	</div>
	        </div>
      	</div>
    </div>
</div>
@endsection