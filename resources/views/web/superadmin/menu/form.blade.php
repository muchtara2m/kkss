@extends('web/'.$pView.'/index')

@section('body')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">		
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li><a href="{{route($pView.'/'.$sView)}}">User</a></li>
			<li class="active">Form</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header" style="margin-top: 10px;">
	            User
	        </h2>
	        <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
	            <a class="btn btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
	                <i class="fa fa-arrow-left"></i>
	            </a>
	            <a class="btn btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
	                <i class="fa fa-arrow-right"></i>
	            </a>
	            <a class="btn btn-default" href="" title="Refresh">
	                <i class="fa fa-rotate-right"></i>
	            </a>
	            <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
	                {{ ($menu) ? 'Last updated : '.$menu->updated_date : '' }}
	            </small>
	        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Form {{ $menu?'Edit':'Add' }} User
				</div>
				<div class="panel-body">
					{{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                        <fieldset>
                            <legend>General</legend>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Name *</span>
                                    <div class="col-md-6">
                                        <input type="text" name="name" id="name" class="form-control form-control-md" placeholder="Type here.." value="{{ $menu?$menu->name:'' }}" autocomplete="off" required="">
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('name'))
                                            <span class="btn btn-danger">Name is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Link *</span>
                                    <div class="col-md-6">
                                        <input type="text" name="link" id="link" class="form-control form-control-md" placeholder="Type here.." value="{{ $menu?$menu->link:'' }}" autocomplete="off" required="">
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('link'))
                                            <span class="btn btn-danger">Link is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Position *</span>
                                    <div class="col-md-6">
                                        <input type="number" name="position" id="position" class="form-control form-control-md" placeholder="Type here.." value="{{ $menu?$menu->position:'' }}" autocomplete="off" required="">
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('position'))
                                            <span class="btn btn-danger">Position is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">For Role *</span>
                                    <div class="col-md-6">
                                        <select name="for_role" id="for_role" class="form-control">
                                            <option value="" disabled="" selected="">Select For Role</option>
                                            <option value="superadmin" {{ $menu?$menu->for_role=='superadmin'?'selected=':'':'' }}>Superadmin</option>
                                            <option value="admin" {{ $menu?$menu->for_role=='admin'?'selected=':'':'' }}>Admin</option>
                                            <option value="member" {{ $menu?$menu->for_role=='member'?'selected=':'':'' }}>Member</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('for_role'))
                                            <span class="btn btn-danger">For Role is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Type *</span>
                                    <div class="col-md-6">
                                        <select name="type" id="type" class="form-control">
                                            <option value="" disabled="" selected="">Select Type</option>
                                            <option value="Stand Alone" {{ $menu?$menu->type=='Stand Alone'?'selected=':'':'' }}>Stand Alone</option>
                                            <option value="Parent Menu" {{ $menu?$menu->type=='Parent Menu'?'selected=':'':'' }}>Parent Menu</option>
                                            <option value="Child Menu" {{ $menu?$menu->type=='Child Menu'?'selected=':'':'' }}>Child Menu</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('type'))
                                            <span class="btn btn-danger">Type is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Parent Menu</span>
                                    <div class="col-md-6">
                                        <select name="parent_id" id="parent_id" class="form-control">
                                            <option value="" disabled="" selected="">Select Parent Menu</option>
                                            @foreach($parent_menu as $r)
                                            <option value="{{$r->id}}" {{ $menu?$menu->parent_id==$r->id?'selected=':'':'' }}>{{$r->name.' | '.$r->for_role}}</option>
                                            @endforeach
                                        </select>
                                        <small class="form-text text-muted g-font-size-default">Optional</small>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Status *</span>
                                    <div class="col-md-6">
                                        <select name="status" id="status" class="form-control">
                                            <option value="Active" {{ $menu?$menu->status=='Active'?'selected=':'':'' }}>Active</option>
                                            <option value="Inactive" {{ $menu?$menu->status=='Inactive'?'selected=':'':'' }}>Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        @if($errors->get('status'))
                                            <span class="btn btn-danger">Status is required</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend></legend>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right"></span>
                                    <div class="col-md-6">
                                        <input type="hidden" name="str_id" id="str_id" value="{{ $menu?$menu->str_id:'' }}">
                                        <button type="submit" class="btn btn-primary btn-class" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                        <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left"></i> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    {{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection