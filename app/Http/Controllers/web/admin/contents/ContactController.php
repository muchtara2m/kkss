<?php

namespace App\Http\Controllers\web\admin\contents;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\contents\ContactQueries;

class ContactController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
	protected $pView = 'admin';

	// Sub Folder (Child) in views
	protected $sView = 'contents/contact';

	// For description select menu by role from database
	protected $vMenu = 'admin';
	
	// For selected menu (Active menu)
	protected $sMenu = 'contents/contact';

	// Session by user for access this controllers
	protected $sessions = 'admin';
	
	function __construct(ControllerQueries $queries_controller, ContactQueries $queries_contact)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_contact = $queries_contact;
	}

    public function index()
    {
        // Global variable data
		$data = [];
		// Title in Website
		$data['vTitle'] = 'Contact > Contents';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		// Get Contact
        $contact = $this->queries_contact->get_contact();
        foreach ($contact as $r) {
            if ($r->description=='phone') {
                $data['phone'] = $r;
            } else
            if ($r->description=='email') {
                $data['email'] = $r;
            }
            $data['last'] = date('d M Y, (H:i)', strtotime($r->updated_at));
        }
		
		// Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $validate = [];
        $validate['id'] = 'required';
        $validate['value_text'] = 'required';
        $this->validate($request, $validate);

        // Define Variable Request
        $id = $request->input('id');
        $value_text = $request->input('value_text');

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        // Update
        for ($i=0; $i < count($id); $i++) {
            $post = [];
            $post['id'] = $id[$i];
            $post['value_text'] = $value_text[$i];
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_contact->update_contents($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
            }
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $_post);
    }
}