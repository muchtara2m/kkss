<meta charset="UTF-8">
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{$vTitle}} | Superadmin</title>
<link rel="shortcut icon" type="image/png" href="{{ $vContents['favicon']['image_url'] }}">
<link href="{{ asset('assets/'.$pView.'/css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/'.$pView.'/css/styles.css') }}" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ asset('assets/admin/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<!-- Datatable -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript">
	const _token = '{{ csrf_token() }}'
</script>