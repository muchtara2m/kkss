<?php

namespace App\Queries\web\admin;

class EventsQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $EventsModel = app($this->model_path.'Events');
        $this->EventsModel = $EventsModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_events_datatable($whereRaw, $order, $offset, $limit)
    {
        $events = $this->EventsModel::select(
                        'events.*', 
                        'events_category.id as events_category_$_id',
                        'events_category.name as events_category_$_name'
                    )
                    ->join('events_category', 'events_category.id', 'events.events_category_id')
                    ->where('events.status', 'Active');
        if ($whereRaw!='')
            $events = $events->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'events.'.$order['coloumn'];
        $events = $events->orderBy($order_column, $order['dir'])
                    ->orderBy('events.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($events as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'events')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->date = date('d M Y', strtotime($value->date));
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($events);
    }

    function get_events_total($whereRaw)
    {
        $events = $this->EventsModel::select('events.id')
                    ->where('events.status', 'Active');
        if ($whereRaw!='')
            $events = $events->whereRaw($whereRaw);
        $events = $events->count();
        return $this->_resJson($events);
    }

    function get_events_by_id($id)
    {
        $data = $this->EventsModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'events')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_events_last()
    {
        $data = $this->EventsModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function get_events_category()
    {
        // Define Model
        $EventsCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsCategory');
        // Get Data
        $category = $EventsCategoryModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $category;
    }

    function insert_events($post)
    {
        $data = $this->EventsModel::insert($post);
        return $this->_resJson($data);
    }

    function update_events($post)
    {
        $data = $this->EventsModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_events($id)
    {
        $data = $this->EventsModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_events($id)
    {
        $data = $this->EventsModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }

    function get_events_register_datatable($whereRaw, $order, $offset, $limit)
    {
        $EventsRegisterModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsRegister');
        $table_a = $EventsRegisterModel::select(
                        'events_register.id as id',
                        'events_register.events_id as events_id',
                        'events_register.name as name',
                        'events_register.email as email',
                        'events_register.phone as phone',
                        'events_register.job_title as job_title',
                        'events_register.address as address',
                        'events_register.is_status as is_status',
                        'events_register.created_at as created_at',
                        'events_register.updated_at as updated_at'
                    )
                    ->whereNull('events_register.user_id')
                    ->where('events_register.status', 'Active');
        $table_b = $EventsRegisterModel::select(
                        'events_register.id as id',
                        'events_register.events_id as events_id',
                        'user.name as name',
                        'user.email as email',
                        'user.phone as phone',
                        'profession.name as job_title',
                        'user_profile.address as address',
                        'events_register.is_status as is_status',
                        'events_register.created_at as created_at',
                        'events_register.updated_at as updated_at'
                    )
                    ->join('user', 'user.id', 'events_register.user_id')
                    ->join('user_profile', 'user_profile.user_id', 'user.id')
                    ->join('profession', 'profession.id', 'user_profile.profession_id')
                    ->whereNotNull('events_register.user_id')
                    ->where('events_register.status', 'Active');
        if ($whereRaw!='') {
            // For Table 
            $whereRaw_a = $whereRaw;
            $table_a = $table_a->whereRaw($whereRaw_a);
            // For Table B
            $whereRaw_b = $whereRaw;
            $whereRaw_b = str_replace('events_register.name', 'user.name', $whereRaw_b);
            $whereRaw_b = str_replace('events_register.email', 'user.email', $whereRaw_b);
            $whereRaw_b = str_replace('events_register.phone', 'user.phone', $whereRaw_b);
            $whereRaw_b = str_replace('events_register.job_title', 'profession.name', $whereRaw_b);
            $whereRaw_b = str_replace('events_register.address', 'user_profile.address', $whereRaw_b);
            $table_b = $table_b->whereRaw($whereRaw_b);
        }
        $events_register = $table_a->union($table_b);
        $order_column = '';
        $order_column = $order['coloumn'];
        $events_register = $events_register->orderBy($order_column, $order['dir'])->orderBy('id', 'asc');
        $events_register = $events_register->skip($offset)->take($limit)->get();
        foreach ($events_register as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($events_register);
    }

    function get_events_register_total($whereRaw)
    {
        $EventsRegisterModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsRegister');
        $events_register = $EventsRegisterModel::select('events_register.id')
                    ->where('events_register.status', 'Active');
        if ($whereRaw!='')
            $events_register = $events_register->whereRaw($whereRaw);
        $events_register = $events_register->count();
        return $this->_resJson($events_register);
    }

    function get_events_register_by_id($id)
    {
        $EventsRegisterModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsRegister');
        $events_register = $EventsRegisterModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($events_register) {
            if ($events_register->user_id) {
                $UserModel = app($this->model_path.'User');
                $user = $UserModel::where('id', $events_register->user_id)->get()->first();
                $events_register->name = $user->name;
                $events_register->email = $user->email;
                $events_register->phone = $user->phone;
                $UserProfileModel = app($this->model_path.'UserProfile');
                $user_profile = $UserProfileModel::where('user_id', $user->id)->get()->first();
                $events_register->address = $user_profile->address;
                $ProfessionModel = app($this->model_path.'Profession');
                $profession = $ProfessionModel::where('id', $user_profile->profession_id)->get()->first();
                $events_register->job_title = $profession->name;
            }
            $events_register->updated_date = date('d M Y, (H:i)', strtotime($events_register->updated_at));
            $events_register->str_id = $events_register->id;
        }
        return $this->_resJson($events_register);
    }

    function get_events_register_last($events_id)
    {
        $EventsRegisterModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsRegister');
        $events_register = $EventsRegisterModel::where('status', 'Active')
                ->where('events_id', $events_id)
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($events_register) {
            $events_register->updated_date = date('d M Y, (H:i)', strtotime($events_register->updated_at));
        }
        return $this->_resJson($events_register);
    }

    function insert_events_register($post)
    {
        $EventsRegisterModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsRegister');
        $data = $EventsRegisterModel::insert($post);
        return $this->_resJson($data);
    }

    function update_events_register($post)
    {
        $EventsRegisterModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsRegister');
        $data = $EventsRegisterModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_events_register($id)
    {
        $EventsRegisterModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsRegister');
        $data = $EventsRegisterModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_events_register($id)
    {
        $EventsRegisterModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsRegister');
        $data = $EventsRegisterModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
