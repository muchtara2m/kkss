<?php

namespace App\Queries\web\user;

class ExpiredQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    public function get_user_by_url($url)
    {
        $data = $this->UserModel::where('status', 'Active')
                ->where('url', $url)
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    public function get_user_by_email($param)
    {
        $data = $this->UserModel::where('email', $param['email'])
                ->where('level', $param['level'])
                ->where('url', $param['url'])
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    public function get_token_by_session_key($session_key)
    {
        $TokenModel = app($this->model_path.'Token');
        $data = $TokenModel::where('session_key', $session_key)
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    public function update_token($post, $id)
    {
        $TokenModel = app($this->model_path.'Token');
        $data = $TokenModel::where('id', $id)
                ->update($post);
        return $this->_resJson($data);
    }
}
