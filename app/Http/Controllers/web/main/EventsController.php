<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\EventsQueries;

class EventsController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'events';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'events';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, EventsQueries $queries_events)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_events = $queries_events;
	}

    public function index(Request $request)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Events';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

		/* Collection variable data */
        // Get User Session
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
		// Get Events
		$events = $this->queries_events->get_events($keyword);
        foreach ($events as $r) {
            $r->is_register = false;
            if ($user_id) {
                $events_register = $this->queries_events->get_events_register_by_events_id($r->id, $user_id);
                if ($events_register)
                    $r->is_register = true;
            }
        	$r->date = date('d M Y', strtotime($r->date));
        	$r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('events/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('events/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
            $r->short_desc = strlen($r->short_description)>225?substr($r->short_description, 0, 225).'..':$r->short_description;
        }
        $data['events'] = $events;
		// Get Events Category
        $events_category = $this->queries_events->get_events_category();
        foreach ($events_category as $r) {
        	$r->total = $this->queries_events->count_events_by_events_category_id($r->id);
        }
        $data['events_category'] = $events_category;
		
		// Render to View
        return $this->_render($data);
    }

    public function category(Request $request, $events_category_id)
    {
        // Global variable data
        $data = [];
        // Title in website
        $data['vTitle'] = 'Not set';
        // Filename in folder view
        $data['cView'] = 'category';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

        /* Collection variable data */
        // Get Events Category Detail
        $category = $this->queries_events->get_events_category_by_id($events_category_id);
        if ($category)
            $data['vTitle'] = $category->name.' | Events ';
        $data['category'] = $category;
        // Get User Session
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        // Get Events
        $events = $this->queries_events->get_events_by_events_category_id($events_category_id, $keyword);
        foreach ($events as $r) {
            $r->is_register = false;
            if ($user_id) {
                $events_register = $this->queries_events->get_events_register_by_events_id($r->id, $user_id);
                if ($events_register)
                    $r->is_register = true;
            }
            $r->date = date('d M Y', strtotime($r->date));
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('events/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('events/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
            $r->short_desc = strlen($r->short_description)>225?substr($r->short_description, 0, 225).'..':$r->short_description;
        }
        $data['events'] = $events;
        // Get Events Category
        $events_category = $this->queries_events->get_events_category();
        foreach ($events_category as $r) {
            $r->total = $this->queries_events->count_events_by_events_category_id($r->id);
            $r->bold = $category?($category->id==$r->id?true:false):false;
        }
        $data['events_category'] = $events_category;
        
        // Render to View
        return $this->_render($data);
    }

    public function single($seo)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Not set';
		// Filename in folder view
		$data['cView'] = 'single';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'single';
		$data['vAdditional']['js'] = 'single';

		/* Collection variable data */
        // Get User Session
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
		// Get Events
		$events = $this->queries_events->get_events_by_seo($seo);
        if ($events) {
			$data['vTitle'] = $events->name.' | Events';
            $events->is_register = false;
            if ($user_id) {
                $events_register = $this->queries_events->get_events_register_by_events_id($events->id, $user_id);
                if ($events_register)
                    $events->is_register = true;
            }
            $events->date = date('d M Y', strtotime($events->date));
        	$events->created_date = date('d M Y, (H:i)', strtotime($events->updated_at));
            $events->image_url = $events->image?\ImageUrl::url('events/images/', $events->image):null;
            $events->thumb_url = $events->image?\ImageUrl::url('events/thumbs/', $events->image):null;
            $events->user_name = $events->user?$events->user->name:null;
            $events->category_name = $events->category?$events->category->name:null;
        }
        $data['events'] = $events;
        // Get Events Category
        $events_category = $this->queries_events->get_events_category();
        foreach ($events_category as $r) {
        	$r->total = $this->queries_events->count_events_by_events_category_id($r->id);
        }
        $data['events_category'] = $events_category;
        // Get Events Comments
        $count_comments = 0;
        $events_comments = [];
        if ($events) {
        	$events_comments = $this->queries_events->get_events_comments_by_events_id($events->id);
        	foreach ($events_comments as $r) {
                $r->sub_comments = $this->queries_events->get_events_comments_by_events_id($events->id, $r->id);
                $count_comments += count($r->sub_comments);
        	}
            $count_comments += count($events_comments);
    	}
        $data['comments'] = $events_comments;
        $data['count_comments'] = $count_comments;
		
		// Render to View
        return $this->_render($data);
    }

    public function comments(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'seo' => 'required',
            'events_id' => 'required',
            'message' => 'required',
        ]);

        // Define Variable Request
        $seo = $request->input('seo');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        $events_id = $request->input('events_id');
        $events_comments_id = $request->input('events_comments_id');
        $message = $request->input('message');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['email'] = $email;
        $post['user_id'] = $user_id;
        $post['events_id'] = $events_id;
        $post['parent_id'] = $events_comments_id;
        $post['message'] = $message;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Insert
        $_post = $this->queries_events->insert_events_comments($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully created!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->sView.'/'.$seo);
    }

    public function register($seo)
    {
        // Global variable data
        $data = [];
        // Title in website
        $data['vTitle'] = 'Not set';
        // Filename in folder view
        $data['cView'] = 'register';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'register';

        /* Collection variable data */
        // Get Events
        $events = $this->queries_events->get_events_by_seo($seo);
        if ($events) {
            $data['vTitle'] = 'Daftar Event - '.$events->name.' | Events';
            $events->date = date('d M Y', strtotime($events->date));
            $events->created_date = date('d M Y, (H:i)', strtotime($events->updated_at));
            $events->image_url = $events->image?\ImageUrl::url('events/images/', $events->image):null;
            $events->thumb_url = $events->image?\ImageUrl::url('events/thumbs/', $events->image):null;
            $events->user_name = $events->user?$events->user->name:null;
            $events->category_name = $events->category?$events->category->name:null;
        }
        $data['events'] = $events;
        // Get Events Category
        $events_category = $this->queries_events->get_events_category();
        foreach ($events_category as $r) {
            $r->total = $this->queries_events->count_events_by_events_category_id($r->id);
        }
        $data['events_category'] = $events_category;
        
        // Render to View
        return $this->_render($data);
    }

    public function register_post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'events_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'job_title' => 'required',
            'address' => 'required',
            'seo' => 'required',
        ]);

        // Define Variable Request
        $events_id = $request->input('events_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $job_title = $request->input('job_title');
        $address = $request->input('address');
        $user_id = $request->input('user_id');
        $seo = $request->input('seo');

        // Check Email
        if ($user_id) {
            $check_user = $this->queries_events->get_events_register_by_user_id($user_id);
            if ($check_user) {
                Session::flash('post', 'Error');
                Session::flash('message', 'You have registered with this account!');
                return $this->_redirect($this->sView.'/register/'.$seo);
            }
        } else {
            $check_email = $this->queries_events->get_events_register_by_email($email);
            if ($check_email) {
                Session::flash('post', 'Error');
                Session::flash('message', 'You have registered with this email!');
                return $this->_redirect($this->sView.'/register/'.$seo);
            }
        }

        // Create Array to Post
        $post = [];
        $post['events_id'] = $events_id;
        if (!$user_id) {
            $post['name'] = $name;
            $post['email'] = $email;
            $post['phone'] = $phone;
            $post['job_title'] = $job_title;
            $post['address'] = $address;
        }
        $post['user_id'] = $user_id;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Insert
        $_post = $this->queries_events->insert_events_register($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Register successfully!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->sView.'/register/'.$seo);
    }

    public function registered_status()
    {
        // Global variable data
        $data = [];
        // Title in website
        $data['vTitle'] = 'Status Register Event';
        // Filename in folder view
        $data['cView'] = 'registered_status';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        /* Collection variable data */
        // Get User Session
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        // Get Events
        $events = $this->queries_events->get_events_registered_by_user_id($user_id);
        foreach ($events as $row) {
            $row->events->date = date('d M Y', strtotime($row->events->date));
            $row->created_date = date('d M Y, (H:i)', strtotime($row->updated_at));
            $row->image_url = $row->image?\ImageUrl::url('events/images/', $row->image):null;
            $row->thumb_url = $row->image?\ImageUrl::url('events/thumbs/', $row->image):null;
            $row->user_name = $row->user?$row->user->name:null;
            $row->category_name = $row->category?$row->category->name:null;
            if ($row->is_status=='Register') $row->is_status = 'Menunggu verifikasi';
            else if ($row->is_status=='Approve') $row->is_status = 'Disetujui';
            else if ($row->is_status=='Reject') $row->is_status = 'Ditolak';
        }
        $data['events'] = $events;
        
        // Render to View
        return $this->_render($data);
    }
}
