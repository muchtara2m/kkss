<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterContentsTable20200511064300 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO contents (value_string, icon, description) VALUES
            ('Phone', 'fa fa-phone', 'phone'),
            ('Email', 'fa fa-envelope', 'email'),
            ('Facebook', 'fa fa-facebook', 'social_media'),
            ('Twitter', 'fa fa-twitter', 'social_media'),
            ('LinkedIn', 'fa fa-linkedin', 'social_media'),
            ('Instagram', 'fa fa-instagram', 'social_media');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
