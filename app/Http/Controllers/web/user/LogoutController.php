<?php

namespace App\Http\Controllers\web\user;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\web\user\LogoutQueries;
use App\Queries\ControllerQueries;
use Redirect;

class LogoutController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
    protected $pView = '';

    // Sub Folder (Child) in views
    protected $sView = '';

    // For description select menu by role from database
    protected $vMenu = '';
    
    // For selected menu (Active menu)
    protected $sMenu = '';

    // Global model in this controllers
    protected $model = '';

    // Session by user for access this controllers
    protected $sessions = '';
	protected function session()
	{
        if (Session::get(env('SESSION').'member')) return 'member';
        $_previous_url = Session::get('_previous')['url'];
        $_previous_url = str_replace(env('APP_URL'), '', $_previous_url);
        $_previous_url = explode('/', $_previous_url);
        $_previous_url = $_previous_url[0];
        return $_previous_url;
	}
	
	function __construct(ControllerQueries $queries_controller, LogoutQueries $queries_logout)
	{
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_logout = $queries_logout;
	}

    public function post()
    {
        // Check Authentication User
        $_auth = $this->queries_controller->check_auth(env('SESSION').$this->session(), env('COOKIE').$this->session());
        if (!$_auth)
            return \ErrorWeb::notfound();

    	// Get Session
    	$session_user = Session::get(env('SESSION').$this->session());
        $user_id = Crypt::decryptString($session_user);
    	// Get User
    	$user = $this->queries_logout->get_user_by_id($user_id);
    	// Update Login
        $post_user = [];
        $post_user['id'] = $user->id;
        $post_user['logout'] = date('Y-m-d H:i:s');
        $this->queries_logout->update_user($post_user);
        // Set Inactive Token
        $this->queries_logout->update_token($session_user);
        // Define Redirect        
        $redirect = $this->session()=='member'?'home':'user/'.$user->url;
        // Destroy Session
        Session::forget(env('SESSION').$this->session());
        // Destroy Cookie
        Cookie::queue(Cookie::forget(env('COOKIE').$user->level));
		return $this->_redirect($redirect);
    }
}
