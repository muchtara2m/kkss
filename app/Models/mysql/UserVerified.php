<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class UserVerified extends Model
{
    protected $connection = 'mysql';
    
    protected $table = 'user_verified';
    
    protected $fillable = [
        'user_id',
        'verified_code',
        'is_verified'
    ];
}
