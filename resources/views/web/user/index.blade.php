<!DOCTYPE html>
<html lang="en">
    <head>
        @include('web/'.$pView.'/head')
    </head>
    <body class="login-page">
        @yield('body')
        @include('web/'.$pView.'/bottom') 
        {{ $vSubFolder = (strpos($cView, '/index') == true) ? '/'.str_replace("/index", "", $cView) : null }}
        @if($vAdditional['css'])
            <link href="{{ asset('assets/admin/views/css/'.$pView.'/'.$sView.$vSubFolder.'/'.$vAdditional['css'].'.css') }}">
        @endif
        @if($vAdditional['html'])
            @include('web/'.$pView.'/'.$sView.$vSubFolder.'/additional/'.$vAdditional['html'])
        @endif
        @if($vAdditional['js'])
            <script src="{{ asset('assets/admin/views/js/'.$pView.'/'.$sView.$vSubFolder.'/'.$vAdditional['js'].'.js') }}"></script>
        @endif
    </body>
    @include('web/'.$pView.'/onloading')
</html>
