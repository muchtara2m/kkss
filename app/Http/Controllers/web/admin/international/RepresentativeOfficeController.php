<?php

namespace App\Http\Controllers\web\admin\international;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\international\RepresentativeOfficeQueries;

class RepresentativeOfficeController extends \App\Http\Core\WebController 
{
    // Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'international/representativeoffice';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'international/representativeoffice';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, RepresentativeOfficeQueries $queries_representative_office)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_representative_office = $queries_representative_office;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Representative Office > International';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_representative_office->get_representative_office_last();
        $data['last'] = $last;

        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('representative_office', $_GET);
        // Get Data
        $data = $this->queries_representative_office->get_representative_office_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        foreach ($data as $r) {
            $r->image_url = $r->image?\ImageUrl::url($this->sView.'/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url($this->sView.'/thumbs/', $r->image):null;
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_representative_office->get_representative_office_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_representative_office->get_representative_office_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
            $data['name'] = $get->name;
            $data['description'] = $get->description;
            $data['display'] = $get->display;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Representative Office > International';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'form';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'form';

        /* Collection variable data */
        // Get Representative Office
        $representative_office = null;
        if ($str_id) {
            $representative_office = $this->queries_representative_office->get_representative_office_by_id($str_id);
            $representative_office->image_id = $representative_office->image?$representative_office->image->id:null;
            $representative_office->image_url = $representative_office->image?\ImageUrl::url($this->sView.'/images/', $representative_office->image):null;
            $representative_office->thumb_url = $representative_office->image?\ImageUrl::url($this->sView.'/thumbs/', $representative_office->image):null;
        }
        $data['representative_office'] = $representative_office;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'title' => 'required',
            'location' => 'required',
            'description' => 'required',
            'embed_maps' => 'required',
            'display' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $title = $request->input('title');
        $location = $request->input('location');
        $type = 'International';
        $description = $request->input('description');
        $embed_maps = $request->input('embed_maps');
        $display = $request->input('display');
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $post['title'] = $title;
        $post['location'] = $location;
        $post['type'] = $type;
        $post['description'] = $description;
        $post['embed_maps'] = $embed_maps;
        $post['display'] = ($display=='true')?true:false;
        $_post = false; // for upload file
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_representative_office->insert_representative_office($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
                $_representative_office = $this->queries_representative_office->get_representative_office_last();
                $_param_id = $_representative_office->id;
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_representative_office->update_representative_office($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
                $_param_id = $id;
            }
        }

        // Proccess Upload
        if ($_post and $file) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'representative_office'; // Set Description for DB Image
            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
            $path = $this->sView; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload)
                return \ErrorWeb::notfound();
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        // Delete
        // $_post = $this->queries_representative_office->destroy_representative_office($id);
        // Soft Delete
        $_post = $this->queries_representative_office->delete_representative_office($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }
}
