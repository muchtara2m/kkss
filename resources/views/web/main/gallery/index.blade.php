@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Gallery</li>
            </ol>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <h2 class="title text-center" style="margin-bottom: 10px;">Gallery</h2>
            <div class="panel-body">
                <form method="get" action="">
                    <div class="search_box">
                        <input type="text" name="keyword" placeholder="Search" value="{{ $keyword }}"/>
                    </div>
                </form>
            </div>
            <div class="category-tab">
                <div class="col-sm-12">
                    <ul class="nav nav-tabs">
                        @foreach($gallery as $i => $row)
                        <li class="{{ $i==0?'active':'' }}"><a href="#{{ $row->id }}" data-toggle="tab">{{ $row->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content">
                    @foreach($gallery as $i => $row)
                    <div class="tab-pane fade {{ $i==0?'active in':'' }}" id="{{ $row->id }}">
                        @if(count($row->gallery))
                            @foreach($row->gallery as $r)
                                <div class="col-sm-3">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="{{ $r->image_url }}" alt="image" style="width: 100%; height: 200px;" />
                                                <h5 style="color: #dc3545;">{{ $r->name }}</h5>
                                                <p title="{{ $r->short_description }}">{{ $r->short_desc }}</p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                        <div class="col-sm-12">
                            <h4 class="text-muted" style="margin-bottom: 50px;">No Content</h4>
                        </div>
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection