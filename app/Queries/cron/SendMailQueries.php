<?php

namespace App\Queries\cron;

class SendMailQueries extends \App\Queries\Queries
{	
	protected $model_path;

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $SendMailModel = app($this->model_path.'SendMail');
        $this->SendMailModel = $SendMailModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    public function get_send_mail()
    {
        $send_mail = $this->SendMailModel::select()
                    ->whereIn('status', ['Pending', 'Failed'])
                    ->orderBy('created_at', 'asc')
                    ->limit(10)
                    ->get();
        return $this->_resJson($send_mail);
    }

    public function update_user($post)
    {
        $data = $this->SendMailModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }
}