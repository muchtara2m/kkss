<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterManuTable20200209152500 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO menu (id, name, link, parent_id, favicon, position, for_role) VALUES
            ('40', 'Home', 'home', NULL, NULL, 1, 'member'),
            ('41', 'Forum', 'forum', NULL, NULL, 2, 'member'),
            ('42', 'Events', 'events', NULL, NULL, 3, 'member'),
            ('43', 'Membership', 'membership', NULL, NULL, 4, 'member'),
            ('44', 'Peluang Bisnis', 'peluangbisnis', NULL, NULL, 5, 'member'),
            ('45', 'Gallery', 'gallery', NULL, NULL, 6, 'member'),
            ('46', 'Tanggap Darurat', 'tanggapdarurat', NULL, NULL, 7, 'member'),
            ('47', 'International', '#', NULL, NULL, 8, 'member'),
            ('48', 'Tokoh', 'international/tokoh', 47, NULL, 1, 'member'),
            ('49', 'Representative Office', 'international/representativeoffice', 47, NULL, 2, 'member');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
