@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Social Media</h1>
        <ol class="breadcrumb">
            <li># Contents</li>
            <li class="active">Social Media</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ ($last) ? 'Last updated : '.$last : '' }}
            </span>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <h2 style="margin-top: -15px; font-size: 20px;">
            <label for="editor1">Form Edit Social Media</label>
        </h2>
        <div class="box">
            <div class="box-body" style="padding-top: 25px;">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                @foreach($social_media as $r)
                                <div class="form-group">
                                    <div class="row">
                                        <span class="col-md-2 text-right">{{ $r->value_string }}</span>
                                        <div class="col-md-6">
                                            <input type="hidden" name="id[]" value="{{ $r?$r->id:'' }}">
                                            <input type="text" name="value_text[]" class="form-control form-control-md" placeholder="Tulis disini.." value="{{ $r?$r->value_text:'' }}" autocomplete="off">
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                @endforeach
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary btn-class" style="border-radius: 0;" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default" style="border-radius: 0;"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop