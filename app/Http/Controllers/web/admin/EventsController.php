<?php

namespace App\Http\Controllers\web\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\EventsQueries;

class EventsController extends \App\Http\Core\WebController 
{
    // Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'events';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'events';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, EventsQueries $queries_events)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_events = $queries_events;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Events';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_events->get_events_last();
        $data['last'] = $last;

        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('events', $_GET);
        // Get Data
        $data = $this->queries_events->get_events_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        foreach ($data as $r) {
            $r->image_url = $r->image?\ImageUrl::url($this->sView.'/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url($this->sView.'/thumbs/', $r->image):null;
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_events->get_events_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_events->get_events_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Events';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'form';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'form';

        /* Collection variable data */
        // Get Data events
        $events = null;
        if ($str_id) {
            $events = $this->queries_events->get_events_by_id($str_id);
            $events->date = date('m/d/Y', strtotime($events->date));
            $events->image_id = $events->image?$events->image->id:null;
            $events->image_url = $events->image?\ImageUrl::url($this->sView.'/images/', $events->image):null;
            $events->thumb_url = $events->image?\ImageUrl::url($this->sView.'/thumbs/', $events->image):null;
        }
        $data['events'] = $events;
        // Get Peluang Bisnis Category
        $events_category = $this->queries_events->get_events_category();
        $data['events_category'] = $events_category;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'name' => 'required',
            'events_category' => 'required',
            'date' => 'required',
            'location' => 'required',
            'pic' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'display' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');
        $seo = strtolower(str_replace(' ', '-', $name));
        $events_category_id = $request->input('events_category');
        $date = $request->input('date');
        $location = $request->input('location');
        $pic = $request->input('pic');
        $short_description = $request->input('short_description');
        $description = $request->input('description');
        $display = $request->input('display');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = Crypt::decryptString($session_user);
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['events_category_id'] = $events_category_id;
        $post['seo'] = $seo;
        $post['date'] = date('Y-m-d', strtotime($date));
        $post['location'] = $location;
        $post['pic'] = $pic;
        $post['short_description'] = $short_description;
        $post['description'] = $description;
        $post['display'] = ($display=='true')?true:false;
        $post['created_by'] = $user_id;
        $_post = false; // for upload file
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_events->insert_events($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
                $_events = $this->queries_events->get_events_last();
                $_param_id = $_events->id;
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_events->update_events($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
                $_param_id = $id;
            }
        }

        // Proccess Upload
        if ($_post and $file) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'events'; // Set Description for DB Image
            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
            $path = $this->sView; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload)
                return \ErrorWeb::notfound();
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        // Delete
        // $_post = $this->queries_events->destroy_events($id);
        // Soft Delete
        $_post = $this->queries_events->delete_events($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function register($events_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Events';
        // Filename in folder view
        $data['cView'] = 'register';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'register';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'register';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_events->get_events_register_last($events_id);
        $data['last'] = $last;
        // Get Events
        $events = $this->queries_events->get_events_by_id($events_id);
        if ($events) {
            $data['vTitle'] = $events->name.' > Register > Events';
        }
        $data['events'] = $events;

        // Render to View
        return $this->_render($data);
    }

    public function register_get_datatable($events_id)
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('events_register', $_GET);
        // Get Data
        if ($modul['whereRaw']!="")
            $modul['whereRaw'] .= ' AND';
        $modul['whereRaw'] .= ' events_register.events_id = '.$events_id;
        $data = $this->queries_events->get_events_register_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_events->get_events_register_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function register_get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_events->get_events_register_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function register_form($events_id, $events_register_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Events';
        // Filename in folder view
        $data['cView'] = 'register_form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'register_form';

        /* Collection variable data */
        // Get Data events
        $events = $this->queries_events->get_events_by_id($events_id);
        if ($events) {
            $data['vTitle'] = 'Form > '.$events->name.' > Register > Events';
        }
        $data['events'] = $events;
        // Get Register
        $events_register = null;
        if ($events_register_id) {
            $events_register = $this->queries_events->get_events_register_by_id($events_register_id);
        }
        $data['events_register'] = $events_register;
        
        // Render to View
        return $this->_render($data);
    }

    public function register_post(Request $request)
    {
        // echo json_encode($request->all()); die();
        // Check Validate Request
        $this->validate($request, [
            'events_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'job_title' => 'required',
            'address' => 'required',
            'is_status' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $events_id = $request->input('events_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $job_title = $request->input('job_title');
        $address = $request->input('address');
        $is_status = $request->input('is_status');
        $user_id = $request->input('user_id');

        // Create Array to Post
        $post = [];
        $post['events_id'] = $events_id;
        if (!$user_id) {
            $post['name'] = $name;
            $post['email'] = $email;
            $post['phone'] = $phone;
            $post['job_title'] = $job_title;
            $post['address'] = $address;
        }
        $post['is_status'] = $is_status;
        $post['user_id'] = $user_id;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_events->insert_events_register($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_events->update_events_register($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
            }
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path.'register/'.$events_id, $post);
    }

    public function register_delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
            'events_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $events_id = $request->input('events_id');

        // Process Query
        // Delete
        // $_post = $this->queries_events->destroy_events_register($id);
        // Soft Delete
        $_post = $this->queries_events->delete_events_register($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path.'register/'.$events_id, $post);
    }
}
