<?php

namespace App\Queries\web\admin;

class DashboardQueries extends \App\Queries\Queries
{	
	protected $model_path;
	protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        // $ModelName = app($this->model_path.'ModelName');
        // $this->ModelName = $ModelName;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}
}
