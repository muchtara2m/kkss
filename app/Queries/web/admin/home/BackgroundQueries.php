<?php

namespace App\Queries\web\admin\home;

class BackgroundQueries extends \App\Queries\Queries
{	
	protected $model_path;
	protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $BackgroundModel = app($this->model_path.'Background');
        $this->BackgroundModel = $BackgroundModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_background_datatable($whereRaw, $order, $offset, $limit)
    {
        $background = $this->BackgroundModel::select(
                        'background.*')
                    ->where('background.status', 'Active');
        if ($whereRaw!='')
            $background = $background->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'background.'.$order['coloumn'];
        $background = $background->orderBy($order_column, $order['dir'])
                    ->orderBy('background.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($background as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'background')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($background);
    }

    function get_background_total($whereRaw)
    {
        $background = $this->BackgroundModel::select('background.id')
                    ->where('background.status', 'Active');
        if ($whereRaw!='')
            $background = $background->whereRaw($whereRaw);
        $background = $background->count();
        return $this->_resJson($background);
    }

    function get_background_by_id($id)
    {
        $data = $this->BackgroundModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'background')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_background_last()
    {
        $data = $this->BackgroundModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function get_peluang_bisnis()
    {
        $PeluangBisnisModel = app($this->model_path.'PeluangBisnis');
        $peluang_bisnis = $PeluangBisnisModel::where('status', 'Active')
                ->where('display', true)
                ->get();
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserModel = app($this->model_path.'User');
        $PeluangBisnisCategoryModel = app($this->model_path.'PeluangBisnisCategory');
        foreach ($peluang_bisnis as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'peluang_bisnis')
                    ->get()
                    ->first();
            $v->image = $image;
            $user = $UserModel::where('id', $v->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $peluang_bisnis_category = $PeluangBisnisCategoryModel::where('id', $v->peluang_bisnis_category_id)
                                        ->get()
                                        ->first();
            $v->category = $peluang_bisnis_category;
        }
        return $this->_resJson($peluang_bisnis);
    }

    function insert_background($post)
    {
        $data = $this->BackgroundModel::insert($post);
        return $this->_resJson($data);
    }

    function update_background($post)
    {
        $data = $this->BackgroundModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_background($id)
    {
        $data = $this->BackgroundModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_background($id)
    {
        $data = $this->BackgroundModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
