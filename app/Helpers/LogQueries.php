<?php

namespace App\Helpers;

use Request;
use App\Models\mongodb\LogQueries as LogQueriesModel;

class LogQueries
{
    public static function insert_log_queries($param)
    {
    	$log = [];
    	$log['message'] = $param['message'];
        $log['request'] = json_encode(Request::input());
        $log['response'] = json_encode($param['response']);
        $log['path'] = Request::path();
        $log['log'] = $param['log'];
    	$log['session_key'] = isset($param['session_key'])?$param['session_key']:'client';
    	LogQueriesModel::create($log);
    }

    public static function get_log_queries()
    {
    	return LogQueriesModel::latest()->get();
    }
}