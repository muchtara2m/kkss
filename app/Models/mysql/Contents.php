<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Contents extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'contents';

	protected $fillable = [
		'value_string',
		'value_text',
		'favcolor',
		'fontsize',
		'icon',
		'description',
		'display'
	];
}
