<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class EventsRegister extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'events_register';

	protected $fillable = [
		'events_id',
		'user_id',
		'name',
		'email',
		'phone',
		'job_title',
		'address',
		'is_status'
	];
}
