<?php

namespace App\Queries\web\admin;

class MembershipQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_user_datatable($whereRaw, $order, $offset, $limit)
    {
        $user = $this->UserModel::select(
                        'user.*',
                        'profession.name as profession_$_name'
                    )
                    ->join('user_profile', 'user_profile.user_id', 'user.id')
                    ->join('profession', 'profession.id', 'user_profile.profession_id')
                    ->where('user.status', 'Active');
        if ($whereRaw!='')
            $user = $user->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'user.'.$order['coloumn'];
        $user = $user->orderBy($order_column, $order['dir'])
                    ->orderBy('user.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($user as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'user')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->login_date = date('d M Y, (H:i)', strtotime($value->login));
            $value->str_id = $value->id;
        }
        return $this->_resJson($user);
    }

    function get_user_total($whereRaw)
    {
        $user = $this->UserModel::select('user.id')
                    ->where('user.status', 'Active');
        if ($whereRaw!='')
            $user = $user->whereRaw($whereRaw);
        $user = $user->count();
        return $this->_resJson($user);
    }

    function get_user_by_id($id)
    {
        $data = $this->UserModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Add param additional
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
            // Get UserProfile
            $user_profile = $this->get_user_profile_by_user_id($data->id);
            $data->profile = $user_profile;
            $village = $this->get_village_by_id($data->profile->village_id);
            $data->profile->district_id = $village->district_id;
            $district = $this->get_district_by_id($data->profile->district_id);
            $data->profile->city_id = $district->city_id;
            $city = $this->get_city_by_id($data->profile->city_id);
            $data->profile->province_id = $city->province_id;
            $province = $this->get_province_by_id($data->profile->province_id);
            $data->profile->country_id = $province->country_id;
            // Get Image
            $image = $this->get_image_by_param_id($data->id);
            $data->image = $image;
        }
        return $this->_resJson($data);
    }

    function get_user_by_email($email, $id=null)
    {
        $user = $this->UserModel::select()
                    ->where('email', $email);
        if ($id) $user = $user->where('id', '<>', $id);
        $user = $user->get()
                ->first();
        return $this->_resJson($user);
    }

    function get_user_profile_by_user_id($user_id)
    {
        // Define Model
        $UserProfileModel = app($this->model_path.'UserProfile');
        // Get User Profile
        $user_profile = $UserProfileModel::select()
                    ->where('user_id', $user_id)
                    ->get()
                    ->first();
        return $this->_resJson($user_profile);
    }

    function get_country()
    {
        // Define Model Country
        $CountryModel = app($this->model_path.'Country');
        // Get Country
        $country = $CountryModel::select()
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($country);
    }

    function get_province_by_id($id)
    {
        // Define Model Province
        $ProvinceModel = app($this->model_path.'Province');
        // Get Province
        $province = $ProvinceModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($province);
    }

    function get_city_by_id($id)
    {
        // Define Model City
        $CityModel = app($this->model_path.'City');
        // Get City
        $city = $CityModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($city);
    }

    function get_district_by_id($id)
    {
        // Define Model District
        $DistrictModel = app($this->model_path.'District');
        // Get District
        $district = $DistrictModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($district);
    }

    function get_village_by_id($id)
    {
        // Define Model Village
        $VillageModel = app($this->model_path.'Village');
        // Get Village
        $village = $VillageModel::select()
                    ->where('id', $id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get()
                    ->first();
        return $this->_resJson($village);
    }

    function get_image_by_param_id($param_id)
    {
        // Define Model
        $ImageModel = app($this->model_path.'Image');
        // Get Image
        $image = $ImageModel::where('description', 'member')
                    ->where('param_id', $param_id)
                    ->get()
                    ->first();
        return $this->_resJson($image);
    }

    function get_user_last()
    {
        $data = $this->UserModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function insert_user($post)
    {
        $data = $this->UserModel::create($post);
        return $this->_resJson($data);
    }

    function insert_user_profile($post)
    {
        // Define Model
        $UserProfileModel = app($this->model_path.'UserProfile');
        // Insert User Profile
        $user_profile = $UserProfileModel::create($post);
        return $this->_resJson($user_profile);
    }

    function insert_user_verified($post)
    {
        // Define Model
        $UserVerifiedModel = app($this->model_path.'UserVerified');
        // Insert User Verified
        $user_verified = $UserVerifiedModel::insert($post);
        return $this->_resJson($user_verified);
    }

    function update_user($post)
    {
        $data = $this->UserModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function update_user_profile($post)
    {
        // Define Model
        $UserProfileModel = app($this->model_path.'UserProfile');
        // Update User Profile
        $user_profile = $UserProfileModel::where('user_id', $post['user_id'])
                ->update($post);
        return $this->_resJson($user_profile);
    }

    function delete_user($id)
    {
        $data = $this->UserModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_user($id)
    {
        $data = $this->UserModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
