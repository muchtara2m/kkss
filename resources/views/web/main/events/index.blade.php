@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Events</li>
            </ol>
        </div>
    </div>
</section>
<section id="cart_items" class="mg-50">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Filter</h2>
                    <div class="brands_products">
                        <div class="panel-body" style="border: 1px solid #F7F7F0;margin-bottom: 20px;">
                            <form method="get" action="">
                                <div class="search_box">
                                    <input type="text" name="keyword" placeholder="Search" value="{{ $keyword }}"/>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="brands_products">
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($events_category as $row)
                                <li><a href="{{ url('events/kategori/'.$row->id) }}"> <span class="pull-right">({{ $row->total }})</span>{{ $row->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <h2 class="title text-center">Events</h2>
                @foreach($events as $row)
                    <div class="table-responsive cart_info">
                        <table class="table table-condensed">
                            <tbody>
                                <tr style="border-bottom: none;">
                                    <td class="cart_description" colspan="3" style="text-align: center; width: 100%;padding-top: 25px; padding-bottom: 15px;">
                                    <div class="container" style="background: #f1f1f1; width: 95%; padding-top: 10px;">
                                        <div class="">
                                            <div class="col-sm-3" style="text-align: left;">
                                                <h6 class="text-muted">Nama Event:</h6>
                                                <h5><strong>{{ $row->name }}</strong></h5>
                                            </div>
                                            <div class="col-sm-3" style="text-align: left;">
                                                <h6 class="text-muted">Tanggal Event:</h6>
                                                <h5><strong>{{ $row->date }}</strong></h5>
                                            </div>
                                            <div class="col-sm-3" style="text-align: left;">
                                                <h6 class="text-muted">Lokasi:</h6>
                                                <h5><strong>{{ $row->location }}</strong></h5>
                                            </div>
                                            <div class="col-sm-3" style="text-align: left;">
                                                <h6 class="text-muted">PIC:</h6>
                                                <h5><strong>{{ $row->pic }}</strong></h5>
                                            </div>
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="cart_description" style="width: 20%; text-align: right;">
                                        <img src="{{ $row->image_url }}" alt="image" style="width: 130px; height: 80px;">
                                    </td>
                                    <td class="cart_description" style="width: 60%;">
                                        <h4><a href="{{ url($sView.'/'.$row->seo) }}" style="color: #dc3545;">{{ $row->name }}</a></h4>
                                        <p title="{{ $row->short_description }}" style="min-height: 55px;">{{ $row->short_desc }}</p>
                                    </td>
                                    <td class="cart_price" style="width: 20%; text-align: {{ !$row->is_register?'right':'left' }}; padding-right: 3%;">
                                        @if(!$row->is_register)
                                        <a class="btn btn-primary" href="{{ url($sView.'/register/'.$row->seo) }}" style="width: 100%;">Register</a>
                                        @else
                                        <small>Kamu sudah pernah mendaftar</small>
                                        <a href="{{ url($sView.'/'.$row->seo) }}">Lihat Detail</a>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endforeach
                <div class="text-center">
                    {!! $events->render() !!}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
