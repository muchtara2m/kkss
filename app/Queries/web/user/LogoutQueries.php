<?php

namespace App\Queries\web\user;

class LogoutQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $UserModel = app($this->model_path.'User');
        $this->UserModel = $UserModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    public function get_user_by_id($id)
    {
        $data = $this->UserModel::where('id', $id)
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    public function update_user($post)
    {
        $data = $this->UserModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    public function update_token($session_key)
    {
        $TokenModel = app($this->model_path.'Token');
        $data = $TokenModel::where('session_key', $session_key)
                ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }
}
