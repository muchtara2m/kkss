<?php

$router->group([
    'middleware' => 'role.admin',
    'prefix'     => 'admin'
], function () use ($router) {
	$router->get('', 'error\NotFoundController@index')->name('admin');
    $router->group([
            'prefix'     => 'account'
        ], function () use ($router) {
            $router->get('', 'web\admin\AccountController@index')->name('admin/account');
            $router->post('/post', 'web\admin\AccountController@post')->name('admin/account/post');
    });
    $router->group([
            'prefix'     => 'dashboard'
        ], function () use ($router) {
            $router->get('', 'web\admin\DashboardController@index')->name('admin/dashboard');
    });
    $router->group([
            'prefix'     => 'contents'
        ], function () use ($router) {
            $router->get('', 'error\NotFoundController@index')->name('admin/contents');
        	$router->group([
		            'prefix'     => 'logo'
		        ], function () use ($router) {
		            $router->get('', 'web\admin\contents\LogoController@index')->name('admin/contents/logo');
		            $router->post('/post', 'web\admin\contents\LogoController@post')->name('admin/contents/logo/post');
		    });
		    $router->group([
		            'prefix'     => 'favicon'
		        ], function () use ($router) {
		            $router->get('', 'web\admin\contents\FaviconController@index')->name('admin/contents/favicon');
		            $router->post('/post', 'web\admin\contents\FaviconController@post')->name('admin/contents/favicon/post');
		    });
            $router->group([
                    'prefix'     => 'title'
                ], function () use ($router) {
                    $router->get('', 'web\admin\contents\TitleController@index')->name('admin/contents/title');
                    $router->post('/post', 'web\admin\contents\TitleController@post')->name('admin/contents/title/post');
            });
            $router->group([
                    'prefix'     => 'contact'
                ], function () use ($router) {
                    $router->get('', 'web\admin\contents\ContactController@index')->name('admin/contents/contact');
                    $router->post('/post', 'web\admin\contents\ContactController@post')->name('admin/contents/contact/post');
            });
            $router->group([
                    'prefix'     => 'socialmedia'
                ], function () use ($router) {
                    $router->get('', 'web\admin\contents\SocialMediaController@index')->name('admin/contents/socialmedia');
                    $router->post('/post', 'web\admin\contents\SocialMediaController@post')->name('admin/contents/socialmedia/post');
            });
    });
    $router->group([
            'prefix'     => 'master'
        ], function () use ($router) {
            $router->get('', 'error\NotFoundController@index')->name('admin/master');
            $router->group([
                    'prefix'     => 'regiontype'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\RegionTypeController@index')->name('admin/master/regiontype');
                    $router->get('/form', 'web\admin\master\RegionTypeController@form')->name('admin/master/regiontype/form');
                    $router->get('/form/{str_id}', 'web\admin\master\RegionTypeController@form')->name('admin/master/regiontype/form/id');
                    $router->get('/get/datatable', 'web\admin\master\RegionTypeController@get_datatable')->name('admin/master/regiontype/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\RegionTypeController@get_data_by_id')->name('admin/master/regiontype/get/data/id');
                    $router->post('/post', 'web\admin\master\RegionTypeController@post')->name('admin/master/regiontype/post');
                    $router->post('/delete', 'web\admin\master\RegionTypeController@delete')->name('admin/master/regiontype/delete');
            });
            $router->group([
                    'prefix'     => 'country'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\CountryController@index')->name('admin/master/country');
                    $router->get('/form', 'web\admin\master\CountryController@form')->name('admin/master/country/form');
                    $router->get('/form/{str_id}', 'web\admin\master\CountryController@form')->name('admin/master/country/form/id');
                    $router->get('/get/datatable', 'web\admin\master\CountryController@get_datatable')->name('admin/master/country/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\CountryController@get_data_by_id')->name('admin/master/country/get/data/id');
                    $router->post('/post', 'web\admin\master\CountryController@post')->name('admin/master/country/post');
                    $router->post('/delete', 'web\admin\master\CountryController@delete')->name('admin/master/country/delete');
            });
            $router->group([
                    'prefix'     => 'province'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\ProvinceController@index')->name('admin/master/province');
                    $router->get('/form', 'web\admin\master\ProvinceController@form')->name('admin/master/province/form');
                    $router->get('/form/{str_id}', 'web\admin\master\ProvinceController@form')->name('admin/master/province/form/id');
                    $router->get('/get/datatable', 'web\admin\master\ProvinceController@get_datatable')->name('admin/master/province/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\ProvinceController@get_data_by_id')->name('admin/master/province/get/data/id');
                    $router->post('/post', 'web\admin\master\ProvinceController@post')->name('admin/master/province/post');
                    $router->post('/delete', 'web\admin\master\ProvinceController@delete')->name('admin/master/province/delete');
            });
            $router->group([
                    'prefix'     => 'city'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\CityController@index')->name('admin/master/city');
                    $router->get('/form', 'web\admin\master\CityController@form')->name('admin/master/city/form');
                    $router->get('/form/{str_id}', 'web\admin\master\CityController@form')->name('admin/master/city/form/id');
                    $router->get('/get/datatable', 'web\admin\master\CityController@get_datatable')->name('admin/master/city/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\CityController@get_data_by_id')->name('admin/master/city/get/data/id');
                    $router->post('/post', 'web\admin\master\CityController@post')->name('admin/master/city/post');
                    $router->post('/delete', 'web\admin\master\CityController@delete')->name('admin/master/city/delete');
            });
            $router->group([
                    'prefix'     => 'district'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\DistrictController@index')->name('admin/master/district');
                    $router->get('/form', 'web\admin\master\DistrictController@form')->name('admin/master/district/form');
                    $router->get('/form/{str_id}', 'web\admin\master\DistrictController@form')->name('admin/master/district/form/id');
                    $router->get('/get/datatable', 'web\admin\master\DistrictController@get_datatable')->name('admin/master/district/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\DistrictController@get_data_by_id')->name('admin/master/district/get/data/id');
                    $router->post('/post', 'web\admin\master\DistrictController@post')->name('admin/master/district/post');
                    $router->get('/get/city/province/id/{param}', 'web\admin\master\DistrictController@get_city_by_province_id')->name('admin/master/district/get/city/province/id');
                    $router->post('/delete', 'web\admin\master\DistrictController@delete')->name('admin/master/district/delete');
            });
            $router->group([
                    'prefix'     => 'village'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\VillageController@index')->name('admin/master/village');
                    $router->get('/form', 'web\admin\master\VillageController@form')->name('admin/master/village/form');
                    $router->get('/form/{str_id}', 'web\admin\master\VillageController@form')->name('admin/master/village/form/id');
                    $router->get('/get/datatable', 'web\admin\master\VillageController@get_datatable')->name('admin/master/village/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\VillageController@get_data_by_id')->name('admin/master/village/get/data/id');
                    $router->post('/post', 'web\admin\master\VillageController@post')->name('admin/master/village/post');
                    $router->post('/delete', 'web\admin\master\VillageController@delete')->name('admin/master/village/delete');
            });
            $router->group([
                    'prefix'     => 'postalcode'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\PostalCodeController@index')->name('admin/master/postalcode');
                    $router->get('/form', 'web\admin\master\PostalCodeController@form')->name('admin/master/postalcode/form');
                    $router->get('/form/{str_id}', 'web\admin\master\PostalCodeController@form')->name('admin/master/postalcode/form/id');
                    $router->get('/get/datatable', 'web\admin\master\PostalCodeController@get_datatable')->name('admin/master/postalcode/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\PostalCodeController@get_data_by_id')->name('admin/master/postalcode/get/data/id');
                    $router->post('/post', 'web\admin\master\PostalCodeController@post')->name('admin/master/postalcode/post');
                    $router->post('/delete', 'web\admin\master\PostalCodeController@delete')->name('admin/master/postalcode/delete');
            });
            $router->group([
                    'prefix'     => 'continent'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\ContinentController@index')->name('admin/master/continent');
                    $router->get('/form', 'web\admin\master\ContinentController@form')->name('admin/master/continent/form');
                    $router->get('/form/{str_id}', 'web\admin\master\ContinentController@form')->name('admin/master/continent/form/id');
                    $router->get('/get/datatable', 'web\admin\master\ContinentController@get_datatable')->name('admin/master/continent/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\ContinentController@get_data_by_id')->name('admin/master/continent/get/data/id');
                    $router->post('/post', 'web\admin\master\ContinentController@post')->name('admin/master/continent/post');
                    $router->post('/delete', 'web\admin\master\ContinentController@delete')->name('admin/master/continent/delete');
            });
            $router->group([
                    'prefix'     => 'forumcategory'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\ForumCategoryController@index')->name('admin/master/forumcategory');
                    $router->get('/form', 'web\admin\master\ForumCategoryController@form')->name('admin/master/forumcategory/form');
                    $router->get('/form/{str_id}', 'web\admin\master\ForumCategoryController@form')->name('admin/master/forumcategory/form/id');
                    $router->get('/get/datatable', 'web\admin\master\ForumCategoryController@get_datatable')->name('admin/master/forumcategory/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\ForumCategoryController@get_data_by_id')->name('admin/master/forumcategory/get/data/id');
                    $router->post('/post', 'web\admin\master\ForumCategoryController@post')->name('admin/master/forumcategory/post');
                    $router->post('/delete', 'web\admin\master\ForumCategoryController@delete')->name('admin/master/forumcategory/delete');
            });
            $router->group([
                    'prefix'     => 'eventscategory'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\EventsCategoryController@index')->name('admin/master/eventscategory');
                    $router->get('/form', 'web\admin\master\EventsCategoryController@form')->name('admin/master/eventscategory/form');
                    $router->get('/form/{str_id}', 'web\admin\master\EventsCategoryController@form')->name('admin/master/eventscategory/form/id');
                    $router->get('/get/datatable', 'web\admin\master\EventsCategoryController@get_datatable')->name('admin/master/eventscategory/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\EventsCategoryController@get_data_by_id')->name('admin/master/eventscategory/get/data/id');
                    $router->post('/post', 'web\admin\master\EventsCategoryController@post')->name('admin/master/eventscategory/post');
                    $router->post('/delete', 'web\admin\master\EventsCategoryController@delete')->name('admin/master/eventscategory/delete');
            });
            $router->group([
                    'prefix'     => 'tanggapdaruratcategory'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\TanggapDaruratCategoryController@index')->name('admin/master/tanggapdaruratcategory');
                    $router->get('/form', 'web\admin\master\TanggapDaruratCategoryController@form')->name('admin/master/tanggapdaruratcategory/form');
                    $router->get('/form/{str_id}', 'web\admin\master\TanggapDaruratCategoryController@form')->name('admin/master/tanggapdaruratcategory/form/id');
                    $router->get('/get/datatable', 'web\admin\master\TanggapDaruratCategoryController@get_datatable')->name('admin/master/tanggapdaruratcategory/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\TanggapDaruratCategoryController@get_data_by_id')->name('admin/master/tanggapdaruratcategory/get/data/id');
                    $router->post('/post', 'web\admin\master\TanggapDaruratCategoryController@post')->name('admin/master/tanggapdaruratcategory/post');
                    $router->post('/delete', 'web\admin\master\TanggapDaruratCategoryController@delete')->name('admin/master/tanggapdaruratcategory/delete');
            });
            $router->group([
                    'prefix'     => 'gallerycategory'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\GalleryCategoryController@index')->name('admin/master/gallerycategory');
                    $router->get('/form', 'web\admin\master\GalleryCategoryController@form')->name('admin/master/gallerycategory/form');
                    $router->get('/form/{str_id}', 'web\admin\master\GalleryCategoryController@form')->name('admin/master/gallerycategory/form/id');
                    $router->get('/get/datatable', 'web\admin\master\GalleryCategoryController@get_datatable')->name('admin/master/gallerycategory/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\GalleryCategoryController@get_data_by_id')->name('admin/master/gallerycategory/get/data/id');
                    $router->post('/post', 'web\admin\master\GalleryCategoryController@post')->name('admin/master/gallerycategory/post');
                    $router->post('/delete', 'web\admin\master\GalleryCategoryController@delete')->name('admin/master/gallerycategory/delete');
            });
            $router->group([
                    'prefix'     => 'peluangbisniscategory'
                ], function () use ($router) {
                    $router->get('', 'web\admin\master\PeluangBisnisCategoryController@index')->name('admin/master/peluangbisniscategory');
                    $router->get('/form', 'web\admin\master\PeluangBisnisCategoryController@form')->name('admin/master/peluangbisniscategory/form');
                    $router->get('/form/{str_id}', 'web\admin\master\PeluangBisnisCategoryController@form')->name('admin/master/peluangbisniscategory/form/id');
                    $router->get('/get/datatable', 'web\admin\master\PeluangBisnisCategoryController@get_datatable')->name('admin/master/peluangbisniscategory/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\master\PeluangBisnisCategoryController@get_data_by_id')->name('admin/master/peluangbisniscategory/get/data/id');
                    $router->post('/post', 'web\admin\master\PeluangBisnisCategoryController@post')->name('admin/master/peluangbisniscategory/post');
                    $router->post('/delete', 'web\admin\master\PeluangBisnisCategoryController@delete')->name('admin/master/peluangbisniscategory/delete');
            });
    });
    $router->group([
            'prefix'     => 'home'
        ], function () use ($router) {
            $router->group([
                    'prefix'     => 'background'
                ], function () use ($router) {
                    $router->get('', 'web\admin\home\BackgroundController@index')->name('admin/home/background');
                    $router->get('/form', 'web\admin\home\BackgroundController@form')->name('admin/home/background/form');
                    $router->get('/form/{str_id}', 'web\admin\home\BackgroundController@form')->name('admin/home/background/form/id');
                    $router->get('/get/datatable', 'web\admin\home\BackgroundController@get_datatable')->name('admin/home/background/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\home\BackgroundController@get_data_by_id')->name('admin/home/background/get/data/id');
                    $router->post('/post', 'web\admin\home\BackgroundController@post')->name('admin/home/background/post');
                    $router->post('/delete', 'web\admin\home\BackgroundController@delete')->name('admin/home/background/delete');
            });
            $router->group([
                    'prefix'     => 'adatistiadat'
                ], function () use ($router) {
                    $router->get('', 'web\admin\home\AdatIstiadatController@index')->name('admin/home/adatistiadat');
                    $router->get('/form', 'web\admin\home\AdatIstiadatController@form')->name('admin/home/adatistiadat/form');
                    $router->get('/form/{str_id}', 'web\admin\home\AdatIstiadatController@form')->name('admin/home/adatistiadat/form/id');
                    $router->get('/get/datatable', 'web\admin\home\AdatIstiadatController@get_datatable')->name('admin/home/adatistiadat/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\home\AdatIstiadatController@get_data_by_id')->name('admin/home/adatistiadat/get/data/id');
                    $router->post('/post', 'web\admin\home\AdatIstiadatController@post')->name('admin/home/adatistiadat/post');
                    $router->post('/delete', 'web\admin\home\AdatIstiadatController@delete')->name('admin/home/adatistiadat/delete');
            });
            $router->group([
                    'prefix'     => 'strukturorganisasi'
                ], function () use ($router) {
                    $router->get('', 'web\admin\home\StrukturOrganisasiController@index')->name('admin/home/strukturorganisasi');
                    $router->get('/form', 'web\admin\home\StrukturOrganisasiController@form')->name('admin/home/strukturorganisasi/form');
                    $router->get('/form/{str_id}', 'web\admin\home\StrukturOrganisasiController@form')->name('admin/home/strukturorganisasi/form/id');
                    $router->get('/get/datatable', 'web\admin\home\StrukturOrganisasiController@get_datatable')->name('admin/home/strukturorganisasi/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\home\StrukturOrganisasiController@get_data_by_id')->name('admin/home/strukturorganisasi/get/data/id');
                    $router->post('/post', 'web\admin\home\StrukturOrganisasiController@post')->name('admin/home/strukturorganisasi/post');
                    $router->post('/delete', 'web\admin\home\StrukturOrganisasiController@delete')->name('admin/home/strukturorganisasi/delete');
            });
    });
    $router->group([
            'prefix'     => 'forum'
        ], function () use ($router) {
            $router->group([
                    'prefix'     => 'forum'
                ], function () use ($router) {
                    $router->get('', 'web\admin\forum\ForumController@index')->name('admin/forum/forum');
                    $router->get('/form', 'web\admin\forum\ForumController@form')->name('admin/forum/forum/form');
                    $router->get('/form/{str_id}', 'web\admin\forum\ForumController@form')->name('admin/forum/forum/form/id');
                    $router->get('/get/datatable', 'web\admin\forum\ForumController@get_datatable')->name('admin/forum/forum/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\forum\ForumController@get_data_by_id')->name('admin/forum/forum/get/data/id');
                    $router->post('/post', 'web\admin\forum\ForumController@post')->name('admin/forum/forum/post');
                    $router->post('/delete', 'web\admin\forum\ForumController@delete')->name('admin/forum/forum/delete');
            });
            $router->group([
                    'prefix'     => 'forumcategoryfollowers'
                ], function () use ($router) {
                    $router->get('', 'web\admin\forum\ForumCategoryFollowersController@index')->name('admin/forum/forumcategoryfollowers');
                    $router->get('/form', 'web\admin\forum\ForumCategoryFollowersController@form')->name('admin/forum/forumcategoryfollowers/form');
                    $router->get('/form/{str_id}', 'web\admin\forum\ForumCategoryFollowersController@form')->name('admin/forum/forumcategoryfollowers/form/id');
                    $router->get('/get/datatable', 'web\admin\forum\ForumCategoryFollowersController@get_datatable')->name('admin/forum/forumcategoryfollowers/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\forum\ForumCategoryFollowersController@get_data_by_id')->name('admin/forum/forumcategoryfollowers/get/data/id');
                    $router->post('/post', 'web\admin\forum\ForumCategoryFollowersController@post')->name('admin/forum/forumcategoryfollowers/post');
                    $router->post('/delete', 'web\admin\forum\ForumCategoryFollowersController@delete')->name('admin/forum/forumcategoryfollowers/delete');
            });

            // $router->get('', 'web\admin\ForumController@index')->name('admin/forum');
            // $router->get('/form', 'web\admin\ForumController@form')->name('admin/forum/form');
            // $router->get('/form/{str_id}', 'web\admin\ForumController@form')->name('admin/forum/form/id');
            // $router->get('/get/datatable', 'web\admin\ForumController@get_datatable')->name('admin/forum/get/datatable');
            // $router->get('/get/data/id/{param}', 'web\admin\ForumController@get_data_by_id')->name('admin/forum/get/data/id');
            // $router->post('/post', 'web\admin\ForumController@post')->name('admin/forum/post');
            // $router->post('/delete', 'web\admin\ForumController@delete')->name('admin/forum/delete');
    });
    $router->group([
            'prefix'     => 'events'
        ], function () use ($router) {
            $router->get('', 'web\admin\EventsController@index')->name('admin/events');
            $router->get('/form', 'web\admin\EventsController@form')->name('admin/events/form');
            $router->get('/form/{str_id}', 'web\admin\EventsController@form')->name('admin/events/form/id');
            $router->get('/get/datatable', 'web\admin\EventsController@get_datatable')->name('admin/events/get/datatable');
            $router->get('/get/data/id/{param}', 'web\admin\EventsController@get_data_by_id')->name('admin/events/get/data/id');
            $router->post('/post', 'web\admin\EventsController@post')->name('admin/events/post');
            $router->post('/delete', 'web\admin\EventsController@delete')->name('admin/events/delete');
            // 
            $router->get('/register/{events_id}', 'web\admin\EventsController@register')->name('admin/events/register/id');
            $router->get('/register/{events_id}/form', 'web\admin\EventsController@register_form')->name('admin/events/register/form');
            $router->get('/register/{events_id}/form/{id}', 'web\admin\EventsController@register_form')->name('admin/events/register/form/id');
            $router->get('/register/get/datatable/{events_id}', 'web\admin\EventsController@register_get_datatable')->name('admin/events/register/get/datatable');
            $router->get('/register/get/data/id/{param}', 'web\admin\EventsController@register_get_data_by_id')->name('admin/events/register/get/data/id');
            $router->post('/register/post', 'web\admin\EventsController@register_post')->name('admin/events/register/post');
            $router->post('/register/delete', 'web\admin\EventsController@register_delete')->name('admin/events/register/delete');
    });
    $router->group([
            'prefix'     => 'membership'
        ], function () use ($router) {
            $router->get('', 'web\admin\MembershipController@index')->name('admin/membership');
            $router->get('/form', 'web\admin\MembershipController@form')->name('admin/membership/form');
            $router->get('/form/{str_id}', 'web\admin\MembershipController@form')->name('admin/membership/form/id');
            $router->get('/get/datatable', 'web\admin\MembershipController@get_datatable')->name('admin/membership/get/datatable');
            $router->get('/get/data/id/{param}', 'web\admin\MembershipController@get_data_by_id')->name('admin/membership/get/data/id');
            $router->post('/post', 'web\admin\MembershipController@post')->name('admin/membership/post');
            $router->post('/delete', 'web\admin\MembershipController@delete')->name('admin/membership/delete');
    });
    $router->group([
            'prefix'     => 'peluangbisnis'
        ], function () use ($router) {
            $router->get('', 'web\admin\PeluangBisnisController@index')->name('admin/peluangbisnis');
            $router->get('/form', 'web\admin\PeluangBisnisController@form')->name('admin/peluangbisnis/form');
            $router->get('/form/{str_id}', 'web\admin\PeluangBisnisController@form')->name('admin/peluangbisnis/form/id');
            $router->get('/get/datatable', 'web\admin\PeluangBisnisController@get_datatable')->name('admin/peluangbisnis/get/datatable');
            $router->get('/get/data/id/{param}', 'web\admin\PeluangBisnisController@get_data_by_id')->name('admin/peluangbisnis/get/data/id');
            $router->post('/post', 'web\admin\PeluangBisnisController@post')->name('admin/peluangbisnis/post');
            $router->post('/delete', 'web\admin\PeluangBisnisController@delete')->name('admin/peluangbisnis/delete');
    });
    $router->group([
            'prefix'     => 'gallery'
        ], function () use ($router) {
            $router->get('', 'web\admin\GalleryController@index')->name('admin/gallery');
            $router->get('/form', 'web\admin\GalleryController@form')->name('admin/gallery/form');
            $router->get('/form/{str_id}', 'web\admin\GalleryController@form')->name('admin/gallery/form/id');
            $router->get('/get/datatable', 'web\admin\GalleryController@get_datatable')->name('admin/gallery/get/datatable');
            $router->get('/get/data/id/{param}', 'web\admin\GalleryController@get_data_by_id')->name('admin/gallery/get/data/id');
            $router->post('/post', 'web\admin\GalleryController@post')->name('admin/gallery/post');
            $router->post('/delete', 'web\admin\GalleryController@delete')->name('admin/gallery/delete');
    });
    $router->group([
            'prefix'     => 'tanggapdarurat'
        ], function () use ($router) {
            $router->get('', 'web\admin\TanggapDaruratController@index')->name('admin/tanggapdarurat');
            $router->get('/form', 'web\admin\TanggapDaruratController@form')->name('admin/tanggapdarurat/form');
            $router->get('/form/{str_id}', 'web\admin\TanggapDaruratController@form')->name('admin/tanggapdarurat/form/id');
            $router->get('/get/datatable', 'web\admin\TanggapDaruratController@get_datatable')->name('admin/tanggapdarurat/get/datatable');
            $router->get('/get/data/id/{param}', 'web\admin\TanggapDaruratController@get_data_by_id')->name('admin/tanggapdarurat/get/data/id');
            $router->post('/post', 'web\admin\TanggapDaruratController@post')->name('admin/tanggapdarurat/post');
            $router->post('/delete', 'web\admin\TanggapDaruratController@delete')->name('admin/tanggapdarurat/delete');
    });
    $router->group([
            'prefix'     => 'international'
        ], function () use ($router) {
            $router->group([
                    'prefix'     => 'tokoh'
                ], function () use ($router) {
                    $router->get('', 'web\admin\international\TokohController@index')->name('admin/international/tokoh');
                    $router->get('/form', 'web\admin\international\TokohController@form')->name('admin/international/tokoh/form');
                    $router->get('/form/{str_id}', 'web\admin\international\TokohController@form')->name('admin/international/tokoh/form/id');
                    $router->get('/get/datatable', 'web\admin\international\TokohController@get_datatable')->name('admin/international/tokoh/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\international\TokohController@get_data_by_id')->name('admin/international/tokoh/get/data/id');
                    $router->post('/post', 'web\admin\international\TokohController@post')->name('admin/international/tokoh/post');
                    $router->post('/delete', 'web\admin\international\TokohController@delete')->name('admin/international/tokoh/delete');
            });
            $router->group([
                    'prefix'     => 'representativeoffice'
                ], function () use ($router) {
                    $router->get('', 'web\admin\international\RepresentativeOfficeController@index')->name('admin/international/representativeoffice');
                    $router->get('/form', 'web\admin\international\RepresentativeOfficeController@form')->name('admin/international/representativeoffice/form');
                    $router->get('/form/{str_id}', 'web\admin\international\RepresentativeOfficeController@form')->name('admin/international/representativeoffice/form/id');
                    $router->get('/get/datatable', 'web\admin\international\RepresentativeOfficeController@get_datatable')->name('admin/international/representativeoffice/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\international\RepresentativeOfficeController@get_data_by_id')->name('admin/international/representativeoffice/get/data/id');
                    $router->post('/post', 'web\admin\international\RepresentativeOfficeController@post')->name('admin/international/representativeoffice/post');
                    $router->post('/delete', 'web\admin\international\RepresentativeOfficeController@delete')->name('admin/international/representativeoffice/delete');
            });
    });
    $router->group([
            'prefix'     => 'domestik'
        ], function () use ($router) {
            $router->group([
                    'prefix'     => 'tokoh'
                ], function () use ($router) {
                    $router->get('', 'web\admin\domestik\TokohController@index')->name('admin/domestik/tokoh');
                    $router->get('/form', 'web\admin\domestik\TokohController@form')->name('admin/domestik/tokoh/form');
                    $router->get('/form/{str_id}', 'web\admin\domestik\TokohController@form')->name('admin/domestik/tokoh/form/id');
                    $router->get('/get/datatable', 'web\admin\domestik\TokohController@get_datatable')->name('admin/domestik/tokoh/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\domestik\TokohController@get_data_by_id')->name('admin/domestik/tokoh/get/data/id');
                    $router->post('/post', 'web\admin\domestik\TokohController@post')->name('admin/domestik/tokoh/post');
                    $router->post('/delete', 'web\admin\domestik\TokohController@delete')->name('admin/domestik/tokoh/delete');
            });
            $router->group([
                    'prefix'     => 'representativeoffice'
                ], function () use ($router) {
                    $router->get('', 'web\admin\domestik\RepresentativeOfficeController@index')->name('admin/domestik/representativeoffice');
                    $router->get('/form', 'web\admin\domestik\RepresentativeOfficeController@form')->name('admin/domestik/representativeoffice/form');
                    $router->get('/form/{str_id}', 'web\admin\domestik\RepresentativeOfficeController@form')->name('admin/domestik/representativeoffice/form/id');
                    $router->get('/get/datatable', 'web\admin\domestik\RepresentativeOfficeController@get_datatable')->name('admin/domestik/representativeoffice/get/datatable');
                    $router->get('/get/data/id/{param}', 'web\admin\domestik\RepresentativeOfficeController@get_data_by_id')->name('admin/domestik/representativeoffice/get/data/id');
                    $router->post('/post', 'web\admin\domestik\RepresentativeOfficeController@post')->name('admin/domestik/representativeoffice/post');
                    $router->post('/delete', 'web\admin\domestik\RepresentativeOfficeController@delete')->name('admin/domestik/representativeoffice/delete');
            });
    });
});