<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPeluangBisnisTable20200419184800 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE peluang_bisnis Add peluang_bisnis_category_id Int(11) NULL;');
        DB::statement('ALTER TABLE peluang_bisnis ADD INDEX peluang_bisnis_category_id (peluang_bisnis_category_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
