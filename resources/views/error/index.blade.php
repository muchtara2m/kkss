<!DOCTYPE html>
<html lang="en">
    <head>
        @include('error/head')
    </head>
    <body>
        @include('error/header')
        @yield('body')
        @include('error/footer')
        @include('error/bottom')
    </body>
</html>
