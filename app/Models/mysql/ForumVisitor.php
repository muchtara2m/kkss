<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class ForumVisitor extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'forum_visitor';

	protected $fillable = [
		'forum_id',
		'user_id',
		'ip',
		'user_agent'
	];
}
