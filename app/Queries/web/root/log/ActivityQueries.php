<?php

namespace App\Queries\web\root\log;

class ActivityQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'root';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\mongodb';

        // Define Model Global
        $LogActivityModel = app($this->model_path.'\LogActivity');
        $this->LogActivityModel = $LogActivityModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_log_activity_datatable($whereRaw, $order, $offset, $limit)
    {
        $log_activity = $this->LogActivityModel::select();
        if ($whereRaw!='')
            $log_activity = $log_activity->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'log_activity.'.$order['coloumn'];
        $log_activity = $log_activity->orderBy($order_column, $order['dir'])
                    ->orderBy('log_activity._id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($log_activity as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($log_activity);
    }

    function get_log_activity_total($whereRaw)
    {
        $log_activity = $this->LogActivityModel::select();
        if ($whereRaw!='')
            $log_activity = $log_activity->whereRaw($whereRaw);
        $log_activity = $log_activity->count();
        return $this->_resJson($log_activity);
    }

    function get_log_activity_by_id($id)
    {
        $data = $this->LogActivityModel::where('_id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_log_activity_last()
	{
		$data = $this->LogActivityModel::orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}
}
