<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageTable20190326170000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('param_id')->index();
            $table->string('name', 255)->nullable();
            $table->string('ext', 255)->nullable();
            $table->string('size', 255)->nullable();
            $table->string('resolution', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->text('storage', 255)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
        });

        $data = [
            [
                'param_id' => 1,
                'name' => "favicon",
                'ext' => ".png",
                'size' => null,
                'resolution' => null,
                'description' => 'favicon'
            ],
            [
                'param_id' => 2,
                'name' => "logo",
                'ext' => ".png",
                'size' => null,
                'resolution' => null,
                'description' => 'logo'
            ],
        ];
        DB::table('image')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image');
    }
}
