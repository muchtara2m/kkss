<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMenuTable20200414175900 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            UPDATE menu SET link='#' where id = 26;
        ");

        DB::statement("
            INSERT INTO menu (name, link, parent_id, favicon, position, for_role) VALUES
            ('Tokoh', 'domestik/tokoh', 67, NULL, 1, 'admin'),
            ('Kantor Perwakilan', 'domestik/representativeoffice', 67, NULL, 2, 'admin'),
            ('Background', 'home/background', 26, NULL, 1, 'admin'),
            ('Adat Istiadat', 'home/adatistiadat', 26, NULL, 2, 'admin'),
            ('Struktur Organisasi', 'home/strukturorganisasi', 26, NULL, 3, 'admin');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
