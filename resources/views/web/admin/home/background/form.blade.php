@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Background</h1>
        <ol class="breadcrumb">
            <li># <a>Background</a></li>
            <li class="active">Form</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ ($background) ? 'Last updated : '.$background->updated_date : '' }}
            </span>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <h2 style="margin-top: -15px; font-size: 20px;">
            <label for="editor1">Form {{ $background?'Edit':'Add' }} Background</label>
        </h2>
        <div class="box">
            <div class="box-body" style="padding-top: 25px;">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group" {{ $background?'hidden':'' }}>
                                    <div class="row">
                                        <label class="col-md-2 text-right">Tipe Background *</label>
                                        <div class="col-md-6">
                                            <label>
                                                <input type="radio" name="type" class="flat-red" value="Image" required="" {{ $background?$background->type=='Image'?'checked=""':'':'' }}>
                                                &nbsp;&nbsp;
                                                Gambar
                                            </label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="radio" name="type" class="flat-red" value="Link" required="" {{ $background?$background->type=='Link'?'checked=""':'':'' }}>
                                                &nbsp;&nbsp;
                                                Link
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('type'))
                                                <span class="btn btn-danger">Tipe Background harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Nama *</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" id="name" class="form-control form-control-md" placeholder="Tulis disini.." value="{{ $background?$background->name:'' }}" autocomplete="off" required="">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Nama is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group div-link" {{ $background?$background->type=='Image'?'hidden':'':'' }}>
                                    <div class="row">
                                        <label class="col-md-2 text-right">Modul *</label>
                                        <div class="col-md-6">
                                            <select name="modul" id="modul" class="form-control form-control-md" {{ $background?$background->type=='Link'?'required=""':'':'' }}>
                                                <option value="" selected="" disabled="">Pilih modul</option>
                                                <option value="Event" {{ $background?$background->modul=='Events'?'selected':'':'' }}>Event</option>
                                                <option value="Peluang Bisnis" {{ $background?$background->modul=='Peluang Bisnis'?'selected':'':'' }}>Peluang Bisnis</option>
                                                <option value="Tanggap Darurat" {{ $background?$background->modul=='Tanggap Darurat'?'selected':'':'' }}>Tanggap Darurat</option>
                                            </select>
                                            <input type="hidden" id="param_id" value="{{ $background?$background->param_id:'' }}">
                                            <input type="hidden" id="modul_" value="{{ $background?$background->modul:'' }}">
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group div-link" {{ $background?$background->type=='Image'?'hidden':'':'' }}>
                                    <div class="row">
                                        <label class="col-md-2 text-right">Link *</label>
                                        <div class="col-md-6">
                                            <select name="link" id="link" class="form-control form-control-md" {{ $background?$background->type=='Link'?'required=""':'':'' }} style="width: 100%;"></select>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group div-image" {{ $background?$background->type=='Link'?'hidden':'':'' }}>
                                    <div class="row">
                                        <label class="col-md-2 text-right">Deskripsi</label>
                                        <div class="col-md-8">
                                            <textarea name="description" id="edit">{{ $background?$background->description:'' }}</textarea>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                </div>
                                <div class="form-group div-image" {{ $background?$background->type=='Link'?'hidden':'':'' }}>
                                    <div class="row">
                                        <label class="col-md-2 text-right">Gambar *</label>
                                        <div class="col-md-6">
                                            <p>Jenis file <small>(.jpeg/.jpg/.png)</small></p>
                                            <label class="u-file-attach-v2 g-color-gray-dark-v5 mb-20">
                                                <input type="file" name="file" id="file_attachment" class="file_attachment" {{ $background?'':'required=""' }}>
                                                <small style="color: red !important; display: none;" id="error_image">* mohon untuk melilih gambar tipe (.jpeg/.jpg/.png)</small>
                                                <img src="{{ $background?$background->thumb_url?$background->thumb_url:'':'' }}" id="preview_image" alt="Preview Image" style="width: 50%; {{ $background?$background->image_url?'margin-top: 15px;':'display: none':'display: none' }}">
                                                @if($background&&$background->image_url)
                                                <br /><a href="{{ $background->image_url }}" target="_blank" id="link_image">Klik untuk melihat gambar lebih besar</a>
                                                <input type="hidden" name="image_id" id="image_id" value="{{ $background?$background->image_id:'' }}">
                                                @endif
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('image'))
                                                <span class="btn btn-danger">Gambar harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Tampilkan *</label>
                                        <div class="col-md-6">
                                            <label>
                                                <input type="radio" name="display" class="flat-red" id="hide" value="false" required="" {{ $background?$background->display==0?'checked=""':'':'' }}>
                                                &nbsp;&nbsp;
                                                Tidak
                                            </label>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="radio" name="display" class="flat-red" id="view" value="true" required="" {{ $background?$background->display==1?'checked=""':'':'' }}>
                                                &nbsp;&nbsp;
                                                Iya
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('display'))
                                                <span class="btn btn-danger">Tampilkan harus dipilih</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="str_id" id="str_id" value="{{ $background?$background->str_id:'' }}">
                                            <button type="submit" class="btn btn-primary btn-class" style="border-radius: 0;" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default" style="border-radius: 0;"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop