<?php

namespace App\Queries\web\admin\master;

class PeluangBisnisCategoryQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $PeluangBisnisCategoryModel = app($this->model_path.'PeluangBisnisCategory');
        $this->PeluangBisnisCategoryModel = $PeluangBisnisCategoryModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_peluang_bisnis_category_datatable($whereRaw, $order, $offset, $limit)
    {
        $peluang_bisnis_category = $this->PeluangBisnisCategoryModel::select(
                        'peluang_bisnis_category.*'
                    )
                    ->where('peluang_bisnis_category.status', 'Active');
        if ($whereRaw!='')
            $peluang_bisnis_category = $peluang_bisnis_category->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'peluang_bisnis_category.'.$order['coloumn'];
        $peluang_bisnis_category = $peluang_bisnis_category->orderBy($order_column, $order['dir'])
                    ->orderBy('peluang_bisnis_category.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($peluang_bisnis_category as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($peluang_bisnis_category);
    }

    function get_peluang_bisnis_category_total($whereRaw)
    {
        $peluang_bisnis_category = $this->PeluangBisnisCategoryModel::select('peluang_bisnis_category.id')
                    ->where('peluang_bisnis_category.status', 'Active');
        if ($whereRaw!='')
            $peluang_bisnis_category = $peluang_bisnis_category->whereRaw($whereRaw);
        $peluang_bisnis_category = $peluang_bisnis_category->count();
        return $this->_resJson($peluang_bisnis_category);
    }

    function get_peluang_bisnis_category_by_id($id)
    {
        $data = $this->PeluangBisnisCategoryModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_peluang_bisnis_category_last()
	{
		$data = $this->PeluangBisnisCategoryModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_peluang_bisnis_category($post)
	{
		$data = $this->PeluangBisnisCategoryModel::insert($post);
		return $this->_resJson($data);
	}

	function update_peluang_bisnis_category($post)
	{
		$data = $this->PeluangBisnisCategoryModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_peluang_bisnis_category($id)
	{
		$data = $this->PeluangBisnisCategoryModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_peluang_bisnis_category($id)
	{
		$data = $this->PeluangBisnisCategoryModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
