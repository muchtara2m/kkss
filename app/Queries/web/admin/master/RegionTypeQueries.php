<?php

namespace App\Queries\web\admin\master;

class RegionTypeQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $RegionTypeModel = app($this->model_path.'RegionType');
        $this->RegionTypeModel = $RegionTypeModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_region_type_datatable($whereRaw, $order, $offset, $limit)
    {
        $region_type = $this->RegionTypeModel::select(
                        'region_type.*'
                    )
                    ->where('region_type.status', 'Active');
        if ($whereRaw!='')
            $region_type = $region_type->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'region_type.'.$order['coloumn'];
        $region_type = $region_type->orderBy($order_column, $order['dir'])
                    ->orderBy('region_type.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($region_type as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($region_type);
    }

    function get_region_type_total($whereRaw)
    {
        $region_type = $this->RegionTypeModel::select('region_type.id')
                    ->where('region_type.status', 'Active');
        if ($whereRaw!='')
            $region_type = $region_type->whereRaw($whereRaw);
        $region_type = $region_type->count();
        return $this->_resJson($region_type);
    }

    function get_region_type_by_id($id)
    {
        $data = $this->RegionTypeModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_region_type_last()
	{
		$data = $this->RegionTypeModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_region_type($post)
	{
		$data = $this->RegionTypeModel::insert($post);
		return $this->_resJson($data);
	}

	function update_region_type($post)
	{
		$data = $this->RegionTypeModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_region_type($id)
	{
		$data = $this->RegionTypeModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_region_type($id)
	{
		$data = $this->RegionTypeModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
