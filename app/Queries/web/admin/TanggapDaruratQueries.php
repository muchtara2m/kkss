<?php

namespace App\Queries\web\admin;

class TanggapDaruratQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $TanggapDaruratModel = app($this->model_path.'TanggapDarurat');
        $this->TanggapDaruratModel = $TanggapDaruratModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_tanggap_darurat_datatable($whereRaw, $order, $offset, $limit)
    {
        $tanggap_darurat = $this->TanggapDaruratModel::select(
                        'tanggap_darurat.*', 
                        'tanggap_darurat_category.id as tanggap_darurat_category_$_id',
                        'tanggap_darurat_category.name as tanggap_darurat_category_$_name'
                    )
                    ->join('tanggap_darurat_category', 'tanggap_darurat_category.id', 'tanggap_darurat.tanggap_darurat_category_id')
                    ->where('tanggap_darurat.status', 'Active');
        if ($whereRaw!='')
            $tanggap_darurat = $tanggap_darurat->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'tanggap_darurat.'.$order['coloumn'];
        $tanggap_darurat = $tanggap_darurat->orderBy($order_column, $order['dir'])
                    ->orderBy('tanggap_darurat.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($tanggap_darurat as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'tanggap_darurat')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->date = date('d M Y', strtotime($value->date));
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($tanggap_darurat);
    }

    function get_tanggap_darurat_total($whereRaw)
    {
        $tanggap_darurat = $this->TanggapDaruratModel::select('tanggap_darurat.id')
                    ->where('tanggap_darurat.status', 'Active');
        if ($whereRaw!='')
            $tanggap_darurat = $tanggap_darurat->whereRaw($whereRaw);
        $tanggap_darurat = $tanggap_darurat->count();
        return $this->_resJson($tanggap_darurat);
    }

    function get_tanggap_darurat_by_id($id)
    {
        $data = $this->TanggapDaruratModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'tanggap_darurat')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_tanggap_darurat_last()
    {
        $data = $this->TanggapDaruratModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function get_tanggap_darurat_category()
    {
        // Define Model
        $TanggapDaruratCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\TanggapDaruratCategory');
        // Get Data
        $category_blog = $TanggapDaruratCategoryModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $category_blog;
    }

    function insert_tanggap_darurat($post)
    {
        $data = $this->TanggapDaruratModel::insert($post);
        return $this->_resJson($data);
    }

    function update_tanggap_darurat($post)
    {
        $data = $this->TanggapDaruratModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_tanggap_darurat($id)
    {
        $data = $this->TanggapDaruratModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_tanggap_darurat($id)
    {
        $data = $this->TanggapDaruratModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
