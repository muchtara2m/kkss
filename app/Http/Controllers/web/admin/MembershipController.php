<?php

namespace App\Http\Controllers\web\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\MembershipQueries;

class MembershipController extends \App\Http\Core\WebController 
{
    // Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'membership';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'membership';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, MembershipQueries $queries_membership)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_membership = $queries_membership;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Membership';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_membership->get_user_last();
        $data['last'] = $last;

        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('membership', $_GET);
        // Get Data
        $data = $this->queries_membership->get_user_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        foreach ($data as $r) {
            $r->image_url = $r->image?\ImageUrl::url($this->sView.'/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url($this->sView.'/thumbs/', $r->image):null;
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_membership->get_user_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_membership->get_user_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Membership';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'form';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'form';

        /* Collection variable data */
        // Get Data membership
        $membership = null;
        if ($str_id) {
            $membership = $this->queries_membership->get_user_by_id($str_id);
            $membership->image_id = $membership->image?$membership->image->id:null;
            $membership->image_url = $membership->image?\ImageUrl::url('member/images/', $membership->image):null;
            $membership->thumb_url = $membership->image?\ImageUrl::url('member/thumbs/', $membership->image):null;
        }
        $data['membership'] = $membership;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            // 'village' => 'required',
            'profession' => 'required|max:255',
            'email' => 'required|email|max:255',
            'type' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');
        $phone = $request->input('phone');
        $address = $request->input('address');
        $village_id = $request->input('village');
        $profession_id = $request->input('profession');
        $email = $request->input('email');
        $password = $request->input('password');
        $confirm_password = $request->input('confirm_password');
        $type = $request->input('type');
        if ($type=='International') $village_id = null;
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Password
        if ($password) {
            // Check Confirm Password
            if ($password != $confirm_password) {
                Session::flash('post', 'Error');
                Session::flash('message', 'Password Confirm Not Match!');
                return $this->_redirect($this->global_path);
            }
        }

        // Process Query
        if (!$id) {
            // Check Email
            $check_email = $this->queries_membership->get_user_by_email($email);
            if ($check_email) {
                Session::flash('post', 'Error');
                Session::flash('message', 'Email Already Exist!');
                return $this->_redirect($this->global_path);
            }

            // Insert User
            $post_user = [];
            $post_user['name'] = $name;
            $post_user['phone'] = $phone;
            $post_user['email'] = $email;
            $post_user['password'] = Hash::make($password);
            $post_user['level'] = 'member';
            $post_user['type'] = $type;
            $user = $this->queries_membership->insert_user($post_user);
            // Insert User Profile
            $post_user_profile = [];
            $post_user_profile['user_id'] = $user->id;
            $post_user_profile['address'] = $address;
            $post_user_profile['village_id'] = $village_id;
            $post_user_profile['profession_id'] = $profession_id;
            $this->queries_membership->insert_user_profile($post_user_profile);
            // Insert User Verified
            $post_user_verified = [];
            $post_user_verified['user_id'] = $user->id;
            $post_user_verified['verified_code'] = '00000000';
            $post_user_verified['is_verified'] = 1;
            $post_user_verified['status'] = 'Inactive';
            $this->queries_membership->insert_user_verified($post_user_verified);
            $_param_id = $user->id;
        } else {
            // Check Email
            $check_email = $this->queries_membership->get_user_by_email($email, $id);
            if ($check_email) {
                Session::flash('post', 'Error');
                Session::flash('message', 'Email Already Exist!');
                return $this->_redirect($this->global_path);
            }

            // Insert User
            $post_user = [];
            $post_user['id'] = $id;
            $post_user['name'] = $name;
            $post_user['email'] = $email;
            $post_user['updated_at'] = date('Y-m-d H:i:s');
            if ($password)
                $post_user['password'] = Hash::make($password);
            $post_user['type'] = $type;
            // echo json_encode($post_user); die();
            $this->queries_membership->update_user($post_user);
            // Insert User Profile
            $post_user_profile = [];
            $post_user_profile['user_id'] = $id;
            $post_user_profile['address'] = $address;
            $post_user_profile['village_id'] = $village_id;
            $post_user_profile['profession_id'] = $profession_id;
            $post_user_profile['updated_at'] = date('Y-m-d H:i:s');
            $this->queries_membership->update_user_profile($post_user_profile);
            $_param_id = $id;
        }

        // Proccess Upload
        if ($file) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'member'; // Set Description for DB Image
            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
            $path = 'member'; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload) {
                Session::flash('post', 'Error');
                Session::flash('message', 'Failed Upload Image!');
                return $this->_redirect($this->global_path);
            }
        }

        // Set Session Flash
        Session::flash('post', 'Success');
        Session::flash('message', 'Data was successfully created!.');

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        // Delete
        // $_post = $this->queries_membership->destroy_user($id);
        // Soft Delete
        $_post = $this->queries_membership->delete_user($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }
}
