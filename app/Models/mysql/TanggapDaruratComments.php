<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class TanggapDaruratComments extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'tanggap_darurat_comments';

	protected $fillable = [
		'tanggap_darurat_id',
		'parent_id',
		'user_id',
		'name',
		'email',
		'phone',
		'message'
	];
}
