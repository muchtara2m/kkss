@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Peluang Bisnis</li>
            </ol>
        </div>
    </div>
</section>
<section class="mg-50">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Filter</h2>
                    <div class="brands_products">
                        <div class="panel-body" style="border: 1px solid #F7F7F0;margin-bottom: 20px;">
                           <form method="get" action="">
                                <div class="search_box">
                                    <input type="text" name="keyword" placeholder="Search" value="{{ $keyword }}"/>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="brands_products">
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                                @foreach($peluang_bisnis_category as $row)
                                <li><a href="{{ url('peluangbisnis/kategori/'.$row->id) }}"> <span class="pull-right">({{ $row->total }})</span>{{ $row->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="blog-post-area">
                    <h2 class="title text-center">Peluang Bisnis</h2>
                    @foreach($peluang_bisnis as $row)
                        <div class="single-blog-post" style="margin-bottom: 50px;">
                            <h3>{{ $row->name }}</h3>
                            <div class="post-meta">
                                <ul>
                                    <li><i class="fa fa-tag"></i> {{ $row->category->name }}</li>
                                    <li><i class="fa fa-user"></i> {{ $row->user->name }}</li>
                                    <li><i class="fa fa-calendar"></i> {{ $row->created_date }}</li>
                                    <li><i class="fa fa-comments"></i> {{ $row->comments }} Komentar</li>
                                </ul>
                            </div>
                            <p>
                                <img src="{{ $row->image_url }}" alt="image" style="float: left; margin-right: 20px; max-width: 150px;">
                                {{ $row->short_description }}
                            </p>
                            <a class="btn btn-sm btn-primary" href="{{ url($sView.'/'.$row->seo) }}">Selengkapnya</a>
                        </div>
                    @endforeach
                    <div class="text-center">
                        {!! $peluang_bisnis->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
