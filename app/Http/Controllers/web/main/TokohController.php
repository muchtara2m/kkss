<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\TokohQueries;

class TokohController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'tokoh';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'tokoh';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, TokohQueries $queries_tokoh)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_tokoh = $queries_tokoh;
	}

    public function index()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Tokoh';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		//
		
		// Render to View
        return $this->_render($data);
    }
}
