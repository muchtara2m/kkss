<meta charset="UTF-8">
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{$vTitle}} | Root</title>
<link rel="shortcut icon" type="image/png" href="{{ $vContents['favicon']['image_url'] }}">
<link rel="stylesheet" href="{{ asset('assets/'.$pView.'/css/templatemo_main.css') }}">
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript">
	const _token = '{{ csrf_token() }}'
</script>