<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Continent extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'continent';

	protected $fillable = [
		'name'
	];
}
