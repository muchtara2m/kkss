<?php

namespace App\Http\Controllers\error;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\error\NotFoundQueries;

class NotFoundController extends \App\Http\Core\ErrorController 
{
    // Parent Folder in views
    protected $pView = 'notfound';

    // Sub Folder (Child) in views
    protected $sView = '';
    
    function __construct(ControllerQueries $queries_controller, NotFoundQueries $queries_not_found)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_not_found = $queries_not_found;
    }

    public function index($message=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Page Not Found!';
        // Filename in folder view
        $data['cView'] = 'index';

        /* Collection variable data */
        if ($message)
            $data['vTitle'] = 'Error!';
        $data['message'] = $message;
        
        // Render to View
        return $this->_error($data);
    }
}
