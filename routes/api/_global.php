<?php

$router->group([
		'middleware' => 'oauth.user',
        'prefix'     => '',
    ], function () use ($router) {
		$router->get('/get/user', 'api\_GlobalController@get_user')->name('api/get/user');
		$router->get('/get/country', 'api\_GlobalController@get_country')->name('api/get/country');
		$router->get('/get/province/{country_id}', 'api\_GlobalController@get_province_by_country_id')->name('api/get/province/country_id');
		$router->get('/get/city/{province_id}', 'api\_GlobalController@get_city_by_province_id')->name('api/get/city/province_id');
		$router->get('/get/district/{city_id}', 'api\_GlobalController@get_district_by_city_id')->name('api/get/district/city_id');
		$router->get('/get/village/{district_id}', 'api\_GlobalController@get_village_by_district_id')->name('api/get/village/district_id');
});