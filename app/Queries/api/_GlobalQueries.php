<?php

namespace App\Queries\api;

class _GlobalQueries extends \App\Queries\Queries
{	
	protected $model_path;

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    public function get_country()
    {
        // Define Model Country
        $CountryModel = app($this->model_path.'Country');
        // Get Country
        $country = $CountryModel::select(
                        'id',
                        'name'
                    )
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($country);
    }

    public function get_province_by_country_id($country_id)
    {
        // Define Model Province
        $ProvinceModel = app($this->model_path.'Province');
        // Get Province
        $province = $ProvinceModel::select(
                        'id',
                        'name'
                    )
                    ->where('country_id', $country_id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($province);
    }

    public function get_city_by_province_id($province_id)
    {
        // Define Model City
        $CityModel = app($this->model_path.'City');
        // Get City
        $city = $CityModel::select(
                        'id',
                        'name'
                    )
                    ->where('province_id', $province_id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($city);
    }

    public function get_district_by_city_id($city_id)
    {
        // Define Model District
        $DistrictModel = app($this->model_path.'District');
        // Get District
        $district = $DistrictModel::select(
                        'id',
                        'name'
                    )
                    ->where('city_id', $city_id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($district);
    }

    public function get_Village_by_district_id($district_id)
    {
        // Define Model Village
        $VillageModel = app($this->model_path.'Village');
        // Get Village
        $village = $VillageModel::select(
                        'id',
                        'name'
                    )
                    ->where('district_id', $district_id)
                    ->where('status', 'Active')
                    ->orderBy('id', 'asc')
                    ->get();
        return $this->_resJson($village);
    }
}