<?php

namespace App\Queries\web\admin\home;

class AdatIstiadatQueries extends \App\Queries\Queries
{	
	protected $model_path;
	protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $AdatIstiadatModel = app($this->model_path.'AdatIstiadat');
        $this->AdatIstiadatModel = $AdatIstiadatModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_adat_istiadat_datatable($whereRaw, $order, $offset, $limit)
    {
        $adat_istiadat = $this->AdatIstiadatModel::select(
                        'adat_istiadat.*')
                    ->where('adat_istiadat.status', 'Active');
        if ($whereRaw!='')
            $adat_istiadat = $adat_istiadat->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'adat_istiadat.'.$order['coloumn'];
        $adat_istiadat = $adat_istiadat->orderBy($order_column, $order['dir'])
                    ->orderBy('adat_istiadat.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($adat_istiadat as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'adat_istiadat')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($adat_istiadat);
    }

    function get_adat_istiadat_total($whereRaw)
    {
        $adat_istiadat = $this->AdatIstiadatModel::select('adat_istiadat.id')
                    ->where('adat_istiadat.status', 'Active');
        if ($whereRaw!='')
            $adat_istiadat = $adat_istiadat->whereRaw($whereRaw);
        $adat_istiadat = $adat_istiadat->count();
        return $this->_resJson($adat_istiadat);
    }

    function get_adat_istiadat_by_id($id)
    {
        $data = $this->AdatIstiadatModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'adat_istiadat')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_adat_istiadat_last()
    {
        $data = $this->AdatIstiadatModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function insert_adat_istiadat($post)
    {
        $data = $this->AdatIstiadatModel::insert($post);
        return $this->_resJson($data);
    }

    function update_adat_istiadat($post)
    {
        $data = $this->AdatIstiadatModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_adat_istiadat($id)
    {
        $data = $this->AdatIstiadatModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_adat_istiadat($id)
    {
        $data = $this->AdatIstiadatModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
