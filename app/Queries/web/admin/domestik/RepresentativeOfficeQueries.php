<?php

namespace App\Queries\web\admin\domestik;

class RepresentativeOfficeQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $RepresentativeOfficeModel = app($this->model_path.'RepresentativeOffice');
        $this->RepresentativeOfficeModel = $RepresentativeOfficeModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_representative_office_datatable($whereRaw, $order, $offset, $limit)
    {
        $representative_office = $this->RepresentativeOfficeModel::select(
                        'representative_office.*'
                    )
                    ->where('representative_office.type', 'Domestik')
                    ->where('representative_office.status', 'Active');
        if ($whereRaw!='')
            $representative_office = $representative_office->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'representative_office.'.$order['coloumn'];
        $representative_office = $representative_office->orderBy($order_column, $order['dir'])
                    ->orderBy('representative_office.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($representative_office as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'representative_office')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->date = date('d M Y', strtotime($value->date));
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($representative_office);
    }

    function get_representative_office_total($whereRaw)
    {
        $representative_office = $this->RepresentativeOfficeModel::select('representative_office.id')
                    ->where('representative_office.type', 'Domestik')
                    ->where('representative_office.status', 'Active');
        if ($whereRaw!='')
            $representative_office = $representative_office->whereRaw($whereRaw);
        $representative_office = $representative_office->count();
        return $this->_resJson($representative_office);
    }

    function get_representative_office_by_id($id)
    {
        $data = $this->RepresentativeOfficeModel::where('status', 'Active')
                ->where('type', 'Domestik')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'representative_office')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_representative_office_last()
    {
        $data = $this->RepresentativeOfficeModel::where('status', 'Active')
                ->where('type', 'Domestik')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function insert_representative_office($post)
    {
        $data = $this->RepresentativeOfficeModel::insert($post);
        return $this->_resJson($data);
    }

    function update_representative_office($post)
    {
        $data = $this->RepresentativeOfficeModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_representative_office($id)
    {
        $data = $this->RepresentativeOfficeModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_representative_office($id)
    {
        $data = $this->RepresentativeOfficeModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
