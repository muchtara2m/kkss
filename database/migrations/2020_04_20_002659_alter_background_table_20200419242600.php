<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBackgroundTable20200419242600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE background Add type varchar(100) NULL DEFAULT "Image";');
        DB::statement('ALTER TABLE background Add modul varchar(100) NULL;');
        DB::statement('ALTER TABLE background Add param_id Int(11) NULL;');
        DB::statement('ALTER TABLE background ADD INDEX param_id (param_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
