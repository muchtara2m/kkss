<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMenuTable20200210114100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            INSERT INTO menu (id, name, link, parent_id, favicon, position, for_role) VALUES
            ('61', 'Domestik', '#', NULL, NULL, 9, 'member'),
            ('62', 'Tokoh', 'domestik/tokoh', 61, NULL, 1, 'member'),
            ('63', 'Home Base', 'domestik/homebase', 61, NULL, 2, 'member');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
