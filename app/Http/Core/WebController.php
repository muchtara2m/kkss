<?php

namespace App\Http\Core;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class WebController extends Controller 
{
	function _render($data=[])
	{
		// Set Log Activity
		$this->set_log_activity($data, ($this->sessions)?Session::get(env('SESSION').$this->sessions):null);

		$user = null;
		$session_key = Session::get(env('SESSION').$this->sessions);
        $current_cookie = Cookie::get(env('COOKIE').$this->sessions);
        if ($this->sessions && $session_key && $current_cookie) {
        	$user = $this->get_user(env('SESSION').$this->sessions);
        }
        
		$content = [
			'pView' => $this->pView,
			'sView' => $this->sView,
			'vMenu' => $this->get_menu_by_for_role($this->vMenu),
			'sMenu' => $this->sMenu,
			'vContents' => $this->get_contents(),
			'user' => $user,
        	'base_url' => env('APP_URL'),
			'site_url' => env('APP_URL'),
			'global_path' => $this->global_path
		];
		$data = array_merge($content, $data);

		$contents = array(
			'flashdata_upload' => Session::get('upload'),
			'flashdata_post' => Session::get('post'),
			'flashdata_message' => Session::get('message')
		);
		$data = array_merge($contents, $data);

		// Get Contents Main Page
		if ($this->pView=='main') {
			$representative_office = $this->get_representative_office();
			$data['representative_office'] = $representative_office;
			$contact = $this->get_contact();
			foreach ($contact as $r) {
	            if ($r->description=='phone') {
	                $data['contact']['phone'] = $r;
	            } else
	            if ($r->description=='email') {
	                $data['contact']['email'] = $r;
	            }
	        }
			$social_media = $this->get_social_media();
			$data['social_media'] = $social_media;
		}
		
		return response()->view('web/'.$this->global_path.$data['cView'], $data);
	}

	function _redirect($path, $data=[])
	{
		// Set Log Activity
		$this->set_log_activity($data, ($this->sessions)?Session::get(env('SESSION').$this->sessions):null);

		// Redirect to Function
        return redirect($path);
	}

	function get_contents()
	{
		$data = [];

		// Get Favicon
		$favicon = $this->queries_controller->get_contents_by_description('favicon');
		if ($favicon) {
            $favicon->image_id = $favicon->image?$favicon->image->id:null;
            $favicon->image_url = $favicon->image?\ImageUrl::url('contents/images/', $favicon->image):null;
            $favicon->thumb_url = $favicon->image?\ImageUrl::url('contents/thumbs/', $favicon->image):null;
        }
		$data['favicon'] = $favicon;

		// Get Logo
		$logo = $this->queries_controller->get_contents_by_description('logo');
		if ($logo) {
            $logo->image_id = $logo->image?$logo->image->id:null;
            $logo->image_url = $logo->image?\ImageUrl::url('contents/images/', $logo->image):null;
            $logo->thumb_url = $logo->image?\ImageUrl::url('contents/thumbs/', $logo->image):null;
        }
		$data['logo'] = $logo;

		// Get Title
		$title = $this->queries_controller->get_contents_by_description('title');
		if ($title) {
            $title->image_id = $title->image?$title->image->id:null;
            $title->image_url = $title->image?\ImageUrl::url('contents/images/', $title->image):null;
            $title->thumb_url = $title->image?\ImageUrl::url('contents/thumbs/', $title->image):null;
        }
		$data['title'] = $title;

		return $data;
	}

	function get_menu_by_for_role($for_role)
	{
		// Get Menu by for role
		$data = $this->queries_controller->get_menu_by_for_role($for_role);

        return $data;
	}

	function get_user($session)
	{
		$session_user = Session::get($session);
		if (!$session_user)
			return null;
		
		$user_id = Crypt::decryptString($session_user);

		// Get User by ID
		$data = $this->queries_controller->get_user_by_id($user_id);

		return $data;
	}

	function get_representative_office()
	{
		$representative_office = $this->queries_controller->get_representative_office();
		foreach ($representative_office as $r) {
            $r->image_url = $r->image?\ImageUrl::url(strtolower($r->type).'/representativeoffice/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url(strtolower($r->type).'/representativeoffice/thumbs/', $r->image):null;
            $r->str = $r->type=='Domestik'?'Head Office':'Representative Office';
        }

        return $representative_office;
	}

	function get_contact()
	{
		$contact = $this->queries_controller->get_contact();

        return $contact;
	}

	function get_social_media()
	{
		$social_media = $this->queries_controller->get_social_media();

        return $social_media;
	}
}
