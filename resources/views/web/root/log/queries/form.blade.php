@extends('web/'.$pView.'/index')

@section('body')
<div class="templatemo-content-wrapper">
    <div class="templatemo-content">
        <ol class="breadcrumb">
            <li># Log</li>
            <li><a href="{{ route($pView.'/'.$sView) }}">Queries</a></li>
            <li class="active">Detail</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
                    <a class="btn btn-sm btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="" title="Refresh">
                        <i class="fa fa-rotate-right"></i>
                    </a>
                    <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
                        {{ ($log_queries) ? 'Last updated : '.$log_queries->updated_date : '' }}
                    </small>
                </div>
            </div>
        </div>
    	<div class="row" style="padding-bottom: 40px;">
    		<div class="col-lg-12">
    			<div class="panel panel-default" style="border: none;">
                    <h3 class="margin-bottom-15">
                        Detail Queries
                    </h3>
                    <hr/>
    				<div class="panel-body" style="border: 1px solid #eee;">
                        <fieldset>
                            <legend>General</legend>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">_id</span>
                                    <div class="col-md-6">
                                        {{ $log_queries?$log_queries->_id:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Message</span>
                                    <div class="col-md-6">
                                        {{ $log_queries?$log_queries->message:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Request</span>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="4" style="resize: vertical;" readonly="">{{ $log_queries?$log_queries->request:'' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Response</span>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="4" style="resize: vertical;" readonly="">{{ $log_queries?$log_queries->response:'' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Path</span>
                                    <div class="col-md-6">
                                        {{ $log_queries?$log_queries->path:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Log</span>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="6" style="resize: vertical;" readonly="">{{ $log_queries?$log_queries->log:'' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Session Key</span>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="4" style="resize: vertical;" readonly="">{{ $log_queries?$log_queries->session_key:'' }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">created_at</span>
                                    <div class="col-md-6">
                                        {{ $log_queries?$log_queries->created_at:'' }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">updated_at</span>
                                    <div class="col-md-6">
                                        {{ $log_queries?$log_queries->updated_at:'' }}
                                    </div>
                                </div>
                            </div>
                        </fieldset>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
@endsection