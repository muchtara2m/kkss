<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class GalleryCategory extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'gallery_category';

	protected $fillable = [
		'name',
		'description'
	];
}
