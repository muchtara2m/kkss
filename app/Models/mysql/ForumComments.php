<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class ForumComments extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'forum_comments';

	protected $fillable = [
		'forum_id',
		'parent_id',
		'user_id',
		'name',
		'email',
		'phone',
		'message'
	];
}
