<?php

namespace App\Http\Controllers\web\admin\master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\master\ProvinceQueries;

class ProvinceController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'master/province';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'master/province';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, ProvinceQueries $queries_province)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_province = $queries_province;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Province > Master';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

		/* Collection variable data */
        // Get Last
        $last = $this->queries_province->get_province_last();
        $data['last'] = $last;
        
        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('province', $_GET);
        // Get Data
        $data = $this->queries_province->get_province_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_province->get_province_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_province->get_province_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
            $data['name'] = $get->name;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Province > Master';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        /* Collection variable data */
        // Get Data province
        $province = null;
        if ($str_id) {
            $province = $this->queries_province->get_province_by_id($str_id);
        }
        $data['province'] = $province;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $validate = [];
        $validate['name'] = 'required';
        $this->validate($request, $validate);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_province->insert_province($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_province->update_province($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
            }
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $_post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        if ($id) {
            // Delete
            // $_post = $this->queries_category->destroy_category($id);
            // Soft Delete
            $_post = $this->queries_province->delete_province($id);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully deleted!.';
            }
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $_post);
    }
}
