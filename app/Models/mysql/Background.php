<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Background extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'background';

	protected $fillable = [
		'name',
		'type',
		'modul',
		'param_id',
		'deescription',
		'link',
		'display'
	];
}
