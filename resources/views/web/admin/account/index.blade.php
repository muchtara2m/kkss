@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Account
        </h1>
        <ol class="breadcrumb">
            <li class="active"># Account</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ ($account) ? 'Last updated : '.$account->updated_date : 'No Data' }}
            </span>
        </div>
    </div>
    <section class="content">
        <h4 style="margin-top: -15px;">
            <label for="editor1">Form Edit Account</label>
        </h4>
        <div class="box">
            <div class="box-body" style="padding-top: 25px;">
                <div class="row">
                    <div class="col-md-12">
                        <form>
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Name *</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" id="input-name-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->name }}" content="account">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Name is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Email *</label>
                                        <div class="col-md-6">
                                            <input type="email" name="email" id="input-email-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->email }}" content="account">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('email'))
                                                <span class="btn btn-danger">Email is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Phone</label>
                                        <div class="col-md-6">
                                            <input type="number" onkeypress="return isNumberKey(event)" name="phone" id="input-phone-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->phone }}" content="account">
                                            <small class="form-text text-muted g-font-size-default">Optional</small>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Username</label>
                                        <div class="col-md-6">
                                            <input type="text" name="username" id="input-username-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->username }}" content="account">
                                            <small class="form-text text-muted g-font-size-default">Optional</small>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Password</label>
                                        <div class="col-md-6">
                                            <input type="password" name="password" id="input-password-account" class="form-control form-control-md input-class" placeholder="Type here.." value="" content="account">
                                            <small class="form-text text-muted g-font-size-default">Enter if you want to change password, if no you can ignore.</small>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">URL *</label>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    {{ $base_url }} user/
                                                </div>
                                                <input type="text" name="url" id="input-url-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->url }}" content="account">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('url'))
                                                <span class="btn btn-danger">URL is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <button type="reset" class="btn btn-default" style="border-radius: 0;">Reset</button>
                                            <button type="button" class="btn btn-primary btn-class" style="border-radius: 0;" id="btn-account" content="account" disabled value="tes">Submit</button>
                                            <input type="hidden" id="url-account" value="{{ route($pView.'/'.$sView.'/post') }}">
                                            <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                            <i class="fa fa-spinner fa-spin" style="font-size:24px; display: none;" id="spin-account"></i>
                                            <input type="hidden" name="str_id" id="str_id-account" value="{{ $account->str_id }}" />
                                        </div>
                                    </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop