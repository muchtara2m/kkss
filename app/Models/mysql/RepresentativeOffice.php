<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class RepresentativeOffice extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'representative_office';

	protected $fillable = [
		'title',
		'location',
		'description',
		'embed_maps',
		'type',
		'display'
	];
}
