<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class PeluangBisnis extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'peluang_bisnis';

	protected $fillable = [
		'name',
		'peluang_bisnis_category_id',
		'short_description',
		'description',
		'seo',
		'display',
		'created_by',
	];
}
