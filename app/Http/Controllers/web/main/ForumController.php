<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\ForumQueries;

class ForumController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'forum';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'forum';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, ForumQueries $queries_forum)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_forum = $queries_forum;
	}

    public function index(Request $request)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Forum';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;
        // Get Filter
        $filter = $request->query('filter');
        $data['filter'] = $filter;
        if (!$filter) $filter = 'updated_at';
        // Get Sort
        $sort = $request->query('sort');
        $data['sort'] = $sort;
        if (!$sort) $sort = 'desc';

		/* Collection variable data */
        // Get Forum Category
		// $forum_category = $this->queries_forum->get_forum_category($keyword, $sort);
        $forum_category = $this->queries_forum->get_raw_forum_category($keyword, $filter, $sort);
		foreach ($forum_category as $k => $v) {
			$v->image_id = $v->image?$v->image->id:null;
            $v->image_url = $v->image?\ImageUrl::url('master/forumcategory/images/', $v->image):asset('assets/main/images/noimage.png');
            $v->thumb_url = $v->image?\ImageUrl::url('master/forumcategory/thumbs/', $v->image):asset('assets/main/images/noimage.png');
            $v->description = strlen($v->description)>170?substr($v->description, 0, 170).'..':$v->description;
            $v->last_update = date('d M Y, H:i', strtotime($v->updated_at));
            // Get Total Posting
            // $v->posting = $this->queries_forum->count_forum_by_forum_category_id($v->id);
            // Get Status Followed
            $v->is_follow_status = null;
            $v->is_follow_str = 'Follow';
            $session_user = Session::get(env('SESSION').$this->sessions);
            if ($session_user) {
                $user_id = Crypt::decryptString($session_user);
                $forum_category_followers = $this->queries_forum->get_forum_category_followers_by_user($user_id, $v->id);
                if ($forum_category_followers) {
                    $is_status = $forum_category_followers->is_status;
                    if ($is_status=='Approve') {
                        $v->is_follow_status = true;
                        $is_status = 'Followed';
                    } else
                    if ($is_status=='Request') {
                        $v->is_follow_status = true;
                        $is_status = 'Requested';
                    } else 
                    if ($is_status=='Reject') {
                        $is_status = 'Follow';
                    }
                    $v->is_follow_str = $is_status;
                }
            }
            // Get Total Visitor
            // $forum_id = [];
            // $forum_ = $this->queries_forum->get_forum_id($v->id);
            // foreach ($forum_ as $v_)
            //     $forum_id[] = $v_->id;
            // $forum_id = implode(',', $forum_id);
            // $forum_visitor = $this->queries_forum->count_forum_visitor_by_forum_id($forum_id);
            // $v->forum_visitor = $forum_visitor;
		}
		$data['forum_category'] = $forum_category;
        // Get User Login
        $session_user = Session::get(env('SESSION').$this->sessions);
        $data['is_user'] = $session_user?true:false;
        // Get Forum Latest
        $forum = $this->queries_forum->get_forum_latest();
        foreach ($forum as $k => $v) {
            $v->image_id = $v->image?$v->image->id:null;
            $v->image_url = $v->image?\ImageUrl::url('forum/images/', $v->image):null;
            $v->thumb_url = $v->image?\ImageUrl::url('forum/thumbs/', $v->image):null;
            $v->short_description = strlen($v->short_description)>90?substr($v->short_description, 0, 90).'..':$v->short_description;
            $v->forum_date = date('d M Y, H:i', strtotime($v->updated_at));
        }
        $data['forum'] = $forum;
		
		// Render to View
        return $this->_render($data);
    }

    public function category(Request $request, $seo)
    {
    	// Get Forum Category
        $forum_category = $this->queries_forum->get_forum_category_by_seo($seo);
        if (!$forum_category) 
            return \ErrorWeb::notfound();
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = $forum_category->name.' > Forum';
		// Filename in folder view
		$data['cView'] = 'category';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

        // Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;
        // Get Filter
        $filter = $request->query('filter');
        $data['filter'] = $filter;
        if (!$filter) $filter = 'updated_at';
        // Get Sort
        $sort = $request->query('sort');
        $data['sort'] = $sort;
        if (!$sort) $sort = 'desc';

		/* Collection variable data */
		$data['forum_category'] = $forum_category;
		// Get Forum
		// $forum = $this->queries_forum->get_forum_by_forum_category_id($forum_category->id);
        $forum = $this->queries_forum->get_raw_forum_by_forum_category_id($forum_category->id, $keyword, $filter, $sort);
		foreach ($forum as $k => $v) {
			$v->image_id = $v->image?$v->image->id:null;
            $v->image_url = $v->image?\ImageUrl::url('forum/images/', $v->image):asset('assets/main/images/noimage.png');
            $v->thumb_url = $v->image?\ImageUrl::url('forum/thumbs/', $v->image):asset('assets/main/images/noimage.png');
            $v->description = strlen($v->description)>170?substr($v->description, 0, 170).'..':$v->description;
            $v->last_update = date('d M Y, H:i', strtotime($v->updated_at));
		}
		$data['forum'] = $forum;
		
		// Render to View
        return $this->_render($data);
    }

    public function single($seo)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Not set';
		// Filename in folder view
		$data['cView'] = 'single';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'single';
		$data['vAdditional']['js'] = 'single';

		/* Collection variable data */
        // Is Followed
        $data['is_followed'] = false;
		// Get Forum
		$forum = $this->queries_forum->get_forum_by_seo($seo);
        if ($forum) {
			$data['vTitle'] = $forum->title.' | Forum';
        	$forum->created_date = date('d M Y, (H:i)', strtotime($forum->updated_at));
            $forum->image_url = $forum->image?\ImageUrl::url('forum/images/', $forum->image):null;
            $forum->thumb_url = $forum->image?\ImageUrl::url('forum/thumbs/', $forum->image):null;
            $forum->user_name = $forum->user?$forum->user->name:null;
            $forum->category_name = $forum->category?$forum->category->name:null;

            // Check Authentication User
            $auth = $this->queries_controller->check_auth(env('SESSION').$this->sessions, env('COOKIE').$this->sessions);
            if ($auth) {
                $session_user = Session::get(env('SESSION').$this->sessions);
                if ($session_user) {
                    $user_id = Crypt::decryptString($session_user);
                    $forum_category_followers = $this->queries_forum->get_forum_category_followers_by_approve($user_id, $forum->category->id);
                    if ($forum_category_followers)
                        $data['is_followed'] = true;
                }
            }
        }
        $data['forum'] = $forum;
        // Get Forum Category
        $forum_category = $this->queries_forum->get_forum_category();
        foreach ($forum_category as $r) {
        	$r->total = $this->queries_forum->count_forum_by_forum_category_id($r->id);
        }
        $data['forum_category'] = $forum_category;
        // Get Forum Comments
        $count_comments = 0;
        $forum_comments = [];
        if ($forum) {
        	$forum_comments = $this->queries_forum->get_forum_comments_by_forum_id($forum->id);
        	foreach ($forum_comments as $r) {
                $r->sub_comments = $this->queries_forum->get_forum_comments_by_forum_id($forum->id, $r->id);
                $count_comments += count($r->sub_comments);
        	}
            $count_comments += count($forum_comments);
    	}
        $data['comments'] = $forum_comments;
        $data['count_comments'] = $count_comments;
		
		// Render to View
        return $this->_render($data);
    }

    public function comments(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'seo' => 'required',
            'forum_id' => 'required',
            'message' => 'required',
        ]);

        // Define Variable Request
        $seo = $request->input('seo');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        $forum_id = $request->input('forum_id');
        $forum_comments_id = $request->input('forum_comments_id');
        $message = $request->input('message');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['email'] = $email;
        $post['user_id'] = $user_id;
        $post['forum_id'] = $forum_id;
        $post['parent_id'] = $forum_comments_id;
        $post['message'] = $message;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Insert
        $_post = $this->queries_forum->insert_forum_comments($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully created!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->sView.'/'.$seo);
    }

    public function conversation($seo)
    {
        // Check Authentication User
        $auth = $this->queries_controller->check_auth(env('SESSION').$this->sessions, env('COOKIE').$this->sessions);
        if (!$auth)
            return $this->_redirect($this->sView.'/'.$seo);

        // Global variable data
        $data = [];
        // Title in website
        $data['vTitle'] = 'Not set';
        // Filename in folder view
        $data['cView'] = 'conversation';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = 'conversation';
        $data['vAdditional']['js'] = 'conversation';

        /* Collection variable data */
        // Get Forum
        $forum = $this->queries_forum->get_forum_by_seo($seo);
        if ($forum) {
            $data['vTitle'] = $forum->title.' | Forum';
            $forum->created_date = date('d M Y, (H:i)', strtotime($forum->updated_at));
            $forum->image_url = $forum->image?\ImageUrl::url('forum/images/', $forum->image):null;
            $forum->thumb_url = $forum->image?\ImageUrl::url('forum/thumbs/', $forum->image):null;
            $forum->user_name = $forum->user?$forum->user->name:null;
            $forum->category_name = $forum->category?$forum->category->name:null;
        }
        $data['forum'] = $forum;
        // Get Forum Category
        $forum_category = $this->queries_forum->get_forum_category();
        foreach ($forum_category as $r) {
            $r->total = $this->queries_forum->count_forum_by_forum_category_id($r->id);
        }
        $data['forum_category'] = $forum_category;
        // Get Forum Conversation
        $count_conversation = 0;
        $forum_conversation = [];
        if ($forum) {
            $forum_conversation = $this->queries_forum->get_forum_conversation_by_forum_id($forum->id);
            foreach ($forum_conversation as $r) {
                if ($r->parent_id) {
                    $parent = $this->queries_forum->get_forum_conversation_by_parent_id($r->parent_id);
                    $r->parent_user = $parent->user->name;
                    $r->parent_message = $parent->message;
                    $r->parent_date = $parent->created_date;
                }
                $count_conversation ++;
            }

            $session_user = Session::get(env('SESSION').$this->sessions);
            if ($session_user) {
                $user_id = Crypt::decryptString($session_user);
                // Insert as visitor
                $post = [];
                $post['forum_id'] = $forum->id;
                $post['user_id'] = $user_id;
                $post['ip'] = $_SERVER['REMOTE_ADDR'];
                $post['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                $this->queries_forum->insert_forum_visitor($post);
            }
        }
        $data['conversation'] = $forum_conversation;
        $data['count_conversation'] = $count_conversation;
        
        // Render to View
        return $this->_render($data);
    }

    public function conversation_post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'seo' => 'required',
            'forum_id' => 'required',
            'message' => 'required',
        ]);

        // Define Variable Request
        $seo = $request->input('seo');
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        $forum_id = $request->input('forum_id');
        $forum_conversation_id = $request->input('forum_conversation_id');
        $message = $request->input('message');

        // Create Array to Post
        $post = [];
        $post['user_id'] = $user_id;
        $post['forum_id'] = $forum_id;
        $post['parent_id'] = $forum_conversation_id;
        $post['message'] = $message;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Insert
        $_post = $this->queries_forum->insert_forum_conversation($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully created!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->sView.'/conversation/'.$seo);
    }

    public function follow($seo)
    {
        // Check Authentication User
        $auth = $this->queries_controller->check_auth(env('SESSION').$this->sessions, env('COOKIE').$this->sessions);
        if (!$auth)
            return $this->_redirect($this->sView);

        // Get Forum Category
        $forum_category = $this->queries_forum->get_forum_category_by_seo($seo);
        if (!$forum_category) 
            return \ErrorWeb::notfound();

        // Define Variable Request
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;

        // Create Array to Post
        $post = [];
        $post['user_id'] = $user_id;
        $post['forum_category_id'] = $forum_category->id;
        $post['is_status'] = 'Request';

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Insert
        $_post = $this->queries_forum->insert_forum_category_followers($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully created!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->sView);
    }

    public function followed_status()
    {
        // Global variable data
        $data = [];
        // Title in website
        $data['vTitle'] = 'Forum Followed Status';
        // Filename in folder view
        $data['cView'] = 'followed_status';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        /* Collection variable data */
        // Get User Session
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = $session_user?Crypt::decryptString($session_user):null;
        // Get Forum Category Status
        $forum_category_followers = $this->queries_forum->get_forum_category_followers_by_user_id($user_id);
        foreach ($forum_category_followers as $row) {
            if ($row->is_status=='Request') $row->is_status = 'Menunggu verifikasi';
            else if ($row->is_status=='Approve') $row->is_status = 'Disetujui';
            else if ($row->is_status=='Reject') $row->is_status = 'Ditolak';
        }
        $data['forum_category_followers'] = $forum_category_followers;
        
        // Render to View
        return $this->_render($data);
    }
}
