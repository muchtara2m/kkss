<?php

namespace App\Queries;

use Illuminate\Support\Facades\Crypt;
use Session;
use Cookie;

class ControllerQueries extends \App\Queries\Queries
{	
    protected $model_path;

	function __construct()
	{
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';
	}

    public function get_token($session_key, $token_key=null)
    {
        // Define Model
        $TokenModel = app($this->model_path.'Token');
        // Insert Token
        $data = $TokenModel::where('status', 'Active')
                ->where('session_key', $session_key);
        if ($token_key)
            $data = $data->where('token_key', $token_key);
        $data = $data->get()
                ->first();
        return $data;
    }

    public function check_token($session_key, $token_key)
    {
        // Define Model
        $TokenModel = app($this->model_path.'Token');
        // Insert Token
        $data = $TokenModel::where('status', 'Active')
                ->where('session_key', $session_key)
                ->where('token_key', $token_key)
                ->get()
                ->first();
        return $data;
    }

    public function insert_token($post)
    {
        // Define Model
        $TokenModel = app($this->model_path.'Token');
        // Insert Token
        $data = $TokenModel::insert($post);
        return $data;
    }

    public function update_token($post)
    {
        // Define Model
        $TokenModel = app($this->model_path.'Token');
        // Insert Token
        $data = $TokenModel::where('id', $post['id'])
                ->update($post);
        return $data;
    }

    public function get_contents_by_description($description)
    {
        // Define Model
        $ContentsModel = app($this->model_path.'Contents');
        $ImageModel = app($this->model_path.'Image');
        $data = $ContentsModel::where('description', $description)->get()->first();
        if ($data) {
            $data->display = ($data->display==true)?'View':'Hide';
            $data->created_date = date('d M Y, (H:i)', strtotime($data->created_at));
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $image = $ImageModel::where('param_id', $data->id)->where('description', $description)->get()->first();
            if ($image)
                $data->image = $image;
        }
        return $data;
    }

    public function get_menu_by_for_role($for_role)
    {
        // Define Model
        $MenuModel = app($this->model_path.'Menu');
        // Define Variable Return
        $data = [];
        // Get Menu Parent
        $menu_parent = $MenuModel::where('status', 'Active')
                            ->where('parent_id', null)
                            ->where('for_role', $for_role)
                            ->orderBy('position', 'asc')
                            ->get();
        for ($i=0; $i < count($menu_parent); $i++) {
            $array_sub = [];
            // Get Menu by parent id
            $menu_sub = $MenuModel::where('status', 'Active')
                            ->where('parent_id', $menu_parent[$i]->id)
                            ->where('for_role', $for_role)
                            ->orderBy('position', 'asc')
                            ->get();
            for ($j=0; $j < count($menu_sub); $j++) {
                $array_sub[$j]['name'] = $menu_sub[$j]->name;
                $array_sub[$j]['link'] = $menu_sub[$j]->link;
                $array_sub[$j]['favicon'] = $menu_sub[$j]->favicon;
            }
            $data[$i]['name'] = $menu_parent[$i]->name;
            $data[$i]['link'] = $menu_parent[$i]->link;
            $data[$i]['favicon'] = $menu_parent[$i]->favicon;
            $data[$i]['sub'] = $array_sub;
        }
        return $data;
    }

    public function get_user_by_id($id)
    {
        // Define Model
        $UserModel = app($this->model_path.'User');
        // Get User
        $data = $UserModel::select(
                    'user.id',
                    'user.name',
                    'user.email',
                    'user.phone',
                    'user.level',
                    'user.login',
                    'user.logout',
                    'user.url',
                    'user_profile.address as address',
                    'profession.name as profession_name'
                )
                ->leftjoin('user_profile', 'user_profile.user_id', 'user.id')
                ->leftjoin('profession', 'profession.id', 'user_profile.profession_id')
                ->where('user.id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->login = $data->login?date('M d, Y (h:i A)', strtotime($data->login)):'-';
            $data->logout = $data->logout?date('M d, Y (h:i A)', strtotime($data->logout)):'-';
        }
        return $data;
    }

    public function get_image_by_id($id, $description)
    {
        // Define Model
        $ImageModel = app($this->model_path.'Image');
        // Get Image
        $data = $ImageModel::where('id', $id)
                ->where('description', $description)
                ->get()
                ->first();
        return $data;
    }

    public function get_image($post)
    {
        // Define Model
        $ImageModel = app($this->model_path.'Image');
        // Insert Image
        $data = $ImageModel::select();
        if (env('STORAGE_PUBLIC'))
            $data = $data->where('storage', $post['storage']);
        if (env('UPLOAD_PUBLIC'))
            $data = $data->where('name', $post['name']);
        $data = $data->get()->first();
        return $data;
    }

    public function insert_image($post)
    {
        // Define Model
        $ImageModel = app($this->model_path.'Image');
        // Insert Image
        $data = $ImageModel::insert($post);
        return $data;
    }

    public function update_image($post)
    {
        // Define Model
        $ImageModel = app($this->model_path.'Image');
        // Update Image
        $data = $ImageModel::where('id', $post['id'])
                ->update($post);
        return $data;
    }

    public function delete_image($id)
    {
        // Define Model
        $ImageModel = app($this->model_path.'Image');
        // Delete Image
        $data = $ImageModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $data;
    }

    public function get_video_by_id($id, $description)
    {
        // Define Model
        $VideoModel = app($this->model_path.'Video');
        // Get Video
        $data = $VideoModel::where('id', $id)
                ->where('description', $description)
                ->get()
                ->first();
        return $data;
    }

    public function get_video($post)
    {
        // Define Model
        $VideoModel = app($this->model_path.'Video');
        // Insert Video
        $data = $VideoModel::select();
        if (env('STORAGE_PUBLIC'))
            $data = $data->where('storage', $post['storage']);
        if (env('UPLOAD_PUBLIC'))
            $data = $data->where('name', $post['name']);
        $data = $data->get()->first();
        return $data;
    }

    public function insert_video($post)
    {
        // Define Model
        $VideoModel = app($this->model_path.'Video');
        // Insert Video
        $data = $VideoModel::insert($post);
        return $data;
    }

    public function update_video($post)
    {
        // Define Model
        $VideoModel = app($this->model_path.'Video');
        // Update Video
        $data = $VideoModel::where('id', $post['id'])
                ->update($post);
        return $data;
    }

    public function delete_video($id)
    {
        // Define Model
        $VideoModel = app($this->model_path.'Video');
        // Delete Video
        $data = $VideoModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $data;
    }

    public function check_auth($session_name, $cookie_name, $is_decrypt=false)
    {
        $session_key = Session::get($session_name); // Define Session User
        $current_cookie = Cookie::get($cookie_name); // Define Cookie from front
        $token_key = $current_cookie;
        if ($is_decrypt)
            $token_key = ($current_cookie)?Crypt::decryptString($current_cookie):$current_cookie;

        // Define Model
        $TokenModel = app($this->model_path.'Token');
        // Get Token
        $data = $TokenModel::select()
                ->where('session_key', $session_key)
                ->where('token_key', $token_key)
                ->where('status', 'Active')
                ->get()
                ->first();
        if ($data) {
            $cookie_value = Crypt::encryptString($token_key);
            if ($is_decrypt)
                $cookie_value = $current_cookie;
            setcookie($cookie_name, $cookie_value, time() + (3600), '/');
        }
        return $data;
    }

    public function _get_datatable($table)
    {
        // Search Column
        $whereRaw_all_coloumn = '';
        $whereRaw_per_coloumn = '';
        if (isset($_GET['columns'])) {
            // Search all coloumn
            $_i = 0;
            $_param_i = 0;
            $or = 'OR';
            foreach ($_GET['columns'] as $key => $value) {
                if(isset($_GET['search']['value'])) {
                    if($_GET['search']['value']!=''and$value['searchable']=='true') {
                        if (strrpos($value['name'], '$')==true) {
                            $explode = explode('_$_', $value['name']);
                            $value['name'] = $explode[0].'.'.$explode[1];
                        } else
                            $value['name'] = $table.'.'.$value['name'];
                        if ($_param_i==0) {
                            $whereRaw_all_coloumn .= '(';
                            $or = '';
                            $_param_i = 1;
                        } else
                            $or = ' OR ';
                        if (env('DB_CONNECTION') == 'pgsql')
                            $whereRaw_all_coloumn .= $or." LOWER(".$value['name']."::TEXT) LIKE LOWER('%".$_GET["search"]["value"]."%')";
                        else
                            $whereRaw_all_coloumn .= $or.' LOWER('.$value['name'].') LIKE LOWER("%'.$_GET['search']['value'].'%")';
                    }
                }
                if(count($_GET['columns']) - 1 == $_i) 
                    if ($_param_i==1)
                        $whereRaw_all_coloumn .= ')';
                $_i++;
            }

            // Search per coloumn
            $_i = 0;
            $_param_i = 0;
            $or = 'AND';
            foreach ($_GET['columns'] as $key => $value) {
                if(isset($value['search']['value'])) {
                    if($value['search']['value']!='') {
                        if (strrpos($value['name'], '$')==true) {
                            $explode = explode('_$_', $value['name']);
                            $value['name'] = $explode[0].'.'.$explode[1];
                        } else
                            $value['name'] = $table.'.'.$value['name'];
                        if ($_param_i==0) {
                            $whereRaw_per_coloumn .= '(';
                            $or = '';
                            $_param_i = 1;
                        } else 
                            $or = ' AND ';
                        if (stripos($value['name'], 'display')>=0) {
                            if($value['search']['value']=='View')
                                $value['search']['value'] = 1;
                            if($value['search']['value']=='Hide')
                                $value['search']['value'] = 0;
                        }
                        if (env('DB_CONNECTION') == 'pgsql')
                            $whereRaw_per_coloumn .= $or." LOWER(".$value['name']."::TEXT) LIKE LOWER('%".$value["search"]["value"]."%')";
                        else
                            $whereRaw_per_coloumn .= $or.' LOWER('.$value['name'].') LIKE LOWER("%'.$value['search']['value'].'%")';
                    }
                }
                if(count($_GET['columns']) - 1 == $_i) 
                    if ($_param_i==1)
                        $whereRaw_per_coloumn .= ')';
                $_i++;
            }
        }

        // Search
        $whereRaw = '';
        if ($whereRaw_per_coloumn!='')
            $whereRaw = $whereRaw_per_coloumn;
        if ($whereRaw_all_coloumn!='')
            $whereRaw = $whereRaw_all_coloumn;
        if ($whereRaw_per_coloumn!=''and$whereRaw_all_coloumn!='')
            $whereRaw = $whereRaw_per_coloumn.' AND '.$whereRaw_all_coloumn;

        // Order By
        $order = ['coloumn' => 'updated_at', 'dir'=>'desc'];
        if(isset($_GET['order'])and$_GET['columns'][$_GET['order'][0]['column']]['name']!='')
            $order = ['coloumn' => $_GET['columns'][$_GET['order'][0]['column']]['name'], 'dir'=>$_GET['order'][0]['dir']];

        // Limit
        $offset = isset($_GET)?isset($_GET['start'])?(int)$_GET['start']:0:0;
        $limit = isset($_GET)?isset($_GET['length'])?(int)$_GET['length']:10:10;

        $data = [
            'whereRaw' => $whereRaw,
            'order' => $order,
            'offset' => $offset,
            'limit' => $limit
        ];
        return $data;
    }

    public function _set_log_activity($response, $session_key=null)
    {
        unset($response['vTitle']);
        unset($response['cView']);
        unset($response['vAdditional']);
        return $this->set_log_activity($response, $session_key);
    }

    public function insert_send_mail($post)
    {
        // Define Model
        $SendMailModel = app($this->model_path.'SendMail');
        // Insert Send Mail
        $data = $SendMailModel::insert($post);
        return $data;
    }

    public function update_send_mail($post)
    {
        // Define Model
        $SendMailModel = app($this->model_path.'SendMail');
        // Insert Send Mail
        $data = $SendMailModel::where('id', $post['id'])
                ->update($post);
        return $data;
    }

    public function get_send_mail_last($user_id=null, $module=null)
    {
        // Define Model
        $SendMailModel = app($this->model_path.'SendMail');
        // Insert Send Mail
        $data = $SendMailModel::where('status', 'Pending');
        if ($user_id)
            $data = $data->where('user_id', $user_id);
        if ($module)
            $data = $data->where('module', $module);
        $data = $data->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        return $data;
    }

    public function get_representative_office()
    {
        // Define Model
        $RepresentativeOfficeModel = app($this->model_path.'RepresentativeOffice');
        // Get Data
        $data = $RepresentativeOfficeModel::select('id', 'title', 'type')
                ->where('status', 'Active')
                ->where('display', true)
                ->orderBy('updated_at', 'desc')
                ->limit(4)
                ->get();
        // Get Image
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'representative_office')
                    ->get()
                    ->first();
            $v->image = $image;
        }
        return $data;
    }

    public function get_contact()
    {
        // Define Model
        $ContentsModel = app($this->model_path.'Contents');
        // Get Data
        $data = $ContentsModel::whereIn('description', ['phone', 'email'])
                ->where('status', 'Active')
                ->where('display', true)
                ->get();
        return $data;
    }

    public function get_social_media()
    {
        // Define Model
        $ContentsModel = app($this->model_path.'Contents');
        // Get Data
        $data = $ContentsModel::where('description', 'social_media')
                ->where('status', 'Active')
                ->where('display', true)
                ->get();
        return $data;
    }
}
