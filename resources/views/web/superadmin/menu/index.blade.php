@extends('web/'.$pView.'/index')

@section('body')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
			<li class="active">Menu</li>
		</ol>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header" style="margin-top: 10px;">
	            Menu
	        </h2>
	        <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
	            <a class="btn btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
	                <i class="fa fa-arrow-left"></i>
	            </a>
	            <a class="btn btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
	                <i class="fa fa-arrow-right"></i>
	            </a>
	            <a class="btn btn-default" href="" title="Refresh">
	                <i class="fa fa-rotate-right"></i>
	            </a>
	            <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
	                {{ isset($last) ? 'Last updated : '.$last->updated_date : 'No Data' }}
	            </small>
	        </div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Data Menu
					<div class="pull-right">
		                <a href="{{ route($pView.'/'.$sView.'/form') }}" class="btn btn-sm btn-primary pull-right" style="margin-right: 0; margin-top: 5px;"><i class="fa fa-plus"></i> Menu</a>
		            </div>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table id="example1" class="table table-striped table-hover" url="{{ route($pView.'/'.$sView.'/get/datatable') }}" style="width: 100%; font-size: 12.5px;">
			            </table>
		        	</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection