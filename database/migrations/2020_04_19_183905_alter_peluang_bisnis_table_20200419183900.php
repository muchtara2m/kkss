<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPeluangBisnisTable20200419183900 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE adat_istiadat Add short_description Text NULL;');
        DB::statement('ALTER TABLE peluang_bisnis Add short_description Text NULL;');
        DB::statement('ALTER TABLE peluang_bisnis Add created_by Int(11) NOT NULL DEFAULT 3;');
        DB::statement('ALTER TABLE peluang_bisnis ADD INDEX created_by (created_by);');
        DB::statement('ALTER TABLE peluang_bisnis Add seo varchar(255) NULL;');
        DB::statement('ALTER TABLE peluang_bisnis ADD INDEX seo (seo);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
