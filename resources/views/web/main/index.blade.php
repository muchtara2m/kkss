<!DOCTYPE html>
<html lang="en">
    <head>
        @include('web/'.$pView.'/head')
    </head>
    <body>
        @include('web/'.$pView.'/header')
        @yield('body')
        @include('web/'.$pView.'/footer')
        @include('web/'.$pView.'/bottom') 
        @include('web/'.$pView.'/additional')
        {{ $vSubFolder = (strpos($cView, '/index') == true) ? '/'.str_replace("/index", "", $cView) : null }}
        @if($vAdditional['css'])
            <link rel="stylesheet" href="{{ asset('assets/'.$pView.'/views/css/'.$sView.$vSubFolder.'/'.$vAdditional['css'].'.css') }}">
        @endif
        @if($vAdditional['html'])
            @include('web/'.$pView.'/'.$sView.$vSubFolder.'/additional/'.$vAdditional['html'])
        @endif
        @if($vAdditional['js'])
            <script src="{{ asset('assets/'.$pView.'/views/js/'.$sView.$vSubFolder.'/'.$vAdditional['js'].'.js') }}"></script>
        @endif
    </body>
    @yield('external_js')
    @yield('custom_js')
</html>