<?php

namespace App\Http\Controllers\web\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\GalleryQueries;

class GalleryController extends \App\Http\Core\WebController 
{
    // Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'gallery';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'gallery';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, GalleryQueries $queries_gallery)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_gallery = $queries_gallery;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Gallery';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_gallery->get_gallery_last();
        $data['last'] = $last;

        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('gallery', $_GET);
        // Get Data
        $data = $this->queries_gallery->get_gallery_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        foreach ($data as $r) {
            $r->image_url = $r->image?\ImageUrl::url($this->sView.'/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url($this->sView.'/thumbs/', $r->image):null;
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_gallery->get_gallery_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_gallery->get_gallery_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
            $data['name'] = $get->name;
            $data['description'] = $get->description;
            $data['display'] = $get->display;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Gallery';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'form';

        /* Collection variable data */
        // Get Gallery
        $gallery = null;
        if ($str_id) {
            $gallery = $this->queries_gallery->get_gallery_by_id($str_id);
            $gallery->image_id = $gallery->image?$gallery->image->id:null;
            $gallery->image_url = $gallery->image?\ImageUrl::url($this->sView.'/images/', $gallery->image):null;
            $gallery->thumb_url = $gallery->image?\ImageUrl::url($this->sView.'/thumbs/', $gallery->image):null;
        }
        $data['gallery'] = $gallery;
        // Get Gallery Category
        $gallery_category = $this->queries_gallery->get_gallery_category();
        $data['gallery_category'] = $gallery_category;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'name' => 'required',
            'gallery_category' => 'required',
            'display' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');
        $gallery_category_id = $request->input('gallery_category');
        $short_description = $request->input('short_description');
        $display = $request->input('display');
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['gallery_category_id'] = $gallery_category_id;
        $post['short_description'] = $short_description;
        $post['display'] = ($display=='true')?true:false;
        $_post = false; // for upload file
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_gallery->insert_gallery($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
                $_gallery = $this->queries_gallery->get_gallery_last();
                $_param_id = $_gallery->id;
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_gallery->update_gallery($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
                $_param_id = $id;
            }
        }

        // Proccess Upload
        if ($_post and $file) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'gallery'; // Set Description for DB Image
            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
            $path = $this->sView; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload)
                return \ErrorWeb::notfound();
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        // Delete
        // $_post = $this->queries_gallery->destroy_gallery($id);
        // Soft Delete
        $_post = $this->queries_gallery->delete_gallery($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }
}
