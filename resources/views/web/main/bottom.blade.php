<script src="{{ asset('assets/main/js/jquery.js') }}"></script>
<script src="{{ asset('assets/main/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.scrollUp.min.js') }}"></script>
<script src="{{ asset('assets/main/js/price-range.js') }}"></script>
<script src="{{ asset('assets/main/js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('assets/main/js/main.js') }}"></script>
<script src="{{ asset('assets/main/libs/select2/select2.min.js') }}"></script>