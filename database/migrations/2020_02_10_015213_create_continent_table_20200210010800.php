<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContinentTable20200210010800 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('continent', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('name', 255)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
        });

        $data = [
            [
                'name' => "Asia"
            ],
            [
                'name' => "Afrika"
            ],
            [
                'name' => "Amerika Utara"
            ],
            [
                'name' => "Amerika Selatan"
            ],
            [
                'name' => "Antartika"
            ],
            [
                'name' => "Eropa"
            ],
            [
                'name' => "Australia dan Oseania"
            ],
        ];
        DB::table('continent')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('continent_table_20200210010800');
    }
}
