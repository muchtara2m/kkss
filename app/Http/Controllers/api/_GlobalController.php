<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\api\_GlobalQueries;
use Validator;

class _GlobalController extends \App\Http\Core\ApiController
{
	function __construct(ControllerQueries $queries_controller, _GlobalQueries $queries__global)
	{
        $this->queries_controller = $queries_controller;
        $this->queries__global = $queries__global;
	}

	function get_user(Request $request)
	{
		// Define Variable Request
        $session_key = $request->header('SessionKey');
        // Get User ID
		$user_id = Crypt::decryptString($session_key);

		$data = $this->get_user_has_login($user_id);
		if (!$data)
			return $this->res('Nothing user as login')->error();
		
		return $this->res($data)->success();
	}

	public function get_country(Request $request)
	{   
		try {
			// Get Country
			$country = $this->queries__global->get_country();
			// Response
	        return $this->res($country)->success();
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return $this->res(['message' => $e->getMessage()])->error();
    	}
	}

	public function get_province_by_country_id(Request $request, $country_id=1)
	{   
		try {
			// Get Province
			$province = $this->queries__global->get_province_by_country_id($country_id);
			if (count($province) == 0)
				return $this->res('Province not found!')->error();
			// Response
	        return $this->res($province)->success();
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return $this->res(['message' => $e->getMessage()])->error();
    	}
	}

	public function get_city_by_province_id(Request $request, $province_id)
	{   
		try {
			// Check Province ID
			if (!$province_id)
				return $this->res('Province ID not found!')->error();
			// Get City
			$city = $this->queries__global->get_city_by_province_id($province_id);
			if (count($city) == 0)
				return $this->res('City not found!')->error();
			// Response
	        return $this->res($city)->success();
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return $this->res(['message' => $e->getMessage()])->error();
    	}
	}

	public function get_district_by_city_id(Request $request, $city_id)
	{   
		try {
			// Check City ID
			if (!$city_id)
				return $this->res('City ID not found!')->error();
			// Get District
			$district = $this->queries__global->get_district_by_city_id($city_id);
			if (count($district) == 0)
				return $this->res('District not found!')->error();
			// Response
	        return $this->res($district)->success();
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return $this->res(['message' => $e->getMessage()])->error();
    	}
	}

	public function get_village_by_district_id(Request $request, $district_id)
	{   
		try {
			// Check District ID
			if (!$district_id)
				return $this->res('District ID not found!')->error();
			// Get Village
			$village = $this->queries__global->get_village_by_district_id($district_id);
			if (count($village) == 0)
				return $this->res('Village not found!')->error();
			// Response
	        return $this->res($village)->success();
	    } catch (\Exception $e) {
	    	\DB::rollback();
			return $this->res(['message' => $e->getMessage()])->error();
    	}
	}
}
