<?php

namespace App\Http\Controllers\web\admin\domestik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\domestik\TokohQueries;

class TokohController extends \App\Http\Core\WebController 
{
    // Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'domestik/tokoh';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'domestik/tokoh';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, TokohQueries $queries_tokoh)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_tokoh = $queries_tokoh;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Tokoh > Domestik';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_tokoh->get_tokoh_last();
        $data['last'] = $last;

        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('tokoh', $_GET);
        // Get Data
        $data = $this->queries_tokoh->get_tokoh_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        foreach ($data as $r) {
            $r->image_url = $r->image?\ImageUrl::url($this->sView.'/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url($this->sView.'/thumbs/', $r->image):null;
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_tokoh->get_tokoh_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_tokoh->get_tokoh_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
            $data['name'] = $get->name;
            $data['description'] = $get->description;
            $data['display'] = $get->display;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Tokoh > Domestik';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'form';

        /* Collection variable data */
        // Get tokoh
        $tokoh = null;
        if ($str_id) {
            $tokoh = $this->queries_tokoh->get_tokoh_by_id($str_id);
            $tokoh->image_id = $tokoh->image?$tokoh->image->id:null;
            $tokoh->image_url = $tokoh->image?\ImageUrl::url($this->sView.'/images/', $tokoh->image):null;
            $tokoh->thumb_url = $tokoh->image?\ImageUrl::url($this->sView.'/thumbs/', $tokoh->image):null;
        }
        $data['tokoh'] = $tokoh;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'name' => 'required',
            'display' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');
        $type = 'Nasional';
        $short_description = $request->input('short_description');
        $display = $request->input('display');
        $file = $request->file('file');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['type'] = $type;
        $post['short_description'] = $short_description;
        $post['display'] = ($display=='true')?true:false;
        $_post = false; // for upload file
        $_param_id = 0;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_tokoh->insert_tokoh($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
                $_tokoh = $this->queries_tokoh->get_tokoh_last();
                $_param_id = $_tokoh->id;
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_tokoh->update_tokoh($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
                $_param_id = $id;
            }
        }

        // Proccess Upload
        if ($_post and $file) {
            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
            $description = 'tokoh'; // Set Description for DB Image
            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
            $path = $this->sView; // Set Path File Image
            // Proccess Upload
            $upload = $this->upload_single_file_image($_param_id, $name, $description, $file, $path, $image_id);
            if (!$upload)
                return \ErrorWeb::notfound();
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        // Delete
        // $_post = $this->queries_tokoh->destroy_tokoh($id);
        // Soft Delete
        $_post = $this->queries_tokoh->delete_tokoh($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }
}
