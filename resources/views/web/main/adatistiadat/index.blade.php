@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 30px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Adat Istiadat</li>
            </ol>
        </div>
    </div>
</section>
<section class="mg-50">
    <div class="container">
        <div class="row">
            <h2 class="title text-center" style="margin-bottom: 10px;">Adat Istiadat</h2>
            <div class="panel-body">
                <form method="get" action="">
                    <div class="search_box">
                        <input type="text" name="keyword" placeholder="Search" value="{{ $keyword }}"/>
                    </div>
                </form>
            </div>
            <div class="col-sm-12">
                <div class="blog-post-area">
                    @foreach ($adat_istiadat as $row)
                        <div class="single-blog-post row col-sm-12" style="margin-bottom: 50px;">
                            <h3>{{ $row->title }}</h3>
                            <div class="post-meta">
                                <ul>
                                    <li><i class="fa fa-user"></i> {{ $row->user_name }}</li>
                                    <li><i class="fa fa-calendar"></i> {{ $row->created_date }}</li>
                                    <li><i class="fa fa-comments"></i> {{ $row->comments }} Komentar</li>
                                </ul>
                            </div>
                            <p>
                                <img src="{{ $row->image_url }}" alt="image" style="float: left; margin-right: 20px; max-width: 150px;">
                                {{ $row->short_description }}
                            </p>
                            <a class="btn btn-sm btn-primary" href="{{ url($sView.'/'.$row->seo) }}">Selengkapnya</a>
                        </div>
                    @endforeach
                    <div class="text-center">
                        {!! $adat_istiadat->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
