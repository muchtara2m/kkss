<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentativeOfficeTable20200510132200 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representative_office', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('title', 255)->nullable();
            $table->string('location', 255)->nullable();
            $table->text('embed_maps')->nullable();
            $table->text('description')->nullable();
            $table->string('type', 100)->nullable();
            $table->boolean('display')->default(true);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representative_office');
    }
}
