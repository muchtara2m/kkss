<?php

namespace App\Http\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ErrorController extends Controller 
{
	function _error($data=[])
	{
		$content = [
			'pView' => $this->pView,
			'sView' => $this->sView,
			'vContents' => $this->get_contents(),
		];
		$data = array_merge($content, $data);
		
		return response()->view('error/'.$this->global_path.$data['cView'], $data);
	}

	function get_contents()
	{
		$data = [];

		// Get Favicon
		$data['favicon']['image_url'] = env('BASE_URL').'image/favicon.png';

		// Get Logo
		$data['logo']['image_url'] = env('BASE_URL').'image/logo.png';

		// Get Title
		$data['title'] = env('APP_NAME');

		return $data;
	}
}
