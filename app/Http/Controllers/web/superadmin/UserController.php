<?php

namespace App\Http\Controllers\web\superadmin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\superadmin\UserQueries;

class UserController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
	protected $pView = 'superadmin';

	// Sub Folder (Child) in views
	protected $sView = 'user';

	// For description select menu by role from database
	protected $vMenu = 'superadmin';
	
	// For selected menu (Active menu)
	protected $sMenu = 'user';

	// Session by user for access this controllers
	protected $sessions = 'superadmin';
	
	function __construct(ControllerQueries $queries_controller, UserQueries $queries_user)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_user = $queries_user;
	}

    public function index()
    {
        // Global variable data
		$data = [];
		// Title in Website
		$data['vTitle'] = 'User';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = 'index';

		/* Collection variable data */
		// Get Last
        $last = $this->queries_user->get_user_last();
        $data['last'] = $last;
		
		// Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Session
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = Crypt::decryptString($session_user);
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('"user"', $_GET);
        // Get Data
        $data = $this->queries_user->get_user_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit'], $user_id);
        foreach ($data as $k => $v) {
            $v->url = '<a href="'.env('APP_URL').'user/'.$v->url.'" target="_blank">'.env('APP_URL').'user/'.$v->url.'</a>';
        }
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_user->get_user_total($modul['whereRaw'], $user_id); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_user->get_user_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > User';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        /* Collection variable data */
        // Get Data User
        $user = null;
        if ($str_id) {
            $user = $this->queries_user->get_user_by_id($str_id);
        }
        $data['user'] = $user;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $validate = [];
        $validate['name'] = 'required';
        if (!$request->input('str_id'))
            $validate['password'] = 'required';
        $validate['email'] = 'required';
        $validate['phone'] = 'required';
        $validate['level'] = 'required';
        $validate['url'] = 'required';
        $validate['status'] = 'required';
        $this->validate($request, $validate);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        $phone = $request->input('phone');
        $level = $request->input('level');
        $url = $request->input('url');
        $status = $request->input('status');

        // Create Array to Post
        $post = [];
        $post['name'] = $name;
        $post['username'] = $username;
        $post['email'] = $email;
        if ($password)
            $post['password'] = Hash::make($password);
        $post['phone'] = $phone;
        $post['level'] = $level;
        $post['url'] = $url;
        $post['status'] = $status;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        if (!$id) {
            // Insert
            $_post = $this->queries_user->insert_user($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully created!.';
            }
        } else {
            // Update
            $post['id'] = $id;
            $post['updated_at'] = date('Y-m-d H:i:s');
            $_post = $this->queries_user->update_user($post);
            if ($_post) {
                $session_post = 'Success';
                $session_message = 'Data was successfully updated!.';
            }
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $_post);
    }
}