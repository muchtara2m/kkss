<?php

$router->group([
		// 'middleware' => 'web',
        'prefix'     => ''
    ], function () use ($router) {
		include __DIR__.'/main/index.php';
		include __DIR__.'/user/index.php';
		include __DIR__.'/member/index.php';
		include __DIR__.'/admin/index.php';
		include __DIR__.'/superadmin/index.php';
		include __DIR__.'/root/index.php';
});