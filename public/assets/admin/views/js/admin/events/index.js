$(function () {
    // Datatable
    let table
    let thead = `
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th style="text-align: left;">NO</th>
            <th>KATEGORI</th>
            <th>NAMA EVENT</th>
            <th>TANGGAL EVENT</th>
            <th>LOKASI</th>
            <th>PIC</th>
            <th>TAMPILKAN</th>
            <th>CREATED DATE</th>
            <th>UPDATED DATE</th>
        </tr>
    `
    $('#example1').append($('<thead/>').append(thead))
    let tfoot = `
        <tr>
            <th search="false"></th>
            <th search="false"></th>
            <th search="false"></th>
            <th search="false"></th>
            <th search="true">KATEGORI</th>
            <th search="true">NAMA EVENT</th>
            <th search="true">TANGGAL EVENT</th>
            <th search="true">LOKASI</th>
            <th search="true">PIC</th>
            <th search="false">TAMPILKAN</th>
            <th search="true">CREATED DATE</th>
            <th search="true">UPDATED DATE</th>
        </tr>
    ` 
    $('#example1').append($('<tfoot/>').append(tfoot))
    // Setup - add a text input to each footer cell
    $('#example1 tfoot th').each( function () {
        let search = $(this).attr('search')
        if (search == 'true') {
            let title = $(this).text()
            // let title = ''
            $(this).html( '<input type="text" style="width: 90%;" placeholder="Search '+title+'" />' )
        }
    })
    // Define Url get datatable
    let _datatable_url = $('#example1').attr('url')
    // Process render data
    table = $('#example1').DataTable({
        processing: true,
        serverSide: true,
        ajax: _datatable_url,
        // dataType: "json",
        aaSorting: [[11, 'desc']],
        aaColumnDefs: [
            { 'bSortable': false, 'aTargets': [ 0, 1, 2 ] },
            // { 'bSearchable': false }
        ],
        columns: [
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <button 
                            type="button" 
                            title="Daftar peserta register"
                            onclick="_register('`+_datatable_url.replace('/get/datatable', '')+'/register/'+data.str_id+`')" 
                            class="btn btn-sm btn-info btn-class" 
                            style="border-radius: 0;"
                        >
                            <i class="fa fa-users"></i>
                        </button>
                    `
                }

            },
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <button 
                            type="button" 
                            title="Edit"
                            onclick="_form('`+_datatable_url.replace('/get/datatable', '')+'/form/'+data.str_id+`')" 
                            class="btn btn-sm btn-warning btn-class" 
                            style="border-radius: 0;"
                        >
                            <i class="fa fa-edit"></i>
                        </button>
                    `
                }

            },
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <a 
                            data-toggle="modal" 
                            href="#delete"
                            class="btn btn-sm btn-danger btn-class" 
                            style="border-radius: 0;"
                            data-str_id="`+data.str_id+`"
                        >
                            <i class="fa fa-trash-o"></i>
                        </a>
                    `
                }
            },
            {
                data: 'no',
                class: 'text-right',
                width: '5%',
                'bSortable': true,
                'bSearchable': false
            },
            {
                data: 'events_category_$_name', 
                name: 'events_category_$_name',
                width: '10%'
            },
            {
                data: 'name', 
                name: 'name',
                width: '10%'
            },
            {
                data: 'date', 
                name: 'date',
                width: '10%'
            },
            {
                data: 'location', 
                name: 'location',
                width: '15%'
            },
            {
                data: 'pic', 
                name: 'pic',
                width: '7.5%'
            },
            {
                data: 'display', 
                name: 'display',
                width: '7.5%'
            },
            {
                data: 'created_date', 
                name: 'created_at',
                width: '12.5%'
            },
            {
                data: 'updated_date', 
                name: 'updated_at',
                width: '12.5%'
            },
        ],
        initComplete: function () {
            this.api().columns().every( function () {
                if (
                        this[0] == 9
                ) {
                    let column = this
                    let select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            let val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            )     
                            column
                                .search( val ? ''+val+'' : '', true, false )
                                .draw()
                        })
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        })
                }
            })
        },
        'sDom': 'Bfrtpil',
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    })
    // Apply the search
    table.columns().every( function () {
        let that = this
        $('input', this.footer()).on('keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw()
            }
        })
    })

    // Froala JS
    $('#edit').froalaEditor({
        height: 200
    })
})

// Modal Form
function _form(url) {
    window.location.href=url
}
function _register(url) {
    window.location.href=url
}
// Modal Delete
$('#delete').on('show.bs.modal', function (event) {
    let div = $(event.relatedTarget)
    let str_id = div.data('str_id')
    let modal = $(this)
    let _url = modal.find('#str_id_delete').attr('url')+'/'+str_id
    get_data_by_str_id(modal, _url, 'delete')
})

function get_data_by_str_id(modal, _url, type) {
    $.ajax({
        type: 'GET',
        url: _url, 
        success: function(response){
            if (type=='form')
                set_modal_form(modal, response)
            if (type=='delete')
                set_modal_delete(modal, response)
        }
    })
}
function set_modal_delete(modal, response) {
    let str_id = response.str_id
    let images_id = response.images_id
        modal.find('#str_id_delete').val(str_id)
        modal.find('#images_id_delete').val(images_id)
}