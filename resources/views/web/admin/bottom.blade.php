<!-- jQuery 2.1.3 -->
<script src="{{ asset('assets/'.$pView.'/plugins/jQuery/jQuery-2.1.3.min.js') }}"></script>
<!-- jQuery UI 1.11.2 -->
<!-- <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js') }}" type="text/javascript"></script> -->
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('assets/'.$pView.'/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('assets/'.$pView.'/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="{{ asset('assets/'.$pView.'/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
<!-- datepicker -->
<script src="{{ asset('assets/'.$pView.'/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<!-- <script src="{{ asset('assets/'.$pView.'/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script> -->
<!-- iCheck -->
<script src="{{ asset('assets/'.$pView.'/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/'.$pView.'/bootstrap/js/icheck.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('assets/'.$pView.'/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('assets/'.$pView.'/plugins/fastclick/fastclick.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/'.$pView.'/dist/js/app.min.js') }}" type="text/javascript"></script>
<!-- DATA TABES -->
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/buttons.html5.min.js') }}"></script> 
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/buttons.print.min.js') }}"></script> 
<script src="{{ asset('assets/'.$pView.'/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/main/libs/select2/select2.min.js') }}"></script>