<?php

namespace App\Http\Controllers\web\member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\member\AccountQueries;

class AccountController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'account';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'account';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, AccountQueries $queries_account)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_account = $queries_account;
	}

    public function index()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Akun Saya';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = 'index';

		/* Collection variable data */
		// Get Detail User
		$session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = Crypt::decryptString($session_user);
        $user = $this->queries_account->get_user_by_id($user_id);
        if ($user) {
        	$user->image_id = $user->image?$user->image->id:null;
            $user->image_url = $user->image?\ImageUrl::url('member/images/', $user->image):null;
            $user->thumb_url = $user->image?\ImageUrl::url('member/thumbs/', $user->image):null;
        }
        $data['user'] = $user;
        // echo json_encode($user); die();
		
		// Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'user_id' => 'required',
            'name' => 'required|max:255',
            'phone' => 'required',
            'address' => 'required',
            'village' => 'required',
            'profession' => 'required|max:255',
            'email' => 'required|email|max:255'
        ]);

        // Define Variable Request
        $id = $request->input('user_id');
        $name = $request->input('name');
        $phone = $request->input('phone');
        $address = $request->input('address');
        $village_id = $request->input('village');
        $profession_id = $request->input('profession');
        $email = $request->input('email');
        $password = $request->input('password');
        $confirm_password = $request->input('confirm_password');
        $file = $request->file('file');

        // Process Query
        try {
        	// Check Password
        	if ($password) {
	        	// Check Confirm Password
				if ($password != $confirm_password) {
					Session::flash('post', 'Error');
		        	Session::flash('message', 'Password Confirm Not Match!');
					return $this->_redirect('member/account');
				}
			}
        	// Check Email
        	$check_email = $this->queries_account->get_user_by_email($email, $id);
        	if ($check_email) {
        		Session::flash('post', 'Error');
	        	Session::flash('message', 'Email Already Exist!');
        		return $this->_redirect('member/account');
        	}

        	// Transaction Query
        	\DB::beginTransaction();

        	// Insert User
	        $post_user = [];
	        $post_user['id'] = $id;
	        $post_user['name'] = $name;
	        $post_user['phone'] = $phone;
	        $post_user['email'] = $email;
            $post_user['updated_at'] = date('Y-m-d H:i:s');
	        if ($password)
	        	$post_user['password'] = Hash::make($password);
	        // echo json_encode($post_user); die();
	        $this->queries_account->update_user($post_user);
        	// Insert User Profile
	        $post_user_profile = [];
	        $post_user_profile['user_id'] = $id;
	        $post_user_profile['address'] = $address;
	        $post_user_profile['village_id'] = $village_id;
	        $post_user_profile['profession_id'] = $profession_id;
            $post_user_profile['updated_at'] = date('Y-m-d H:i:s');
	        $this->queries_account->update_user_profile($post_user_profile);

        	// Proccess Upload
	        if ($file) {
	            $image_id = $request->input('image_id')!==null?$request->input('image_id'):null;
	            $description = 'member'; // Set Description for DB Image
	            $name = (strrpos($this->sView, '/')==true?explode('/', $this->sView)[1]:$this->sView).'_'.time(); // Set File Image Name
	            $path = 'member'; // Set Path File Image
	            // Proccess Upload
	            $upload = $this->upload_single_file_image($id, $name, $description, $file, $path, $image_id);
	            if (!$upload) {
	            	Session::flash('post', 'Error');
	        		Session::flash('message', 'Failed Upload Image!');
        			return $this->_redirect('member/account');
	            }
	        }

	        \DB::commit();

	        // Set Session Flash
	        Session::flash('post', 'Success');
	        Session::flash('message', 'Data was successfully created!.');

	        // Redirect to Function
	        return $this->_redirect('member/account');
	    } catch (\Exception $e) {
	    	\DB::rollback();

	    	// Set Session Flash
	        Session::flash('post', 'Error');
	        Session::flash('message', 'Something wrong database server :( please try again.');

	        // Redirect to Function
	        return $this->_redirect('member/account');
    	}
    }
}
