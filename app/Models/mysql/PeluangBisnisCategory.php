<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class PeluangBisnisCategory extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'peluang_bisnis_category';

	protected $fillable = [
		'name',
		'description'
	];
}
