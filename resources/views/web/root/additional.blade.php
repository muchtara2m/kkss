<div class="modal fade" id="success" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 0 !important;">
            <div class="modal-header alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <label class="modal-title">Success</label>
            </div>
            <div class="modal-body">
                <span id="success_message" style="font-size:16px"></span>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="error" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <label class="modal-title">Failed</label>
            </div>
            <div class="modal-body">
                <span id="error_message" style="font-size:16px"></span>
            </div>
        </div>
    </div>
</div>
@if(Session::has('post'))
    @if(Session::get('post') == 'Success')
        <script type="text/javascript">
            $(document).ready(function() {
                var flashdata_message = '{{ Session::get("message") }}'
                $('#success_message').text(flashdata_message)
                $('#success').modal('show')
                setTimeout(function(){
                    $('#success').modal('hide')
                }, 2000)
            })
        </script>
    @else
        <script type="text/javascript">
            $(document).ready(function() {
                var flashdata_message = '{{ Session::get("message") }}'
                $('#error_message').text(flashdata_message)
                $('#error').modal('show')
                setTimeout(function(){
                    $('#error').modal('hide')
                }, 2000)
            })
        </script>
    @endif
@endif
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Are you sure you want to sign out?</h4>
            </div>
            <div class="modal-footer">
                <a href="{{ route('logout') }}" class="btn btn-primary">Yes</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>