$(function () {
    // Datatable
    let table
    let thead = `
        <tr>
            <th></th>
            <th></th>
            <th style="text-align: left;">NO</th>
            <th>COUNTRY</th>
            <th>PROVINCE</th>
            <th>NAME</th>
            <th>REGION TYPE</th>
            <th>CREATED DATE</th>
            <th>UPDATED DATE</th>
        </tr>
    `
    $('#example1').append($('<thead/>').append(thead))
    let tfoot = `
        <tr>
            <th search="false"></th>
            <th search="false"></th>
            <th search="false"></th>
            <th search="true">COUNTRY</th>
            <th search="true">PROVINCE</th>
            <th search="true">NAME</th>
            <th search="true">REGION TYPE</th>
            <th search="true">CREATED DATE</th>
            <th search="true">UPDATED DATE</th>
        </tr>
    ` 
    $('#example1').append($('<tfoot/>').append(tfoot))
    // Setup - add a text input to each footer cell
    $('#example1 tfoot th').each( function () {
        let search = $(this).attr('search')
        if (search == 'true') {
            let title = $(this).text()
            // let title = ''
            $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: 90%;" />' )
        }
    })
    // Define Url get datatable
    let _datatable_url = $('#example1').attr('url')
    // Process render data
    table = $('#example1').DataTable({
        processing: true,
        serverSide: true,
        ajax: _datatable_url,
        type: 'GET',
        // dataType: "json",
        aaSorting: [[2, 'asc']],
        aaColumnDefs: [
            { 'bSortable': false, 'aTargets': [ 0, 1 ] },
            // { 'bSearchable': false }
        ],
        columns: [
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <button 
                            type="button" 
                            title="Edit"
                            onclick="_form('`+_datatable_url.replace('/get/datatable', '')+'/form/'+data.str_id+`')" 
                            class="btn btn-sm btn-warning btn-class" 
                            style="border-radius: 0;"
                        >
                            <i class="fa fa-lg fa-edit"></i>
                        </button>
                    `
                }

            },
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <a 
                            data-toggle="modal" 
                            href="#delete"
                            title="Delete"
                            class="btn btn-sm btn-danger btn-class" 
                            style="border-radius: 0;"
                            data-str_id="`+data.str_id+`" 
                        >
                            <i class="fa fa-lg fa-trash-o"></i>
                        </a>
                    `
                }
            },
            {
                data: 'no',
                class: 'text-right',
                width: '5%',
                'bSortable': true,
                'bSearchable': false
            },
            {
                data: 'country_$_name', 
                name: 'country_$_name',
                width: '15%'
            },
            {
                data: 'province_$_name', 
                name: 'province_$_name',
                width: '15%'
            },
            {
                data: 'name', 
                name: 'name',
                width: '15%'
            },
            {
                data: 'region_type_$_name', 
                name: 'region_type_$_name',
                width: '10%'
            },
            {
                data: 'created_date', 
                name: 'created_at',
                width: '15%'
            },
            {
                data: 'updated_date', 
                name: 'updated_at',
                width: '15%'
            },
        ],
        initComplete: function () {
            this.api().columns().every( function () {
                if (
                        this[0] == 6
                ) {
                    let column = this
                    let select = $('<select style="width: 90%;"><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            let val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            )     
                            column
                                .search( val ? ''+val+'' : '', true, false )
                                .draw()
                        })
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        })
                }
            })
        },
        'sDom': 'Bfrtpil',
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    })
    // Apply the search
    table.columns().every( function () {
        let that = this
        $('input', this.footer()).on('keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw()
            }
        })
    })
})

// Modal Form
function _form(url) {
    window.location.href=url
}
// Modal Delete
$('#delete').on('show.bs.modal', function (event) {
    let div = $(event.relatedTarget)
    let str_id = div.data('str_id')
    let modal = $(this)
    let _url = modal.find('#str_id_delete').attr('url')+'/'+str_id
    get_data_by_str_id(modal, _url, 'delete')
})

function get_data_by_str_id(modal, _url, type) {
    $.ajax({
        type: 'GET',
        url: _url, 
        success: function(response){
            if (type=='delete')
                set_modal_delete(modal, response)
        }
    })
}
function set_modal_delete(modal, response) {
    let str_id = response.str_id
        modal.find('#str_id_delete').val(str_id)
}