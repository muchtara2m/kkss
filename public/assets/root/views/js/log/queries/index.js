$(function () {
    // Datatable
    let table
    let thead = `
        <tr>
            <th></th>
            <th>_ID</th>
            <th>REQUSET</th>
            <th>RESPONSE</th>
            <th>PATH</th>
            <th>LOG</th>
            <th>SESSION KEY</th>
            <th>CREATED AT</th>
            <th>UPDATED AT</th>
        </tr>
    `
    $('#example1').append($('<thead/>').append(thead))
    // Define Url get datatable
    let _datatable_url = $('#example1').attr('url')
    // Process render data
    table = $('#example1').DataTable({
        processing: true,
        serverSide: true,
        ajax: _datatable_url,
        type: 'GET',
        // dataType: "json",
        aaSorting: [[8, 'desc']],
        aaColumnDefs: [
            { 'bSortable': false, 'aTargets': [ 0 ] },
            // { 'bSearchable': false }
        ],
        columns: [
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <button 
                            type="button" 
                            title="Detail"
                            onclick="_form('`+_datatable_url.replace('/get/datatable', '')+'/form/'+data.str_id+`')" 
                            class="btn btn-sm btn-info"
                            style="padding: 3px 6px;"
                        >
                            View
                        </button>
                    `
                }

            },
            {
                data: '_id',
                class: 'text-right',
                width: '5%',
                'bSortable': true,
                'bSearchable': false
            },
            {
                data: null, 
                name: 'request',
                width: '10%',
                render:
                function(data) {
                    return `[ <span class="btn-link">JSON Data</span> ]`
                }
            },
            {
                data: null, 
                name: 'response',
                width: '10%',
                render:
                function(data) {
                    return `[ <span class="btn-link">JSON Data</span> ]`
                }
            },
            {
                data: 'path', 
                name: 'path',
                width: '10%'
            },
            {
                data: null, 
                name: 'log',
                width: '10%',
                render:
                function(data) {
                    return `[ <span class="btn-link">JSON Data</span> ]`
                }
            },
            {
                data: null, 
                name: 'session_key',
                width: '10%',
                render: 
                function(data) {
                    return `<span style="color: red;">Encrypted</span>`
                }
            },
            {
                data: 'created_date', 
                name: 'created_at',
                width: '10%'
            },
            {
                data: 'updated_date', 
                name: 'updated_at',
                width: '10%'
            },
        ],
        // 'sDom': 'Bfrtpil',
        // dom: 'Bfrtip',
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    })
})

// Modal Form
function _form(url) {
    window.location.href=url
}