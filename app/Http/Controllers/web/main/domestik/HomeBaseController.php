<?php

namespace App\Http\Controllers\web\main\domestik;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\domestik\HomeBaseQueries;

class HomeBaseController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'domestik/homebase';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'domestik/homebase';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, HomeBaseQueries $queries_home_base)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_home_base = $queries_home_base;
	}

    public function index()
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Home Base > Domestik';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		// Get Kantor Perwakilan
		$kantor_perwakilan = $this->queries_home_base->get_representative_office();
		foreach ($kantor_perwakilan as $r) {
            $r->image_url = $r->image?\ImageUrl::url('domestik/representativeoffice/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('domestik/representativeoffice/thumbs/', $r->image):null;
        }
		$data['kantor_perwakilan'] = $kantor_perwakilan;
		
		// Render to View
        return $this->_render($data);
    }
}
