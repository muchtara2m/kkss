<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTokohTable20200419231600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokoh', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('name', 255)->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->string('type', 100)->nullable();
            $table->boolean('display')->default(true);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokoh');
    }
}
