<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'token';

	protected $fillable = [
		'session_key',
		'token_key',
		'expired_at'
	];
}
