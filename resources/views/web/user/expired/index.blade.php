@extends('web/'.$pView.'/index')

@section('body')
<section class="login-box">
    <div class="login-logo">
        <a href="{{ $site_url }}" style="font-size: 24px;">
            <b>AUTH</b>
            <span> | ADMINISTRATOR</span>
        </a>
    </div>
    <div>
        <div class="col-md-12" style="background-color: #c6000d; height: 2.5px;"></div>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">
            <label>Session Expired, </label>
            <br />
            <span>Please click sign in (again) or sign out</span>
        </p>
        <form action="{{ url('user/expired/login') }}" method="POST" id="myForm">
            @if(Session::has('post'))
                @if(Session::get('post') == 'error')
                    <div class="alert alert-danger alert-dismissable" style="padding-top: 5px; padding-bottom: 5px;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <span>
                            <i class="icon fa fa-ban"></i>
                            <span>{{ Session::get("message") }}</span>
                        </span>
                    </div>
                @endif
            @endif
            <div class="form-group has-feedback">
                <input type="hidden" name="url" value="{{ (isset($userdata))?$userdata->url:null }}" required>
                <input type="hidden" name="level" value="{{ (isset($userdata))?$userdata->level:null }}" required>
                <input type="hidden" id="csrf_token" name='_token' value="{{ csrf_token() }}">
                <input class="form-control" type="text" name="email" placeholder="Email" value="{{ (isset($userdata))?$userdata->email:'your email' }}"  autocomplete="off" readonly>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input class="form-control" type="password" name="password" placeholder="Password" autocomplete="off" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <button class="btn btn-danger btn-block btn-flat" type="submit" onclick="onLoading();">Sign In</button>
                </div>
                <div class="col-xs-4">
                    <a class="btn btn-default btn-block btn-flat" href="{{ route('logout') }}">Sign Out</a>
                </div>
            </div>
        </form>
    </div>
</section>
@stop