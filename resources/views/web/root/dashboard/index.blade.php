@extends('web/'.$pView.'/index')

@section('body')
<div class="templatemo-content-wrapper">
    <div class="templatemo-content">
        <ol class="breadcrumb">
            <li class="active"># Dashboard</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
                    <a class="btn btn-sm btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="" title="Refresh">
                        <i class="fa fa-rotate-right"></i>
                    </a>
                    <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
                        Welcome to Dashboard Page!
                    </small>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom: 40px;">
            <div class="col-md-12">
                <div class="table-responsive">
                    <h3 class="margin-bottom-15">
                      Welcome to Dashboard Page!
                    </h3>
                    <hr/>
                    <div class="panel-body" style="border: 1px solid #eee;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Last logout :</h4>
                                        <h2>{{ $user->logout_time }}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Login (now) at :</h4>
                                        <h2>{{ $user->login_time }}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection