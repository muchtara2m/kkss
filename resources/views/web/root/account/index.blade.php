@extends('web/'.$pView.'/index')

@section('body')
<div class="templatemo-content-wrapper">
    <div class="templatemo-content">
        <ol class="breadcrumb">
            <li># <a href="{{ route($pView.'/'.$sView) }}">Account</a></li>
            <li class="active">Form</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
                    <a class="btn btn-sm btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="" title="Refresh">
                        <i class="fa fa-rotate-right"></i>
                    </a>
                    <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
                        {{ ($account) ? 'Last updated : '.$account->updated_date : '' }}
                    </small>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom: 40px;">
            <div class="col-lg-12">
                <div class="panel panel-default" style="border: none;">
                    <h3 class="margin-bottom-15">
                        Form {{ $account?'Edit':'Add' }} Account
                    </h3>
                    <hr/>
                    <div class="panel-body" style="border: 1px solid #eee;">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <span class="col-md-2 text-right">Name *</span>
                                        <div class="col-md-6">
                                            <input type="text" name="name" id="input-name-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->name }}" content="account">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Name is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <span class="col-md-2 text-right">Email *</span>
                                        <div class="col-md-6">
                                            <input type="email" name="email" id="input-email-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->email }}" content="account">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('email'))
                                                <span class="btn btn-danger">Email is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <span class="col-md-2 text-right">Phone</span>
                                        <div class="col-md-6">
                                            <input type="number" onkeypress="return isNumberKey(event)" name="phone" id="input-phone-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->phone }}" content="account">
                                            <small class="form-text text-muted g-font-size-default">Optional</small>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <span class="col-md-2 text-right">Username</span>
                                        <div class="col-md-6">
                                            <input type="text" name="username" id="input-username-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->username }}" content="account">
                                            <small class="form-text text-muted g-font-size-default">Optional</small>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <span class="col-md-2 text-right">Password</span>
                                        <div class="col-md-6">
                                            <input type="password" name="password" id="input-password-account" class="form-control form-control-md input-class" placeholder="Type here.." value="" content="account">
                                            <small class="form-text text-muted g-font-size-default">Enter if you want to change password, if no you can ignore.</small>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <span class="col-md-2 text-right">URL *</span>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    {{ BASE_URL }}user/
                                                </div>
                                                <input type="text" name="url" id="input-url-account" class="form-control form-control-md input-class" placeholder="Type here.." value="{{ $account->url }}" content="account">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('url'))
                                                <span class="btn btn-danger">URL is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <span class="col-md-2 text-right"></span>
                                        <div class="col-md-6">
                                            <input type="hidden" name="str_id" id="str_id" value="{{ $account?$account->str_id:'' }}">
                                            <button type="submit" class="btn btn-primary btn-class" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop