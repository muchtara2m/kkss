<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable20190326170000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('name', 255)->nullable();
            $table->string('link', 255)->nullable();
            $table->integer('parent_id')->index()->nullable();
            $table->string('favicon', 255)->nullable();
            $table->integer('position')->nullable();
            $table->string('for_role', 255)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
        });

        $data = [
            [
                'id' => 1,
                'name' => 'Dashboard',
                'link' => 'dashboard',
                'parent_id' => null,
                'favicon' => null,
                'position' => 1,
                'for_role' => 'root',
            ],
            [
                'id' => 2,
                'name' => 'Contents',
                'link' => '#',
                'parent_id' => null,
                'favicon' => null,
                'position' => 2,
                'for_role' => 'root',
            ],
            [
                'id' => 3,
                'name' => 'Logo',
                'link' => 'contents/logo',
                'parent_id' => 2,
                'favicon' => null,
                'position' => 1,
                'for_role' => 'root',
            ],
            [
                'id' => 4,
                'name' => 'Favicon',
                'link' => 'contents/favicon',
                'parent_id' => 2,
                'favicon' => null,
                'position' => 2,
                'for_role' => 'root',
            ],
            [
                'id' => 5,
                'name' => 'Title',
                'link' => 'contents/title',
                'parent_id' => 2,
                'favicon' => null,
                'position' => 3,
                'for_role' => 'root',
            ],
            [
                'id' => 6,
                'name' => 'Menu',
                'link' => 'menu',
                'parent_id' => null,
                'favicon' => null,
                'position' => 3,
                'for_role' => 'root',
            ],
            [
                'id' => 7,
                'name' => 'Header',
                'link' => 'header',
                'parent_id' => null,
                'favicon' => null,
                'position' => 4,
                'for_role' => 'root',
            ],
            [
                'id' => 8,
                'name' => 'User',
                'link' => 'user',
                'parent_id' => null,
                'favicon' => null,
                'position' => 4,
                'for_role' => 'root',
            ],
            [
                'id' => 9,
                'name' => 'Log',
                'link' => '#',
                'parent_id' => null,
                'favicon' => null,
                'position' => 5,
                'for_role' => 'root',
            ],
            [
                'id' => 10,
                'name' => 'Activity',
                'link' => 'log/activity',
                'parent_id' => 9,
                'favicon' => null,
                'position' => 1,
                'for_role' => 'root',
            ],
            [
                'id' => 11,
                'name' => 'Queries',
                'link' => 'log/queries',
                'parent_id' => 9,
                'favicon' => null,
                'position' => 2,
                'for_role' => 'root',
            ],
        ];
        DB::table('menu')->insert($data);

        $data = [
            [
                'id' => 12,
                'name' => 'Dashboard',
                'link' => 'dashboard',
                'parent_id' => null,
                'favicon' => null,
                'position' => 1,
                'for_role' => 'superadmin',
            ],
            [
                'id' => 13,
                'name' => 'Contents',
                'link' => '#',
                'parent_id' => null,
                'favicon' => null,
                'position' => 2,
                'for_role' => 'superadmin',
            ],
            [
                'id' => 14,
                'name' => 'Logo',
                'link' => 'contents/logo',
                'parent_id' => 13,
                'favicon' => null,
                'position' => 1,
                'for_role' => 'superadmin',
            ],
            [
                'id' => 15,
                'name' => 'Favicon',
                'link' => 'contents/favicon',
                'parent_id' => 13,
                'favicon' => null,
                'position' => 2,
                'for_role' => 'superadmin',
            ],
            [
                'id' => 16,
                'name' => 'Title',
                'link' => 'contents/title',
                'parent_id' => 13,
                'favicon' => null,
                'position' => 3,
                'for_role' => 'superadmin',
            ],
            [
                'id' => 17,
                'name' => 'Menu',
                'link' => 'menu',
                'parent_id' => null,
                'favicon' => null,
                'position' => 3,
                'for_role' => 'superadmin',
            ],
            [
                'id' => 18,
                'name' => 'Header',
                'link' => 'header',
                'parent_id' => null,
                'favicon' => null,
                'position' => 4,
                'for_role' => 'superadmin',
            ],
            [
                'id' => 19,
                'name' => 'User',
                'link' => 'user',
                'parent_id' => null,
                'favicon' => null,
                'position' => 4,
                'for_role' => 'superadmin',
            ],
        ];
        DB::table('menu')->insert($data);

        $data = [
            [
                'id' => 20,
                'name' => 'Dashboard',
                'link' => 'dashboard',
                'parent_id' => null,
                'favicon' => null,
                'position' => 1,
                'for_role' => 'admin',
            ],
            [
                'id' => 21,
                'name' => 'Contents',
                'link' => '#',
                'parent_id' => null,
                'favicon' => null,
                'position' => 2,
                'for_role' => 'admin',
            ],
            [
                'id' => 22,
                'name' => 'Logo',
                'link' => 'contents/logo',
                'parent_id' => 21,
                'favicon' => null,
                'position' => 1,
                'for_role' => 'admin',
            ],
            [
                'id' => 23,
                'name' => 'Favicon',
                'link' => 'contents/favicon',
                'parent_id' => 21,
                'favicon' => null,
                'position' => 2,
                'for_role' => 'admin',
            ],
            [
                'id' => 24,
                'name' => 'Title',
                'link' => 'contents/title',
                'parent_id' => 21,
                'favicon' => null,
                'position' => 3,
                'for_role' => 'admin',
            ],
            [
                'id' => 25,
                'name' => 'MAIN MENU',
                'link' => 'header',
                'parent_id' => null,
                'favicon' => null,
                'position' => 3,
                'for_role' => 'admin',
            ],
            [
                'id' => 26,
                'name' => 'Home',
                'link' => 'home',
                'parent_id' => null,
                'favicon' => null,
                'position' => 4,
                'for_role' => 'admin',
            ],
            [
                'id' => 27,
                'name' => 'Blog',
                'link' => '#',
                'parent_id' => null,
                'favicon' => null,
                'position' => 5,
                'for_role' => 'admin',
            ],
            [
                'id' => 28,
                'name' => 'Category',
                'link' => 'blog/category',
                'parent_id' => 27,
                'favicon' => null,
                'position' => 1,
                'for_role' => 'admin',
            ],
            [
                'id' => 29,
                'name' => 'Blog',
                'link' => 'blog/blog',
                'parent_id' => 27,
                'favicon' => null,
                'position' => 2,
                'for_role' => 'admin',
            ],
        ];
        DB::table('menu')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
    }
}
