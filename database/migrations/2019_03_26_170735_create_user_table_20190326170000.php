<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable20190326170000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('name', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('username', 255)->nullable();
            $table->string('phone', 255)->nullable();
            $table->string('password', 255)->nullable();
            $table->string('url', 255)->nullable();
            $table->string('level', 255)->default(null)->nullable();
            $table->datetime('login')->nullable();
            $table->datetime('logout')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->default('Active');
        });

        $data = [
            [
                'name' => 'ROOT',
                'email' => 'root@mail.com',
                'phone' => '062',
                'username' => 'root',
                'password' => bcrypt('secret'),
                'url' => 'root',
                'level' => 'root',
                'login' => null,
                'logout' => null,
            ],
            [
                'name' => 'SUPERADMIN',
                'email' => 'superadmin@mail.com',
                'phone' => '062',
                'username' => 'superadmin',
                'password' => bcrypt('secret'),
                'url' => 'superadmin',
                'level' => 'superadmin',
                'login' => null,
                'logout' => null,
            ],
            [
                'name' => 'ADMIN',
                'email' => 'admin@mail.com',
                'phone' => '062',
                'username' => 'admin',
                'password' => bcrypt('secret'),
                'url' => 'admin',
                'level' => 'admin',
                'login' => null,
                'logout' => null,
            ]
        ];
        DB::table('user')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
