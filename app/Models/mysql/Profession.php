<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'profession';

	protected $fillable = [
		'name'
	];
}
