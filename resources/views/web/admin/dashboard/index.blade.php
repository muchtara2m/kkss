@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Dashboard</h1>
        <ol class="breadcrumb">
            <li># Dashboard</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
        </div>
    </div>
    <section class="content">
        <h4 style="margin-top: -15px;">
            <label for="editor1">Welcome to page Admin !</label>
        </h4>
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body" style="padding: 10px 20px;">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Last logout :</h4>
                                <h2>{{ $user->logout }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body" style="padding: 10px 20px;">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Login (now) at :</h4>
                                <h2>{{ $user->login }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop