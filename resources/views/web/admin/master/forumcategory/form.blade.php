@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Forum Category</h1>
        <ol class="breadcrumb">
            <li># <a>Master</a></li>
            <li><a href="{{ route($pView.'/'.$sView) }}">Forum Category</a></li>
            <li class="active">Form</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ ($forum_category) ? 'Last updated : '.$forum_category->updated_date : '' }}
            </span>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <h2 style="margin-top: -15px; font-size: 20px;">
            <label for="editor1">Form {{ $forum_category?'Edit':'Add' }} Forum Category</label>
        </h2>
        <div class="box">
            <div class="box-body" style="padding-top: 25px;">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Name *</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" id="name" class="form-control form-control-md" placeholder="Type here.." value="{{ $forum_category?$forum_category->name:'' }}" autocomplete="off" required="">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Name is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Description *</label>
                                        <div class="col-md-8">
                                            <textarea name="description" id="description" class="form-control form-control-md" rows="5" required="">{{ $forum_category?$forum_category->description:'' }}</textarea>
                                        </div>
                                        <div class="col-md-2">
                                            @if($errors->get('description'))
                                                <span class="btn btn-danger">Description is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Image *</label>
                                        <div class="col-md-6">
                                            <p>Attach file <small>(.jpeg/.jpg/.png)</small></p>
                                            <label class="u-file-attach-v2 g-color-gray-dark-v5 mb-20">
                                                <input type="file" name="file" id="file_attachment" class="file_attachment" {{ $forum_category?'':'required=""' }}>
                                                <small style="color: red !important; display: none;" id="error_image">* please select file which on type (.jpeg/.jpg/.png)</small>
                                                <img src="{{ $forum_category?$forum_category->thumb_url?$forum_category->thumb_url:'':'' }}" id="preview_image" alt="Preview Image" style="width: 50%; {{ $forum_category?$forum_category->image_url?'margin-top: 15px;':'display: none':'display: none' }}">
                                                @if($forum_category&&$forum_category->image_url)
                                                <br /><a href="{{ $forum_category->image_url }}" target="_blank" id="link_image">Click to view large</a>
                                                <input type="hidden" name="image_id" id="image_id" value="{{ $forum_category?$forum_category->image_id:'' }}">
                                                @endif
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('image'))
                                                <span class="btn btn-danger">Image is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="str_id" id="str_id" value="{{ $forum_category?$forum_category->str_id:'' }}">
                                            <button type="submit" class="btn btn-primary btn-class" style="border-radius: 0;" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default" style="border-radius: 0;"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop