<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertMenuTable20200323205600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            UPDATE menu SET link='#' where id=40;
        ");
        DB::statement("
            INSERT INTO menu (name, link, parent_id, favicon, position, for_role) VALUES
            ('Adat Istiadat', 'adatistiadat', 40, NULL, 1, 'member'),
            ('Struktur Organisasi', 'strukturorganisasi', 40, NULL, 2, 'member');
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
