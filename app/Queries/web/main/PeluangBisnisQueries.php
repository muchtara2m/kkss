<?php

namespace App\Queries\web\main;

class PeluangBisnisQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $PeluangBisnisModel = app($this->model_path.'PeluangBisnis');
        $this->PeluangBisnisModel = $PeluangBisnisModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_peluang_bisnis($keyword=null)
    {
        $data = $this->PeluangBisnisModel::where('status', 'Active')
                ->where('display', true);
        if ($keyword)
            $data = $data->where('name', 'like', '%'.$keyword.'%');
        $data = $data->orderBy('updated_at', 'desc')
                    ->paginate(2);
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserModel = app($this->model_path.'User');
        $PeluangBisnisCategoryModel = app($this->model_path.'PeluangBisnisCategory');
        $PeluangBisnisCommentsModel = app($this->model_path.'PeluangBisnisComments');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'peluang_bisnis')
                    ->get()
                    ->first();
            $v->image = $image;
            $user = $UserModel::where('id', $v->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $peluang_bisnis_category = $PeluangBisnisCategoryModel::where('id', $v->peluang_bisnis_category_id)
                                        ->get()
                                        ->first();
            $v->category = $peluang_bisnis_category;
            $comments = $PeluangBisnisCommentsModel::where('peluang_bisnis_id', $v->id)
                        ->count();
            $v->comments = $comments;
        }
        return $this->_resJson($data);
    }

    function get_peluang_bisnis_by_peluang_bisnis_category_id($peluang_bisnis_category_id, $keyword=null)
    {
        $data = $this->PeluangBisnisModel::where('status', 'Active')
                ->where('display', true)
                ->where('peluang_bisnis_category_id', $peluang_bisnis_category_id);
        if ($keyword)
            $data = $data->where('name', 'like', '%'.$keyword.'%');
        $data = $data->orderBy('updated_at', 'desc')
                ->paginate(2);
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserModel = app($this->model_path.'User');
        $PeluangBisnisCategoryModel = app($this->model_path.'PeluangBisnisCategory');
        $PeluangBisnisCommentsModel = app($this->model_path.'PeluangBisnisComments');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'peluang_bisnis')
                    ->get()
                    ->first();
            $v->image = $image;
            $user = $UserModel::where('id', $v->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $peluang_bisnis_category = $PeluangBisnisCategoryModel::where('id', $v->peluang_bisnis_category_id)
                                        ->get()
                                        ->first();
            $v->category = $peluang_bisnis_category;
            $comments = $PeluangBisnisCommentsModel::where('peluang_bisnis_id', $v->id)
                        ->count();
            $v->comments = $comments;
        }
        return $this->_resJson($data);
    }

    function get_peluang_bisnis_by_seo($seo)
    {
        $data = $this->PeluangBisnisModel::where('status', 'Active')
                ->where('display', true)
                ->where('seo', $seo)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'peluang_bisnis')
                    ->get()
                    ->first();
            $data->image = $image;
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->created_by)
                    ->get()
                    ->first();
            $data->user = $user;
            // Get Peluang Bisnis Category
            $PeluangBisnisCategoryModel = app($this->model_path.'PeluangBisnisCategory');
            $peluang_bisnis_category = $PeluangBisnisCategoryModel::where('id', $data->peluang_bisnis_category_id)
                    ->get()
                    ->first();
            $data->category = $peluang_bisnis_category;
        }
        return $this->_resJson($data);
    }

    function get_peluang_bisnis_by_id($id)
    {
        $data = $this->PeluangBisnisModel::where('status', 'Active')
                ->where('display', true)
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'peluang_bisnis')
                    ->get()
                    ->first();
            $data->image = $image;
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->created_by)
                    ->get()
                    ->first();
            $data->user = $user;
        }
        return $this->_resJson($data);
    }

    function get_peluang_bisnis_category()
    {
        // Define Model
        $PeluangBisnisCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\PeluangBisnisCategory');
        // Get Data
        $category_blog = $PeluangBisnisCategoryModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $category_blog;
    }

    function get_peluang_bisnis_category_by_id($id)
    {
        // Define Model
        $PeluangBisnisCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\PeluangBisnisCategory');
        // Get Data
        $category_blog = $PeluangBisnisCategoryModel::where('status', 'Active')
                    ->where('id', $id)
                    ->get()
                    ->first();
        return $category_blog;
    }

    function count_peluang_bisnis_by_peluang_bisnis_category_id($peluang_bisnis_category_id)
    {
        $data = $this->PeluangBisnisModel::where('status', 'Active')
                ->where('display', true)
                ->where('peluang_bisnis_category_id', $peluang_bisnis_category_id)
                ->get()
                ->count();
        return $this->_resJson($data);
    }

    function get_peluang_bisnis_comments_by_peluang_bisnis_id($peluang_bisnis_id, $parent_id=null)
    {
        $PeluangBisnisCommentsModel = app($this->model_path.'PeluangBisnisComments');
        $data = $PeluangBisnisCommentsModel::where('status', 'Active')
                ->where('peluang_bisnis_id', $peluang_bisnis_id);
        if ($parent_id) {
            $data = $data->where('parent_id', $parent_id);
        } else
            $data = $data->whereNull('parent_id');
        $data = $data->get();
        // Get User & Image
        $UserModel = app($this->model_path.'User');
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $r) {
            $r->image = asset('assets/main/images/user-photo.jpg');
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            if ($r->user_id) {
                $user = $UserModel::where('status', 'Active')
                        ->where('id', $r->user_id)
                        ->get()
                        ->first();
                $r->user = $user ?? null;
                if ($user) {
                    $r->name = $r->user->name;
                    $r->email = $r->user->email;
                    $r->phone = $r->user->phone;
                    $image = $ImageModel::where('param_id', $user->id)
                            ->where('description', 'member')
                            ->get()
                            ->first();
                    if ($image)
                        $r->image = \ImageUrl::url('member/images/', $image);
                }
            }
        }
        return $this->_resJson($data);
    }

    function insert_peluang_bisnis_comments($post)
    {
        $PeluangBisnisCommentsModel = app($this->model_path.'PeluangBisnisComments');
        $data = $PeluangBisnisCommentsModel::insert($post);
        return $this->_resJson($data);
    }
}