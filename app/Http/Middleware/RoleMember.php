<?php

namespace App\Http\Middleware;

use App\Queries\ControllerQueries;
use Closure;
use Redirect;

class RoleMember
{
    public function handle($request, Closure $next)
    {
        // Define Session
        $session = 'member';

        // Define Library Extends
        $queries_controller = new ControllerQueries();

        // Check Authentication User
        $_auth = $queries_controller->check_auth(env('SESSION').$session, env('COOKIE').$session);
        if (!$_auth)
            return \ErrorWeb::notfound();

        return $next($request);
    
    }
}