// Validation Form
$(function() {
    $('#file_attachment').on('change', function(){
        $('#link_image').hide()
        let file = $(this)[0].files[0]
        if(file.type == 'image/jpeg' || file.type == 'image/jpg' || file.type == 'image/png') {
            $('.btn-class').attr('disabled', false)
            $('#error_image').css('display', 'none')
        } else {
            $('.btn-class').attr('disabled', true)
            $('#error_image').css('display', '')
        }
    })
    // Froala JS
    $('#edit').froalaEditor({
        height: 200
    })
    // DatePicker JS
    $('#date').datepicker()
    $('#events_category').select2({
        placeholder: "Pilih Kategori",
        allowClear: true
    });
})
// Preview Image
function preview_image(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader()
        reader.onload = function (e) {
            $('#preview_image').attr('src', e.target.result)
            $('#preview_image').css('display', '')
            $('#preview_image').css('margin-top', '15px')
        }
        reader.readAsDataURL(input.files[0])
    }
}
$('.file_attachment').change(function(){
    preview_image(this)
})

// Set Froala JS Value
$('#edit').froalaEditor('html.set', '-- Value --')