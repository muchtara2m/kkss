<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEventsTable20200420112100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE events Add created_by Int(11) NOT NULL DEFAULT 3;');
        DB::statement('ALTER TABLE events ADD INDEX created_by (created_by);');
        DB::statement('ALTER TABLE events Add seo varchar(255) NULL;');
        DB::statement('ALTER TABLE events ADD INDEX seo (seo);');
        DB::statement('ALTER TABLE events Add events_category_id Int(11) NULL;');
        DB::statement('ALTER TABLE events ADD INDEX events_category_id (events_category_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
