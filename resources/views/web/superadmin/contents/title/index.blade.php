@extends('web/'.$pView.'/index')

@section('body')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">       
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Title</li>
        </ol>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header" style="margin-top: 10px;">
                Title
            </h2>
            <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
                <a class="btn btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
                    <i class="fa fa-arrow-left"></i>
                </a>
                <a class="btn btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
                    <i class="fa fa-arrow-right"></i>
                </a>
                <a class="btn btn-default" href="" title="Refresh">
                    <i class="fa fa-rotate-right"></i>
                </a>
                <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
                    {{ ($title) ? 'Last updated : '.$title->updated_date : '' }}
                </small>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form {{ $title?'Edit':'Add' }} Title
                </div>
                <div class="panel-body">
                    {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                        <fieldset>
                            <legend>General</legend>
                            <div class="form-group">
                                <div class="row">
                                    <span class="col-md-2 text-right">Title</span>
                                    <div class="col-md-6">
                                        <input type="text" name="value_string" id="value_string" class="form-control form-control-md" placeholder="Type here.." value="{{ $title?$title->value_string:'' }}" autocomplete="off">
                                        <small class="form-text text-muted g-font-size-default">Optional</small>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-2 text-right">Image *</label>
                                    <div class="col-md-6">
                                        <p>Attach file <small>(.jpeg/.jpg/.png)</small></p>
                                        <label class="u-file-attach-v2 g-color-gray-dark-v5 mb-20">
                                            <input type="file" name="file" id="file_attachment" class="file_attachment">
                                            <small style="color: red !important; display: none;" id="error_image">* please select file which on type (.jpeg/.jpg/.png)</small>
                                            <img src="{{ $title?$title->image_url?$title->image_url:'':'' }}" id="preview_image" alt="Preview Image" style="width: 50%; {{ $title?$title->image_url?'margin-top: 15px;':'display: none':'display: none' }}">
                                            @if($title&&$title->image_url)
                                            <br /><a href="{{ $title->image_url }}" target="_blank" id="link_image">Click to view large</a>
                                            <input type="hidden" name="image_id" id="image_id" value="{{ $title?$title->image_id:'' }}">
                                            @endif
                                        </label>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend></legend>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-2 text-right"></label>
                                    <div class="col-md-6">
                                        <input type="hidden" name="str_id" id="str_id" value="{{ $title?$title->str_id:'' }}">
                                        <button type="submit" class="btn btn-primary btn-class" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                        <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left"></i> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop