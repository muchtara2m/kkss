<!-- MODAL -->
<div class="modal fade" id="delete" tabindex="-1" style="top:5%" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="border-radius: 0 !important;">
            <div class="modal-header">
                <h4 class="modal-title">Confirm</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <span style="font-size:16px">Are you sure want to delete this ?</span>
            </div>
            {{ Form::open(['route' => $pView.'/'.$sView.'/register/delete', 'method' => 'post']) }}
                <div class="modal-footer">
                    {{ Form::hidden('delete', 'soft', ['id' => 'delete_elete']) }}
                    {{ Form::hidden('str_id', null, ['id' => 'str_id_delete', 'url' => url($pView.'/'.$sView.'/register/get/data/id')]) }}
                    {{ Form::hidden('events_id', $events->id, ['id' => 'events_id']) }}
                    {{ Form::submit('Yes', ['class' => 'btn btn-sm btn-danger', 'style' => 'border-radius: 0;']) }}
                    {{ Form::button('Cancel', ['class' => 'btn btn-sm btn-default', 'style' => 'border-radius: 0;', 'data-dismiss' => 'modal']) }}
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>