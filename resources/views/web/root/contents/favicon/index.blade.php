@extends('web/'.$pView.'/index')

@section('body')
<div class="templatemo-content-wrapper">
    <div class="templatemo-content">
        <ol class="breadcrumb">
            <li># <a href="{{ route($pView.'/'.$sView) }}">Favicon</a></li>
            <li class="active">Form</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <div class="" style="margin-top: -10px;padding-bottom: 10px;border-bottom: 1px solid #ddd;">
                    <a class="btn btn-sm btn-default" href="javascript: history.go(-1)" title="Back" style="margin-right: 5px;">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="javascript: history.forward()" title="Forward" style="margin-right: 5px;">
                        <i class="fa fa-arrow-right"></i>
                    </a>
                    <a class="btn btn-sm btn-default" href="" title="Refresh">
                        <i class="fa fa-rotate-right"></i>
                    </a>
                    <small class="pull-right" style="margin-top: 15px; color: #6b6b6b; font-style: italic;">
                        {{ ($favicon) ? 'Last updated : '.$favicon->updated_date : '' }}
                    </small>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom: 40px;">
            <div class="col-lg-12">
                <div class="panel panel-default" style="border: none;">
                    <h3 class="margin-bottom-15">
                        Form {{ $favicon?'Edit':'Add' }} Favicon
                    </h3>
                    <hr/>
                    <div class="panel-body" style="border: 1px solid #eee;">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Image *</label>
                                        <div class="col-md-6">
                                            <p>Attach file <small>(.jpeg/.jpg/.png)</small></p>
                                            <label class="u-file-attach-v2 g-color-gray-dark-v5 mb-20">
                                                <input type="file" name="file" id="file_attachment" class="file_attachment" required="">
                                                <small style="color: red !important; display: none;" id="error_image">* please select file which on type (.jpeg/.jpg/.png)</small>
                                                <img src="{{ $favicon?$favicon->image_url?$favicon->image_url:'':'' }}" id="preview_image" alt="Preview Image" style="width: 50%; {{ $favicon?$favicon->image_url?'margin-top: 15px;':'display: none':'display: none' }}">
                                                @if($favicon&&$favicon->image_url)
                                                <br /><a href="{{ $favicon->image_url }}" target="_blank" id="link_image">Click to view large</a>
                                                <input type="hidden" name="image_id" id="image_id" value="{{ $favicon?$favicon->image_id:'' }}">
                                                @endif
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('image'))
                                                <span class="btn btn-danger">Image is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="str_id" id="str_id" value="{{ $favicon?$favicon->str_id:'' }}">
                                            <button type="submit" class="btn btn-primary btn-class" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop