<?php

namespace App\Http\Controllers\web\main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\MembershipQueries;

class MembershipController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'membership';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'membership';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, MembershipQueries $queries_membership)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_membership = $queries_membership;
	}

    public function index(Request $request)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Membership';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'index';
		$data['vAdditional']['js'] = null;

		// Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

		/* Collection variable data */
		// Get Membership
		$membership = $this->queries_membership->get_membership('Nasional', $keyword);
        foreach ($membership as $r) {
        	$r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('member/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('member/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
        }
        $data['membership']['nasional'] = $membership;
        $membership = $this->queries_membership->get_membership('International', $keyword);
        foreach ($membership as $r) {
        	$r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $r->image_url = $r->image?\ImageUrl::url('member/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('member/thumbs/', $r->image):null;
            $r->user_name = $r->user?$r->user->name:null;
        }
        $data['membership']['international'] = $membership;
		
		// Render to View
        return $this->_render($data);
    }

    public function member($param)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Member > Mmbership';
		// Filename in folder view
		$data['cView'] = 'member';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = null;
		$data['vAdditional']['js'] = null;

		/* Collection variable data */
		// Get Detail User
        $user_id = $param;
        $user = $this->queries_membership->get_user_by_id($user_id);
        if ($user) {
        	$user->image_id = $user->image?$user->image->id:null;
            $user->image_url = $user->image?\ImageUrl::url('member/images/', $user->image):null;
            $user->thumb_url = $user->image?\ImageUrl::url('member/thumbs/', $user->image):null;
        }
        $data['member'] = $user;
        // echo json_encode($user); die();
		
		// Render to View
        return $this->_render($data);
    }
}
