<?php

namespace App\Queries\web\admin\master;

class GalleryCategoryQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $GalleryCategoryModel = app($this->model_path.'GalleryCategory');
        $this->GalleryCategoryModel = $GalleryCategoryModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_gallery_category_datatable($whereRaw, $order, $offset, $limit)
    {
        $gallery_category = $this->GalleryCategoryModel::select(
                        'gallery_category.*'
                    )
                    ->where('gallery_category.status', 'Active');
        if ($whereRaw!='')
            $gallery_category = $gallery_category->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'gallery_category.'.$order['coloumn'];
        $gallery_category = $gallery_category->orderBy($order_column, $order['dir'])
                    ->orderBy('gallery_category.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($gallery_category as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($gallery_category);
    }

    function get_gallery_category_total($whereRaw)
    {
        $gallery_category = $this->GalleryCategoryModel::select('gallery_category.id')
                    ->where('gallery_category.status', 'Active');
        if ($whereRaw!='')
            $gallery_category = $gallery_category->whereRaw($whereRaw);
        $gallery_category = $gallery_category->count();
        return $this->_resJson($gallery_category);
    }

    function get_gallery_category_by_id($id)
    {
        $data = $this->GalleryCategoryModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_gallery_category_last()
	{
		$data = $this->GalleryCategoryModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_gallery_category($post)
	{
		$data = $this->GalleryCategoryModel::insert($post);
		return $this->_resJson($data);
	}

	function update_gallery_category($post)
	{
		$data = $this->GalleryCategoryModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_gallery_category($id)
	{
		$data = $this->GalleryCategoryModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_gallery_category($id)
	{
		$data = $this->GalleryCategoryModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
