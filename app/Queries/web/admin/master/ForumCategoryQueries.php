<?php

namespace App\Queries\web\admin\master;

class ForumCategoryQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $ForumCategoryModel = app($this->model_path.'ForumCategory');
        $this->ForumCategoryModel = $ForumCategoryModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_forum_category_datatable($whereRaw, $order, $offset, $limit)
    {
        $forum_category = $this->ForumCategoryModel::select(
                        'forum_category.*'
                    )
                    ->where('forum_category.status', 'Active');
        if ($whereRaw!='')
            $forum_category = $forum_category->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'forum_category.'.$order['coloumn'];
        $forum_category = $forum_category->orderBy($order_column, $order['dir'])
                    ->orderBy('forum_category.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        foreach ($forum_category as $key => $value) {
            $value->no = $key + 1 + $offset;
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($forum_category);
    }

    function get_forum_category_total($whereRaw)
    {
        $forum_category = $this->ForumCategoryModel::select('forum_category.id')
                    ->where('forum_category.status', 'Active');
        if ($whereRaw!='')
            $forum_category = $forum_category->whereRaw($whereRaw);
        $forum_category = $forum_category->count();
        return $this->_resJson($forum_category);
    }

    function get_forum_category_by_id($id)
    {
        $data = $this->ForumCategoryModel::where('status', 'Active')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'forum_category')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

	function get_forum_category_last()
	{
		$data = $this->ForumCategoryModel::where('status', 'Active')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
	}

	function insert_forum_category($post)
	{
		$data = $this->ForumCategoryModel::create($post);
		return $this->_resJson($data);
	}

	function update_forum_category($post)
	{
		$data = $this->ForumCategoryModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
	}

	function delete_forum_category($id)
	{
		$data = $this->ForumCategoryModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
	}

	function destroy_forum_category($id)
	{
		$data = $this->ForumCategoryModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
	}
}
