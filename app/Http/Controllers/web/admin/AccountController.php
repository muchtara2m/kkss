<?php

namespace App\Http\Controllers\web\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\AccountQueries;

class AccountController extends \App\Http\Core\WebController 
{
	// Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'account';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'account';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, AccountQueries $queries_account)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_account = $queries_account;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Account';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get User
        $session_user = Session::get(env('SESSION').$this->sessions);
        $user_id = Crypt::decryptString($session_user);
        $user = $this->queries_account->get_user_by_id($user_id);
        $data['account'] = $user;

    	// Render to View
        return $this->_render($data);
    }

    function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'url' => 'required|max:255'
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $url = $request->input('url');
        $phone = $request->input('phone');
        $username = $request->input('username');
        $password = (!empty($request->input('password'))) ? $request->input('password') : null;
        if ($password == '' or $password == null) {
            $user = $this->queries_account->get_user_by_id($id);
            $password = $user->password;
        } else {
            $password = Hash::make($password);
        }

        // Create Array to Post
        $post = [];
        $post['id'] = $id;
        $post['name'] = $name;
        $post['email'] = $email;
        $post['url'] = $url;
        $post['phone'] = $phone;
        $post['username'] = $username;
        $post['password'] = $password;

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        // Update
        $_post = $this->queries_account->update_user($post);
        if ($post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully updated!.';
        }

        // Return
        if (!empty($request->input('post_form')) && $request->input('post_form') == 'ajax') {
            echo json_encode([
                'ajax' => $session_post,
                'message' => $session_message,
                'csrf_token' => csrf_token()
                // 'csrf_token' => $request->session()->token()
            ]);
        } else {
            // Set Session Flash
            Session::flash('post', $session_post);
            Session::flash('message', $session_message);

            // Redirect to Function
            return $this->_redirect($this->global_path, $_post);
        }
    }
}
