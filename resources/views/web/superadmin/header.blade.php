<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route($pView.'/dashboard')}}" target="_blank"><span>SUPERADMIN</span> PAGE</a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="{{ route($pView.'/account') }}"><span class="glyphicon glyphicon-user"></span> Account</a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="{{ route('logout') }}"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>