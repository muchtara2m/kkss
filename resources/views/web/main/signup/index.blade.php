@extends('web/'.$pView.'/index')

@section('body')
<section id="form">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 box-form-a">
                <div class="login-form">
                    <h2 class="text-center">Daftar akun anda</h2>
                    <p class="text-center" style="margin-top: -20px; margin-bottom: 30px;">Isi form dibawah dengan benar.</p>
                    @if(Session::has('post'))
                        @if(Session::get('post') == 'Error')
                            <span class="help-block text-center" style="margin-top: -15px;">
                                <strong style="color: red;">{{ Session::get("message") }}</strong>
                            </span>
                        @endif
                    @endif
                    {{ Form::open(['route' => $sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'signup']) }}
                        <div class="form-group">
                            <span class="text-right">Nama Lengkap</span>
                            <input type="text" name="name" id="name" required="" autocomplete="off" value="{{ old('name') }}">
                            @if($errors->get('name'))
                                <small class="btn btn-sm btn-danger">Nama harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">No. Handphone</span>
                            <input type="number" name="phone" onkeypress="return isNumberKey(event)" id="phone" autocomplete="off" required="" value="{{ old('phone') }}">
                            @if($errors->get('phone'))
                                <small class="btn btn-sm btn-danger">No. Handphone harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Profesi</span>
                            <select name="profession" id="profession" required=""></select>
                            @if($errors->get('profession'))
                                <small class="btn btn-sm btn-danger">Profesi harus diisi</small>
                            @endif
                        </div>
                        <hr/>
                        <div class="form-group">
                            <span class="text-right">Alamat Lengkap</span>
                            <textarea name="address" id="address" cols="10" rows="3" required="" autocomplete="off">{{ old('address') }}</textarea>
                            @if($errors->get('address'))
                                <small class="btn btn-sm btn-danger">Alamat harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Provinsi</span>
                            <select name="province" id="province" required=""></select>
                            @if($errors->get('province'))
                                <small class="btn btn-sm btn-danger">Provinsi harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Kota/Kabupaten</span>
                            <select name="city" id="city" required=""></select>
                            @if($errors->get('city'))
                                <small class="btn btn-sm btn-danger">Kota/Kabupaten harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Kecamatan</span>
                            <select name="district" id="district" required=""></select>
                            @if($errors->get('district'))
                                <small class="btn btn-sm btn-danger">Kecamatan harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Desa/Kelurahan</span>
                            <select name="village" id="village" required=""></select>
                            @if($errors->get('village'))
                                <small class="btn btn-sm btn-danger">Desa/Kelurahan harus diisi</small>
                            @endif
                        </div>
                        <hr/>
                        <div class="form-group">
                            <span class="text-right">Email</span>
                            <input type="email" name="email" id="email" required="" autocomplete="off" value="{{ old('email') }}">
                            @if($errors->get('email'))
                                <small class="btn btn-sm btn-danger">Email harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Password</span>
                            <input type="password" name="password" id="password" required="" autocomplete="off">
                            @if($errors->get('password'))
                                <small class="btn btn-sm btn-danger">Password harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span class="text-right">Konfirmasi Password</span>
                            <input type="password" name="confirm_password" id="confirm_password" required="" autocomplete="off">
                            <small id="confirm_password_error" class="btn btn-sm btn-danger" style="display: none;">Password harus sama</small>
                        </div>
                        <div class="form-group">
                            <span class="text-right">Foto Profil</span>
                            <p>Jenis file <small>(.jpeg/.jpg/.png)</small></p>
                            <input type="file" name="file" id="file_attachment" class="file_attachment" required="">
                            <small style="color: red !important; display: none;" id="error_image">* please select file which on type (.jpeg/.jpg/.png)</small>
                            <img src="" id="preview_image" alt="Preview Image" style="width: 30%; display: none;">
                            @if($errors->get('image'))
                                <small class="btn btn-sm btn-danger">Foto profil harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <span>
                                <input type="checkbox" class="checkbox" name="agreement" id="agreement"> 
                                *Dengan mendaftar berarti Anda menyetujui Syarat & Ketentuan yang ada di KKSS
                            </span>
                        </div>
                        <center>
                            <button type="submit" class="btn btn-default btn-class" style="width: 40%;">Daftar</button>
                        </center>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection