<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'video';

	protected $fillable = [
		'param_id',
		'name',
		'ext',
		'size',
		'resolution',
		'description',
		'storage'
	];
}
