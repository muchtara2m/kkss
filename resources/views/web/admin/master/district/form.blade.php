@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>District</h1>
        <ol class="breadcrumb">
            <li># <a>Master</a></li>
            <li><a href="{{ route($pView.'/'.$sView) }}">District</a></li>
            <li class="active">Form</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ ($district) ? 'Last updated : '.$district->updated_date : '' }}
            </span>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <h2 style="margin-top: -15px; font-size: 20px;">
            <label for="editor1">Form {{ $district?'Edit':'Add' }} District</label>
        </h2>
        <div class="box">
            <div class="box-body" style="padding-top: 25px;">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Province *</label>
                                        <div class="col-md-6">
                                            <select name="province_id" id="province_id" class="form-control" required="" url="{{ url($pView.'/'.$sView.'/get/city/province/id/') }}">
                                                <option value="">Select Province</option>
                                                @foreach($province as $r)
                                                <option value="{{ $r->id }}" {{$district?$r->id==$district->province_id?'selected=""':'':''}}>{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Name is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">City *</label>
                                        <div class="col-md-6">
                                            <select name="city_id" id="city_id" class="form-control" required="" readonly="" style="pointer-events: none;">
                                                <option value="">Select City</option>
                                                @foreach($city as $r)
                                                <option value="{{ $r->id }}" {{$district?$r->id==$district->city_id?'selected=""':'':''}}>{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Name is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Name *</label>
                                        <div class="col-md-6">
                                            <input type="text" name="name" id="name" class="form-control form-control-md" placeholder="Type here.." value="{{ $district?$district->name:'' }}" autocomplete="off" required="">
                                        </div>
                                        <div class="col-md-4">
                                            @if($errors->get('name'))
                                                <span class="btn btn-danger">Name is required</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="str_id" id="str_id" value="{{ $district?$district->str_id:'' }}">
                                            <button type="submit" class="btn btn-primary btn-class" style="border-radius: 0;" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default" style="border-radius: 0;"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop