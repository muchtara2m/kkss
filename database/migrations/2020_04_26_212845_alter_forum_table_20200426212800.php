<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterForumTable20200426212800 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE forum Add short_description Text NULL;');
        DB::statement('ALTER TABLE forum Add created_by Int(11) NOT NULL DEFAULT 3;');
        DB::statement('ALTER TABLE forum ADD INDEX created_by (created_by);');
        DB::statement('ALTER TABLE forum Add seo varchar(255) NULL;');
        DB::statement('ALTER TABLE forum ADD INDEX seo (seo);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
