<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterForumCategoryTable20200212134200 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE forum_category Add seo VARCHAR(255) NULL;');
        DB::statement('
            UPDATE forum_category AS t1 
                INNER JOIN forum_category AS t2 ON t2.id = t1.id
                SET t1.seo = REPLACE(LOWER(t2.name), " ", "-")
            WHERE 1=1;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
