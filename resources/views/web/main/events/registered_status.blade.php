@extends('web/'.$pView.'/index')

@section('body')
<section id="do_action" style="margin-bottom: 40px;">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="{{ url($sView) }}">Events</a></li>
              <li class="active">Register Status</li>
            </ol>
        </div>
    </div>
</section>
<section id="cart_items" class="mg-50">
	<div class="container">
		<div class="table-responsive">
			<table class="table table-responsive table-hover">
				<thead>
					<tr style="background: #dc3545; color: white;">
						<th></th>
						<th>Nama Events</th>
						<th>Detail Events</th>
						<th>Kategori</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($events as $row)
					<tr>
						<td><img src="{{ $row->image_url }}" alt="image" style="width: 130px; height: 80px;"></td>
						<td><a href="{{ url('events/'.$row->events->seo) }}" class="btn btn-lg btn-link" style="color: #dc3545;">{{ $row->events->name }}</a></td>
						<td>
							<p class="text-muted" style="font-size: 12px;">
								Tanggal <b>{{ $row->events->date }}</b><br/>
								Lokasi <b>{{ $row->events->location }}</b><br/>
								PIC <b>{{ $row->events->pic }}</b>
							</p>
						</td>
						<td>{{ $row->category_name }}</td>
						<td><a href="javascript:;" class="btn btn-sm btn-default">{{ $row->is_status }}</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>
@endsection