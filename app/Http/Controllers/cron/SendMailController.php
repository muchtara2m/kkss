<?php

namespace App\Http\Controllers\cron;

use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\cron\SendMailQueries;
use App\Jobs\RegisterVerifiedJob;
use App\Jobs\ForgotPasswordJob;

class SendMailController extends \App\Http\Core\CronController
{
    // Global variable queries controller
    protected $queries_controller = null;

    // Global variable queries send mail
    protected $queries_send_mail = null;

    protected $mode = 'normal'; // normal, queue

	public function send_mail()
	{
        // Define Queries Controller
        $queries_controller = (new ControllerQueries());
        $this->queries_controller = $queries_controller;
        // Define Queries Send Mail
        $queries_send_mail = (new SendMailQueries());
        $this->queries_send_mail = $queries_send_mail;

    	$send_mail = $this->queries_send_mail->get_send_mail();
    	foreach ($send_mail as $k => $v) {
            $v['send_email_id'] = $v['id'];
            // Send Mail with Job - Queue
            if ($this->mode=='queue') {
                if ($v['module']=='register_verified') {
                    $jobClass = (new RegisterVerifiedJob($v->getAttributes()));
                } else
                if ($v['module']=='forgot_password') {
                    $jobClass = (new ForgotPasswordJob($v->getAttributes()));
                }
                $onQueue = 'email_'.$v['module'].'_'.date('YmdHis');
                $this->set_queue_send_email($jobClass, $onQueue);
            } else
            // Send Mail with Normally
            if ($this->mode=='normal') {
                $this->send_mail_to($v->getAttributes(), $v['module'], $v['to']);
            }
    	}
	}
}
