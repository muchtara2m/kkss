<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="companyinfo">
                        <h2><span>KK</span>SS</h2>
                        <p>Sulawesi Selatan</p>
                    </div>
                </div>
                <div class="col-sm-7">
                    @foreach($representative_office as $row)
                    <div class="col-sm-3">
                        <div class="video-gallery text-center">
                            <a href="#">
                                <div class="iframe-img">
                                    <img src="{{ $row->image_url }}" alt="" />
                                </div>
                            </a>
                            <p>{{ $row->str }}</p>
                            <h2>{{ $row->title }}</h2>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="col-sm-3">
                    <div class="address">
                        <img src="{{ asset('assets/main/images/home/map.png') }}" alt="" />
                        <p>Sulawesi Selatan, Indonesia</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2020 KKSS. All rights reserved.</p>
                <p class="pull-right">Designed by <span><a target="_blank" href="#">Aybis Company</a></span></p>
            </div>
        </div>
    </div>
</footer>
