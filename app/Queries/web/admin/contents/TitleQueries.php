<?php

namespace App\Queries\web\admin\contents;

class TitleQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = 'admin';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $ContentsModel = app($this->model_path.'Contents');
        $this->ContentsModel = $ContentsModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

	public function get_contents_by_description($description)
    {
        $data = $this->ContentsModel::where('description', $description)->get()->first();
        if ($data) {
        	$ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)->where('description', $description)->get()->first();
            if ($image)
                $data->image = $image;
            $data->display = ($data->display==true)?'View':'Hide';
            $data->created_date = date('d M Y, (H:i)', strtotime($data->created_at));
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function update_contents($post)
    {
        $data = $this->ContentsModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }
}
