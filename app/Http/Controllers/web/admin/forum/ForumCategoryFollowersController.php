<?php

namespace App\Http\Controllers\web\admin\forum;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\admin\forum\ForumCategoryFollowersQueries;

class ForumCategoryFollowersController extends \App\Http\Core\WebController 
{
    // Parent Folder in views
    protected $pView = 'admin';

    // Sub Folder (Child) in views
    protected $sView = 'forum/forumcategoryfollowers';

    // For description select menu by role from database
    protected $vMenu = 'admin';
    
    // For selected menu (Active menu)
    protected $sMenu = 'forum/forumcategoryfollowers';

    // Session by user for access this controllers
    protected $sessions = 'admin';
    
    function __construct(ControllerQueries $queries_controller, ForumCategoryFollowersQueries $queries_forum_category_followers)
    {
        $this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_forum_category_followers = $queries_forum_category_followers;
    }

    public function index()
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Forum Category Followers > Forum';
        // Filename in folder view
        $data['cView'] = 'index';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = 'index';
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = 'index';

        /* Collection variable data */
        // Get Last
        $last = $this->queries_forum_category_followers->get_forum_category_followers_last();
        $data['last'] = $last;

        // Render to View
        return $this->_render($data);
    }

    public function get_datatable()
    {
        // Get Modul for Datatable
        $modul = $this->queries_controller->_get_datatable('forum', $_GET);
        // Get Data
        $data = $this->queries_forum_category_followers->get_forum_category_followers_datatable($modul['whereRaw'], $modul['order'], $modul['offset'], $modul['limit']);
        // Set Datatable
        $datatable = \DataTables::of($data)->make(true);
        $datatable->original['recordsFiltered'] = $this->queries_forum_category_followers->get_forum_category_followers_total($modul['whereRaw']); // Set Total Data
        $datatable->original['data'] = $data; // Set Data 
        // Return Data
        return $datatable->original;
    }

    function get_data_by_id($str_id)
    {
        // Global variable data
        $data = [];
        // Define Variable (Post/Get)
        $id = (!empty($str_id)) ? $str_id : null;
        // Get data
        $get = $this->queries_forum_category_followers->get_forum_category_followers_by_id($id);
        // Collection data
        if ($get) {
            $data['str_id'] = $get->id;
        }
 
        // Return JSON Format
        return response()->json($data);
    }

    public function form($str_id=null)
    {
        // Global variable data
        $data = [];
        // Title in Website
        $data['vTitle'] = 'Form > Forum Category Followers > Forum';
        // Filename in folder view
        $data['cView'] = 'form';
        // Component additional to View (Optional)
        $data['vAdditional']['html'] = null;
        $data['vAdditional']['css'] = null;
        $data['vAdditional']['js'] = null;

        /* Collection variable data */
        // Get Data Forum Category Followers
        $forum_category_followers = null;
        if ($str_id) {
            $forum_category_followers = $this->queries_forum_category_followers->get_forum_category_followers_by_id($str_id);
        }
        $data['forum_category_followers'] = $forum_category_followers;
        
        // Render to View
        return $this->_render($data);
    }

    public function post(Request $request)
    {
        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
            'is_status' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');
        $is_status = $request->input('is_status');

        // Create Array to Post
        $post = [];
        $post['id'] = $id;
        $post['is_status'] = $is_status;
        $post['updated_at'] = date('Y-m-d H:i:s');

        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Process Query
        // Update
        $_post = $this->queries_forum_category_followers->update_forum_category_followers($post);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully updated!.';
        }

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }

    public function delete(Request $request)
    {
        // Define Value for Session Flash
        $session_post = 'Error';
        $session_message = 'Something wrong database server :( please try again.';

        // Check Validate Request
        $this->validate($request, [
            'str_id' => 'required',
        ]);

        // Define Variable Request
        $id = $request->input('str_id');

        // Process Query
        // Delete
        // $_post = $this->queries_forum_category_followers->destroy_forum_category_followers($id);
        // Soft Delete
        $_post = $this->queries_forum_category_followers->delete_forum_category_followers($id);
        if ($_post) {
            $session_post = 'Success';
            $session_message = 'Data was successfully deleted!.';
        }
        $post['id'] = $id;

        // Set Session Flash
        Session::flash('post', $session_post);
        Session::flash('message', $session_message);

        // Redirect to Function
        return $this->_redirect($this->global_path, $post);
    }
}
