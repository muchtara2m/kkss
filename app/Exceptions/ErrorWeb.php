<?php

namespace App\Exceptions;

use Exception;

class ErrorWeb extends Exception
{
    public static function notfound($message=null)
    {
    	$NotFoundController = app('App\Http\Controllers\error\NotFoundController');
        return $NotFoundController->index($message);
    }
}
