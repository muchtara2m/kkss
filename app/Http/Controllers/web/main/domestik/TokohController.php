<?php

namespace App\Http\Controllers\web\main\domestik;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\ControllerQueries;
use App\Queries\web\main\domestik\TokohQueries;

class TokohController extends \App\Http\Core\WebController
{
	// Parent Folder in views
	protected $pView = 'main';

	// Sub Folder (Child) in views
	protected $sView = 'domestik/tokoh';

	// For description select menu by role from database
	protected $vMenu = 'member';
	
	// For selected menu (Active menu)
	protected $sMenu = 'domestik/tokoh';

	// Session by user for access this controllers
	protected $sessions = 'member';
	
	function __construct(ControllerQueries $queries_controller, TokohQueries $queries_tokoh)
	{
		$this->global_path = $this->pView.'/'.$this->sView.'/';
        $this->queries_controller = $queries_controller;
        $this->queries_tokoh = $queries_tokoh;
	}

    public function index(Request $request)
    {
    	// Global variable data
		$data = [];
		// Title in website
		$data['vTitle'] = 'Tokoh Nasional';
		// Filename in folder view
		$data['cView'] = 'index';
		// Component additional to View (Optional)
		$data['vAdditional']['html'] = null;
		$data['vAdditional']['css'] = 'index';
		$data['vAdditional']['js'] = null;

		// Get Keyword
        $keyword = $request->query('keyword');
        $data['keyword'] = $keyword;

		/* Collection variable data */
		// Get Tokoh
		$tokoh = $this->queries_tokoh->get_tokoh($keyword);
        foreach ($tokoh as $r) {
            $r->image_url = $r->image?\ImageUrl::url('domestik/tokoh/images/', $r->image):null;
            $r->thumb_url = $r->image?\ImageUrl::url('domestik/tokoh/thumbs/', $r->image):null;
        }
        $data['tokoh'] = $tokoh;
		
		// Render to View
        return $this->_render($data);
    }
}
