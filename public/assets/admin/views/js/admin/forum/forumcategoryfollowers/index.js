$(function () {
    // Datatable
    let table
    let thead = `
        <tr>
            <th></th>
            <th></th>
            <th style="text-align: left;">NO</th>
            <th>FORUM CATEGORY</th>
            <th>NAMA</th>
            <th>EMAIL</th>
            <th>STATUS</th>
            <th>CREATED DATE</th>
            <th>UPDATED DATE</th>
        </tr>
    `
    $('#example1').append($('<thead/>').append(thead))
    let tfoot = `
        <tr>
            <th search="false"></th>
            <th search="false"></th>
            <th search="false"></th>
            <th search="true">FORUM CATEGORY</th>
            <th search="true">NAMA</th>
            <th search="true">EMAIL</th>
            <th search="true">STATUS</th>
            <th search="true">CREATED DATE</th>
            <th search="true">UPDATED DATE</th>
        </tr>
    ` 
    $('#example1').append($('<tfoot/>').append(tfoot))
    // Setup - add a text input to each footer cell
    $('#example1 tfoot th').each( function () {
        let search = $(this).attr('search')
        if (search == 'true') {
            let title = $(this).text()
            // let title = ''
            $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: 95%;" />' )
        }
    })
    // Define Url get datatable
    let _datatable_url = $('#example1').attr('url')
    // Process render data
    table = $('#example1').DataTable({
        processing: true,
        serverSide: true,
        ajax: _datatable_url,
        // dataType: "json",
        aaSorting: [[8, 'desc']],
        aaColumnDefs: [
            { 'bSortable': false, 'aTargets': [ 0, 1 ] },
            // { 'bSearchable': false }
        ],
        columns: [
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <button 
                            type="button" 
                            title="Edit"
                            onclick="_form('`+_datatable_url.replace('/get/datatable', '')+'/form/'+data.str_id+`')" 
                            class="btn btn-sm btn-warning btn-class" 
                            style="border-radius: 0;"
                        >
                            <i class="fa fa-lg fa-edit"></i>
                        </button>
                    `
                }

            },
            {
                data: null, 
                class: 'text-center',
                width: '5%',
                'bSortable': false, 
                'bSearchable': false, 
                render:
                function(data) {
                    return `
                        <a 
                            data-toggle="modal" 
                            href="#delete"
                            class="btn btn-sm btn-danger btn-class" 
                            style="border-radius: 0;"
                            data-str_id="`+data.str_id+`"
                        >
                            <i class="fa fa-lg fa-trash-o"></i>
                        </a>
                    `
                }
            },
            {
                data: 'no',
                class: 'text-right',
                width: '5%',
                'bSortable': true,
                'bSearchable': false
            },
            {
                data: 'forum_category_$_name', 
                name: 'forum_category_$_name',
                width: '15%'
            },
            {
                data: 'user_$_name', 
                name: 'user_$_name',
                width: '15%'
            },
            {
                data: 'user_$_email', 
                name: 'user_$_email',
                width: '15%'
            },
            {
                data: 'is_status', 
                name: 'is_status',
                width: '10%'
            },
            {
                data: 'created_date', 
                name: 'created_at',
                width: '15%'
            },
            {
                data: 'updated_date', 
                name: 'updated_at',
                width: '15%'
            },
        ],
        initComplete: function () {
            this.api().columns().every( function () {
                if (
                        this[0] == 9
                ) {
                    let column = this
                    let select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            let val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            )     
                            column
                                .search( val ? ''+val+'' : '', true, false )
                                .draw()
                        })
                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        })
                }
            })
        },
        'sDom': 'Bfrtpil',
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    })
    // Apply the search
    table.columns().every( function () {
        let that = this
        $('input', this.footer()).on('keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw()
            }
        })
    })

    // Froala JS
    $('#edit').froalaEditor({
        height: 200
    })
})

// Modal Form
function _form(url) {
    window.location.href=url
}
// Modal Delete
$('#delete').on('show.bs.modal', function (event) {
    let div = $(event.relatedTarget)
    let str_id = div.data('str_id')
    let modal = $(this)
    let _url = modal.find('#str_id_delete').attr('url')+'/'+str_id
    get_data_by_str_id(modal, _url, 'delete')
})

function get_data_by_str_id(modal, _url, type) {
    $.ajax({
        type: 'GET',
        url: _url, 
        success: function(response){
            if (type=='form')
                set_modal_form(modal, response)
            if (type=='delete')
                set_modal_delete(modal, response)
        }
    })
}
function set_modal_delete(modal, response) {
    let str_id = response.str_id
    let images_id = response.images_id
        modal.find('#str_id_delete').val(str_id)
        modal.find('#images_id_delete').val(images_id)
}