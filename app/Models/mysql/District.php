<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'district';

	protected $fillable = [
		'city_id',
		'name'
	];
}
