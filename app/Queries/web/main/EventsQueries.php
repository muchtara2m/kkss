<?php

namespace App\Queries\web\main;

class EventsQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $EventsModel = app($this->model_path.'Events');
        $this->EventsModel = $EventsModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_events($keyword=null)
    {
        $data = $this->EventsModel::where('status', 'Active')
                ->where('display', true);
        if ($keyword) {
            $data = $data->where('name', 'like', '%'.$keyword.'%')
                    ->orWhere('pic', 'like', '%'.$keyword.'%')
                    ->orWhere('location', 'like', '%'.$keyword.'%');
        }
        $data = $data->orderBy('updated_at', 'desc')
                ->paginate(2);
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserModel = app($this->model_path.'User');
        $EventsCategoryModel = app($this->model_path.'EventsCategory');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'events')
                    ->get()
                    ->first();
            $v->image = $image;
            $user = $UserModel::where('id', $v->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $events_category = $EventsCategoryModel::where('id', $v->events_category_id)
                                        ->get()
                                        ->first();
            $v->category = $events_category;
        }
        return $this->_resJson($data);
    }

    function get_events_by_events_category_id($events_category_id, $keyword=null)
    {
        $data = $this->EventsModel::where('status', 'Active')
                ->where('display', true)
                ->where('events_category_id', $events_category_id);
        if ($keyword) {
            $data = $data->where('name', 'like', '%'.$keyword.'%')
                    ->orWhere('pic', 'like', '%'.$keyword.'%')
                    ->orWhere('location', 'like', '%'.$keyword.'%');
        }
        $data = $data->orderBy('updated_at', 'desc')
                ->paginate(2);
        // Get Image & User
        $ImageModel = app($this->model_path.'Image');
        $UserModel = app($this->model_path.'User');
        $EventsCategoryModel = app($this->model_path.'EventsCategory');
        foreach ($data as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'events')
                    ->get()
                    ->first();
            $v->image = $image;
            $user = $UserModel::where('id', $v->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $events_category = $EventsCategoryModel::where('id', $v->events_category_id)
                                        ->get()
                                        ->first();
            $v->category = $events_category;
        }
        return $this->_resJson($data);
    }

    function get_events_by_seo($seo)
    {
        $data = $this->EventsModel::where('status', 'Active')
                ->where('display', true)
                ->where('seo', $seo)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'events')
                    ->get()
                    ->first();
            $data->image = $image;
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->created_by)
                    ->get()
                    ->first();
            $data->user = $user;
            // Get Events Category
            $EventsCategoryModel = app($this->model_path.'EventsCategory');
            $events_category = $EventsCategoryModel::where('id', $data->events_category_id)
                    ->get()
                    ->first();
            $data->category = $events_category;
        }
        return $this->_resJson($data);
    }

    function get_events_by_id($id)
    {
        $data = $this->EventsModel::where('status', 'Active')
                ->where('display', true)
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'events')
                    ->get()
                    ->first();
            $data->image = $image;
            // Get User
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $data->created_by)
                    ->get()
                    ->first();
            $data->user = $user;
        }
        return $this->_resJson($data);
    }

    function get_events_category()
    {
        // Define Model
        $EventsCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsCategory');
        // Get Data
        $data = $EventsCategoryModel::where('status', 'Active')
                    ->orderBy('updated_at', 'desc')
                    ->get();
        return $data;
    }

    function get_events_category_by_id($id)
    {
        // Define Model
        $EventsCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\EventsCategory');
        // Get Data
        $data = $EventsCategoryModel::where('status', 'Active')
                    ->where('id', $id)
                    ->get()
                    ->first();
        return $data;
    }

    function count_events_by_events_category_id($events_category_id)
    {
        $data = $this->EventsModel::where('status', 'Active')
                ->where('display', true)
                ->where('events_category_id', $events_category_id)
                ->get()
                ->count();
        return $this->_resJson($data);
    }

    function get_events_comments_by_events_id($events_id, $parent_id=null)
    {
        $EventsCommentsModel = app($this->model_path.'EventsComments');
        $data = $EventsCommentsModel::where('status', 'Active')
                ->where('events_id', $events_id);
        if ($parent_id) {
            $data = $data->where('parent_id', $parent_id);
        } else
            $data = $data->whereNull('parent_id');
        $data = $data->get();
        // Get User & Image
        $UserModel = app($this->model_path.'User');
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $r) {
            $r->image = asset('assets/main/images/user-photo.jpg');
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            if ($r->user_id) {
                $user = $UserModel::where('status', 'Active')
                        ->where('id', $r->user_id)
                        ->get()
                        ->first();
                $r->user = $user ?? null;
                if ($user) {
                    $r->name = $r->user->name;
                    $r->email = $r->user->email;
                    $r->phone = $r->user->phone;
                    $image = $ImageModel::where('param_id', $user->id)
                            ->where('description', 'member')
                            ->get()
                            ->first();
                    if ($image)
                        $r->image = \ImageUrl::url('member/images/', $image);
                }
            }
        }
        return $this->_resJson($data);
    }

    function insert_events_comments($post)
    {
        $EventsCommentsModel = app($this->model_path.'EventsComments');
        $data = $EventsCommentsModel::insert($post);
        return $this->_resJson($data);
    }

    function insert_events_register($post)
    {
        $EventsRegisterModel = app($this->model_path.'EventsRegister');
        $data = $EventsRegisterModel::insert($post);
        return $this->_resJson($data);
    }

    function get_events_register_by_email($email)
    {
        $EventsRegisterModel = app($this->model_path.'EventsRegister');
        $data = $EventsRegisterModel::where('email', $email)
                ->where('status', 'Active')
                // ->where('is_status', '!=', 'Reject')
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    function get_events_register_by_user_id($user_id)
    {
        $EventsRegisterModel = app($this->model_path.'EventsRegister');
        $data = $EventsRegisterModel::where('user_id', $user_id)
                ->where('status', 'Active')
                // ->where('is_status', '!=', 'Reject')
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    function get_events_register_by_events_id($events_id, $user_id)
    {
        $EventsRegisterModel = app($this->model_path.'EventsRegister');
        $data = $EventsRegisterModel::where('status', 'Active')
                    ->where('events_id', $events_id)
                    ->where('user_id', $user_id)
                    // ->where('is_status', '!=', 'Reject')
                    ->get()
                    ->first();
        return $data;
    }

    function get_events_registered_by_user_id($user_id)
    {
        $EventsRegisterModel = app($this->model_path.'EventsRegister');
        $data = $EventsRegisterModel::where('user_id', $user_id)
                ->where('status', 'Active')
                // ->where('is_status', '!=', 'Reject')
                ->get();
        foreach ($data as $v) {
            $events = $this->EventsModel::where('id', $v->events_id)
                        ->get()
                        ->first();
            $v->events = $events;
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $events->id)
                    ->where('description', 'events')
                    ->get()
                    ->first();
            $v->image = $image;
            $UserModel = app($this->model_path.'User');
            $user = $UserModel::where('id', $events->created_by)
                    ->get()
                    ->first();
            $v->user = $user;
            $EventsCategoryModel = app($this->model_path.'EventsCategory');
            $events_category = $EventsCategoryModel::where('id', $events->events_category_id)
                                        ->get()
                                        ->first();
            $v->category = $events_category;
        }
        return $this->_resJson($data);
    }
}