<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class ForumCategory extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'forum_category';

	protected $fillable = [
		'name',
		'description'
	];
}
