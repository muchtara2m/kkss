<div class="modal fade" id="success" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <label class="modal-title">Success</label>
            </div>
            <div class="modal-body">
                <span id="success_message" style="font-size:16px"></span>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="error" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <label class="modal-title">Failed</label>
            </div>
            <div class="modal-body">
                <span id="error_message" style="font-size:16px"></span>
            </div>
        </div>
    </div>
</div>
@if(Session::has('post'))
    @if(Session::get('post') == 'Success')
        <script type="text/javascript">
            $(document).ready(function() {
                var flashdata_message = '{{ Session::get("message") }}'
                $('#success_message').text(flashdata_message)
                $('#success').modal('show')
                setTimeout(function(){
                    $('#success').modal('hide')
                }, 2000)
            })
        </script>
    @else
        <script type="text/javascript">
            $(document).ready(function() {
                var flashdata_message = '{{ Session::get("message") }}'
                $('#error_message').text(flashdata_message)
                $('#error').modal('show')
                setTimeout(function(){
                    $('#error').modal('hide')
                }, 2000)
            })
        </script>
    @endif
@endif
<div class="modal fade" id="loading_modal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue">
                <h4 class="modal-title">Loading Data ...</h4>
            </div>
            <div class="modal-body text-center modal-loading"></div>
        </div>
    </div>
</div>