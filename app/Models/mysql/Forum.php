<?php

namespace App\Models\mysql;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
	protected $connection = 'mysql';
	
    protected $table = 'forum';

	protected $fillable = [
		'forum_category',
		'title',
		'display'
	];
}
