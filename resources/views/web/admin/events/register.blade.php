@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>{{ $events->name }}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url($pView.'/'.$sView) }}"># Events</a></li>
            <li><a href="{{ url($pView.'/'.$sView.'/register/'.$events->id) }}">Register</a></li>
            <li class="active">{{ $events->name }}</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ isset($last) ? 'Last updated : '.$last->updated_date : 'No Data' }}
            </span>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <h2 style="margin-top: -15px; font-size: 20px;">
            <label for="editor1">Data Register Events: {{ $events->name }}</label>
            <div class="pull-right">
                <a href="{{ url($pView.'/'.$sView.'/register/'.$events->id.'/form') }}" class="btn btn-sm btn-primary btn-class" style="border-radius: 0;">
                    <i class="fa fa-plus"></i> Register
                </a>
            </div>
        </h4>
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-body">
                            <div class="table-responsive" style="width: 100%;">
                                <table id="example1" class="table table-bordered table-striped" url="{{ url($pView.'/'.$sView.'/register/get/datatable/'.$events->id) }}" style="width: 100%; font-size: 12.5px;">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop