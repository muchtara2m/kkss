<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Queries\ControllerQueries;
use App\Mail\ForgotPasswordMail as ForgotPasswordMail;
use Mail;

class ForgotPasswordJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Define Mail Content
        $forgot_password_mail = (new ForgotPasswordMail($this->data));
        $notes = null;
        $status = 'Success';
        
        try {
            // Send Mail Process
            Mail::to($this->data['to'])
                    // ->cc()
                    // ->bcc()
                    ->send($forgot_password_mail)
                    ;

            // Check Mail Response
            if (count(Mail::failures()) > 0) {
                $notes = json_encode(Mail::failures());
                $status = 'Failed';
            }
        } catch (\Exception $e) {
            $notes = json_encode($e->getMessage());
            $status = 'Failed';
        }

        // Update Send Mail DB Table
        $post_send_mail = [];
        $post_send_mail['id'] = $this->data['send_email_id'];
        $post_send_mail['notes'] = $notes;
        $post_send_mail['status'] = $status;
        $queries_controller = new ControllerQueries(); // Define Controller Queries
        $queries_controller->update_send_mail($post_send_mail);

        // Response
        return true;
    }
}
