$(function () {
	$('#confirm_password').keyup(function(){
        let confirm_password = $(this).val()
        let new_password = $('#new_password').val()
        if (confirm_password == new_password) {
            $('#confirm_password_error').css('display', 'none')
            $('#btn-submit').attr('disabled', false)
        } else {
            $('#confirm_password_error').css('display', '')
            $('#btn-submit').attr('disabled', true)
        }
    })
})