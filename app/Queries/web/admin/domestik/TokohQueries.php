<?php

namespace App\Queries\web\admin\domestik;

class TokohQueries extends \App\Queries\Queries
{   
    protected $model_path;
    protected $sessions = 'admin';

    function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $TokohModel = app($this->model_path.'Tokoh');
        $this->TokohModel = $TokohModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
    }

    function get_tokoh_datatable($whereRaw, $order, $offset, $limit)
    {
        $tokoh = $this->TokohModel::select(
                        'tokoh.*'
                    )
                    ->where('tokoh.type', 'Nasional')
                    ->where('tokoh.status', 'Active');
        if ($whereRaw!='')
            $tokoh = $tokoh->whereRaw($whereRaw);
        $order_column = '';
        if (strrpos($order['coloumn'], '$')==true) {
            $explode = explode('_$_', $order['coloumn']);
            $order_column = $explode[0].'.'.$explode[1];
        } else
            $order_column = 'tokoh.'.$order['coloumn'];
        $tokoh = $tokoh->orderBy($order_column, $order['dir'])
                    ->orderBy('tokoh.id', 'asc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
        $ImageModel = app($this->model_path.'Image');
        foreach ($tokoh as $key => $value) {
            // Get Image
            $image = $ImageModel::where('param_id', $value->id)
                    ->where('description', 'tokoh')
                    ->get()
                    ->first();
            $value->image = $image; 
            // 
            $value->no = $key + 1 + $offset;
            $value->date = date('d M Y', strtotime($value->date));
            $value->display = ($value->display==true)?'View':'Hide';
            $value->created_date = date('d M Y, (H:i)', strtotime($value->created_at));
            $value->updated_date = date('d M Y, (H:i)', strtotime($value->updated_at));
            $value->str_id = $value->id;
        }
        return $this->_resJson($tokoh);
    }

    function get_tokoh_total($whereRaw)
    {
        $tokoh = $this->TokohModel::select('tokoh.id')
                    ->where('tokoh.type', 'Nasional')
                    ->where('tokoh.status', 'Active');
        if ($whereRaw!='')
            $tokoh = $tokoh->whereRaw($whereRaw);
        $tokoh = $tokoh->count();
        return $this->_resJson($tokoh);
    }

    function get_tokoh_by_id($id)
    {
        $data = $this->TokohModel::where('status', 'Active')
                ->where('type', 'Nasional')
                ->where('id', $id)
                ->get()
                ->first();
        if ($data) {
            // Get Image
            $ImageModel = app($this->model_path.'Image');
            $image = $ImageModel::where('param_id', $data->id)
                    ->where('description', 'tokoh')
                    ->get()
                    ->first();
            $data->image = $image; 
            // 
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
            $data->str_id = $data->id;
        }
        return $this->_resJson($data);
    }

    function get_tokoh_last()
    {
        $data = $this->TokohModel::where('status', 'Active')
                ->where('type', 'Nasional')
                ->orderBy('updated_at', 'desc')
                ->get()
                ->first();
        if ($data) {
            $data->updated_date = date('d M Y, (H:i)', strtotime($data->updated_at));
        }
        return $this->_resJson($data);
    }

    function insert_tokoh($post)
    {
        $data = $this->TokohModel::insert($post);
        return $this->_resJson($data);
    }

    function update_tokoh($post)
    {
        $data = $this->TokohModel::where('id', $post['id'])
                ->update($post);
        return $this->_resJson($data);
    }

    function delete_tokoh($id)
    {
        $data = $this->TokohModel::where('id', $id)
                 ->update(['status' => 'Inactive']);
        return $this->_resJson($data);
    }

    function destroy_tokoh($id)
    {
        $data = $this->TokohModel::where('id', $id)
                 ->delete();
        return $this->_resJson($data);
    }
}
