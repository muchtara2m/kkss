<meta charset="UTF-8">
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{$vTitle}} | Admin</title>
<!-- <link rel="shortcut icon" type="image/png" href="{{ $vContents['favicon']['image_url'] }}"> -->
<link rel="shortcut icon" href="{{ asset('assets/main/images/favicon/favicon.jpg') }}">
<!-- Bootstrap 3.3.2 -->
<link href="{{ asset('assets/'.$pView.'/bootstrap/css/bootstrap.css') }}" rel="stylesheet" type="text/css">    
<!-- FontAwesome 4.3.0 -->
<!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
<link href="{{ asset('assets/'.$pView.'/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<!-- Ionicons 2.0.0 -->
<link href="{{ asset('assets/'.$pView.'/bootstrap/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">      
<!-- Theme style -->
<link href="{{ asset('assets/'.$pView.'/dist/css/AdminLTE.css') }}" rel="stylesheet" type="text/css">
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link href="{{ asset('assets/'.$pView.'/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css">
<!-- <link href="{{ asset('assets/'.$pView.'/dist/css/skins/skin-custom.css') }}" rel="stylesheet" type="text/css"> -->
<!-- iCheck -->
<!-- <link href="{{ asset('assets/'.$pView.'/plugins/iCheck/flat/all.css') }}" rel="stylesheet" type="text/css"> -->
<!-- iCheck for checkboxes and radio inputs -->
<link href="{{ asset('assets/'.$pView.'/plugins/iCheck/all.css') }}" rel="stylesheet" type="text/css">
<!-- Morris chart -->
<link href="{{ asset('assets/'.$pView.'/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css">
<!-- jvectormap -->
<link href="{{ asset('assets/'.$pView.'/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css">
<!-- Date Picker -->
<link href="{{ asset('assets/'.$pView.'/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css">
<!-- Daterange picker -->
<link href="{{ asset('assets/'.$pView.'/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css">
<!-- bootstrap wysihtml5 - text editor -->
<link href="{{ asset('assets/'.$pView.'/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css">
<!-- Theme style -->
<link href="{{ asset('assets/'.$pView.'/plugins/iCheck/all.css') }}" rel="stylesheet" type="text/css">
<!-- DATA TABLES -->
<link href="{{ asset('assets/'.$pView.'/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('assets/'.$pView.'/plugins/datatables/buttons.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/main/libs/select2/select2.min.css') }}" rel="stylesheet">
<script type="text/javascript">
	const _token = '{{ csrf_token() }}'
</script>