<?php

namespace App\Queries\web\main;

class ForumQueries extends \App\Queries\Queries
{	
	protected $model_path;
    protected $sessions = '';

	function __construct()
    {
        // Define Path Model
        $this->model_path = 'App\Models\\'.env('DB_CONNECTION').'\\';

        // Define Model Global
        $ForumModel = app($this->model_path.'Forum');
        $this->ForumModel = $ForumModel;

        // Start Query Log
        \DB::connection()->enableQueryLog();
	}

    function get_forum_category($keyword=null, $sort='desc')
    {
        // Define Model
        $ForumCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\ForumCategory');
        // Get Data
        $forum_category = $ForumCategoryModel::where('status', 'Active')
                    ->where('display', true);
        if ($keyword) 
            $forum_category = $forum_category->where('name', 'like', '%'.$keyword.'%');
        $forum_category = $forum_category->orderBy('updated_at', $sort)
                            ->get();
        // Get Image
        $ImageModel = app($this->model_path.'Image');
        foreach ($forum_category as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'forum_category')
                    ->get()
                    ->first();
            $v->image = $image ?? null;
        }
        return $forum_category;
    }

    function get_raw_forum_category($keyword=null, $filter='updated_at', $sort='desc')
    {   
        $whereKey = '';
        if ($keyword)
            $whereKey = 'AND a.name LIKE "%'.$keyword.'%"';

        $sql = '
            SELECT 
                a.id AS id,
                a.name AS name,
                a.seo AS seo,
                a.description AS description,
                a.updated_at AS updated_at,
                (
                    SELECT count(b.id) 
                    FROM forum_category_followers b 
                    WHERE b.forum_category_id = a.id
                ) AS followers,
                (
                    SELECT count(c.id) 
                    FROM forum c 
                    WHERE c.forum_category_id = a.id
                ) AS posting,
                (
                    SELECT count(e.id) 
                    FROM forum d 
                    JOIN forum_visitor e ON e.forum_id = d.id
                    WHERE d.forum_category_id = a.id
                ) AS forum_visitor
            FROM
                forum_category a 
            WHERE
                1=1
                AND a.status = "Active"
                AND a.display = true
                '.$whereKey.'
            ORDER BY
                '.$filter.' '.$sort.'

        ';
        $forum_category = \DB::select(\DB::raw($sql));
        // Get Image
        $ImageModel = app($this->model_path.'Image');
        foreach ($forum_category as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'forum_category')
                    ->get()
                    ->first();
            $v->image = $image ?? null;
        }
        return $forum_category;
    }

    function get_forum_category_by_seo($seo)
    {
        // Define Model
        $ForumCategoryModel = app('App\Models\\'.env('DB_CONNECTION').'\\ForumCategory');
        // Get Data
        $forum_category = $ForumCategoryModel::where('status', 'Active')
                    ->where('seo', $seo)
                    ->where('display', true)
                    ->get()
                    ->first();
        return $forum_category;
    }

    function get_forum_by_forum_category_id($forum_category_id)
    {
        // Get Data
        $forum = $this->ForumModel::where('status', 'Active')
                    ->where('display', true)
                    ->where('forum_category_id', $forum_category_id)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(2);
        // Get Image, Comments, Followers, Visitor
        $ImageModel = app($this->model_path.'Image');
        $ForumCommentsModel = app($this->model_path.'ForumComments');
        $ForumCategoryFollowersModel = app($this->model_path.'ForumCategoryFollowers');
        $ForumVisitorModel = app($this->model_path.'ForumVisitor');
        foreach ($forum as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'forum')
                    ->get()
                    ->first();
            $v->image = $image ?? null;
            $comments = $ForumCommentsModel::where('forum_id', $v->id)
                        ->where('status', 'Active')
                        ->count();
            $v->comments = $comments;
            $followers = $ForumCategoryFollowersModel::where('forum_category_id', $v->forum_category_id)
                        ->where('is_status', 'Approve')
                        ->where('status', 'Active')
                        ->count();
            $v->followers = $followers;
            $visitor = $ForumVisitorModel::where('forum_id', $v->id)
                        ->where('status', 'Active')
                        ->count();
            $v->visitor = $visitor;
        }
        return $forum;
    }

    function get_raw_forum_by_forum_category_id($forum_category_id, $keyword=null, $filter='updated_at', $sort='desc')
    {
        /*
        $sql = '
            SELECT 
                a.id AS id,
                a.forum_category_id AS forum_category_id,
                a.title AS title,
                a.seo AS seo,
                a.description AS description,
                a.short_description AS short_description,
                a.updated_at AS updated_at,
                (
                    SELECT count(b.id) 
                    FROM forum_comments b 
                    WHERE b.forum_id = a.id
                ) AS comments,
                (
                    SELECT count(c.id) 
                    FROM forum_visitor c 
                    WHERE c.forum_id = a.id
                ) AS visitor,
                (
                    SELECT count(e.id) 
                    FROM forum_category d 
                    JOIN forum_category_followers e ON e.forum_category_id = d.id
                    WHERE d.id = a.forum_category_id
                ) AS followers
            FROM
                forum a 
            WHERE
                1=1
                AND a.status = "Active"
                AND a.display = true
                AND forum_category_id = "'.$forum_category_id.'"
        ';
        $forum = \DB::select(\DB::raw($sql));
        return $forum;
        */
        $selectRaw = '
            forum.id AS id,
            forum.forum_category_id AS forum_category_id,
            forum.title AS title,
            forum.seo AS seo,
            forum.description AS description,
            forum.short_description AS short_description,
            forum.updated_at AS updated_at,
            (
                SELECT count(b.id) 
                FROM forum_comments b 
                WHERE b.forum_id = forum.id
            ) AS comments,
            (
                SELECT count(c.id) 
                FROM forum_visitor c 
                WHERE c.forum_id = forum.id
            ) AS visitor,
            (
                SELECT count(e.id) 
                FROM forum_category d 
                JOIN forum_category_followers e ON e.forum_category_id = d.id
                WHERE d.id = forum.forum_category_id
            ) AS followers
        ';
        $forum = $this->ForumModel::select(\DB::raw($selectRaw))
            ->where('status', 'Active')
            ->where('display', true)
            ->where('forum_category_id', $forum_category_id);
        if ($keyword) {
            $forum = $forum->where('title', 'like', '%'.$keyword.'%');
        }
        $forum = $forum->orderBy($filter, $sort)
                ->paginate(2);
        // Get Image
        $ImageModel = app($this->model_path.'Image');
        foreach ($forum as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'forum')
                    ->get()
                    ->first();
            $v->image = $image ?? null;
        }
        return $forum;
    }

    function get_forum_by_seo($seo)
    {
        // Get Data
        $forum = $this->ForumModel::where('status', 'Active')
                    ->where('seo', $seo)
                    ->where('display', true)
                    ->get()
                    ->first();
        // Get Image
        $ImageModel = app($this->model_path.'Image');
        $ForumCategoryModel = app($this->model_path.'ForumCategory');
        $UserModel = app($this->model_path.'User');
        if ($forum) {
            $image = $ImageModel::where('param_id', $forum->id)
                    ->where('description', 'forum')
                    ->get()
                    ->first();
            $forum->image = $image ?? null;
            $forum_category = $ForumCategoryModel::where('id', $forum->forum_category_id)
                    ->get()
                    ->first();
            $forum->category = $forum_category ?? null;
            $user = $UserModel::where('id', $forum->created_by)
                    ->get()
                    ->first();
            $forum->user = $user ?? null;
        }
        return $forum;
    }

    function count_forum_by_forum_category_id($forum_category_id)
    {
        $data = $this->ForumModel::where('status', 'Active')
                ->where('display', true)
                ->where('forum_category_id', $forum_category_id)
                ->get()
                ->count();
        return $this->_resJson($data);
    }

    function get_forum_comments_by_forum_id($forum_id, $parent_id=null)
    {
        $ForumCommentsModel = app($this->model_path.'ForumComments');
        $data = $ForumCommentsModel::where('status', 'Active')
                ->where('forum_id', $forum_id);
        if ($parent_id) {
            $data = $data->where('parent_id', $parent_id);
        } else
            $data = $data->whereNull('parent_id');
        $data = $data->get();
        // Get User & Image
        $UserModel = app($this->model_path.'User');
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $r) {
            $r->image = asset('assets/main/images/user-photo.jpg');
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            if ($r->user_id) {
                $user = $UserModel::where('status', 'Active')
                        ->where('id', $r->user_id)
                        ->get()
                        ->first();
                $r->user = $user ?? null;
                if ($user) {
                    $r->name = $r->user->name;
                    $r->email = $r->user->email;
                    $r->phone = $r->user->phone;
                    $image = $ImageModel::where('param_id', $user->id)
                            ->where('description', 'member')
                            ->get()
                            ->first();
                    if ($image)
                        $r->image = \ImageUrl::url('member/images/', $image);
                }
            }
        }
        return $this->_resJson($data);
    }

    function insert_forum_comments($post)
    {
        $ForumCommentsModel = app($this->model_path.'ForumComments');
        $data = $ForumCommentsModel::insert($post);
        return $this->_resJson($data);
    }

    function get_forum_conversation_by_forum_id($forum_id)
    {
        $ForumConversationModel = app($this->model_path.'ForumConversation');
        $data = $ForumConversationModel::where('status', 'Active')
                ->where('forum_id', $forum_id)
                ->get();
        // Get User & Image
        $UserModel = app($this->model_path.'User');
        $ImageModel = app($this->model_path.'Image');
        foreach ($data as $r) {
            $r->image = asset('assets/main/images/user-photo.jpg');
            $r->created_date = date('d M Y, (H:i)', strtotime($r->updated_at));
            $user = $UserModel::where('status', 'Active')
                    ->where('id', $r->user_id)
                    ->get()
                    ->first();
            $r->user = $user ?? null;
            if ($user) {
                $r->name = $r->user->name;
                $r->email = $r->user->email;
                $r->phone = $r->user->phone;
                $image = $ImageModel::where('param_id', $user->id)
                        ->where('description', 'member')
                        ->get()
                        ->first();
                if ($image)
                    $r->image = \ImageUrl::url('member/images/', $image);
            }
        }
        return $this->_resJson($data);
    }

    function get_forum_conversation_by_parent_id($parent_id)
    {
        $ForumConversationModel = app($this->model_path.'ForumConversation');
        $data = $ForumConversationModel::where('status', 'Active')
                ->where('id', $parent_id)
                ->get()
                ->first();
        // Get User & Image
        $UserModel = app($this->model_path.'User');
        $ImageModel = app($this->model_path.'Image');
        $data->image = asset('assets/main/images/user-photo.jpg');
        $data->created_date = date('d M Y, H:i', strtotime($data->updated_at));
        $user = $UserModel::where('status', 'Active')
                ->where('id', $data->user_id)
                ->get()
                ->first();
        $data->user = $user ?? null;
        if ($user) {
            $data->name = $data->user->name;
            $data->email = $data->user->email;
            $data->phone = $data->user->phone;
            $image = $ImageModel::where('param_id', $user->id)
                    ->where('description', 'member')
                    ->get()
                    ->first();
            if ($image)
                $data->image = \ImageUrl::url('member/images/', $image);
        }
        return $this->_resJson($data);
    }

    function insert_forum_conversation($post)
    {
        $ForumConversationModel = app($this->model_path.'ForumConversation');
        $data = $ForumConversationModel::insert($post);
        return $this->_resJson($data);
    }

    function insert_forum_category_followers($post)
    {
        $ForumCategoryFollowersModel = app($this->model_path.'ForumCategoryFollowers');
        $data = $ForumCategoryFollowersModel::insert($post);
        return $this->_resJson($data);
    }

    function get_forum_category_followers_by_user($user_id, $forum_category_id)
    {
        $ForumCategoryFollowersModel = app($this->model_path.'ForumCategoryFollowers');
        $data = $ForumCategoryFollowersModel::where('status', 'Active')
                ->where('user_id', $user_id)
                ->where('forum_category_id', $forum_category_id)
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    function get_forum_category_followers_by_user_id($user_id)
    {
        $ForumCategoryFollowersModel = app($this->model_path.'ForumCategoryFollowers');
        $data = $ForumCategoryFollowersModel::where('status', 'Active')
                ->where('user_id', $user_id)
                ->get();
        // Get Forum Category & User
        $ForumCategoryModel = app($this->model_path.'ForumCategory');
        $UserModel = app($this->model_path.'User');
        foreach ($data as $k => $v) {
            $forum_category = $ForumCategoryModel::where('id', $v->forum_category_id)
                                ->get()
                                ->first();
            $v->forum_category = $forum_category;
        }
        return $this->_resJson($data);
    }

    function get_forum_category_followers_by_approve($user_id, $forum_category_id)
    {
        $ForumCategoryFollowersModel = app($this->model_path.'ForumCategoryFollowers');
        $data = $ForumCategoryFollowersModel::where('status', 'Active')
                ->where('is_status', 'Approve')
                ->where('forum_category_id', $forum_category_id)
                ->where('user_id', $user_id)
                ->get()
                ->first();
        return $this->_resJson($data);
    }

    function insert_forum_visitor($post)
    {
        $ForumVisitorModel = app($this->model_path.'ForumVisitor');
        $data = $ForumVisitorModel::insert($post);
        return $this->_resJson($data);
    }

    function get_forum_id($forum_category_id)
    {
        // Get Data
        $forum = $this->ForumModel::select('id')
                    ->where('status', 'Active')
                    ->where('display', true)
                    ->where('forum_category_id', $forum_category_id)
                    ->get();
        return $forum;
    }

    function count_forum_visitor_by_forum_id($forum_id)
    {
        // Define Model
        $ForumVisitorModel = app($this->model_path.'ForumVisitor');
        // Get Data
        $forum = $ForumVisitorModel::where('status', 'Active')
                    ->whereIn('forum_id', [$forum_id])
                    ->count();
        return $forum;
    }

    function get_forum_latest()
    {
        // Get Data
        $forum = $this->ForumModel::where('status', 'Active')
                    ->where('display', true)
                    ->orderBy('updated_at', 'desc')
                    ->limit(6)
                    ->get();
        // Get Image, Category, Comments, Followers, Visitor
        $ImageModel = app($this->model_path.'Image');
        $ForumCategoryModel = app($this->model_path.'ForumCategory');
        $ForumCommentsModel = app($this->model_path.'ForumComments');
        $ForumCategoryFollowersModel = app($this->model_path.'ForumCategoryFollowers');
        $ForumVisitorModel = app($this->model_path.'ForumVisitor');
        foreach ($forum as $k => $v) {
            $image = $ImageModel::where('param_id', $v->id)
                    ->where('description', 'forum')
                    ->get()
                    ->first();
            $v->image = $image ?? null;
            $category = $ForumCategoryModel::where('id', $v->forum_category_id)
                        ->where('status', 'Active')
                        ->get()
                        ->first();
            $v->category = $category;
            $comments = $ForumCommentsModel::where('forum_id', $v->id)
                        ->where('status', 'Active')
                        ->count();
            $v->comments = $comments;
            $followers = $ForumCategoryFollowersModel::where('forum_category_id', $v->forum_category_id)
                        ->where('is_status', 'Approve')
                        ->where('status', 'Active')
                        ->count();
            $v->followers = $followers;
            $visitor = $ForumVisitorModel::where('forum_id', $v->id)
                        ->where('status', 'Active')
                        ->count();
            $v->visitor = $visitor;
        }
        return $forum;
    }
}