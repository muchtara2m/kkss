@extends('web/'.$pView.'/index')

@section('body')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Forum</h1>
        <ol class="breadcrumb">
            <li># <a>Forum</a></li>
            <li><a href="{{ url($pView.'/'.$sView) }}">Forum Category Followers</a></li>
            <li class="active">Form</li>
        </ol>
    </section>
    <div class="panel-body">
        <div style="padding-top: 0px; padding-bottom: 10px; border-bottom: 1px solid #ddd;">
            <a href="javascript: history.go(-1)" class="btn btn-default " title="Back">
                <i class="fa fa-arrow-left"></i>
            </a>
            <a href="javascript: history.forward()" class="btn btn-default " title="Forward">
                <i class="fa fa-arrow-right"></i>
            </a>
            <a href="" class="btn btn-default" title="Refresh">
                <i class="fa fa-rotate-right"></i>
            </a>
            <span class="pull-right" title="Date" style="margin-top: 10px; font-size: 12px; font-style: italic; color: #999999;">
                {{ ($forum_category_followers) ? 'Last updated : '.$forum_category_followers->updated_date : '' }}
            </span>
        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <h2 style="margin-top: -15px; font-size: 20px;">
            <label for="editor1">Form Edit Forum Category Followers</label>
        </h2>
        <div class="box">
            <div class="box-body" style="padding-top: 25px;">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::open(['route' => $pView.'/'.$sView.'/post', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'myForm']) }}
                            <fieldset>
                                <legend>General</legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Forum Category</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-md" placeholder="Type here.." value="{{ $forum_category_followers?$forum_category_followers->forum_category->name:'' }}" readonly="">
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Nama</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-md" placeholder="Type here.." value="{{ $forum_category_followers?$forum_category_followers->user->name:'' }}" readonly="">
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Email</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control form-control-md" placeholder="Type here.." value="{{ $forum_category_followers?$forum_category_followers->user->email:'' }}" readonly="">
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right">Status *</label>
                                        <div class="col-md-8">
                                            <select class="form-control form-control-md" name="is_status" style="width: 200px;">
                                                <option value="Request" selected="" disabled="">Menunggu Konfirmasi</option>
                                                <option value="Approve" {{ $forum_category_followers?($forum_category_followers->is_status=='Approve'?'selected=""':''):'' }}>Disetujui</option>
                                                <option value="Reject" {{ $forum_category_followers?($forum_category_followers->is_status=='Reject'?'selected=""':''):'' }}>Ditolak</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            @if($errors->get('is_status'))
                                                <span class="btn btn-danger">Status harus diisi</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend></legend>
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-2 text-right"></label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="str_id" id="str_id" value="{{ $forum_category_followers?$forum_category_followers->str_id:'' }}">
                                            <button type="submit" class="btn btn-primary btn-class" style="border-radius: 0;" onclick="onLoading();"><i class="fa fa-save"></i> Save</button>
                                            <button type="reset" class="btn btn-default" style="border-radius: 0;"><i class="fa fa-rotate-left"></i> Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop